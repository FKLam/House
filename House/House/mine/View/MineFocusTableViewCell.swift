//
//  MineFocusTableViewCell.swift
//  House
//
//  Created by 吕品 on 2016/9/3.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

import UIKit
@available (iOS 9.0, *)
class MineFocusTableViewCell: UITableViewCell {

    let picImageView = UIImageView()//缩略图
    let nameLabel = UILabel()//名称
    let areaLabel = UILabel()//面积
    let detailLabel = UILabel()//详情
    let priceLabel = UILabel()//价格
    let statusLabel = UILabel()//成交与否
    var statusLabelHidden: Bool{//是否隐藏交易状态
        set {
            statusLabel.isHidden = newValue
//            contentView.layoutIfNeeded()
        }
        get {
            return self.statusLabel.isHidden
        }
    }
    let tagView = UIStackView()
    var tagArr :[String] {
        set {
            guard newValue.count > 0 else {
                return
            }
            tagView.alignment = .fill
            tagView.axis = .horizontal
            tagView.distribution = .fillEqually
            let colorArr = [UIColor.orange, UIColor.blue, UIColor.yellow, UIColor.purple]
            for i in 0 ..< newValue.count {
                let label = UILabel()
                label.layer.masksToBounds = true
                label.layer.borderColor = colorArr[i].cgColor
                label.layer.borderWidth = 2
                label.textColor = colorArr[i]
                label.text = self.tagArr[i]
                tagView.addArrangedSubview(label)
            }

//            contentView.layoutIfNeeded()
        }
        get {
            return self.tagArr
        }
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        let width = (contentView.mj_w - 10 * 2) / 3
        let height = (120 - 10 * 4) / 3

        contentView.addSubview(picImageView)
        contentView.addSubview(nameLabel)
        contentView.addSubview(detailLabel)
        contentView.addSubview(priceLabel)
        contentView.addSubview(areaLabel)
        contentView.addSubview(tagView)

        picImageView.snp.makeConstraints { (make) in
            make.top.equalTo(contentView.snp.top).offset(20)
            make.bottom.equalTo(contentView.snp.bottom).offset(-20)
            make.left.equalTo(contentView.snp.left).offset(5)
            make.width.equalTo(width)
        }
        //MARK: -Need override
        //上面的状态约束(先关闭)
//        picImageView.addSubview(statusLabel)
//        statusLabel.snp.makeConstraints { (make) in
//            make.top.equalTo(picImageView.snp.top).offset(10)
//            make.right.equalTo(picImageView.snp.right).offset(-20)
//            make.left.equalTo(picImageView)
//            make.height.equalTo(20)
//        }
        statusLabel.backgroundColor = UIColor.orange
        statusLabel.textColor = UIColor.white
        statusLabel.isHidden = true
        nameLabel.font = UIFont.systemFont(ofSize: 18)
        nameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(contentView.snp.top).offset(10)
            make.bottom.equalTo(detailLabel.snp.top).offset(-10)
            make.width.equalTo(width * 1.5)
            make.height.equalTo(height)
            make.left.equalTo(picImageView.snp.right).offset(5)
        }
        nameLabel.textAlignment = .left
        detailLabel.font = UIFont.systemFont(ofSize: 14)
        detailLabel.numberOfLines = 0
        detailLabel.sizeToFit()
        detailLabel.textColor = UIColor.darkText
        detailLabel.snp.makeConstraints { (make) in
            make.left.equalTo(nameLabel)
            make.bottom.equalTo(tagView.snp.top).offset(-10)
            make.height.equalTo(height)
            make.width.equalTo(width * 1.3)
        }

        tagView.snp.makeConstraints { (make) in
            make.left.equalTo(detailLabel)
            make.right.equalTo(contentView.snp.right).offset(-10)
            make.bottom.equalTo(contentView.snp.bottom).offset(-10)
            make.height.equalTo(height)
        }
        detailLabel.textAlignment = .left
        priceLabel.textAlignment = .right
        priceLabel.font = UIFont.systemFont(ofSize: 17)
        priceLabel.textColor = UIColor.red
        priceLabel.snp.makeConstraints { (make) in
            make.right.equalTo(contentView.snp.right).offset(-10)
            make.left.equalTo(detailLabel.snp.right)
            make.width.equalTo(width * 0.7)
            make.height.equalTo(height)
            make.top.equalTo(nameLabel)
            make.bottom.equalTo(nameLabel)
        }
        areaLabel.textAlignment = .right
        areaLabel.font = UIFont.systemFont(ofSize: 14)
        areaLabel.textColor = UIColor.darkText
        areaLabel.snp.makeConstraints { (make) in
            make.left.equalTo(priceLabel)
            make.right.equalTo(priceLabel)
            make.bottom.equalTo(detailLabel)
            make.height.equalTo(height)
            make.width.equalTo(width * 0.7)
            make.top.equalTo(detailLabel)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
