//
//  MinePublishModel.swift
//  House
//
//  Created by 吕品 on 2016/9/26.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

import UIKit

class MinePublishModel: BaseModel {
    var areaid: NSNumber?
    var areaname: String?
    var bus :String?
    var cixuan: NSNumber?
    var end: NSNumber?
    var id: NSNumber?
    var kanfangtime: NSNumber?
    var mendian: NSNumber?
    var must: String?
    var parentid: NSNumber?
    var rentmethod: NSNumber?
    var room: NSNumber?
    var sex: NSNumber?
    var start: NSNumber?
    var telephone: NSNumber?
    var truename: String?
    var userid: NSNumber?
    var zhuxuan: NSNumber?

    var zhuangxiu: String?
    var floor: String?
    var fukuanfangshi: String?
    var ps: String?
    var special: String?
    var status: String?
}
/*
 areaid = 28;位置
 areaname = "\U9053\U91cc\U533a";位置
 bus = "\U534a\U5e74";付款方式
 cixuan = 52;次选位置
 end = 2;预算结束
 id = 65;
 kanfangtime = 2;看房时间
 mendian = 25;门店
 must = "\U5e8a,\U51b0\U7bb1,\U7535\U89c6,\U7a7a\U8c03,\U5bb6\U5177,\U5bbd\U5e26,\U7535\U68af,\U70ed\U6c34\U5668,\U6d17\U8863\U673a,\U53ef\U505a\U996d";配套
 parentid = 23;
 rentmethod = 2;租赁方式
 room = 4;居室
 sex = 2;性别
 start = 1;预算开始
 telephone = 13154521007;电话号码
 time = 1474520105;
 truename = "\U770b\U770b";真是姓名
 userid = 69;
 zhuxuan = 5;主选位置


 floor: "555",
 zhuangxiu: "豪华装修",
 fukuanfangshi: "公积金贷款",
 status: "null" 状态
 ps: "来来来",
 special: "把",



 */
