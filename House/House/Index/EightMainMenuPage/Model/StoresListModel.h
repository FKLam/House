//
//  StoresListModel.h
//  House
//
//  Created by 张垚 on 16/8/17.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "BaseModel.h"


@interface StoresListModel : BaseModel

@property (nonatomic, copy) NSString *userid;
@property (nonatomic, copy) NSString *areaid;
@property (nonatomic, copy) NSString *company;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *truename;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *groupid;
@property (nonatomic, copy) NSString *addr;

@end
