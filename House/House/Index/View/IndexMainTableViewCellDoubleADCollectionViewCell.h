//
//  IndexMainTableViewCellDoubleADCollectionViewCell.h
//  House
//
//  Created by 吕品 on 16/6/7.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>
/*!
 *  @author 众杰网络, 16-06-07 13:06:50
 *
 *  @brief 带有两个广告的collectionviewcell
 *
 *  @since 1.0
 */
@interface IndexMainTableViewCellDoubleADCollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) UIImageView *headImageView;

@end
