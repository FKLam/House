//
//  IndexMainRecommondTableViewCell.h
//  House
//
//  Created by 吕品 on 16/6/12.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IndexMainRecommondTableViewCell : UITableViewCell
@property (nonatomic, copy) NSArray<NSString *>* tagsArray;
@end
