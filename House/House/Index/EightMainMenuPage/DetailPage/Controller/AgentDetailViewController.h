//
//  AgentDetailViewController.h
//  House
//
//  Created by 张垚 on 16/7/20.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "BaseViewController.h"
@class AgentListModel;
@interface AgentDetailViewController : BaseViewController
@property (nonatomic, strong) AgentListModel *model;
@property (nonatomic, assign) NSInteger type;
@end
