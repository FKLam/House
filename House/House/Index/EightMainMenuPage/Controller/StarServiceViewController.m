//
//  StarServiceViewController.m
//  House
//
//  Created by 张垚 on 16/7/7.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "StarServiceViewController.h"
#import "BaseTitleNavigationBar.h"
#import "StarServiceListTableViewCell.h"
#import "StarServiceListModel.h"

@interface StarServiceViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) SDCycleScrollView *cycleScrollView;

@property (nonatomic, strong) NSMutableArray *imageArr;

@property (nonatomic, strong) NSMutableArray *dataArr;

@property (nonatomic, strong) MBProgressHUD *hud;
@end

@implementation StarServiceViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = YES;
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.tabBarController.tabBar.hidden = NO;
}

#pragma mark - 主程序入口
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    //顶部视图 添加返回手势
    BaseTitleNavigationBar *myBar = [[BaseTitleNavigationBar alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 64) WithTitleStr:@"星级服务"];
    [self.view addSubview:myBar];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(back:)];
    [myBar addGestureRecognizer:tap];
    
    [self initTableView];
    [self getServiceData];
    
    _hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _hud.label.text = @"正在加载数据";
    _hud.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
    _hud.backgroundView.backgroundColor = [UIColor whiteColor];
    _hud.bezelView.style = MBProgressHUDBackgroundStyleBlur;
    _hud.animationType = MBProgressHUDAnimationZoomIn;
    // Do any additional setup after loading the view.
}

//返回上一视图
- (void)back:(UITapGestureRecognizer *)tap {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - tableview
//初始化
- (void)initTableView {
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, kScreenWidth, kScreenHeight - 64) style:UITableViewStyleGrouped];
    [self.view addSubview:_tableView];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [UIView new];
    [_tableView registerClass:[StarServiceListTableViewCell class] forCellReuseIdentifier:NSStringFromClass([StarServiceListTableViewCell class])];
    
    //顶部视图
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 220)];
    //轮播图
    _cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, kScreenWidth, 170) imageURLStringsGroup:_imageArr];
    _cycleScrollView.placeholderImage = [UIImage imageNamed:@"placeHolderPic"];
    [topView addSubview:_cycleScrollView];
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 170, kScreenWidth, 50)];
    titleView.backgroundColor = [UIColor colorWithRed:0.96 green:0.96 blue:0.96 alpha:1.0];
    [topView addSubview:titleView];
    UIView *tagView = [[UIView alloc] initWithFrame:CGRectMake(20, 10, 10, 30)];
    tagView.backgroundColor = [UIColor colorWithRed:212 / 255.0f green:38 / 255.0f blue:26 / 255.0f alpha:1.0];
    [titleView addSubview:tagView];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 10, 200, 30)];
    titleLabel.text = @"星级服务套餐";
    titleLabel.font = [UIFont systemFontOfSize:20];
    [titleView addSubview:titleLabel];
    _tableView.tableHeaderView = topView;
}
//行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}
//分区数
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _dataArr.count;
}
//cell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    StarServiceListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([StarServiceListTableViewCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //点击事件
    [cell.callButton addTarget:self action:@selector(callNumber:) forControlEvents:UIControlEventTouchUpInside];
    [cell.serviceButton1 addTarget:self action:@selector(changeContent:) forControlEvents:UIControlEventTouchUpInside];
    [cell.serviceButton2 addTarget:self action:@selector(changeContent:) forControlEvents:UIControlEventTouchUpInside];
    [cell.serviceButton3 addTarget:self action:@selector(changeContent:) forControlEvents:UIControlEventTouchUpInside];
    //取model赋值给cell
    StarServiceListModel *model = _dataArr[indexPath.section];
    cell.model = model;
    return cell;
}
//行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    StarServiceListModel *model = _dataArr[indexPath.section];
    return [StarServiceListTableViewCell heightForModel:model];
}
//尾部视图最小化
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

#pragma mark - 数据处理
- (void)getServiceData {
    _imageArr = [NSMutableArray array];
    _dataArr = [NSMutableArray array];
    [NetRequest getInformationWithGetMethodWithUrl:@"http://aizufang.hrbzjwl.com/tp/api.php/Mendian/xjfw" success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"flag"] isEqualToString:@"1"]) {
            _hud.label.text = @"加载成功";
            [_hud hideAnimated:YES afterDelay:1];
            //遍历数组 将字典转换为model
            NSArray *dataArr = responseObject[@"xingji"];
            for (NSDictionary *dic1 in dataArr) {
                StarServiceListModel *model = [StarServiceListModel modelWithDic:dic1];
                model.currentIntroduction = model.introduce1;
                [_dataArr addObject:model];
            }
            //遍历轮播 将图片URL取出 赋值给轮播图
            NSArray *arr = responseObject[@"lunbo"];
            NSDictionary *dic = [arr firstObject];
            for (NSString *urlStr in [dic allValues]) {
                [_imageArr addObject:urlStr];
            }
            _cycleScrollView.imageURLStringsGroup = _imageArr;
            //刷新数据
            [_tableView reloadData];
        }else {
            _hud.label.text = @"加载失败";
            [_hud hideAnimated:YES afterDelay:1];
 
        }
       
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        _hud.label.text = @"服务器异常";
        [_hud hideAnimated:YES afterDelay:1];

    }];
}
#pragma mark - 点击事件
//拨打电话
- (void)callNumber:(UIButton *)button {
    UIView *view = [button superview];
    UIView *maxView = [view superview];
    StarServiceListTableViewCell *cell = (StarServiceListTableViewCell *)maxView;
    NSMutableString *str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",[cell.model.mobile description]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}
//三个项目切换
- (void)changeContent:(UIButton *)button {
    UIView *view = [button superview];
    UIView *maxView = [view superview];
    StarServiceListTableViewCell *cell = (StarServiceListTableViewCell *)maxView;
    NSIndexPath *indexPath = [_tableView indexPathForCell:cell];
    StarServiceListModel *model = _dataArr[indexPath.section];
    button.selected = YES;
    switch (button.tag) {
        case 100:
        {
            model.currentIntroduction = model.introduce1;
            cell.serviceButton2.selected = NO;
            cell.serviceButton3.selected = NO;
        }
            break;
        case 101:
        {
            model.currentIntroduction = model.introduce2;
            cell.serviceButton1.selected = NO;
            cell.serviceButton3.selected = NO;
        }
            break;
        case 102:
        {
            model.currentIntroduction = model.introduce3;
            cell.serviceButton1.selected = NO;
            cell.serviceButton2.selected = NO;
        }
            break;
        default:
            break;
    }
    //刷新数据
    [_tableView reloadData];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
