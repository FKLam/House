//
//  MinePublishDetailTableViewCell.swift
//  House
//
//  Created by 吕品 on 2016/9/23.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

import UIKit

class MinePublishDetailTableViewCell: UITableViewCell {

    internal let placeLabel = UILabel()
    internal let contentLabel = UILabel()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(placeLabel)
        placeLabel.textAlignment = .center
        contentView.addSubview(contentLabel)
        contentLabel.textAlignment = .left
        placeLabel.snp.makeConstraints { (make) in
            make.top.left.bottom.equalTo(contentView)
            make.right.equalTo(contentLabel.snp.left)
            make.width.equalTo(contentView).multipliedBy(0.3)
        }
        contentLabel.snp.makeConstraints { (make) in
            make.top.bottom.right.equalTo(contentView)
            make.left.equalTo(placeLabel.snp.right)
            make.width.equalTo(contentView).multipliedBy(0.7)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
