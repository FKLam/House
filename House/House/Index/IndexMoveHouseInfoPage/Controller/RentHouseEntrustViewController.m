//
//  RentHouseEntrustViewController.m
//  IndexMoveHousePage
//
//  Created by 张垚 on 16/6/17.
//  Copyright © 2016年 众杰网络. All rights reserved.
//

#import "RentHouseEntrustViewController.h"
#import "MoveHousePushTableViewCell.h"
#import "BudgetRageChooseTableViewCell.h"
#import "LabelAndTextFieldView.h"
#import "BaseTitleNavigationBar.h"
#import "EntrustRentWayViewController.h"
#import "EntrustLocationViewController.h"
#import "EntrustHouseTypeViewController.h"
#import "SubmitMenuManager.h"
#import "MoveHouseAreaChooseViewController.h"
#import "ReuseCategoryChooseTableViewCell.h"
#import "House-Swift.h"

@interface RentHouseEntrustViewController ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>
{
    NSString *_areaid;
    NSString *_bus;
    NSString *_room;
    NSString *_start;
    NSString *_end;
    NSString *_sex;
    NSString *_truename;
    NSString *_telephone;
    NSString *_rentmethod;
    NSString *_mendian;
    NSString *_kanfangtime;
    NSString *_zhuxuan;
    NSString *_cixuan;
    NSString *_must;
}
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIButton *submitButton;
@property (nonatomic, strong) NSMutableArray *descriptionArr;
@property (nonatomic, strong) NSMutableArray *placeHolderArr;
@property (nonatomic, assign) CGFloat kbHeight;

@property (nonatomic, strong) BaseTitleNavigationBar *myBar;

@property (nonatomic, strong) NSArray *wayArr;
@property (nonatomic, strong) NSArray *keyArr;
@property (nonatomic, strong) MBProgressHUD *hud;

@property (nonatomic, strong) NSMutableArray *titleArray;
@end

@implementation RentHouseEntrustViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = YES;
}


#pragma mark - 主程序入口
- (void)viewDidLoad {
    [super viewDidLoad];
     self.automaticallyAdjustsScrollViewInsets = NO;
    _sex = @"0";
    _telephone = [UserInfoManager sharedManager].phoneNumber;
    /* 键盘高度 */
    _kbHeight = 258.0f;
    /* 数据 */
    _descriptionArr = @[@"位置", @"付款方式", @"租赁方式", @"户型", @"预算", @"看房时间", @"主选位置", @"次选位置", @"选择门店", @"配套设施", @"姓名", @"性别", @"手机"].mutableCopy;
    if (_model) {
        self.titleArray = @[_model.areaname, _model.bus, [_model.rentmethod.description isEqualToString:@"1"] ? @"整租" : @"合厨", [_model.room.description stringByAppendingString:@"居室"] , @"", _model.kanfangtime.description, _model.zhuxuan.description, _model.cixuan.description, [StoresInfomationManager getNameFromID:_model.mendian.description], @""].mutableCopy;
        _areaid = _model.areaid.description;
        _bus = _model.bus.description;
        _room = _model.room.description;
        _start = _model.start.description;
        _end = _model.end.description;
        _sex = _model.sex.description;
        _truename = _model.truename.description;
        _sex = _model.sex.description;
        _rentmethod = _model.rentmethod.description;
        _mendian = _model.mendian.description;
        _kanfangtime = _model.kanfangtime.description;
        _zhuxuan = _model.zhuxuan.description;
        _cixuan = _model.cixuan.description;
        _must = _model.must.description;
    } else {
        _placeHolderArr = @[@"请选择位置", @"请选择付款方式", @"请选择租赁方式", @"请选择户型", @"请输入预算", @"请输入看房时间", @"请输入主选位置", @"请选择次选位置", @"请选择门店", @"", @"请输入您的姓名", @"请选择您的性别", @"请输入手机号"].mutableCopy;
    }
    [self initTableView];
    /* 添加观察者 */
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [self initNavigationBar];
    [self handleData];
    // Do any additional setup after loading the view.
}
//顶部自定义导航栏
- (void)initNavigationBar {
    _myBar = [[BaseTitleNavigationBar alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 64) WithTitleStr:@"租房委托"];
    [self.view addSubview:_myBar];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(back)];
    [_myBar.leftView addGestureRecognizer:tap];
}
//返回上一界面
- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark - 键盘监听事件
- (void)keyboardWillShow:(NSNotification *)showNotification {
    /*获取键盘高度 中英文下高度是不同的 */
    CGFloat kbHeight = [[showNotification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    /* 将高度赋给属性 */
    _kbHeight = kbHeight;
    
}
- (void)keyboardWillHide:(NSNotification *)hideNotification {
    // 取得键盘的动画时间，这样可以在视图上移的时候更连贯
    double duration = [[hideNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        _tableView.frame = CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height - 64);
    }];
}
#pragma mark - 提交表单
/* 提交事件*/
- (void)submit:(UIButton *)button {
    //配套设施没有选项就给空值 否则上传会炸
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:9 inSection:0];
    ReuseCategoryChooseTableViewCell *cell = [_tableView cellForRowAtIndexPath:indexPath];
    _must = cell.matchStr;
    if (_must.length == 0) {
        _must = @"";
    }
    
    if (_areaid.length == 0) {
        [self alertMessage:@"请选择区域"];
    }else if (_bus.length == 0) {
        [self alertMessage:@"请选择付款方式"];
    }else if (_rentmethod.length == 0) {
        [self alertMessage:@"请选择租赁方式"];
    }else if (_room.length == 0) {
        [self alertMessage:@"请选择户型"];
    }else if (_start.length == 0 || _end.length == 0) {
        [self alertMessage:@"请输入预算"];
    }else if (![self checkNum:_start] || ![self checkNum:_end]) {
        [self alertMessage:@"预算必须为非负整数"];
    }else if ([_start integerValue] >= [_end integerValue]) {
        [self alertMessage:@"最低预算需小于最高预算"];
    }else if (_kanfangtime.length == 0) {
        [self alertMessage:@"请输入看房时间"];
    }else if (_zhuxuan.length == 0) {
        [self alertMessage:@"请输入主选位置"];
    }else if (_cixuan.length == 0) {
        [self alertMessage:@"请输入次选位置"];
    }else if (_mendian.length == 0) {
        [self alertMessage:@"请选择门店"];
    }else if (_truename.length == 0) {
        [self alertMessage:@"请输入姓名"];
    }else if ([_sex isEqualToString:@"0"]) {
        [self alertMessage:@"请选择性别"];
    }else {
        if (![self valiMobile:_telephone]) {
            NSMutableDictionary *submitInfoDic = [NSMutableDictionary dictionary];
            [submitInfoDic setObject:[UserInfoManager sharedManager].uid forKey:@"userid"];
            
            [submitInfoDic setObject:_areaid forKey:@"areaid"];
            [submitInfoDic setObject:_bus forKey:@"bus"];
            [submitInfoDic setObject:_rentmethod forKey:@"rentmethod"];
            [submitInfoDic setObject:_room forKey:@"room"];
            [submitInfoDic setObject:_start forKey:@"start"];
            [submitInfoDic setObject:_end forKey:@"end"];
            [submitInfoDic setObject:_kanfangtime forKey:@"kanfangtime"];
            [submitInfoDic setObject:_zhuxuan forKey:@"zhuxuan"];
            [submitInfoDic setObject:_cixuan forKey:@"cixuan"];
            [submitInfoDic setObject:_mendian forKey:@"mendian"];
            [submitInfoDic setObject:_must forKey:@"must"];
            
            [submitInfoDic setObject:_truename forKey:@"truename"];
            [submitInfoDic setObject:_sex forKey:@"sex"];
            [submitInfoDic setObject:_telephone forKey:@"telephone"];
            _hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            _hud.label.text = @"上传中...";
            NSString *url;
            if (!_model) {
                url = @"http://aizufang.hrbzjwl.com/tp/api.php/Index/App_zfwt";
            } else {
                url = @"http://aizufang.hrbzjwl.com/tp/api.php/Index/App_edit_zfwt";
                [submitInfoDic setObject:_model.id.description forKey:@"id"];
            }
            NSLog(@"%@", submitInfoDic);
            [NetRequest updateOrGetInformationFromUrl:url withParameters:submitInfoDic success:^(NSURLSessionDataTask *task, id responseObject) {
                NSLog(@"%@", responseObject);
                if ([[responseObject[@"flg"] description] isEqualToString:@"1"]) {
                    _hud.label.text = @"上传成功";
                    [_hud hideAnimated:YES afterDelay:1];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self back];
                    });
                }else {
                    _hud.label.text = @"上传失败";
                    [_hud hideAnimated:YES afterDelay:1];
                }
            } failure:^(NSURLSessionDataTask *task, NSError *error) {
                _hud.label.text = @"服务器异常";
                [_hud hideAnimated:YES afterDelay:1];
            }];
}else {
            [self alertMessage:[self valiMobile:_telephone]];
        }
    }
}
/* 检查输入的手机号码是否全为数字 返回BOOL值 */
-(BOOL)checkNum:(NSString *)inputStr {
    /* 创建扫描类 */
    NSScanner *scanner = [NSScanner scannerWithString:inputStr];
    int value;
    /* [scanner scanInt:&value]该方法将扫描的int放入value 返回布尔值 */
    /* [scanner isAtEnd] 该方法判断是否走到了字符串末端 返回的也是布尔值 */
    return[scanner scanInt:&value] && [scanner isAtEnd];
}
- (NSString *)valiMobile:(NSString *)mobile{
    if (mobile.length < 11)
    {
        return @"请输入11位手机号";
    }else{
        /**
         * 移动号段正则表达式
         */
        NSString *CM_NUM = @"^((13[4-9])|(147)|(15[0-2,7-9])|(178)|(18[2-4,7-8]))\\d{8}|(1705)\\d{7}$";
        /**
         * 联通号段正则表达式
         */
        NSString *CU_NUM = @"^((13[0-2])|(145)|(15[5-6])|(176)|(18[5,6]))\\d{8}|(1709)\\d{7}$";
        /**
         * 电信号段正则表达式
         */
        NSString *CT_NUM = @"^((133)|(153)|(177)|(18[0,1,9]))\\d{8}$";
        NSPredicate *pred1 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM_NUM];
        BOOL isMatch1 = [pred1 evaluateWithObject:mobile];
        NSPredicate *pred2 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU_NUM];
        BOOL isMatch2 = [pred2 evaluateWithObject:mobile];
        NSPredicate *pred3 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT_NUM];
        BOOL isMatch3 = [pred3 evaluateWithObject:mobile];
        
        if (isMatch1 || isMatch2 || isMatch3) {
            return nil;
        }else{
            return @"请输入正确格式的手机号";
        }
    }
    return nil;
}

- (void)alertMessage:(NSString *)message {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:YES completion:nil];
    });
    [self presentViewController:alertController animated:YES completion:nil];
}
#pragma mark - tableview
/* 创建 */
- (void)initTableView {
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height - 64) style:UITableViewStyleGrouped];
    _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    [_tableView registerClass:[MoveHousePushTableViewCell class] forCellReuseIdentifier:NSStringFromClass([MoveHousePushTableViewCell class])];
    [_tableView registerClass:[BudgetRageChooseTableViewCell class] forCellReuseIdentifier:NSStringFromClass([BudgetRageChooseTableViewCell class])];
    [_tableView registerClass:[ReuseCategoryChooseTableViewCell class] forCellReuseIdentifier:NSStringFromClass([ReuseCategoryChooseTableViewCell class])];
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 80)];
    _submitButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [footerView addSubview:_submitButton];
    [_submitButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(footerView.mas_top).offset(20);
        make.left.equalTo(footerView.mas_left).offset(40);
        make.right.equalTo(footerView.mas_right).offset(-40);
        make.height.equalTo(@50);
    }];
    _submitButton.layer.masksToBounds = YES;
    _submitButton.layer.cornerRadius = 6;
    [_submitButton setTitle:@"提交" forState:UIControlStateNormal];
    [_submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _submitButton.backgroundColor = [UIColor colorWithRed:212 / 255.0f green:38 / 255.0f blue:26 / 255.0f alpha:1.0];
    [_submitButton addTarget:self action:@selector(submit:) forControlEvents:UIControlEventTouchUpInside];
    _tableView.tableFooterView = footerView;
}
/* 行数 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}
/* 每个区中的行数 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 10;
    }else {
        return 3;
    }
}

/* cell */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        //预算
        if (indexPath.row == 4) {
            BudgetRageChooseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([BudgetRageChooseTableViewCell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.minPriceTextfield.delegate = self;
            cell.maxPriceTextfiled.delegate = self;
            if (_model) {
                cell.minPriceTextfield.text = _model.start.description;
                cell.maxPriceTextfiled.text = _model.end.description;
            }
            cell.descriptionLabel.text = _descriptionArr[indexPath.row];
            cell.typeTag = 1;
            return cell;
        }
        //配套设施
        else if (indexPath.row == 9) {
            ReuseCategoryChooseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ReuseCategoryChooseTableViewCell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.descriptionLabel.text = _descriptionArr[indexPath.row];
            return cell;
        }
        else {
            MoveHousePushTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([MoveHousePushTableViewCell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.inputView.titleLabel.text = _descriptionArr[indexPath.row];
            if (indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 8) {
                //关闭交互 用cell去交互
                cell.inputView.contextTextField.userInteractionEnabled = NO;
            }else {
                cell.isPush = NO;
                cell.inputView.contextTextField.delegate = self;
                cell.inputView.contextTextField.tag = indexPath.row;
            }

            cell.inputView.contextTextField.placeholder = _placeHolderArr[indexPath.row];
            if (_model) {
                cell.inputView.contextTextField.text = _titleArray[indexPath.row];
            }
            return cell;
        }
    }else {
        NSString *cellID = [NSString stringWithFormat:@"cell%ld%ld", (long)indexPath.section,(long)indexPath.row];
        MoveHousePushTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [[MoveHousePushTableViewCell alloc] init];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.inputView.titleLabel.text = _descriptionArr[indexPath.section * 10 + indexPath.row];
        cell.inputView.contextTextField.tag = indexPath.section * 10 + indexPath.row;
        if (indexPath.row == 1) {
            cell.inputView.contextTextField.userInteractionEnabled = NO;
            if (_model) {
                cell.inputView.contextTextField.text = [_model.sex.description isEqualToString:@"1"] ? @"男" : @"女";
            } else {
                if ([_sex isEqualToString:@"0"]) {
                    cell.inputView.contextTextField.placeholder = _placeHolderArr[indexPath.section * 10 + indexPath.row];
                }else if ([_sex isEqualToString:@"1"]) {
                    cell.inputView.contextTextField.text = @"男";
                }else {
                    cell.inputView.contextTextField.text = @"女";
                }
            }
        }else {
            cell.inputView.contextTextField.delegate = self;
            cell.isPush = NO;
            if (indexPath.row == 0) {
                if (_model) {
                    cell.inputView.contextTextField.text = _model.truename.description;
                } else {
                    if (_truename.length == 0) {
                        cell.inputView.contextTextField.placeholder = _placeHolderArr[indexPath.section * 10 + indexPath.row];
                    }else {
                        cell.inputView.contextTextField.text = _truename;
                    }
                }
            }else {
                if (_model) {
                    cell.inputView.contextTextField.text = _model.telephone.description;
                } else {
                    cell.inputView.contextTextField.text = _telephone;
                }
            }
        }
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
     MoveHousePushTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.section == 0) {
        if (indexPath.row == 1) {
            EntrustRentWayViewController *wayVC = [[EntrustRentWayViewController alloc] init];
            wayVC.wayArr = _wayArr;
            wayVC.currentStr = cell.inputView.contextTextField.text;
            [wayVC passInfoToPreviousPage:^(NSString *wayStr) {
                cell.inputView.contextTextField.text = wayStr;
                _bus = wayStr;
                if (_model) {
                    [_titleArray replaceObjectAtIndex:indexPath.row withObject:wayStr];
                }
            }];
            [self.navigationController pushViewController:wayVC animated:YES];
        }else if (indexPath.row == 2) {
            EntrustHouseTypeViewController *entrustVC = [[EntrustHouseTypeViewController alloc] init];
            entrustVC.tag = 2;
            entrustVC.currentStr = cell.inputView.contextTextField.text;
            [entrustVC passInfoToPreviousPage:^(NSString *currentStr, NSInteger currentTag) {
                cell.inputView.contextTextField.text = currentStr;
                _rentmethod = [NSString stringWithFormat:@"%ld", (long)currentTag];
                if (_model) {
                    [_titleArray replaceObjectAtIndex:indexPath.row withObject:currentStr];
                }
            }];
            [self.navigationController pushViewController:entrustVC animated:YES];
        }else if (indexPath.row == 0){
            MoveHouseAreaChooseViewController *areaChooseVC = [[MoveHouseAreaChooseViewController alloc] init];
            areaChooseVC.currentStr = cell.inputView.contextTextField.text;
            areaChooseVC.categoryStr = @"区域选择";
            [areaChooseVC passInfoToPreviousPage:^(NSString *infoStr, NSInteger tag) {
                cell.inputView.contextTextField.text = infoStr;
                _areaid = [NSString stringWithFormat:@"%ld", (long)tag];
                if (_model) {
                    [_titleArray replaceObjectAtIndex:indexPath.row withObject:infoStr];
                }
            }];
            [self.navigationController pushViewController:areaChooseVC animated:YES];
        }else if (indexPath.row == 3){
            EntrustHouseTypeViewController *entrustVC = [[EntrustHouseTypeViewController alloc] init];
            entrustVC.currentStr = cell.inputView.contextTextField.text;
            entrustVC.tag = 1;
            [entrustVC passInfoToPreviousPage:^(NSString *currentStr, NSInteger currentTag) {
                cell.inputView.contextTextField.text = currentStr;
                _room = [NSString stringWithFormat:@"%ld", (long)currentTag];
                if (_model) {
                    [_titleArray replaceObjectAtIndex:indexPath.row withObject:currentStr];
                }
            }];
            [self.navigationController pushViewController:entrustVC animated:YES];
        }else if (indexPath.row == 8){
            if (_model) {
                [self alertMessage:@"不能更改门店"];
            } else {
                if (_areaid == NULL) {
                    [self alertMessage:@"请先选择区域"];
                }else {
                    EntrustHouseTypeViewController *entrustVC = [[EntrustHouseTypeViewController alloc] init];
                    entrustVC.tag = 3;
                    entrustVC.currentStr = cell.inputView.contextTextField.text;
                    entrustVC.areaID = _areaid;
                    [entrustVC passInfoToPreviousPage:^(NSString *currentStr, NSInteger currentTag) {
                        cell.inputView.contextTextField.text = currentStr;
                        _mendian = [NSString stringWithFormat:@"%ld", (long)currentTag];
                        if (_model) {
                            [_titleArray replaceObjectAtIndex:indexPath.row withObject:currentStr];
                        }
                    }];
                    [self.navigationController pushViewController:entrustVC animated:YES];
                }
            }
        }
    } else {
        if (indexPath.row == 1) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
            UIAlertAction *manAction = [UIAlertAction actionWithTitle:@"男" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                cell.inputView.contextTextField.text = @"男";
                _sex = @"1";
            }];
            UIAlertAction *womanAction = [UIAlertAction actionWithTitle:@"女" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                cell.inputView.contextTextField.text = @"女";
                _sex = @"2";
            }];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
            [alertController addAction:manAction];
            [alertController addAction:womanAction];
            [alertController addAction:cancel];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
}
/* Footer */
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}
/* Header */
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return CGFLOAT_MIN;
    }else {
        return 20;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 9) {
            return 120;
        }else {
            return 44;
        }
    }else {
        return 44;
    }
}

#pragma mark - 代理协议方法
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}



/* 编辑完成 监听textfiled上的字段 */
- (void)textFieldDidEndEditing:(UITextField *)textField {
    //根据标签 给对应的成员变量赋值
    if (textField.tag == 100) {
        _start = textField.text;
    }else if (textField.tag == 101) {
        _end = textField.text;
    }else if (textField.tag == 5) {
        _kanfangtime = textField.text;
    }else if (textField.tag == 6) {
        _zhuxuan = textField.text;
    }else if (textField.tag == 7) {
        _cixuan = textField.text;
    }else if (textField.tag == 10) {
        _truename = textField.text;
    }else if (textField.tag == 12) {
        _telephone = textField.text;
    }
}



- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    UIView *view = textField.superview;
    while (![view isKindOfClass:[UITableViewCell class]]) {
        view = [view superview];
    }
    UITableViewCell *cell = (UITableViewCell*)view;
    CGRect rect = [cell convertRect:cell.frame toView:self.view];
    if (rect.origin.y / 2 + rect.size.height >= self.view.frame.size.height - _kbHeight) {
        
        CGFloat offset = _kbHeight;
        [UIView animateWithDuration:0.25 animations:^{
            _tableView.frame = CGRectMake(0, -offset + 64, self.view.frame.size.width, self.view.frame.size.height);
            
        }];
    }
    return YES;
}
#pragma mark - 移除观察者
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)handleData {
    [NetRequest getInformationWithGetMethodWithUrl:@"http://aizufang.hrbzjwl.com/tp/api.php/Index/App_zumethod" success:^(NSURLSessionDataTask *task, id responseObject) {
        _wayArr = responseObject;
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
