
//
//  main.m
//  House
//
//  Created by 吕品 on 16/5/31.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
