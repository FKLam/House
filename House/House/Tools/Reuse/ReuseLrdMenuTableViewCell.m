//
//  ReuseLrdMenuTableViewCell.m
//  House
//
//  Created by 吕品 on 16/6/18.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "ReuseLrdMenuTableViewCell.h"
#import "LrdSuperMenu.h"
@implementation ReuseLrdMenuTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.menu = [[LrdSuperMenu alloc] initWithOrigin:CGPointMake(0, 0) andHeight:self.contentView.frame.size.width];
        [self.contentView addSubview:_menu];
    }
    return self;
}
- (void)layoutSubviews {
    [super layoutSubviews];
    self.menu.frame = self.contentView.frame;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
