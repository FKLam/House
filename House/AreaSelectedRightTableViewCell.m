//
//  AreaSelectedRightTableViewCell.m
//  House
//
//  Created by 吕品 on 16/6/18.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "AreaSelectedRightTableViewCell.h"
@interface AreaSelectedRightTableViewCell ()

@end
@implementation AreaSelectedRightTableViewCell
#pragma mark init
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.nameLabel = [UILabel new];
        [self.contentView addSubview:_nameLabel];
        _nameLabel.font = [UIFont systemFontOfSize:14];
        self.distanceLabel = [UILabel new];
        [self.contentView addSubview:_distanceLabel];
        _distanceLabel.font = [UIFont systemFontOfSize:14];
        self.positionLabel = [UILabel new];
        [self.contentView addSubview:_positionLabel];
        _positionLabel.font = [UIFont systemFontOfSize:12];

    }
    return self;
}
#pragma mark layout
- (void)layoutSubviews {
    [super layoutSubviews];
    CGFloat padding = 15;
    CGFloat width = (self.contentView.frame.size.width - padding * 2 ) / 10.0f;
    CGFloat height = (self.contentView.frame.size.height - padding * 3) / 2.0f;
    UIView *superView = self.contentView;
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(superView).with.offset(padding);
        make.top.mas_equalTo(superView.mas_top).with.offset(5);
        make.width.mas_equalTo(width * 7);
        make.height.mas_equalTo(height);
    }];

    [_distanceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.and.right.mas_equalTo(superView).with.offset(padding);
        make.width.mas_equalTo(width * 3);
        make.height.mas_equalTo(height);
    }];

    [_positionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_nameLabel.mas_bottom).with.offset(padding);
        make.left.mas_equalTo(superView.mas_left).with.offset(padding);
        make.width.mas_equalTo(width * 10);
        make.height.mas_equalTo(height);
    }];

}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
