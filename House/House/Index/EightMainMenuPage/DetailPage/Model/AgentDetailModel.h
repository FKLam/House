//
//  AgentDetailModel.h
//  House
//
//  Created by 张垚 on 16/8/11.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "BaseModel.h"

typedef NS_ENUM(NSUInteger, AgentDetailType) {
    Agent = 0,
    Community,
};

@interface AgentDetailModel : BaseModel

@property (nonatomic, copy) NSString *hall;
@property (nonatomic, copy) NSString *houseearm;
@property (nonatomic, copy) NSString *itemid;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *room;
@property (nonatomic, copy) NSString *thumb;
@property (nonatomic, copy) NSString *title;

@end
