//
//  IndexMainTableViewCellCollectionViewCell.h
//  House
//
//  Created by 吕品 on 16/6/4.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>
/*!
 *  @author 众杰网络, 16-06-07 13:06:59
 *
 *  @brief 图片和label的collectionviewcell
 *
 *  @since 1.0
 */
@interface IndexMainTableViewCellCollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *headImageView;
@property (nonatomic, assign) BOOL isWithGrayView;
@end
