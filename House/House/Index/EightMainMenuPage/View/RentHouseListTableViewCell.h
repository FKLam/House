//
//  RentHouseListTableViewCell.h
//  House
//
//  Created by 张垚 on 16/7/19.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SecondaryListDetailView.h"
@class RentHouseListModel;
@class RentHouseListTableViewCell;
@class ImageViewAndLabelButton;

@protocol RentHouseListTableViewCellDelegate <NSObject>

//两个手势 代理方法
- (void)leftSwipe:(RentHouseListTableViewCell *)obj;
- (void)rightSwipe:(RentHouseListTableViewCell *)obj;
- (void)passButton:(ImageViewAndLabelButton *)button;

@end



@interface RentHouseListTableViewCell : UITableViewCell
@property (nonatomic, assign) id <RentHouseListTableViewCellDelegate> delegate;
//详情视图属性
@property (nonatomic, strong) SecondaryListDetailView *myVIew;
@property (nonatomic, strong) RentHouseListModel *model;
@end
