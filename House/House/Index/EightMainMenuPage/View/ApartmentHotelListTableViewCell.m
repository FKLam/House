//
//  ApartmentHotelListTableViewCell.m
//  House
//
//  Created by 张垚 on 16/7/7.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "ApartmentHotelListTableViewCell.h"
#import "ApartmentHotelListModel.h"


@interface ApartmentHotelListTableViewCell ()
@property (nonatomic, strong) UIImageView *houseImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *typeLabel;
@property (nonatomic, strong) UILabel *addressLabel;
@property (nonatomic, strong) UILabel *priceLabel;


@end


@implementation ApartmentHotelListTableViewCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _houseImageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_houseImageView];
        _nameLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_nameLabel];
        _typeLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_typeLabel];
        _addressLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_addressLabel];
        _priceLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_priceLabel];
        
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self.contentView;
    [_houseImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(10);
        make.top.equalTo(superView.mas_top).offset(10);
        make.bottom.equalTo(superView.mas_bottom).offset(-10);
        make.width.equalTo(@100);
    }];
    _houseImageView.layer.cornerRadius = 6;
    _houseImageView.layer.masksToBounds = YES;
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_houseImageView.mas_right).offset(10);
        make.top.equalTo(_houseImageView.mas_top).offset(10);
        make.height.equalTo(@20);
        make.right.equalTo(_priceLabel.mas_left).offset(-10);
    }];
    [_typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_priceLabel.mas_left).offset(-10);
        make.left.equalTo(_houseImageView.mas_right).offset(10);
        make.top.equalTo(_nameLabel.mas_bottom);
        make.height.equalTo(@15);
    }];
    _typeLabel.textColor = [UIColor colorWithRed:0.502 green:0.502 blue:0.502 alpha:1.0];
    _typeLabel.font = [UIFont systemFontOfSize:11];
    [_addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_houseImageView.mas_right).offset(10);
        make.top.equalTo(_typeLabel.mas_bottom);
        make.height.equalTo(@15);
        make.right.equalTo(_priceLabel.mas_left).offset(-10);
    }];
    _addressLabel.numberOfLines = 0;
    _addressLabel.textColor = [UIColor colorWithRed:0.502 green:0.502 blue:0.502 alpha:1.0];
    _addressLabel.font = [UIFont systemFontOfSize:11];
    [_priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(superView.mas_right).offset(-5);
        make.width.equalTo(@60);
        make.height.equalTo(@20);
        make.centerY.equalTo(superView.mas_centerY);
    }];
    _priceLabel.font = [UIFont systemFontOfSize:13];
    _priceLabel.textColor = KNaviBackColor;
}

-(void)setModel:(ApartmentHotelListModel *)model {
    if (model != _model) {
        _model = model;
    }
    [_houseImageView setImageWithURL:[NSURL URLWithString:[model.thumb description]] placeholderImage:[UIImage imageNamed:@"placeHolderImage"]];
    _nameLabel.text = [_model.housename description];
    _typeLabel.text = [NSString stringWithFormat:@"房间类型:%@", [model.type description]];
    _addressLabel.text = [model.streets description];
    _priceLabel.text = [NSString stringWithFormat:@"%@元/天", [model.price description]];
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
