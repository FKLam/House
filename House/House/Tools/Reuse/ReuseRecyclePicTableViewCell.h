//
//  ReuseRecyclePicTableViewCell.h
//  House
//
//  Created by 吕品 on 16/6/7.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ZJRecyclePicView;
/*!
 *  @author 众杰网络, 16-06-07 09:06:54
 *
 *  @brief 可以复用的tableviewcell带有轮播图
 *
 *  @since 1.0
 */
@interface ReuseRecyclePicTableViewCell : UITableViewCell
@property (nonatomic, strong) ZJRecyclePicView *picView;
@end
