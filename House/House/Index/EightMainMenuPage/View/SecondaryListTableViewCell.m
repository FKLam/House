//
//  SecondaryListTableViewCell.m
//  House
//
//  Created by 张垚 on 16/6/29.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "SecondaryListTableViewCell.h"
#import "ImageViewAndLabelButton.h"
#import "SecondaryListDetailView.h"
#import "SecondaryListModel.h"

@interface SecondaryListTableViewCell ()<SecondaryListDetailViewDelegate>

@property (nonatomic, strong) UISwipeGestureRecognizer *leftSwipe;

@property (nonatomic, strong) UISwipeGestureRecognizer *rightSwipe;
//赋值给侧滑按钮的数据 标题 图片 颜色
@property (nonatomic, strong) NSArray *titleArr;
@property (nonatomic, strong) NSArray *imageArr;
@property (nonatomic, strong) NSArray *colorArr;


@end

@implementation SecondaryListTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        /* cell 添加手势 */
        _leftSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftOffset:)];
        
        _leftSwipe.direction = UISwipeGestureRecognizerDirectionLeft;
        [self addGestureRecognizer:_leftSwipe];
        
        _rightSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightOffset:)];
        _rightSwipe.direction = UISwipeGestureRecognizerDirectionRight;
        [self addGestureRecognizer:_rightSwipe];
        
        
        /* 选择框背景颜色 */
        _colorArr = @[[UIColor colorWithRed:0.8867 green:0.8825 blue:0.891 alpha:1.0], [UIColor colorWithRed:0.2628 green:0.7406 blue:0.9366 alpha:1.0], [UIColor colorWithRed:1.0 green:0.196 blue:0.2054 alpha:1.0]];
        _titleArr = @[@"关注", @"卖房", @"相似"];
        _imageArr = @[@"attentionSuccessWhite", @"sellHouse", @"similar"];
        //图片暂未设置
        _myVIew = [[SecondaryListDetailView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth + 300, 100) WithTitleArr:_titleArr WithImageArr:_imageArr WithColorArr:_colorArr WithButtonNum:3 ];
        _myVIew.delegate = self;
        [self.contentView addSubview:_myVIew];
        _myVIew.backgroundColor = [UIColor whiteColor];
        
    }
    return self;
}

//点击事件 通过代理传出去 在VC里面执行

- (void)passButton:(ImageViewAndLabelButton *)button {
    if (button.tag == 1000) {
        button.selected = !button.selected;
        if (button.selected) {
            button.logoImage.image = [UIImage imageNamed:@"attentionSuccess"];
        }else {
            button.logoImage.image = [UIImage imageNamed:@"attentionSuccessWhite"];
        }
    }
    [self.delegate passButton:button];
    
}
//侧滑手势
- (void)leftOffset:(UISwipeGestureRecognizer *)swipe {
    [self.delegate leftSwipe:self];
}

- (void)rightOffset:(UISwipeGestureRecognizer *)swipe {
    [self.delegate rightSwipe:self];
}

- (void)setModel:(SecondaryListModel *)model {
    if (model != _model) {
        _model = model;
    }
    _myVIew.tilteLabel.text = model.title;
    [_myVIew.roomImageView setImageWithURL:[NSURL URLWithString:model.thumb] placeholderImage:[UIImage imageNamed:@"placeHolderImage"]];
    _myVIew.doorModelLabels.text = [NSString stringWithFormat:@"2室2厅·%@·南",model.houseearm];
    _myVIew.communityLabels.text = [NSString stringWithFormat:@"海河绿源·%@", model.address];
    _myVIew.priceLabel.text = [NSString stringWithFormat:@"%@万", model.price];
    _myVIew.areaSizeLabel.text = @"10000元/㎡";
    NSArray *colorArr = @[[UIColor redColor], [UIColor purpleColor], [UIColor orangeColor], [UIColor cyanColor]];
    if ([model.fyts isEqualToString:@""]) {
        return;
    }else {
        for (int i = 0; i < [model.fyts componentsSeparatedByString:@","].count; i++) {
            if (i == 0) {
                _myVIew.tagLabel1.layer.borderColor = [UIColor redColor].CGColor;
                _myVIew.tagLabel1.text = [model.fyts componentsSeparatedByString:@","][i];
                _myVIew.tagLabel1.textColor = colorArr[i];
                
                _myVIew.tagLabel1.layer.borderWidth = 1.0;
                _myVIew.tagLabel1.font = [UIFont systemFontOfSize:8];
                _myVIew.tagLabel1.text = [model.fyts componentsSeparatedByString:@","][i];
                _myVIew.tagLabel1.textAlignment = NSTextAlignmentCenter;
            }else if (i == 1) {
                _myVIew.tagLabel2.text = [model.fyts componentsSeparatedByString:@","][i];
                _myVIew.tagLabel2.textColor = colorArr[i];
                _myVIew.tagLabel2.layer.borderColor = [UIColor purpleColor].CGColor;
                _myVIew.tagLabel2.layer.borderWidth = 1.0;
                _myVIew.tagLabel2.font = [UIFont systemFontOfSize:8];
                _myVIew.tagLabel2.text = [model.fyts componentsSeparatedByString:@","][i];
                _myVIew.tagLabel2.textAlignment = NSTextAlignmentCenter;
            }else if (i == 2) {
                _myVIew.tagLabel3.text = [model.fyts componentsSeparatedByString:@","][i];
                _myVIew.tagLabel3.textColor = colorArr[i];
                _myVIew.tagLabel3.layer.borderColor = [UIColor orangeColor].CGColor;
                _myVIew.tagLabel3.layer.borderWidth = 1.0;
                _myVIew.tagLabel3.font = [UIFont systemFontOfSize:8];
                _myVIew.tagLabel3.text = [model.fyts componentsSeparatedByString:@","][i];
                _myVIew.tagLabel3.textAlignment = NSTextAlignmentCenter;
            }else {
                _myVIew.tagLabel4.text = [model.fyts componentsSeparatedByString:@","][i];
                _myVIew.tagLabel4.textColor = colorArr[i];
                _myVIew.tagLabel4.layer.borderColor = [UIColor cyanColor].CGColor;
                _myVIew.tagLabel4.layer.borderWidth = 1.0;
                _myVIew.tagLabel4.font = [UIFont systemFontOfSize:8];
                _myVIew.tagLabel4.text = [model.fyts componentsSeparatedByString:@","][i];
                _myVIew.tagLabel4.textAlignment = NSTextAlignmentCenter;
            }
        }
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
