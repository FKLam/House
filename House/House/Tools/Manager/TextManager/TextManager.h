//
//  TextManager.h
//  House
//
//  Created by 吕品 on 16/6/3.
//  Copyright © 2016年 ZJWL. All rights reserved.
//
/*!
 *  @author 众杰网络, 16-06-03 18:06:37
 *
 *  @brief 处理各种文字
 *
 *  @since 1.0
 */
#ifndef TextManager_h
#define TextManager_h
#pragma mark userInfo
static NSString *userID = @"useID";
static NSString *userPhoneNumber = @"userPhoneNumber";
static NSString *userLoginStatus = @"loginStatus";
static NSString *userNickNmae = @"userNickName";
static NSString *userCityId = @"userCityID";//城市ID
#pragma mark public
static NSString *pleaseTryAgain = @"请再试一次!";
#pragma mark load
static NSString *loadSuccess = @"加载成功";
static NSString *loadFaild = @"加载失败";
#pragma mark upload
static NSString *uploadSuccess = @"上传成功";
static NSString *uploadFaild = @"上传失败";
#pragma mark 定位
static NSString *locationProgressing = @"定位中~";
static NSString *locationSuccess = @"定位成功~";
static NSString *locationFaild = @"定位失败~";
#pragma mark 统一电话
static NSString *unifyServiceNumber = @"4000404717";
#pragma mark 刷新
#pragma mark 首页图标数组字段
#define KindexMainTableViewCollectionViewHeadArray @[@"二手房", @"公寓酒店", @"租房", @"门店", @"地图找房", @"委托服务", @"星级服务", @"经纪人"]
#define KindexMainTableViewCollectionViewMoveHomeHeadTitleArray @[@"搬家", @"保洁", @"装修", @"租房贷"]
#pragma mark TAB
#define KTABTitleArray @[@"首页", @"活动", @"社区门店", @"我的"]

#pragma mark - 数据库
static NSString *dataBaseName = @"area.db";
static NSString *tableName = @"area_table";
static NSString *cityID = @"cityID";
//#define dataBaseName @"area.db"
//#define tableName @"area_table"
//#define cityID @"cityID"

#endif /* TextManager_h */
