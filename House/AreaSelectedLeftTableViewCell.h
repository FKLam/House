//
//  AreaSelectedLeftTableViewCell.h
//  House
//
//  Created by 吕品 on 16/6/18.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AreaSelectedLeftTableViewCell : UITableViewCell
@property (nonatomic, strong) UILabel *nameLabel;
@end
