//
//  RentHouseRoomFillTableViewCell.m
//  IndexMoveHousePage
//
//  Created by 张垚 on 16/6/15.
//  Copyright © 2016年 众杰网络. All rights reserved.
//

#import "RentHouseRoomFillTableViewCell.h"
#import "MultiLabelAndMultiTextfiledView.h"
#import "Masonry.h"

@implementation RentHouseRoomFillTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _descriptionLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_descriptionLabel];
        _roomView = [[MultiLabelAndMultiTextfiledView alloc] init];
        [self.contentView addSubview:_roomView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self.contentView;
    [_descriptionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(5);
        make.left.equalTo(superView.mas_left);
        make.bottom.equalTo(superView.mas_bottom).offset(-5);
        make.width.equalTo(@60);
    }];
    _descriptionLabel.font = [UIFont systemFontOfSize:15];
    _descriptionLabel.textAlignment = NSTextAlignmentCenter;
    
    [_roomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(5);
        make.left.equalTo(_descriptionLabel.mas_right).offset(10);
        make.right.equalTo(superView.mas_right);
        make.bottom.equalTo(superView.mas_bottom).offset(-5);
    }];
    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
