//
//  StoresInfomationManager.h
//  House
//
//  Created by 吕品 on 2016/9/27.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <Foundation/Foundation.h>
@class StoresInfomationModel;
@interface StoresInfomationManager : NSObject
+ (NSArray *)getAllObject;
+ (NSString *)getNameFromID:(NSString *)uid;
+ (void)putObjectsWithArray:(NSArray<StoresInfomationModel *>*)arr;
@end

@interface StoresInfomationModel : NSObject
@property (nonatomic, copy) NSString *userid;
@property (nonatomic, copy) NSString *company;
@end
