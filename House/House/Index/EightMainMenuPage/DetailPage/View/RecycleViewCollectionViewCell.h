//
//  RecycleViewCollectionViewCell.h
//  House
//
//  Created by 张垚 on 16/7/12.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecycleViewCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *mainImageView;
@property (nonatomic, strong) UILabel *pageNumLabel;

@end
