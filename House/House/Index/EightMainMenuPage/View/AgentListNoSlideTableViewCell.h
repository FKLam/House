//
//  AgentListNoSlideTableViewCell.h
//  House
//
//  Created by 张垚 on 16/9/2.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AgentListModel;

@interface AgentListNoSlideTableViewCell : UITableViewCell

@property (nonatomic, strong) AgentListModel *model;

//@property (nonatomic, strong) UIStackView *buttonStackView;
@property (nonatomic, strong) UIImageView *headImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UIView *dividerView;
@property (nonatomic, strong) UILabel *yearLabel;
@property (nonatomic, strong) UILabel *storesLabel;
@property (nonatomic, strong) UIView *instructionView;

@end
