//
//  MinePersonalInfoTableViewCell.swift
//  House
//
//  Created by 吕品 on 16/7/6.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

import UIKit

class MinePersonalInfoTableViewCell: UITableViewCell {

    var mixView = MinePersonalInfoLabelAndTextFieldView()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        mixView = MinePersonalInfoLabelAndTextFieldView.init(frame: CGRect.zero)
        contentView.addSubview(mixView)

        accessoryType = .disclosureIndicator
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        mixView.snp.makeConstraints { (make) in
            make.top.right.bottom.equalTo(contentView)
            make.left.equalTo(contentView.snp.left).offset(10)
        }
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
