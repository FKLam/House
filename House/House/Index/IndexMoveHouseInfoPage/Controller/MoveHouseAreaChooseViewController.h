//
//  MoveHouseAreaChooseViewController.h
//  IndexMoveHousePage
//
//  Created by 张垚 on 16/6/8.
//  Copyright © 2016年 众杰网络. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

typedef void(^chooseInfoBlock)(NSString *infoStr, NSInteger tag);
@interface MoveHouseAreaChooseViewController : BaseViewController

@property (nonatomic, copy) NSString *areaChooseInfo;
@property (nonatomic, copy) chooseInfoBlock chooseInfoBlock;
@property (nonatomic, assign) NSInteger tag;
@property (nonatomic, copy) NSString *categoryStr;
@property (nonatomic, copy) NSString *currentStr;

- (void)passInfoToPreviousPage:(chooseInfoBlock)infoBlock;

@end
