//
//  BaseNavigationBar.m
//  House
//
//  Created by 张垚 on 16/7/8.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "BaseNavigationBar.h"
#import "MySearchBar.h"

@implementation BaseNavigationBar

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:212 / 255.0f green:38 / 255.0f blue:26 / 255.0f alpha:1.0];
        _leftView = [[UIView alloc] init];
        [self addSubview:_leftView];
        _searchBar = [[MySearchBar alloc] init];
        [self addSubview:_searchBar];
        _searchBar.textFiled.userInteractionEnabled = NO;
        _leftImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 2.5, 25, 25)];
        [_leftView addSubview:_leftImageView];
        _leftLabel = [[UILabel alloc] initWithFrame:CGRectMake(25, 0, 55, 30)];
        _leftLabel.textColor = [UIColor whiteColor];
        [_leftView addSubview:_leftLabel];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self;
    [_leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(5);
        make.bottom.equalTo(superView.mas_bottom).offset(-5);
        make.height.equalTo(@30);
        make.width.equalTo(@80);
    }];
    [_searchBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_leftView.mas_right).offset(5);
        make.bottom.equalTo(superView.mas_bottom).offset(-5);
        make.height.equalTo(@30);
        make.right.equalTo(superView.mas_right).offset(-5);
    }];
    _searchBar.backgroundColor = [UIColor colorWithRed:0.665 green:0.0 blue:0.0062 alpha:1.0];

}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
