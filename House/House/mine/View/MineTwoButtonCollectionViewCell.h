//
//  MineTwoButtonCollectionViewCell.h
//  House
//
//  Created by 吕品 on 16/6/24.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MineTwoMixView;
@interface MineTwoButtonCollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) MineTwoMixView *mixView;
@end
