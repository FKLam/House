//
//  ReuseRandomCodeTableViewCell.h
//  IndexMoveHousePage
//
//  Created by 张垚 on 16/6/8.
//  Copyright © 2016年 众杰网络. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LabelAndTextFieldView;
@interface ReuseRandomCodeTableViewCell : UITableViewCell

@property (nonatomic, strong) LabelAndTextFieldView *inputView;
@property (nonatomic, strong) UIButton *getRandomCodeButton;


@end
