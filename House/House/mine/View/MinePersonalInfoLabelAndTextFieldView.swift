//
//  MinePersonalInfoLabelAndTextFieldView.swift
//  House
//
//  Created by 吕品 on 16/7/6.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

import UIKit

class MinePersonalInfoLabelAndTextFieldView: UIView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */



    var label = UILabel()
    var textField = UITextField.init()
    var imageView = UIImageView()
    var showTextField: Bool{
        set {
            if newValue {
//                imageView.hidden = true
                textField.snp.makeConstraints({ (make) in
                    make.left.equalTo(label.snp.right)
                    make.right.top.bottom.equalTo(self)
                })
            } else {
                textField.isHidden = true
                imageView.snp.makeConstraints({ (make) in
                    make.bottom.equalTo(self).offset(-10)
                    make.right.equalTo(self)
                    make.top.equalTo(self).offset(10)
                    make.width.equalTo(80 - 20)
                })
            }
            self.setNeedsLayout()
        }
        get {
            return self.showTextField
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(textField)
        textField.isEnabled = false
        textField.textAlignment = .right
        self.addSubview(label)
        self.addSubview(imageView)
        label.snp.makeConstraints { (make) in
            make.top.left.bottom.equalTo(self)
            make.width.equalTo(self).multipliedBy(0.6)
        }
        textField.adjustsFontSizeToFitWidth = true
//        imageView.backgroundColor = UIColor.blackColor()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
