//
//  LocationManager.h
//  House
//
//  Created by 吕品 on 16/6/3.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CLLocation;
@class AMapLocationReGeocode;
typedef void (^AMapLocatingCompletionBlock)(CLLocation *location, AMapLocationReGeocode *regeocode, NSError *error);
@interface LocationManager : NSObject
+ (LocationManager *)manager;
- (void)requestLocationWithBlock:(AMapLocatingCompletionBlock)block;
+ (void)regAMapKey;
@end
