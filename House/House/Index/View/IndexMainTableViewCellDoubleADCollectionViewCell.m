//
//  IndexMainTableViewCellDoubleADCollectionViewCell.m
//  House
//
//  Created by 吕品 on 16/6/7.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "IndexMainTableViewCellDoubleADCollectionViewCell.h"
@interface IndexMainTableViewCellDoubleADCollectionViewCell ()
@end
@implementation IndexMainTableViewCellDoubleADCollectionViewCell
#pragma mark init
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.headImageView = [UIImageView new];
        [self.contentView addSubview:_headImageView];
        _headImageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return self;
}
- (void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    [super applyLayoutAttributes:layoutAttributes];
    _headImageView.frame = self.contentView.frame;
}

@end
