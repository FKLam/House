//
//  ZJRecyclePicView.m
//  House
//
//  Created by 吕品 on 16/5/31.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "ZJRecyclePicView.h"
#import "UIImageView+AFNetworking.h"
@interface ZJRecyclePicView ()<UIScrollViewDelegate>
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray<UIImageView *>*imageViewArray;//装有imageView的数组
@property (nonatomic, strong) UIPageControl *pageControl;
@property (nonatomic, strong) NSTimer *timer;
@end

@implementation ZJRecyclePicView

- (instancetype)init
{
    self = [super init];
    if (self) {
        KMethodUseError;
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        KMethodUseError;
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.scrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
        [self addSubview:_scrollView];
        _scrollView.delegate = self;
        _scrollView.pagingEnabled = YES;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
    }
    return self;
}
- (void)recyclePic {
    const NSInteger count = _scrollView.subviews.count;
    static BOOL b = YES;
    static NSInteger i = 0;
    [_scrollView setContentOffset:CGPointMake(i * self.bounds.size.width, 0) animated:b];
    _pageControl.currentPage = i;
    i ++;
    if (i < count) {
        b = YES;
        return ;
    } else {
        i = 0;
        b = NO;
    }
}
- (void)createImageViewsWithArray:(NSArray *)array {
    self.imageViewArray = [NSMutableArray array];
    _scrollView.contentSize = CGSizeMake(self.bounds.size.width * array.count, 0);
    [_scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    for (NSInteger i = 0; i < array.count; i ++) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.bounds.size.width * i, 0, self.bounds.size.width, self.bounds.size.height)];
        imageView.userInteractionEnabled = YES;
        imageView.contentMode = UIViewContentModeScaleToFill;
        [_scrollView addSubview:imageView];
        imageView.tag = tagManager + i;
        [_imageViewArray addObject:imageView];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(beTouched:)];
        [imageView addGestureRecognizer:tap];
    }
    [self createPageControlWithCount:array.count];
    if (array.count > 1) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            self.timer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(recyclePic) userInfo:nil repeats:YES];
            [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
        });
    }
}

- (void)createPageControlWithCount:(NSInteger)count {
    self.pageControl = [[UIPageControl alloc] init];
    [self addSubview:_pageControl];
    _pageControl.numberOfPages = count;
    _pageControl.hidesForSinglePage = YES;
    [self bringSubviewToFront:_pageControl];

    _pageControl.currentPageIndicatorTintColor = [UIColor redColor];
    _pageControl.pageIndicatorTintColor = [UIColor grayColor];
}

- (void)setImageArray:(NSArray<UIImage *> *)imageArray {
    [self createImageViewsWithArray:imageArray];
    for (NSInteger i = 0; i < imageArray.count; i ++) {
        _imageViewArray[i].image = imageArray[i];
    }
}

- (void)setImageUrlArray:(NSArray<NSString *> *)imageUrlArray {
    [self createImageViewsWithArray:imageUrlArray];
    for (NSInteger i = 0; i < imageUrlArray.count; i ++) {
        [_imageViewArray[i] setImageWithURL:[NSURL URLWithString:imageUrlArray[i]] placeholderImage:_placeImage];
    }
}

- (void)beTouched:(UITapGestureRecognizer *)tap {
    NSInteger index = tap.view.tag - tagManager;
    [self.delegate didSelectedPicAtIndex:index];
}
- (void)layoutSubviews {
    [super layoutSubviews];
    CGFloat topPadding = 2;
    UIView *superView = self;
    [_pageControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(superView.mas_centerX);
        make.bottom.mas_equalTo(superView.mas_bottom).with.offset(-topPadding);
        make.size.mas_equalTo([_pageControl sizeForNumberOfPages:_pageControl.numberOfPages]);
    }];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    _pageControl.currentPage = scrollView.contentOffset.x / self.bounds.size.width;
}
@end
