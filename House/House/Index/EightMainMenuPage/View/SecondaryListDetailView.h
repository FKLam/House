//
//  SecondaryListDetailView.h
//  House
//
//  Created by 张垚 on 16/6/30.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ImageViewAndLabelButton;

/* 代理协议 将三个按钮点击方法传出去 */
@protocol SecondaryListDetailViewDelegate <NSObject>

- (void)passButton:(ImageViewAndLabelButton *)button;

@end

@interface SecondaryListDetailView : UIView

/* 标签视图 三个按钮视图 */
@property (nonatomic, strong) UIStackView *buttonStackView;
@property (nonatomic, strong) UIStackView *tagLabelStackView;
/* 重写初始化方法*/
- (instancetype)initWithFrame:(CGRect)frame WithTitleArr:(NSArray *)titleArr WithImageArr:(NSArray *)imageArr WithColorArr:(NSArray *)colorArr WithButtonNum:(NSUInteger)buttonNum;
/* 协议属性 */
@property (nonatomic, assign) id<SecondaryListDetailViewDelegate>delegate;

/* 详情属性 */
@property (nonatomic, strong) UIImageView *roomImageView;
@property (nonatomic, strong) UILabel *tilteLabel;
@property (nonatomic, strong) UILabel *doorModelLabels;
@property (nonatomic, strong) UILabel *communityLabels;

//四个标签
@property (nonatomic, strong) UILabel *tagLabel1;
@property (nonatomic, strong) UILabel *tagLabel2;
@property (nonatomic, strong) UILabel *tagLabel3;
@property (nonatomic, strong) UILabel *tagLabel4;
//右侧视图 
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UILabel *areaSizeLabel;
@property (nonatomic, strong) NSArray *tagTitleArr;

@property (nonatomic, copy) NSString *tagStr;

@end
