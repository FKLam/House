//
//  HouseEvaluateTableViewCell.m
//  House
//
//  Created by 张垚 on 16/7/14.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "HouseEvaluateTableViewCell.h"
#import "UnfoldModel.h"
#import "UnfoldFrameModel.h"

@interface HouseEvaluateTableViewCell ()
@property (nonatomic, strong) UILabel *agentEvaluateLabel;

@property (nonatomic, strong) UIImageView *agentImgaeView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *yearLabel;
@property (nonatomic, strong) UILabel *storeLabel;
@property (nonatomic, strong) UIButton *callButton;
@property (nonatomic, strong) UILabel *contextLabel;
@property (nonatomic, strong) UIButton *houseEvaluateButton;
@property (nonatomic, strong) UIImageView *adImageView;

@end




@implementation HouseEvaluateTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _agentEvaluateLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_agentEvaluateLabel];
        _adImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"trusteeshipImage"]];
        [self.contentView addSubview:_adImageView];
        _agentImgaeView = [[UIImageView alloc] init];
        [self.contentView addSubview:_agentImgaeView];
        _nameLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_nameLabel];
        _yearLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_yearLabel];
        _storeLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_storeLabel];
        _callButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_callButton addTarget:self action:@selector(callPhone:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_callButton];
        _contextLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_contextLabel];
        _contextLabel.numberOfLines = 0;
        _houseEvaluateButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _houseEvaluateButton.layer.borderColor = [UIColor colorWithRed:0.4 green:0.8 blue:1.0 alpha:1.0].CGColor;
        _houseEvaluateButton.layer.borderWidth = 1.0f;
        [self.contentView addSubview:_houseEvaluateButton];
        _houseEvaluateButton.layer.cornerRadius = 4;
        _houseEvaluateButton.layer.masksToBounds = YES;
        [_houseEvaluateButton setTitleColor:[UIColor colorWithRed:0.4 green:0.8 blue:1.0 alpha:1.0] forState:UIControlStateNormal];
        [_houseEvaluateButton addTarget:self action:@selector(unflodCell) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}
-(void)unflodCell
{
    if ([self.delegate respondsToSelector:@selector(UnfoldCellDidClickUnfoldBtn:)]) {
        [self.delegate UnfoldCellDidClickUnfoldBtn:self.frameModel];
    }
}
- (void)callPhone:(UIButton *)button {
    
    [self.delegate callAgentPhoneNumber];
}

-(void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self.contentView;
    [_agentEvaluateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(15);
        make.top.equalTo(superView.mas_top).offset(10);
        make.height.equalTo(@20);
        make.width.equalTo(@100);
    }];
    _agentEvaluateLabel.text = @"经纪人房评";
    [_agentImgaeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(15);
        make.top.equalTo(_agentEvaluateLabel.mas_bottom).offset(10);
        make.width.equalTo(@60);
        make.height.equalTo(@90);
    }];
    _agentImgaeView.layer.cornerRadius = 6;
    _agentImgaeView.layer.masksToBounds = YES;
    [_adImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(superView.mas_right).offset(-20);
        make.top.equalTo(superView.mas_top).offset(5);
        make.height.equalTo(@30);
        make.width.equalTo(@180);
    }];
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_agentImgaeView.mas_right).offset(5);
        make.top.equalTo(_agentImgaeView.mas_top).offset(10);
        make.height.equalTo(@20);
        make.right.equalTo(_callButton.mas_left).offset(-10);
    }];
    [_yearLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_agentImgaeView.mas_right).offset(5);
        make.top.equalTo(_nameLabel.mas_bottom).offset(5);
        make.height.equalTo(@20);
        make.right.equalTo(_callButton.mas_left).offset(-10);
    }];
    _yearLabel.font = [UIFont systemFontOfSize:15];
    [_storeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_agentImgaeView.mas_right).offset(5);
        make.top.equalTo(_yearLabel.mas_bottom).offset(5);
        make.height.equalTo(@20);
        make.right.equalTo(_callButton.mas_left).offset(-10);
    }];
    _storeLabel.font = [UIFont systemFontOfSize:15];
    [_callButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(superView.mas_right).offset(-10);
        make.centerY.equalTo(_agentImgaeView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];
    [_callButton setImage:[UIImage imageNamed:@"phone"] forState:UIControlStateNormal];
    
}

-(void)setFrameModel:(UnfoldFrameModel *)frameModel
{
    _frameModel = frameModel;
    
    UnfoldModel *model = frameModel.model;
    if (model.isFold) {
        [self.houseEvaluateButton setTitle:@"隐藏" forState:UIControlStateNormal];
    }else{
        [self.houseEvaluateButton setTitle:@"查看房评" forState:UIControlStateNormal];
    }
    self.contextLabel.frame = frameModel.contextFrame;
    self.houseEvaluateButton.frame = frameModel.btnFrame;
    self.contextLabel.text = model.contextStr;
}

- (void)setRemarkDic:(NSDictionary *)remarkDic {
    if (remarkDic != _remarkDic) {
        _remarkDic = remarkDic;
    }
    [_agentImgaeView setImageWithURL:[NSURL URLWithString:[remarkDic[@"thumb"] description]]  placeholderImage:[UIImage imageNamed:@"placeHolderPic"]];
    _nameLabel.text = [remarkDic[@"truename"] description];
    _yearLabel.text = [[NSString stringWithFormat:@"从业年限:%@年", remarkDic[@"career"]] description];
    _storeLabel.text = [[NSString stringWithFormat:@"所属门店:%@", remarkDic[@"mendian"]] description];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
