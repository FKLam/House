//
//  AreaAndTypeChooseViewController.m
//  House
//
//  Created by 张垚 on 16/8/12.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "AreaAndTypeChooseViewController.h"
#import "BaseTitleNavigationBar.h"

@interface AreaAndTypeChooseViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) BaseTitleNavigationBar *myBar;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *dataArr;
@property (nonatomic, strong) NSArray *submitArr;
@end

static NSString *cellID = @"AreaAndTypeChooseViewControllerID";
@implementation AreaAndTypeChooseViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = YES;
}

#pragma mark - 主程序入口
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initNavigationBar];
    //本地数据
    if (_tag == 100) {
        //取出区域
//        YTKKeyValueStore *areaStore = [[YTKKeyValueStore alloc] initDBWithName:dataBaseName];
//        _dataArr = [areaStore getObjectById:[[NSUserDefaults standardUserDefaults] objectForKey:cityID] fromTable:tableName];
        [self handleData];
        
    }else if (_tag == 1) {
        _dataArr = @[@"0-50元", @"50-100元", @"100-150元", @"150-200元", @"200-250元", @"250-300元", @"300元以上"];
        _submitArr = @[@"不限",@"0,50", @"50,100", @"100,150", @"150,200", @"200,250", @"250,300", @"300"];
    }
    else if (_tag == 2) {
        _dataArr = @[@"日租", @"月租", @"钟点房"];
    }else if (_tag == 3) {
        _dataArr = @[@"单人房", @"大床房", @"双床房", @"标准间", @"三人间"];
    }
    else {
       _dataArr = @[@"一居", @"两居", @"三居", @"四居", @"五居", @"六居", @"六居以上"];
    }
    [self initTableView];
    
    // Do any additional setup after loading the view.
}

//区域数据获取
- (void)handleData {
    _dataArr = [NSArray array];
    [NetRequest getInformationWithGetMethodWithUrl:@"http://aizufang.hrbzjwl.com/tp/api.php/Index/qu" success:^(NSURLSessionDataTask *task, id responseObject) {
            _dataArr = responseObject;
            [_tableView reloadData];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(@"%@", error);
        }];

}


#pragma mark - 顶部视图
- (void)initNavigationBar {
    _myBar = [[BaseTitleNavigationBar alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 64) WithTitleStr:@"区域选择"];
    [self.view addSubview:_myBar];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(back)];
    [_myBar.leftView addGestureRecognizer:tap];
}

//返回上一界面
- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
}

//私有方法
- (void)passInfoToPreviousPage:(chooseInfoBlock)infoBlock {
    _chooseInfoBlock = infoBlock;
}

- (void)passSubmitInfoToPreviousPage:(chooseSubmitInfoBlock)infoBlock {
    _chooseSubmitInfoBlock = infoBlock;
}

#pragma mark - tableview
//创建
- (void)initTableView {
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, kScreenWidth, kScreenHeight - 64) style:UITableViewStylePlain];
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _tableView.scrollEnabled = NO;
    [self.view addSubview:_tableView];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
}
//行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   return _dataArr.count + 1;
}


//cell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellID];
    }
    if (indexPath.row == 0) {
        cell.textLabel.text = @"不限";
    }else {
        if (_tag == 100) {
            NSDictionary *dic = _dataArr[indexPath.row - 1];
            cell.textLabel.text = dic[@"areaname"];
                   }
        else {
            cell.textLabel.text = _dataArr[indexPath.row - 1];
        }
        if ([_currentStr isEqualToString:cell.textLabel.text]) {
            cell.textLabel.textColor = KNaviBackColor;
        }
    }
    cell.tag = indexPath.row;
    return cell;
}
//行数
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ((kScreenHeight - 64) / (_dataArr.count + 1) >= 44) {
        return 44;
    }else {
        return (kScreenHeight - 64) / (_dataArr.count + 1);
    }
}
//cell点击事件
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (_tag == 1) {
        _chooseSubmitInfoBlock(cell.textLabel.text, _submitArr[indexPath.row]);
    }else {
        _chooseInfoBlock(cell.textLabel.text, cell.tag);
    }
    [self back];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
