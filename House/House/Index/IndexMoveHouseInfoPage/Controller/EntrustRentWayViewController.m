//
//  EntrustRentWayViewController.m
//  House
//
//  Created by 张垚 on 16/7/25.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "EntrustRentWayViewController.h"
#import "BaseTitleNavigationBar.h"

@interface EntrustRentWayViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) BaseTitleNavigationBar *myBar;
@end

@implementation EntrustRentWayViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = YES;
}

#pragma mark - 主程序入口
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    //自定义导航栏
    _myBar = [[BaseTitleNavigationBar alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 64) WithTitleStr:@"支付方式"];
    [self.view addSubview:_myBar];
    //添加手势
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(back)];
    [_myBar.leftView addGestureRecognizer:tap];
    [self initTableView];
      // Do any additional setup after loading the view.
}
//返回上一层级
- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
}

/* block传值 */
- (void)passInfoToPreviousPage:(chooseInfoStrBlock)infoBlock {
    self.chooseInfoBlock = infoBlock;
}


#pragma mark - tableview
/* 创建 */
- (void)initTableView {
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, kScreenWidth, _wayArr.count * 44) style:UITableViewStylePlain];
    _tableView.scrollEnabled = NO;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
}
//行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _wayArr.count;
}

/* cell */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *wayCellID = @"wayCellID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:wayCellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:wayCellID];
    }
    NSDictionary *dic = _wayArr[indexPath.row];
    cell.textLabel.text = dic[@"method"];
    //判断之前选择的方式 标注为红色
    if ([cell.textLabel.text isEqualToString:_currentStr]) {
        cell.textLabel.textColor = KNaviBackColor;
    }
    return cell;
}
//cell点击事件
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    _chooseInfoBlock(cell.textLabel.text);
    [self back];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
