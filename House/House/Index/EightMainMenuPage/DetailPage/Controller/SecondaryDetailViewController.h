//
//  SecondaryDetailViewController.h
//  House
//
//  Created by 张垚 on 16/7/12.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "BaseViewController.h"

@interface SecondaryDetailViewController : BaseViewController

@property (nonatomic, copy) NSString *itemID;

@end
