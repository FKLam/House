//
//  SubmitMenuManager.m
//  House
//
//  Created by 张垚 on 16/7/25.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "SubmitMenuManager.h"
#import "MoveHousePushTableViewCell.h"
#import "BudgetRageChooseTableViewCell.h"
#import "LabelAndTextFieldView.h"
#import "HouseTypeDescriptionTableViewCell.h"
#import "ReuseRandomCodeTableViewCell.h"
#import "MoveHouseDescriptionTableViewCell.h"
#import "RentHouseRoomFillTableViewCell.h"
#import "MultiLabelAndMultiTextfiledView.h"
#import "ReuseCategoryChooseTableViewCell.h"
#import "UserInfoManager.h"


@implementation SubmitMenuManager
//买房、租房委托
+ (NSDictionary *)submitTableView:(UITableView *)tableView WithKeyArr:(NSArray *)KeyArr {
    NSMutableArray *infoArr = [NSMutableArray array];
    NSMutableDictionary *submitDic = [NSMutableDictionary dictionaryWithCapacity:0];
    [submitDic setObject:[UserInfoManager sharedManager].uid forKey:KeyArr[0]];
    for (UITableViewCell *cell in tableView.visibleCells) {
        if ([cell isKindOfClass:[MoveHousePushTableViewCell class]]) {
            MoveHousePushTableViewCell *moveCell = (MoveHousePushTableViewCell *)cell;
            if ([moveCell.inputView.titleLabel.text isEqualToString:@"方式"] || [moveCell.inputView.titleLabel.text isEqualToString:@"姓名"] || [moveCell.inputView.titleLabel.text isEqualToString:@"手机"]) {
               [infoArr addObject:moveCell.inputView.contextTextField.text];
            }else {
                [infoArr addObject:[NSString stringWithFormat:@"%ld", (long)moveCell.tag]];
            }
        }
        else {
            BudgetRageChooseTableViewCell *budgetCell = (BudgetRageChooseTableViewCell *)cell;
            [infoArr addObject:budgetCell.minPriceTextfield.text];
            [infoArr addObject:budgetCell.maxPriceTextfiled.text];
        }
    }
    for (int i = 1; i <= infoArr.count; i++) {
        [submitDic setValue:infoArr[i - 1] forKey:KeyArr[i]];
    }
    return submitDic;
}
//装修
+ (NSDictionary *)submitDecorateTableView:(UITableView *)tableView WithKeyArr:(NSArray *)KeyArr {
    NSMutableArray *infoArr = [NSMutableArray array];
    NSMutableDictionary *submitDic = [NSMutableDictionary dictionaryWithCapacity:0];
    [submitDic setObject:[UserInfoManager sharedManager].uid forKey:KeyArr[0]];
    for (UITableViewCell *cell in tableView.visibleCells) {
        if ([cell isKindOfClass:[MoveHousePushTableViewCell class]]) {
            MoveHousePushTableViewCell *moveCell = (MoveHousePushTableViewCell *)cell;
            if ([moveCell.inputView.titleLabel.text isEqualToString:@"联系人"] || [moveCell.inputView.titleLabel.text isEqualToString:@"详细地址"] || [moveCell.inputView.titleLabel.text isEqualToString:@"房屋面积"] || [moveCell.inputView.titleLabel.text isEqualToString:@"手机号"]) {
                [infoArr addObject:moveCell.inputView.contextTextField.text];
            }else {
                [infoArr addObject:[NSString stringWithFormat:@"%ld", (long)moveCell.tag]];
            }
        }else if ([cell isKindOfClass:[HouseTypeDescriptionTableViewCell class]]) {
            HouseTypeDescriptionTableViewCell *budgetCell = (HouseTypeDescriptionTableViewCell *)cell;
            [infoArr addObject:budgetCell.minPriceTextfield.text];
            [infoArr addObject:budgetCell.maxPriceTextfiled.text];
        }else if ([cell isKindOfClass:[ReuseRandomCodeTableViewCell class]]) {
            
        }else {
            BudgetRageChooseTableViewCell *budgetCell = (BudgetRageChooseTableViewCell *)cell;
            [infoArr addObject:budgetCell.minPriceTextfield.text];
            [infoArr addObject:budgetCell.maxPriceTextfiled.text];
        }
    }
    for (int i = 1; i <= infoArr.count; i++) {
        [submitDic setValue:infoArr[i - 1] forKey:KeyArr[i]];
    }
    return submitDic;
}
//保洁
+(NSDictionary *)submitCleanHouseTableView:(UITableView *)tableView WithKeyArr:(NSArray *)KeyArr {
    NSMutableArray *infoArr = [NSMutableArray array];
    NSMutableDictionary *submitDic = [NSMutableDictionary dictionaryWithCapacity:0];
    [submitDic setObject:[UserInfoManager sharedManager].uid forKey:KeyArr[0]];
    for (UITableViewCell *cell in tableView.visibleCells) {
        if ([cell isKindOfClass:[MoveHousePushTableViewCell class]]) {
            MoveHousePushTableViewCell *moveCell = (MoveHousePushTableViewCell *)cell;
            if ([moveCell.inputView.titleLabel.text isEqualToString:@"服务区域"] || [moveCell.inputView.titleLabel.text isEqualToString:@"服务项目"]) {
                [infoArr addObject:[NSString stringWithFormat:@"%ld", (long)moveCell.tag]];
            }else {
                 [infoArr addObject:moveCell.inputView.contextTextField.text];
            }
        }
        else {
            
        }
    }
    for (int i = 1; i <= infoArr.count; i++) {
        [submitDic setValue:infoArr[i - 1] forKey:KeyArr[i]];
    }
    return submitDic;
}
//搬家
+(NSDictionary *)submitMoveHouseTableView:(UITableView *)tableView WithKeyArr:(NSArray *)KeyArr WithCellNumber:(NSInteger)cellNumber {
    NSMutableArray *infoArr = [NSMutableArray array];
    NSMutableDictionary *submitDic = [NSMutableDictionary dictionaryWithCapacity:0];
    [submitDic setObject:[UserInfoManager sharedManager].uid forKey:KeyArr[0]];
    for (UITableViewCell *cell in tableView.visibleCells) {
        if ([cell isKindOfClass:[MoveHousePushTableViewCell class]]) {
            MoveHousePushTableViewCell *moveCell = (MoveHousePushTableViewCell *)cell;
            if ([moveCell.inputView.titleLabel.text isEqualToString:@"服务区域"] || [moveCell.inputView.titleLabel.text isEqualToString:@"类别"]) {
                [infoArr addObject:[NSString stringWithFormat:@"%ld", (long)moveCell.tag]];
            }else {
                [infoArr addObject:moveCell.inputView.contextTextField.text];
            }
        }
        else {
            MoveHouseDescriptionTableViewCell *desCell = (MoveHouseDescriptionTableViewCell *)cell;
            [infoArr addObject:desCell.mainTextView.text];
        }
    }
    for (int i = 1; i <= infoArr.count; i++) {
        [submitDic setValue:infoArr[i - 1] forKey:KeyArr[i]];
    }
    return submitDic;
}


//酒店公寓提交订单
+(NSDictionary *)submitHotelTableView:(UITableView *)tableView WithKeyArr:(NSArray *)KeyArr WithHotelId:(NSString *)HotelID {
    NSMutableArray *infoArr = [NSMutableArray array];
    NSMutableDictionary *submitDic = [NSMutableDictionary dictionaryWithCapacity:0];
    [submitDic setObject:[UserInfoManager sharedManager].uid forKey:KeyArr[0]];
    [submitDic setObject:HotelID forKey:KeyArr[1]];
    for (UITableViewCell *cell in tableView.visibleCells) {
            MoveHousePushTableViewCell *moveCell = (MoveHousePushTableViewCell *)cell;
                            [infoArr addObject:moveCell.inputView.contextTextField.text];
    }
    for (int i = 2; i <= infoArr.count + 1; i++) {
        [submitDic setValue:infoArr[i - 2] forKey:KeyArr[i]];
    }
    return submitDic;
}


@end
