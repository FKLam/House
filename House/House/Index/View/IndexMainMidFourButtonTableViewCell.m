//
//  IndexMainMidFourButtonTableViewCell.m
//  House
//
//  Created by 张垚 on 16/10/6.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "IndexMainMidFourButtonTableViewCell.h"

@implementation LogoAndTextButton

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _logoImage = [[UIImageView alloc] init];
        [self addSubview:_logoImage];
        _logoNameLabel = [[UILabel alloc] init];
        [self addSubview:_logoNameLabel];
    }
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self;
    [_logoImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.centerX.equalTo(superView.mas_centerX);
        make.bottom.equalTo(superView.mas_centerY);
    }];
    [_logoNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(60, 20));
        make.centerX.equalTo(superView.mas_centerX);
        make.top.equalTo(superView.mas_centerY).offset(10);
    }];
    _logoNameLabel.font = [UIFont systemFontOfSize:15];
    _logoNameLabel.textAlignment = NSTextAlignmentCenter;
}

@end

@implementation IndexMainMidFourButtonTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *titleArr = @[@"搬家", @"保洁", @"装修", @"租房贷"];
        NSArray *imageArr = @[[UIImage imageNamed:@"moveHome-0"], [UIImage imageNamed:@"moveHome-1"], [UIImage imageNamed:@"moveHome-2"], [UIImage imageNamed:@"moveHome-3"]];
        for (int i = 0; i < 4; i++) {
            LogoAndTextButton *button = [[LogoAndTextButton alloc] initWithFrame:CGRectMake(kScreenWidth / 4 * i, 0, kScreenWidth / 4, 80)];
            [self.contentView addSubview:button];
            button.logoNameLabel.text = titleArr[i];
            button.logoImage.image = imageArr[i];
            button.tag = i + 100;
            [button addTarget:self action:@selector(action:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    return self;
}

- (void)action:(LogoAndTextButton *)button {
    [self.delegate IndexMainMidFourButtonTableViewCellDidSelectCurrentButtonTag:button.tag];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
