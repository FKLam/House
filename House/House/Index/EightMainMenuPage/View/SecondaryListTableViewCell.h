//
//  SecondaryListTableViewCell.h
//  House
//
//  Created by 张垚 on 16/6/29.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SecondaryListDetailView.h"
@class SecondaryListTableViewCell;
@class ImageViewAndLabelButton;
@class SecondaryListDetailView;
@class SecondaryListModel;

@protocol SecondaryListTableViewCellDelegate <NSObject>

//两个手势 代理方法
- (void)leftSwipe:(SecondaryListTableViewCell *)obj;
- (void)rightSwipe:(SecondaryListTableViewCell *)obj;
- (void)passButton:(ImageViewAndLabelButton *)button;
@end

@interface SecondaryListTableViewCell : UITableViewCell

@property (nonatomic, assign) id <SecondaryListTableViewCellDelegate> delegate;
//详情视图属性
@property (nonatomic, strong) SecondaryListDetailView *myVIew;
@property (nonatomic, strong) SecondaryListModel *model;

@end
