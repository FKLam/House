//
//  ZJRecyclePicView.h
//  House
//
//  Created by 吕品 on 16/5/31.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZJRecyclePicDelegate <NSObject>
@optional
- (void)didSelectedPicAtIndex:(NSInteger)index;

@end
@interface ZJRecyclePicView : UIView
@property (nonatomic, copy) NSArray<UIImage *> *imageArray;
@property (nonatomic, copy) NSArray<NSString *>*imageUrlArray;
@property (nonatomic, strong) UIImage *placeImage;
@property (nonatomic, assign) id<ZJRecyclePicDelegate>delegate;
@end
