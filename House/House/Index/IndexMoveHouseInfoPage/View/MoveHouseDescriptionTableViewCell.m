//
//  MoveHouseDescriptionTableViewCell.m
//  House
//
//  Created by 张垚 on 16/8/8.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "MoveHouseDescriptionTableViewCell.h"

@implementation MoveHouseDescriptionTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _titleLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_titleLabel];
        _mainTextView = [[UITextView alloc] init];
        [self.contentView addSubview:_mainTextView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self.contentView;
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(10);
        make.left.equalTo(superView.mas_left).offset(5);
        make.height.equalTo(@20);
        make.width.equalTo(@60);
    }];
    _titleLabel.font = [UIFont systemFontOfSize:15];
    [_mainTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(3);
        make.left.equalTo(_titleLabel.mas_right);
        make.right.equalTo(superView.mas_right).offset(-10);
        make.bottom.equalTo(superView.mas_bottom).offset(-5);
    }];
    _mainTextView.font = [UIFont systemFontOfSize:15];
    _mainTextView.keyboardType = UIKeyboardTypeDefault;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
