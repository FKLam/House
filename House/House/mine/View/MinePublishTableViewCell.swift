//
//  MinePublishTableViewCell.swift
//  House
//
//  Created by 吕品 on 2016/9/22.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

import UIKit

class MinePublishTableViewCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource {
    public var tableView: UITableView!
    fileprivate var contentArray = [String]()
    internal var model: MinePublishModel {
        set {
            let bus = newValue.bus ?? newValue.fukuanfangshi
            let yusuan = (newValue.start?.description)! + "--" + (newValue.end?.description)! + "元"
            let room = (newValue.room?.description)! + "居室"
            let sex = newValue.sex?.description == "1" ? "男" : "女"
            self.contentArray = [newValue.areaname!, bus!, room, yusuan, newValue.truename!, sex, (newValue.telephone?.description)!, "aaaaa"]
            self.tableView.reloadData()
        }
        get {
            return self.model
        }
    }
    private let placeHolderArr = ["位置", "支付", "户型", "预算", "姓名", "性别", "手机"]
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        tableView = UITableView.init(frame: .init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 44.0 * CGFloat.init(placeHolderArr.count)), style: .plain)
        contentView.addSubview(tableView!)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isScrollEnabled = false
        tableView.register(MinePublishDetailTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(MinePublishDetailTableViewCell.self))
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contentArray.count
    }
    public func reloadData() -> Swift.Void {
        tableView.reloadData()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(MinePublishDetailTableViewCell.self)) as!
            MinePublishDetailTableViewCell
        cell.placeLabel.text = placeHolderArr[indexPath.row]
        cell.contentLabel.text = contentArray[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
