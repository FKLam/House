//
//  MySearchBar.m
//  House
//
//  Created by 张垚 on 16/6/27.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "MySearchBar.h"

@implementation MySearchBar


-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _imageView  = [[UIImageView alloc] init];
        [self addSubview:_imageView];
        _textFiled = [[UITextField alloc] init];
        [self addSubview:_textFiled];
        self.layer.cornerRadius = 4;
        self.layer.masksToBounds = YES;
        self.backgroundColor = [UIColor whiteColor];
       }
    return self;
}


-(void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self;
   [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
       make.left.equalTo(superView.mas_left).offset(3);
       make.centerY.equalTo(superView.mas_centerY);
       make.size.mas_equalTo(CGSizeMake(20, 20));
   }];
    [_textFiled mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_imageView.mas_right).offset(5);
        make.right.equalTo(superView.mas_right).offset(-5);
        make.height.equalTo(@30);
        make.centerY.equalTo(superView.mas_centerY);
    }];
    [_textFiled setReturnKeyType:UIReturnKeySearch];
    _textFiled.font = [UIFont systemFontOfSize:13];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
