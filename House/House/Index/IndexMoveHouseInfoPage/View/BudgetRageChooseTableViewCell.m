//
//  BudgetRageChooseTableViewCell.m
//  IndexMoveHousePage
//
//  Created by 张垚 on 16/6/17.
//  Copyright © 2016年 众杰网络. All rights reserved.
//

#import "BudgetRageChooseTableViewCell.h"

@implementation BudgetRageChooseTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _descriptionLabel = [[UILabel alloc] init];
        _descriptionLabel.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:_descriptionLabel];
        _minPriceTextfield = [[UITextField alloc] init];
        [self.contentView addSubview:_minPriceTextfield];
        _midView = [[UIView alloc] init];
        [self.contentView addSubview:_midView];
        _maxPriceTextfiled = [[UITextField alloc] init];
        [self.contentView addSubview:_maxPriceTextfiled];
        _unitLabel = [[UILabel alloc] init];
       [self.contentView addSubview:_unitLabel];
        
    }
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self.contentView;
    [_descriptionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(10);
        make.left.equalTo(superView.mas_left).offset(5);
        make.centerY.equalTo(superView.mas_centerY);
        make.width.equalTo(@60);
    }];
    _descriptionLabel.font = [UIFont systemFontOfSize:15];
    [_minPriceTextfield mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(10);
        make.left.equalTo(_descriptionLabel.mas_right).offset(5);
        make.centerY.equalTo(superView.mas_centerY);
        make.width.equalTo(@60);
    }];
    _minPriceTextfield.tag = 100;
    _minPriceTextfield.layer.cornerRadius = 4;
    _minPriceTextfield.layer.masksToBounds = YES;
    _minPriceTextfield.layer.borderColor = [UIColor colorWithRed:0.902 green:0.902 blue:0.902 alpha:1.0].CGColor;
    _minPriceTextfield.layer.borderWidth = 1.0;
    _minPriceTextfield.textAlignment = NSTextAlignmentCenter;
    [_midView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(superView.mas_centerY);
        make.left.equalTo(_minPriceTextfield.mas_right).offset(5);
        make.size.mas_equalTo(CGSizeMake(10, 2));
    }];
    _midView.backgroundColor = [UIColor colorWithRed:0.902 green:0.902 blue:0.902 alpha:1.0];
    [_maxPriceTextfiled mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(10);
        make.left.equalTo(_midView.mas_right).offset(5);
        make.centerY.equalTo(superView.mas_centerY);
        make.width.equalTo(_minPriceTextfield.mas_width);
    }];
    _maxPriceTextfiled.tag = 101;
    _maxPriceTextfiled.layer.cornerRadius = 4;
    _maxPriceTextfiled.layer.masksToBounds = YES;
    _maxPriceTextfiled.layer.borderColor = [UIColor colorWithRed:0.902 green:0.902 blue:0.902 alpha:1.0].CGColor;
    _maxPriceTextfiled.layer.borderWidth = 1.0;
    _maxPriceTextfiled.textAlignment = NSTextAlignmentCenter;
    [_unitLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(10);
        make.left.equalTo(_maxPriceTextfiled.mas_right).offset(5);
        make.centerY.equalTo(superView.mas_centerY);
        make.width.equalTo(@60);
    }];
    _unitLabel.textAlignment = NSTextAlignmentLeft;
    _unitLabel.textColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0];
}
-(void)setTypeTag:(NSInteger)typeTag {
    if (typeTag != _typeTag) {
        _typeTag = typeTag;
    }
    if (typeTag == 1) {
        _unitLabel.text = @"元";
    }else {
        _unitLabel.text = @"万元";
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
