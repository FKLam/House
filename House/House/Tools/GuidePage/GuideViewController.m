//
//  GuideViewController.m
//  dirve
//
//  Created by 众杰网络 on 16/4/1.
//  Copyright © 2016年 众杰网络. All rights reserved.
//

#import "GuideViewController.h"
#import "KYAnimatedPageControl.h"
@interface GuideViewController ()<UIScrollViewDelegate>
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) KYAnimatedPageControl *page;
@end

@implementation GuideViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    UIView *superView = self.view;
    [self.view addSubview:_scrollView];
    _scrollView.contentSize = CGSizeMake(kScreenWidth * 3, 0);
    _scrollView.pagingEnabled = YES;
    _scrollView.bounces = NO;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.delegate = self;
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenWidth, 0, kScreenWidth, kScreenHeight)];
    UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenWidth * 2, 0, kScreenWidth, kScreenHeight)];
    imageView3.userInteractionEnabled = YES;
    [imageView3 addSubview:button];
    [button setTitle:@"立即体验" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(intoMain)forControlEvents:UIControlEventTouchUpInside];
    button.backgroundColor = KNaviBackColor;
    [_scrollView addSubview:imageView1];
    [_scrollView addSubview:imageView2];
    [_scrollView addSubview:imageView3];

    imageView1.image = [UIImage imageNamed:@"guide1.jpg"];
    imageView2.image = [UIImage imageNamed:@"guide2.jpg"];
    imageView3.image = [UIImage imageNamed:@"guide3.jpg"];
    CGFloat padding = 40;
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(imageView3.mas_left).with.offset(padding);
        make.right.mas_equalTo(imageView3.mas_right).with.offset(-padding);
        make.bottom.mas_equalTo(imageView3.mas_bottom).with.offset(-padding);
        make.height.equalTo(@(padding));
    }];


    self.page = [[KYAnimatedPageControl alloc] initWithFrame:CGRectMake(padding, kScreenHeight - padding * 2 - 20, kScreenWidth - padding * 2, padding)];
    _page.pageCount = 3;
    _page.unSelectedColor = [UIColor whiteColor];
    _page.selectedColor = KNaviBackColor;
    _page.bindScrollView = _scrollView;
    _page.shouldShowProgressLine = YES;
    _page.indicatorStyle = IndicatorStyleGooeyCircle;
    _page.indicatorSize = 15;
    _page.swipeEnable = NO;
    [self.view addSubview:_page];
   [_page mas_makeConstraints:^(MASConstraintMaker *make) {
       make.left.mas_equalTo(superView.mas_left).with.offset(padding);
       make.right.mas_equalTo(superView.mas_right).with.offset(-padding);
       make.bottom.mas_equalTo(superView.mas_bottom).with.offset(-padding * 2 - 10);
       make.height.equalTo(@(padding));
   }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [_page.indicator animateIndicatorWithScrollView:scrollView andIndicator:_page];
    if (scrollView.dragging || scrollView.isDecelerating || scrollView.tracking)
    {
        [_page.pageControlLine animateSelectedLineWithScrollView:scrollView];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    self.page.indicator.lastContentOffset = scrollView.contentOffset.x;
}

-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    [self.page.indicator restoreAnimation:@(1.0/self.page.pageCount)];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    self.page.indicator.lastContentOffset = scrollView.contentOffset.x;
}

//调到主界面
- (void)intoMain
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"changeVC" object:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
