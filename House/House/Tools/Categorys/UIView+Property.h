//
//  UIView+Property.h
//  LPRecyclePictureScrollView
//
//  Created by 吕品 on 16/5/12.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Property)
@property (nonatomic, assign) CGSize size;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) CGFloat width;

@property (nonatomic, assign) CGPoint origin;
@property (nonatomic, assign) CGFloat x;
@property (nonatomic, assign) CGFloat y;

@property (nonatomic, assign) CGFloat center_x;
@property (nonatomic, assign) CGFloat center_y;
@end
