//
//  ImageViewAndTextFieldMixView.swift
//  House
//
//  Created by 吕品 on 16/7/1.
//  Copyright © 2016年 ZJWL. All rights reserved.
//


import UIKit
import SnapKit
class ImageViewAndTextFieldMixView: UIView, UITextFieldDelegate {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

    var isShowButton:Bool {
        get{
            return self.isShowButton
        }

        set{
            codeButton.isHidden = !newValue
        }
    }

    var imageView = UIImageView()
    var textField = UITextField()
    var codeButton = UIButton.init(type: .custom)

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    @discardableResult
    override init(frame: CGRect) {
        super.init(frame: frame)
        codeButton.backgroundColor = UIColor.orange
        textField.delegate = self
        textField.keyboardType = .default
        imageView.contentMode = .scaleAspectFit
        self.addSubview(imageView)
        self.addSubview(textField)
        self.addSubview(codeButton)

        imageView.snp.makeConstraints { (make) in
            make.left.equalTo(self)
            make.width.equalTo(self).multipliedBy(0.15)
            make.centerY.equalTo(self)
            make.height.equalTo(self).multipliedBy(0.4)
        }

        textField.snp.makeConstraints { (make) in
            make.centerY.equalTo(self)
            make.height.equalTo(self).multipliedBy(0.4)
            make.left.equalTo(imageView.snp.right)
            make.width.equalTo(self).multipliedBy(0.4)
        }
        codeButton.snp.makeConstraints { (make) in
            make.top.bottom.right.equalTo(self)
            make.left.equalTo(textField.snp.right).offset(10)
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
