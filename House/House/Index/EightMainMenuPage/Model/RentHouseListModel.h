//
//  RentHouseListModel.h
//  House
//
//  Created by 张垚 on 16/8/1.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "BaseModel.h"

@interface RentHouseListModel : BaseModel

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *areaid;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *fytsr;
@property (nonatomic, copy) NSString *houseearm;
@property (nonatomic, copy) NSString *itemid;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *thumb;
@property (nonatomic, copy) NSString *userid;
@property (nonatomic, copy) NSString *housename;
@property (nonatomic, copy) NSString *hall;
@property (nonatomic, copy) NSString *room;
@property (nonatomic, copy) NSString *rentmethod;
@property (nonatomic, copy) NSString *toward;

@end
