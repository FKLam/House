//
//  CustomPriceChooseTableViewCell.m
//  House
//
//  Created by 张垚 on 16/8/12.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "CustomPriceChooseTableViewCell.h"

@implementation CustomPriceChooseTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _descriptionLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_descriptionLabel];
        _minPriceTextfield = [[UITextField alloc] init];
        [self.contentView addSubview:_minPriceTextfield];
        _midView = [[UIView alloc] init];
        [self.contentView addSubview:_midView];
        _maxPriceTextfiled = [[UITextField alloc] init];
        [self.contentView addSubview:_maxPriceTextfiled];
        _unitLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_unitLabel];
        _sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.contentView addSubview:_sureButton];
        
    }
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self.contentView;
    [_descriptionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(10);
        make.left.equalTo(superView.mas_left).offset(15);
        make.centerY.equalTo(superView.mas_centerY);
        make.width.equalTo(@80);
    }];
    _descriptionLabel.text = @"自定义价格";
    _descriptionLabel.font = [UIFont systemFontOfSize:16];
    _descriptionLabel.textColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0];
    [_minPriceTextfield mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(10);
        make.left.equalTo(_descriptionLabel.mas_right).offset(5);
        make.centerY.equalTo(superView.mas_centerY);
        make.width.equalTo(@60);
    }];
    _minPriceTextfield.tag = 100;
    _minPriceTextfield.placeholder = @"最小";
    _minPriceTextfield.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [_minPriceTextfield setValue:[UIFont systemFontOfSize:15] forKeyPath:@"_placeholderLabel.font"];
    _minPriceTextfield.layer.cornerRadius = 4;
    _minPriceTextfield.layer.masksToBounds = YES;
    _minPriceTextfield.layer.borderColor = [UIColor colorWithRed:0.902 green:0.902 blue:0.902 alpha:1.0].CGColor;
    _minPriceTextfield.layer.borderWidth = 1.0;
    _minPriceTextfield.textAlignment = NSTextAlignmentCenter;
    [_midView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(superView.mas_centerY);
        make.left.equalTo(_minPriceTextfield.mas_right).offset(5);
        make.size.mas_equalTo(CGSizeMake(10, 2));
    }];
    _midView.backgroundColor = [UIColor colorWithRed:0.902 green:0.902 blue:0.902 alpha:1.0];
    [_maxPriceTextfiled mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(10);
        make.left.equalTo(_midView.mas_right).offset(5);
        make.centerY.equalTo(superView.mas_centerY);
        make.width.equalTo(_minPriceTextfield.mas_width);
    }];
    _maxPriceTextfiled.tag = 101;
    _maxPriceTextfiled.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    _maxPriceTextfiled.layer.cornerRadius = 4;
    _maxPriceTextfiled.placeholder = @"最大";
    [_maxPriceTextfiled setValue:[UIFont systemFontOfSize:15] forKeyPath:@"_placeholderLabel.font"];
    _maxPriceTextfiled.layer.masksToBounds = YES;
    _maxPriceTextfiled.layer.borderColor = [UIColor colorWithRed:0.902 green:0.902 blue:0.902 alpha:1.0].CGColor;
    _maxPriceTextfiled.layer.borderWidth = 1.0;
    _maxPriceTextfiled.textAlignment = NSTextAlignmentCenter;
    [_unitLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(10);
        make.left.equalTo(_maxPriceTextfiled.mas_right).offset(5);
        make.centerY.equalTo(superView.mas_centerY);
        make.width.equalTo(@60);
    }];
    _unitLabel.textAlignment = NSTextAlignmentLeft;
    _unitLabel.textColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0];
    
    [_sureButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(10);
        make.right.equalTo(superView.mas_right).offset(-10);
        make.centerY.equalTo(superView.mas_centerY);
        make.width.equalTo(@40);
    }];
    [_sureButton setTitle:@"确定" forState:UIControlStateNormal];
    [_sureButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
