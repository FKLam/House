//
//  UIButton+ImageTitleSpacing.h
//  House
//
//  Created by 张垚 on 16/8/23.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>

//UIButton知识点
//1.titleEdgeInsets是titleLabel相对于其上下左右的inset，跟tableView的contentInset是类似的；
//2.如果只有title，那titleLabel的上下左右都是相对于Button的；
//3.如果只有image，那imageView的上下左右都是相对于Button的；
//4.如果同时有image和label，那image的上下左是相对于Button的，右是相对于label 的；
//label的上下右是相对于Button的，左是相对于label的。
//原文链接：http://www.jianshu.com/p/3052a3b14a4e



typedef NS_ENUM(NSInteger, ZYButtonEdgeInsetStyle) {
    ZYButtonEdgeInsetStyleTop,//image在上 title在下
    ZYButtonEdgeInsetStyleLeft,//image在左 title在右
    ZYButtonEdgeInsetStyleBottom,//image在下 title在上
    ZYButtonEdgeInsetStyleRight//image在右 title在左
};

@interface UIButton (ImageTitleSpacing)

/**
 *  设置button的titleLabel和imageView的布局样式，及间距
 *
 *  @param style titleLabel和imageView的布局样式
 *  @param space titleLabel和imageView的间距
 */
- (void)layoutButtonWithEdgeInsetsStyle:(ZYButtonEdgeInsetStyle)style
                        imageTitleSpace:(CGFloat)space;

@end
