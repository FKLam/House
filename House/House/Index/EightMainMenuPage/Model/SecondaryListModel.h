//
//  SecondaryListModel.h
//  House
//
//  Created by 张垚 on 16/8/3.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "BaseModel.h"

@interface SecondaryListModel : BaseModel
//房源ID
@property (nonatomic, copy) NSString *itemid;
//区域id
@property (nonatomic, copy) NSString *areaid;
//地址
@property (nonatomic, copy) NSString *address;
//标题
@property (nonatomic, copy) NSString *title;
//建筑面积
@property (nonatomic, copy) NSString *houseearm;
//租金
@property (nonatomic, copy) NSString *price;
//配套设施
@property (nonatomic, copy) NSString *pei;
//图片地址
@property (nonatomic, copy) NSString *thumb;
//谁发布的
@property (nonatomic, copy) NSString *userid;
//标签
@property (nonatomic, copy) NSString *fyts;

@property (nonatomic, copy) NSString *toward;
@property (nonatomic, copy) NSString *room;
@property (nonatomic, copy) NSString *hall;
@property (nonatomic, copy) NSString *housename;


@end
