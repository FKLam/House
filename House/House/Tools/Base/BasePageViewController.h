//
//  BasePageViewController.h
//  House
//
//  Created by 张垚 on 16/6/27.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "BaseViewController.h"
@class BaseNavigationBar;

@interface BasePageViewController : BaseViewController
- (void)initNavigationBar;


@property (nonatomic, strong) BaseNavigationBar *myBar;
@end
