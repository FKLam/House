//
//  ReuseHouseDetailInfoTableViewCell.h
//  House
//
//  Created by 张垚 on 16/7/12.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RentHouseDetailModel;


@interface ReuseHouseDetailInfoTableViewCell : UITableViewCell
@property (nonatomic, strong) RentHouseDetailModel *model;
@end
