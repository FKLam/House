//
//  ActionCenterDetailViewController.h
//  House
//
//  Created by 张垚 on 16/9/9.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "BaseViewController.h"

@interface ActionCenterDetailViewController : BaseViewController

@property (nonatomic, copy) NSString *titleStr;
@property (nonatomic, copy) NSString *urlStr;

@end
