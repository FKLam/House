//
//  MoveHouseAreaChooseViewController.m
//  IndexMoveHousePage
//
//  Created by 张垚 on 16/6/8.
//  Copyright © 2016年 众杰网络. All rights reserved.
//

#import "MoveHouseAreaChooseViewController.h"
#import "BaseTitleNavigationBar.h"


@interface MoveHouseAreaChooseViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray *infoArr;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) BaseTitleNavigationBar *myBar;
@end

@implementation MoveHouseAreaChooseViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = YES;
}

#pragma mark - 主程序入口
- (void)viewDidLoad {
    [super viewDidLoad];
     self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = [UIColor whiteColor];
    _infoArr = [NSMutableArray array];
    /* 根据传入的标题来判断显示的数据 */
    if ([_categoryStr isEqualToString:@"区域选择"]) {
        [self handleData];
    }else if ([_categoryStr isEqualToString:@"装修类型"]) {
         [self handleTypeData];
    }else if ([_categoryStr isEqualToString:@"类别"]) {
        [self moveHouseTypeData];
    }
    else {
        [self getCleanHouseServiceItemData];
    }
    [self initTableView];
    [self initNavigationBar];
   
    // Do any additional setup after loading the view.
}
#pragma mark - 请求数据部分

- (void)handleData {
    [NetRequest getInformationWithGetMethodWithUrl:@"http://aizufang.hrbzjwl.com/tp/api.php/Index/qu" success:^(NSURLSessionDataTask *task, id responseObject) {
        _infoArr = responseObject;
        [_tableView reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@", error);
    }];
    
}
//搬家类别数据
- (void)moveHouseTypeData {
    [NetRequest getInformationWithGetMethodWithUrl:@"http://aizufang.hrbzjwl.com/tp/api.php/Iosapp/banjia" success:^(NSURLSessionDataTask *task, id responseObject) {
        _infoArr = responseObject[@"move"];
        [_tableView reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@", error);
    }];
}
//保洁服务项目数据
- (void)getCleanHouseServiceItemData {
    [NetRequest getInformationWithGetMethodWithUrl:@"http://aizufang.hrbzjwl.com/tp/api.php/Index/App_baojie_fwxm" success:^(NSURLSessionDataTask *task, id responseObject) {
        _infoArr = responseObject;
        [_tableView reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@", error);
    }];
}
//装修类型
- (void)handleTypeData {
    [NetRequest getInformationWithGetMethodWithUrl:@"http://aizufang.hrbzjwl.com/tp/api.php/Zhuangxiu/type" success:^(NSURLSessionDataTask *task, id responseObject) {
        _infoArr = responseObject;
        [_tableView reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@", error);
    }];
    
    
}

#pragma mark - 顶部自定义视图、返回上一界面
- (void)initNavigationBar {
    _myBar = [[BaseTitleNavigationBar alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 64) WithTitleStr:_categoryStr];
    [self.view addSubview:_myBar];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(back:)];
    [_myBar.leftView addGestureRecognizer:tap];
}
//返回上一界面
- (void)back:(UITapGestureRecognizer *)tap {
    [self.navigationController popViewControllerAnimated:YES];
}

/* block传值 -- 将内容传到上一界面用于显示 */
- (void)passInfoToPreviousPage:(chooseInfoBlock)infoBlock {
    self.chooseInfoBlock = infoBlock;
}
#pragma mark - tableView
/* 创建tableView */
- (void)initTableView {
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height - 64) style:UITableViewStylePlain];
    [self.view addSubview:_tableView];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:NSStringFromClass([UITableViewCell class])];
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
}
/* 行数 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _infoArr.count;
}
/* cell */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:NSStringFromClass([UITableViewCell class])];
    if ([_categoryStr isEqualToString:@"区域选择"]) {
        NSDictionary *dic = _infoArr[indexPath.row];
        cell.textLabel.text = dic[@"areaname"];
        if ([_currentStr isEqualToString:cell.textLabel.text]) {
            cell.textLabel.textColor = KNaviBackColor;
        }
        cell.tag = [dic[@"areaid"] integerValue];
    }else {
        NSDictionary *dic = _infoArr[indexPath.row];
        cell.textLabel.text = dic[@"catname"];
        if ([_currentStr isEqualToString:cell.textLabel.text]) {
            cell.textLabel.textColor = KNaviBackColor;
        }
        cell.tag = [dic[@"catid"] integerValue];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}
/* cell点击事件 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    //传值
    self.chooseInfoBlock(cell.textLabel.text, cell.tag);
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
