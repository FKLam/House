//
//  LabelAndTextFieldView.h
//  baoxian
//
//  Created by 吕品 on 16/6/2.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LabelAndTextFieldView : UIView
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UITextField *contextTextField;
@end
