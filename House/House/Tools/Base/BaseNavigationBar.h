//
//  BaseNavigationBar.h
//  House
//
//  Created by 张垚 on 16/7/8.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MySearchBar;


@interface BaseNavigationBar : UIView

@property (nonatomic, strong) UIView *leftView;
@property (nonatomic, strong) MySearchBar *searchBar;
@property (nonatomic, strong) UIImageView *leftImageView;
@property (nonatomic, strong) UILabel *leftLabel;

@end
