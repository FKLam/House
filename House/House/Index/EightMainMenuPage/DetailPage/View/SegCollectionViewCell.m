//
//  SegCollectionViewCell.m
//  House
//
//  Created by 张垚 on 16/7/20.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "SegCollectionViewCell.h"

@interface SegCollectionViewCell ()
@property (nonatomic, strong) UIView *mainView;
@end

@implementation SegCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _mainView = [[UIView alloc] init];
        [self.contentView addSubview:_mainView];
    }
    return self;
}
-(void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    _mainView.frame = self.contentView.frame;
    _mainView.backgroundColor = [UIColor colorWithRed:0.9528 green:0.9435 blue:0.962 alpha:1.0];
}
@end
