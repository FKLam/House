//
//  NetRequest.m
//  House
//
//  Created by 吕品 on 16/5/31.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "NetRequest.h"
#import "House-Swift.h"
@implementation NetRequest

+ (void)getInformationWithGetMethodWithUrl:(NSString *)url
                                   success:(void (^)(NSURLSessionDataTask *, id))success
                                   failure:(void (^)(NSURLSessionDataTask *, NSError *))failure {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"text/plain", @"text/json", @"application/json", @"text/javascript", nil];
    [manager GET:url parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {

    } success:^(NSURLSessionDataTask * _Nonnull task, id _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];

}
+ (void)upLoadImage:(UIImage *)image
            withUrl:(NSString *)url
     withParameters:(NSDictionary *)parameters
           progress:(void (^)(NSProgress *))progress
            success:(void (^)(NSURLSessionDataTask *, id))success
            failure:(void (^)(NSURLSessionDataTask *, NSError *))failure {
    //图片转data
    NSData *imageData = nil;
    if (UIImagePNGRepresentation(image)) {
        imageData = UIImagePNGRepresentation(image);
    } else {
        imageData = UIImageJPEGRepresentation(image, 0.5);
    }
    //上传接口
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"text/plain", @"text/json", @"application/json", @"text/javascript", nil];
    [manager POST:url parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        //上传流名称
        [formData appendPartWithFileData:imageData name:@"file" fileName:@"1.jpg" mimeType:@"image/jpeg"];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        progress(uploadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}
+ (void)updateOrGetInformationFromUrl:(NSString *)url
                       withParameters:(NSDictionary *)parameters
                              success:(void (^)(NSURLSessionDataTask *, id))success
                              failure:(void (^)(NSURLSessionDataTask *, NSError *))failure {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"text/plain", @"text/json", @"application/json", @"text/javascript",nil];
    [manager POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {

    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
    
}

+ (void)getCodeNumberWithPhoneNumber:(NSString *)phonenumber
                             success:(void (^)(NSURLSessionDataTask *, id))success
                             failure:(void (^)(NSURLSessionDataTask *, NSError *))failure {
    [self updateOrGetInformationFromUrl:[PHPHeader manager].yzmRogerPHP withParameters:@{@"mobile": phonenumber} success:^(NSURLSessionDataTask *task, id responseObject) {
        success(task, [responseObject[@"yzm"] description]);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(task, error);
    }];
}

/* GET请求数据 */
+ (void)getWithURLString:(NSString *)URLString
             parameters:(id)parameters
               progress:(void(^)(NSProgress *progress))progress
                success:(void (^)(id response))success
                failure:(void (^)(NSError *error))failure {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    /**
     *  请求队列的最大并发数
     */
    //   manager.operationQueue.maxConcurrentOperationCount = 5;
    /**
     *  请求超时的时间
     */
    //    manager.requestSerializer.timeoutInterval = 5;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"text/plain", @"text/json", @"application/json", @"text/javascript", nil];
    [manager GET:URLString parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
        progress(downloadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);
    }];
    
}

+ (void)downloadImageWithUrl:(NSString *)url button:(UIButton *)button placeHolder:(UIImage *)placeHolder {
    [button setImage:placeHolder forState:UIControlStateNormal];
    [self downLoadImageWithUrl:url withBlock:^(UIImage *image) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (image) {
                [button setImage:image forState:UIControlStateNormal];
            }else {
                NSLog(@"no image");
            }
        });
    }];
}

+ (void)downloadImageWithUrl:(NSString *)url imageView:(UIImageView *)imageView placeHolder:(UIImage *)placeHolder{
    imageView.image = placeHolder;
    [self downLoadImageWithUrl:url withBlock:^(UIImage *image) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (image) {
                imageView.image = image;
            } else {
                NSLog(@"no image");
            }
        });
    }];
}

+ (void)downLoadImageWithUrl:(NSString *)url withBlock:(void (^)(UIImage *image))block{

    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithURL:[NSURL URLWithString:url] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (!error) {
            UIImage *image = [UIImage imageWithData:data];
            block(image);
        } else {
            NSLog(@"error:%@", error);
        }
    }];
    [task resume];
}

+ (void)upLoadImageArr:(NSArray *)imageArr
            withUrl:(NSString *)url
     withParameters:(NSDictionary *)parameters
           progress:(void (^)(NSProgress *))progress
            success:(void (^)(NSURLSessionDataTask *, id))success
            failure:(void (^)(NSURLSessionDataTask *, NSError *))failure {
    //上传接口
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"text/plain", @"text/json", @"application/json", @"text/javascript", nil];
    [manager POST:url parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        NSData *data;
        for (int i = 0; i < imageArr.count; i++) {
            UIImage *image = imageArr[i];
            if (UIImagePNGRepresentation(image)) {
                data = UIImagePNGRepresentation(image);
            }else {
                data = UIImageJPEGRepresentation(image, 0.5);
            }
            //上传流名称
            [formData appendPartWithFileData:data name:[NSString stringWithFormat:@"bj%ld", (long)i + 1] fileName:[NSString stringWithFormat:@"bj%ld.jpg", (long)i + 1] mimeType:@"image/jpeg"];
        }
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        progress(uploadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

+ (void)upLoadEntrustImageArr:(NSArray *)imageArr
               withUrl:(NSString *)url
        withParameters:(NSDictionary *)parameters
              progress:(void (^)(NSProgress *))progress
               success:(void (^)(NSURLSessionDataTask *, id))success
               failure:(void (^)(NSURLSessionDataTask *, NSError *))failure {
    //上传接口
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"text/plain", @"text/json", @"application/json", @"text/javascript", nil];
    [manager POST:url parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        NSData *data;
        for (int i = 0; i < imageArr.count; i++) {
            UIImage *image = imageArr[i];
            if (UIImagePNGRepresentation(image)) {
                data = UIImagePNGRepresentation(image);
            }else {
                data = UIImageJPEGRepresentation(image, 0.1);
            }
            //上传流名称
            [formData appendPartWithFileData:data name:[NSString stringWithFormat:@"file%ld", (long)i + 1] fileName:[NSString stringWithFormat:@"target_file%ld.jpg", (long)i + 1] mimeType:@"image/jpeg"];
        }
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        progress(uploadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}



@end
