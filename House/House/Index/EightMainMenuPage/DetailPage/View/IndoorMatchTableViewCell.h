//
//  IndoorMatchTableViewCell.h
//  House
//
//  Created by 张垚 on 16/7/19.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RentHouseDetailModel;

@interface IndoorMatchTableViewCell : UITableViewCell

@property (nonatomic, strong) RentHouseDetailModel *model;
@end
