//
//  MessageMainViewController.m
//  House
//
//  Created by 吕品 on 16/5/31.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "MessageMainViewController.h"
#import "ActionCenterMianListTableViewCell.h"
#import "ActionCenterListModel.h"
#import "ActionCenterDetailViewController.h"


@interface MessageMainViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, copy) NSArray *dataArr;

@property (nonatomic, strong) MBProgressHUD *hud;
@end
static NSString *cellID = @"ActionCenterMianListTableViewCell";

@implementation MessageMainViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabbarHidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
    UIView *barView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 64)];
    [self.view addSubview:barView];
    barView.backgroundColor = KNaviBackColor;
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 29, kScreenWidth, 30)];
    [barView addSubview:titleLabel];
    titleLabel.font = [UIFont systemFontOfSize:20 weight:20];
    titleLabel.text = @"活动中心";
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    [self initTableView];
    [self handleData];
    [self setHeaderRefresh];
    //菊花属性设置
    _hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _hud.label.text = @"正在加载数据";
    //    _hud.dimBackground = YES;
    _hud.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
    _hud.backgroundView.backgroundColor = [UIColor whiteColor];
    _hud.bezelView.style = MBProgressHUDBackgroundStyleBlur;
    _hud.animationType = MBProgressHUDAnimationZoomIn;
    // Do any additional setup after loading the view.
}

- (void)handleData {
    _dataArr = [NSArray array];
    [NetRequest getInformationWithGetMethodWithUrl:@"http://aizufang.hrbzjwl.com/tp/index.php/Api/Guanggao/hdxq" success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([[responseObject[@"flag"] description] isEqualToString:@"1"]) {
            _hud.label.text = @"加载成功";
            [_hud hideAnimated:YES afterDelay:1];
            _dataArr = responseObject[@"hdxq"] ;
            [self.tableView.mj_header endRefreshing];
            [_tableView reloadData];
        }else {
            _hud.label.text = @"加载失败";
            [_hud hideAnimated:YES afterDelay:1];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        _hud.label.text = @"服务器异常，请重试";
        [_hud hideAnimated:YES afterDelay:1];
    }];
}

//下拉刷新
- (void)setHeaderRefresh {
    [self.tableView.mj_header beginRefreshing];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self handleData];
    }];
}

- (void)initTableView {
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, kScreenWidth, kScreenHeight - 108) style:UITableViewStylePlain];
    [self.view addSubview:_tableView];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView registerClass:[ActionCenterMianListTableViewCell class] forCellReuseIdentifier:cellID];
    UIView *blockView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 20)];
    blockView.backgroundColor = [UIColor whiteColor];
    _tableView.tableHeaderView = blockView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ActionCenterMianListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *dic = _dataArr[indexPath.row];
    ActionCenterListModel *model = [ActionCenterListModel modelWithDic:dic];
    cell.model = model;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dic = _dataArr[indexPath.row];
    ActionCenterDetailViewController *actionDetailVC = [[ActionCenterDetailViewController alloc] init];
    actionDetailVC.titleStr = dic[@"title"];
    actionDetailVC.urlStr = [NSString stringWithFormat:@"%@/id/%@", dic[@"content"], dic[@"id"]];
    [self.navigationController pushViewController:actionDetailVC animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 230;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
