//
//  ApartmentHotelListTableViewCell.h
//  House
//
//  Created by 张垚 on 16/7/7.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ApartmentHotelListModel;


@interface ApartmentHotelListTableViewCell : UITableViewCell
@property (nonatomic, strong) ApartmentHotelListModel *model;
@end
