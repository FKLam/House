//
//  BuyHouseEntrustViewController.h
//  House
//
//  Created by 张垚 on 16/6/24.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "BaseViewController.h"
@class MinePublishModel;
@interface BuyHouseEntrustViewController : BaseViewController
@property (nonatomic, strong) MinePublishModel *model;
@end
