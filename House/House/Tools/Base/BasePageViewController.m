//
//  BasePageViewController.m
//  House
//
//  Created by 张垚 on 16/6/27.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "BasePageViewController.h"
#import "LrdSuperMenu.h"
#import "BaseNavigationBar.h"
#import "MySearchBar.h"


@interface BasePageViewController ()

@end

@implementation BasePageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;
    [self initNavigationBar];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    // Do any additional setup after loading the view.
}

- (void)initNavigationBar {
    _myBar = [[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 64)];
    [self.view addSubview:_myBar];
    _myBar.leftImageView.image = [UIImage imageNamed:@"newBack"];
    _myBar.leftLabel.text = @"返回";
    _myBar.searchBar.imageView.image = [UIImage imageNamed:@"search"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
