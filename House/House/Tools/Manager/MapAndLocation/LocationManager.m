//
//  LocationManager.m
//  House
//
//  Created by 吕品 on 16/6/3.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "LocationManager.h"
#import "KeyManager.h"
@interface LocationManager ()

@end
@implementation LocationManager
+ (LocationManager *)manager {
    static LocationManager *manager = nil;
    if (!manager) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            manager = [[self alloc] init];
            [self regAMapKey];
        });
    }
    return manager;
}
- (void)requestLocationWithBlock:(void (^)(CLLocation *, AMapLocationReGeocode *, NSError *))block {
    static dispatch_once_t onceToken;
    static AMapLocationManager *manager = nil;
    dispatch_once(&onceToken, ^{
        manager = [[AMapLocationManager alloc] init];
    });
    [manager requestLocationWithReGeocode:YES completionBlock:^(CLLocation *location, AMapLocationReGeocode *regeocode, NSError *error) {
        block(location, regeocode, error);
    }];
}
+ (void)regAMapKey {
    [AMapServices sharedServices].apiKey = AMapKey;
}

@end
