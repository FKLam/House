//
//  BaseModel.m
//  House
//
//  Created by 吕品 on 16/5/31.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "BaseModel.h"

@implementation BaseModel
- (id)valueForUndefinedKey:(NSString *)key {
    return nil;
}

+ (NSArray *)setValuesAndKeysWithArray:(NSArray<NSDictionary *>*)arr {
    NSMutableArray *array = [NSMutableArray array];
    for (NSDictionary *dict in arr) {
        BaseModel *model = [[self alloc] init];
        [model setValuesForKeysWithDictionary:dict];
        [array addObject:model];
    }
    return array.copy;
}

/* 如果出现系统关键字的反馈方式 */
-(void)setValue:(id)value forUndefinedKey:(NSString *)key {
    
    if ([key isEqualToString:@"id"]) {
        
        self.nID = value;
    }
    
}
/* KVC */
+ (id)modelWithDic:(NSDictionary *)dic{
    __strong Class model = [[[self class] alloc]init];
    [model setValuesForKeysWithDictionary:dic];
    return model;
}

@end
