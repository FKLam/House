//
//  UserInfoManager.h
//  House
//
//  Created by 吕品 on 2016/7/26.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserInfoManager : NSObject
@property (nonatomic, assign, readonly, getter=isLogin) BOOL Login;
@property (nonatomic, copy, readonly) NSString *uid;
@property (nonatomic, copy) NSString *nickName;
@property (nonatomic, copy) NSString *phoneNumber;
@property (nonatomic, copy) NSString *userCityID;
+ (UserInfoManager *)sharedManager;
- (void)initWithUserID:(NSString *)userId phoneNumber:(NSString *)phoneNumber userName:(NSString *)name;
+ (BOOL)checkLogin;
- (void)logOff;
@end
