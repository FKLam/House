//
//  ReuseHouseDetailInfoTableViewCell.m
//  House
//
//  Created by 张垚 on 16/7/12.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "ReuseHouseDetailInfoTableViewCell.h"
#import "RentHouseDetailModel.h"

@interface ReuseHouseDetailInfoTableViewCell ()

@property (nonatomic, strong) UILabel *yearLabel;
@property (nonatomic, strong) UILabel *directionLabel;
@property (nonatomic, strong) UILabel *floorLabel;
@property (nonatomic, strong) UILabel *roomModelLabel;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UILabel *areaLabel;
@property (nonatomic, strong) UILabel *decorateLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *numberLabel;
@property (nonatomic, strong) UILabel *paymentLabel;

@end



@implementation ReuseHouseDetailInfoTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _yearLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_yearLabel];
        _directionLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_directionLabel];
        _floorLabel = [[UILabel alloc] init];
        _floorLabel.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:_floorLabel];
        _roomModelLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_roomModelLabel];
        _priceLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_priceLabel];
        _areaLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_areaLabel];
        _decorateLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_decorateLabel];
        _timeLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_timeLabel];
        _numberLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_numberLabel];
        _paymentLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_paymentLabel];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self.contentView;
    [_yearLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(15);
        make.top.equalTo(superView.mas_top).offset(5);
        make.width.mas_equalTo(kScreenWidth / 2);
        make.height.equalTo(@20);
    }];
    [_directionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_yearLabel.mas_right);
        make.right.equalTo(superView.mas_right);
        make.height.equalTo(@20);
        make.top.equalTo(superView.mas_top).offset(5);
    }];

    [_floorLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       make.left.equalTo(superView.mas_left).offset(15);
        make.top.equalTo(_yearLabel.mas_bottom).offset(10);
        make.width.mas_equalTo(kScreenWidth / 2);
        make.height.equalTo(@20);
    }];
    
    [_roomModelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_floorLabel.mas_right);
        make.top.equalTo(_yearLabel.mas_bottom).offset(10);
        make.right.equalTo(superView.mas_right);
        make.height.equalTo(@20);
    }];
    [_priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       make.left.equalTo(superView.mas_left).offset(15);
        make.top.equalTo(_floorLabel.mas_bottom).offset(10);
        make.width.mas_equalTo(kScreenWidth / 2);
        make.height.equalTo(@20);
    }];

    [_areaLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_priceLabel.mas_right);
        make.top.equalTo(_floorLabel.mas_bottom).offset(10);
        make.right.equalTo(superView.mas_right);
        make.height.equalTo(@20);
    }];
    [_decorateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       make.left.equalTo(superView.mas_left).offset(15);
        make.top.equalTo(_priceLabel.mas_bottom).offset(10);
        make.width.mas_equalTo(kScreenWidth / 2);
        make.height.equalTo(@20);
    }];

    
    [_paymentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_decorateLabel.mas_right);
        make.top.equalTo(_priceLabel.mas_bottom).offset(10);
        make.right.equalTo(superView.mas_right);
        make.height.equalTo(@20);
    }];
    _paymentLabel.adjustsFontSizeToFitWidth = YES;
    [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(15);
        make.top.equalTo(_decorateLabel.mas_bottom).offset(10);
        make.width.mas_equalTo(kScreenWidth / 2);
        make.height.equalTo(@20);
    }];

    [_numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_timeLabel.mas_right);
        make.top.equalTo(_decorateLabel.mas_bottom).offset(10);
       make.right.equalTo(superView.mas_right);
        make.height.equalTo(@20);
    }];

}

- (void)setModel:(RentHouseDetailModel *)model {
    if (model != _model) {
        _model = model;
    }
    if ([self checkNum:[model.houseyear description]]) {
        NSMutableAttributedString *yearStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"房龄:%@年", model.houseyear]];
        [yearStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, 3)];
        _yearLabel.attributedText = yearStr;
    }else {
        NSMutableAttributedString *yearStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"房龄:%@", model.houseyear]];
        [yearStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, 3)];
        _yearLabel.attributedText = yearStr;
    }
    
    NSMutableAttributedString *directionStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"朝向:%@", [model.toward description]]];
    [directionStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, 3)];
    _directionLabel.attributedText = directionStr;
    
    
    NSMutableAttributedString *floorStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"楼层:第%@层/共%@层", [model.floor1 description], [model.floor2 description]]];
    [floorStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, 3)];
    _floorLabel.attributedText = floorStr;
    
//    _floorLabel.textColor = [UIColor lightGrayColor];
//    _floorLabel.text = [NSString stringWithFormat:@"楼层:第%@层/共%@层", model.floor1, model.floor2];
    
    NSMutableAttributedString *roomStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"户型:%@室%@厅", model.room, model.hall]];
    [roomStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, 3)];
    _roomModelLabel.attributedText = roomStr;
    
    NSMutableAttributedString *priceStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"租金:%@元/月", model.price]];
    [priceStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, 3)];
    _priceLabel.attributedText = priceStr;
    
    NSMutableAttributedString *areaStr = [[NSMutableAttributedString alloc] initWithString:@"租赁方式:"];
    [areaStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, 5)];
    if ([model.rentmethod isEqualToString:@"1"]) {
        [areaStr appendAttributedString:[[NSAttributedString alloc] initWithString:@"整租"] ];
    }else {
        [areaStr appendAttributedString:[[NSAttributedString alloc] initWithString:@"合租"] ];
    }
    _areaLabel.attributedText = areaStr;
    
    NSMutableAttributedString *decorateStr = [[NSMutableAttributedString alloc] initWithString:@"装修:"];
    [decorateStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, 3)];
    if ([model.zhuanxiu isEqualToString:@"1"]) {
        [decorateStr appendAttributedString:[[NSAttributedString alloc] initWithString:@"毛坯"] ];
    }else if ([model.zhuanxiu isEqualToString:@"2"]) {
         [decorateStr appendAttributedString:[[NSAttributedString alloc] initWithString:@"精装修"] ];
    }else if ([model.zhuanxiu isEqualToString:@"3"]) {
         [decorateStr appendAttributedString:[[NSAttributedString alloc] initWithString:@"简装修"] ];
    }else {
         [decorateStr appendAttributedString:[[NSAttributedString alloc] initWithString:@"豪华装修"] ];
    }
    _decorateLabel.attributedText = decorateStr;
    
    NSMutableAttributedString *paymentStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"付款方式:%@", model.bus]];
    [paymentStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, 5)];
    _paymentLabel.attributedText = paymentStr;
    
    
    NSMutableAttributedString *timeStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"上架时间:%@", model.adddate]];
    [timeStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, 5)];
    if (model.adddate == NULL) {
        
    }else {
        [timeStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15] range:NSMakeRange(5, 10)];
    }
    _timeLabel.attributedText = timeStr;
    
    NSMutableAttributedString *numberStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"编号:%@", model.code]];
    [numberStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, 3)];
    _numberLabel.attributedText = numberStr;
}

/* 检查输入的是否全为数字 返回BOOL值 */
-(BOOL)checkNum:(NSString *)inputStr {
    /* 创建扫描类 */
    NSScanner *scanner = [NSScanner scannerWithString:inputStr];
    int value;
    /* [scanner scanInt:&value]该方法将扫描的int放入value 返回布尔值 */
    /* [scanner isAtEnd] 该方法判断是否走到了字符串末端 返回的也是布尔值 */
    return[scanner scanInt:&value] && [scanner isAtEnd];
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
