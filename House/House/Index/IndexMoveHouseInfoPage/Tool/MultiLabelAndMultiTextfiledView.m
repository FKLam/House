//
//  MultiLabelAndMultiTextfiledView.m
//  IndexMoveHousePage
//
//  Created by 张垚 on 16/6/15.
//  Copyright © 2016年 众杰网络. All rights reserved.
//

#import "MultiLabelAndMultiTextfiledView.h"



@implementation MultiLabelAndMultiTextfiledView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _buildingNumTextfield = [[UITextField alloc] init];
        _buildingNumTextfield.tag = 100;
        [self addSubview:_buildingNumTextfield];
        _buildingNumLabel = [[UILabel alloc] init];
        [self addSubview:_buildingNumLabel];
        _unitNumTextfiled = [[UITextField alloc] init];
        _unitNumTextfiled.tag = 101;
        [self addSubview:_unitNumTextfiled];
        _unitNumLabel = [[UILabel alloc] init];
        [self addSubview:_unitNumLabel];
        _roomNumTextField = [[UITextField alloc] init];
        _roomNumTextField.tag = 102;
        [self addSubview:_roomNumTextField];
        _roomNumLabel = [[UILabel alloc] init];
        [self addSubview:_roomNumLabel];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self;
    [_buildingNumTextfield mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(5);
        make.left.equalTo(superView.mas_left);
        make.bottom.equalTo(superView.mas_bottom).offset(-5);
        make.right.equalTo(_buildingNumLabel.mas_left).offset(-5);
        make.width.equalTo(@30);
    }];
    [_buildingNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(5);
        make.height.equalTo(_buildingNumTextfield.mas_height);
        make.width.equalTo(@30);
    }];
    [_buildingNumLabel sizeToFit];
    [_unitNumTextfiled mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(5);
        make.left.equalTo(_buildingNumLabel.mas_right).offset(5);
        make.height.equalTo(_buildingNumTextfield.mas_height);
        make.width.equalTo(_buildingNumTextfield.mas_width);
    }];
    [_unitNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(5);
        make.height.equalTo(_buildingNumTextfield.mas_height);
        make.width.equalTo(@45);
        make.left.equalTo(_unitNumTextfiled.mas_right).offset(5);
    }];
    [_unitNumLabel sizeToFit];
    [_roomNumTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(5);
        make.height.equalTo(_buildingNumTextfield.mas_height);
        make.left.equalTo(_unitNumLabel.mas_right).offset(5);
        make.width.equalTo(_buildingNumTextfield.mas_width);
    }];
    [_roomNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(5);
        make.height.equalTo(_buildingNumTextfield.mas_height);
        make.left.equalTo(_roomNumTextField.mas_right).offset(5);
        make.width.equalTo(_unitNumLabel.mas_width);
    }];
    [_roomNumLabel sizeToFit];
    _buildingNumTextfield.layer.borderColor = [UIColor colorWithRed:0.902 green:0.902 blue:0.902 alpha:1.0].CGColor;
    _buildingNumTextfield.layer.borderWidth = 1;
    _buildingNumTextfield.layer.masksToBounds = YES;
    _buildingNumTextfield.layer.cornerRadius = 4;
    
    _buildingNumLabel.text = @"楼号";
    _buildingNumLabel.font = [UIFont systemFontOfSize:15];
    _buildingNumLabel.textColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0];
    _buildingNumLabel.textAlignment = NSTextAlignmentCenter;
    
    _unitNumTextfiled.layer.borderColor = [UIColor colorWithRed:0.902 green:0.902 blue:0.902 alpha:1.0].CGColor;
    _unitNumTextfiled.layer.borderWidth = 1;
    _unitNumTextfiled.layer.masksToBounds = YES;
    _unitNumTextfiled.layer.cornerRadius = 4;

    _unitNumLabel.text = @"单元号";
    _unitNumLabel.font = [UIFont systemFontOfSize:15];
    _unitNumLabel.textColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0];
    _unitNumLabel.textAlignment = NSTextAlignmentCenter;
    
    _roomNumTextField.layer.borderColor = [UIColor colorWithRed:0.902 green:0.902 blue:0.902 alpha:1.0].CGColor;
    _roomNumTextField.layer.borderWidth = 1;
    _roomNumTextField.layer.masksToBounds = YES;
    _roomNumTextField.layer.cornerRadius = 4;

    _roomNumLabel.text = @"房间号";
    _roomNumLabel.font = [UIFont systemFontOfSize:15];
    _roomNumLabel.textColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0];
    _roomNumLabel.textAlignment = NSTextAlignmentCenter;
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
