//
//  RentHouseEntrustViewController.h
//  IndexMoveHousePage
//
//  Created by 张垚 on 16/6/17.
//  Copyright © 2016年 众杰网络. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
/**
 @brief 租房委托
 */


@class MinePublishModel;
@interface RentHouseEntrustViewController : BaseViewController
@property (nonatomic, strong) MinePublishModel *model;
@end
