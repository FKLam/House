//
//  BaseModel.h
//  House
//
//  Created by 吕品 on 16/5/31.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <Foundation/Foundation.h>
/*!
 *  @author 众杰网络, 16-06-07 09:06:37
 *
 *  @brief Model基类
 *
 *  @since 1.0
 */
@interface BaseModel : NSObject

@property (nonatomic, copy) NSString *nID;

+ (id)modelWithDic:(NSDictionary *)dic;

+ (NSArray *)setValuesAndKeysWithArray:(NSArray<NSDictionary *>*)arr;
@end
