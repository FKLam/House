//
//  StarServiceListTableViewCell.m
//  House
//
//  Created by 张垚 on 16/8/22.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "StarServiceListTableViewCell.h"
#import "StarServiceListModel.h"

#define SYSTEM_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]
#define SCREEN_BOUNDS [[UIScreen mainScreen] bounds]
#define WIDTH SCREEN_BOUNDS.size.width
#define HEIGHT SCREEN_BOUNDS.size.height

#define WidthRate(a) WIDTH / 750*a
#define HeightRate(a) HEIGHT / 1334*a
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]

@implementation StarServiceListButton

- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    return CGRectMake(contentRect.size.width - contentRect.size.height + contentRect.size.height/4, contentRect.size.height /4, contentRect.size.height/2, contentRect.size.height/2);
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    return CGRectMake(0, 0, contentRect.size.width - contentRect.size.height , contentRect.size.height);
}

@end



@interface StarServiceListTableViewCell ()


@property (nonatomic, strong) CWStarRateView *starView;
@property (nonatomic, strong) UIView *midView1;
@property (nonatomic, strong) UIView *midView2;
@property (nonatomic, strong) UIView *buttonMidView1;
@property (nonatomic, strong) UIView *buttonMidView2;


@end

@implementation StarServiceListTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _tilteLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_tilteLabel];
        _starView = [[CWStarRateView alloc] initWithFrame:CGRectMake(125, 10, 100, 30) numberOfStars:5];
        _starView.userInteractionEnabled = NO;
        [self.contentView addSubview:_starView];
        _callButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.contentView addSubview:_callButton];
        
        
        
        _serviceButton1 = [StarServiceListButton buttonWithType:UIButtonTypeCustom];
        _serviceButton1.selected = YES;
        _serviceButton1.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        _serviceButton1.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.serviceButton1 setTitleColor:RGBA(59, 59, 59, 1) forState:UIControlStateNormal];
        [self.serviceButton1 setTitleColor:RGBA(250, 90, 90, 1) forState:UIControlStateSelected];
        [self.serviceButton1 setImage:[UIImage imageNamed:@"upIcon"] forState:UIControlStateNormal];
        [self.serviceButton1 setImage:[UIImage imageNamed:@"downIcon"] forState:UIControlStateSelected];
        self.serviceButton1.titleLabel.textAlignment  = NSTextAlignmentRight;
        _serviceButton1.tag = 100;
        [self.contentView addSubview:_serviceButton1];
        
        
        
        
        _serviceButton2 = [StarServiceListButton buttonWithType:UIButtonTypeCustom];
        _serviceButton2.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        _serviceButton2.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.serviceButton2 setTitleColor:RGBA(59, 59, 59, 1) forState:UIControlStateNormal];
        [self.serviceButton2 setTitleColor:RGBA(250, 90, 90, 1) forState:UIControlStateSelected];
        [self.serviceButton2 setImage:[UIImage imageNamed:@"upIcon"] forState:UIControlStateNormal];
        [self.serviceButton2 setImage:[UIImage imageNamed:@"downIcon"] forState:UIControlStateSelected];
        self.serviceButton2.titleLabel.textAlignment  = NSTextAlignmentRight;
        _serviceButton2.tag = 101;
        [self.contentView addSubview:_serviceButton2];
        
        
        _serviceButton3 = [StarServiceListButton buttonWithType:UIButtonTypeCustom];
        _serviceButton3.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        _serviceButton3.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.serviceButton3 setTitleColor:RGBA(59, 59, 59, 1) forState:UIControlStateNormal];
        [self.serviceButton3 setTitleColor:RGBA(250, 90, 90, 1) forState:UIControlStateSelected];
        [self.serviceButton3 setImage:[UIImage imageNamed:@"upIcon"] forState:UIControlStateNormal];
        [self.serviceButton3 setImage:[UIImage imageNamed:@"downIcon"] forState:UIControlStateSelected];
        self.serviceButton3.titleLabel.textAlignment  = NSTextAlignmentRight;
        _serviceButton3.tag = 102;
        [self.contentView addSubview:_serviceButton3];
        
        
        
        _midView1 = [[UIView alloc] init];
        [self.contentView addSubview:_midView1];
        _midView2 = [[UIView alloc] init];
        [self.contentView addSubview:_midView2];
        _buttonMidView1 = [[UIView alloc] init];
        [self.contentView addSubview:_buttonMidView1];
        _buttonMidView2 = [[UIView alloc] init];
        [self.contentView addSubview:_buttonMidView2];
        _contentLabel = [[UILabel alloc] init];
        _contentLabel.numberOfLines = 0;
        [self.contentView addSubview:_contentLabel];
    }
    return self;
}


- (void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self;
    [_tilteLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(5);
        make.top.equalTo(superView.mas_top).offset(10);
        make.width.equalTo(@100);
        make.height.equalTo(@30);
    }];
    _tilteLabel.adjustsFontSizeToFitWidth = YES;
    [_starView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_tilteLabel.mas_right).offset(5);
        make.top.equalTo(superView.mas_top).offset(10);
        make.width.equalTo(@100);
        make.height.equalTo(@30);
    }];
    [_callButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(10);
        make.right.equalTo(superView.mas_right).offset(-10);
        make.width.equalTo(@70);
        make.height.equalTo(@30);
    }];
    _callButton.layer.masksToBounds = YES;
    _callButton.layer.cornerRadius = 2;
    _callButton.backgroundColor = [UIColor colorWithRed:0.976 green:0.415 blue:0.431 alpha:1.0];
    [_callButton setTitle:@"电话预订" forState:UIControlStateNormal];
    _callButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [_callButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_midView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_tilteLabel.mas_bottom).offset(10);
        make.width.equalTo(superView.mas_width);
        make.left.equalTo(superView.mas_left);
        make.height.equalTo(@1);
    }];
    _midView1.backgroundColor = [UIColor colorWithRed:0.91 green:0.91 blue:0.91 alpha:1.0];
    [_serviceButton1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left);
        make.height.equalTo(@40);
        make.top.equalTo(_midView1.mas_bottom);
        make.width.mas_equalTo((kScreenWidth - 2) / 3);
    }];
    [_buttonMidView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_serviceButton1.mas_right);
        make.height.equalTo(@40);
        make.top.equalTo(_midView1.mas_bottom);
        make.width.mas_equalTo(1);
    }];
    _buttonMidView1.backgroundColor = [UIColor colorWithRed:0.91 green:0.91 blue:0.91 alpha:1.0];
    [_serviceButton2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_buttonMidView1.mas_right);
        make.height.equalTo(@40);
        make.top.equalTo(_midView1.mas_bottom);
        make.width.mas_equalTo((kScreenWidth - 2) / 3);
    }];
    [_buttonMidView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_serviceButton2.mas_right);
        make.height.equalTo(@40);
        make.top.equalTo(_midView1.mas_bottom);
        make.width.mas_equalTo(1);
    }];
    _buttonMidView2.backgroundColor = [UIColor colorWithRed:0.91 green:0.91 blue:0.91 alpha:1.0];
    [_serviceButton3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_buttonMidView2.mas_right);
        make.height.equalTo(@40);
        make.top.equalTo(_midView1.mas_bottom);
        make.width.mas_equalTo((kScreenWidth - 2) / 3);
    }];
    [_midView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_serviceButton1.mas_bottom);
        make.width.equalTo(superView.mas_width);
        make.left.equalTo(superView.mas_left);
        make.height.equalTo(@1);
    }];
    _midView2.backgroundColor = [UIColor colorWithRed:0.91 green:0.91 blue:0.91 alpha:1.0];
    [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_midView2.mas_bottom);
        make.width.equalTo(superView.mas_width);
        make.left.equalTo(superView.mas_left);
        make.bottom.equalTo(superView.mas_bottom).offset(-10);
    }];
    _contentLabel.font = [UIFont systemFontOfSize:15];
}

+ (CGFloat)heightForModel:(StarServiceListModel *)model
{
    CGSize size = [model.currentIntroduction boundingRectWithSize:CGSizeMake(WIDTH - 30, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:15.0f]} context:nil].size;
    return size.height + 120;
}


- (void)setModel:(StarServiceListModel *)model {
    if (model != _model) {
        _model = model;
    }
    _tilteLabel.text = model.sever;
    CGFloat number = [model.xingxing floatValue];
    CGFloat currentNum = number / 5;
    _starView.scorePercent = currentNum;
    
    [_serviceButton1 setTitle:model.name1 forState:UIControlStateNormal];
    [_serviceButton2 setTitle:model.name2 forState:UIControlStateNormal];
     [_serviceButton2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_serviceButton3 setTitle:model.name3 forState:UIControlStateNormal];
     [_serviceButton3 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _contentLabel.text = model.currentIntroduction;
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
