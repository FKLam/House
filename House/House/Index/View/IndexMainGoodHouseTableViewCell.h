//
//  IndexMainGoodHouseTableViewCell.h
//  House
//
//  Created by 张垚 on 16/9/6.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface moreButton : UIButton

@end

@interface IndexMainGoodHouseTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) moreButton *moreButton;

@end
