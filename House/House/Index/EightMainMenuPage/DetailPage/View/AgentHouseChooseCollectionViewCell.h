//
//  AgentHouseChooseCollectionViewCell.h
//  House
//
//  Created by 张垚 on 16/7/21.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgentHouseChooseCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) UIButton *secondaryButton;
@property (nonatomic, strong) UIButton *rentHouseButton;
@property (nonatomic, strong) UIView *segView;
@property (nonatomic, strong) UIView *halfView1;
@property (nonatomic, strong) UIView *halfView2;

@end
