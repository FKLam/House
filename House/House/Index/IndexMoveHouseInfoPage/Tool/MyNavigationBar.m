//
//  MyNavigationBar.m
//  House
//
//  Created by 张垚 on 16/6/27.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "MyNavigationBar.h"
#import "MySearchBar.h"


@implementation MyNavigationBar

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:212 / 255.0f green:38 / 255.0f blue:26 / 255.0f alpha:1.0];
        _mySearchBar = [[MySearchBar alloc] init];
        [self addSubview:_mySearchBar];
        _cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:_cancelButton];
        
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self;
    [_mySearchBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(10);
        make.bottom.equalTo(superView.mas_bottom).offset(-5);
        make.height.equalTo(@30);
    }];
    _mySearchBar.imageView.image = [UIImage imageNamed:@"graySearch"];
    [_cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(superView.mas_right).offset(-5);
        make.width.equalTo(@40);
        make.left.equalTo(_mySearchBar.mas_right).offset(20);
        make.bottom.equalTo(superView.mas_bottom).offset(-5);
        make.height.equalTo(@30);
    }];
    [_cancelButton setTitle:@"取消" forState:UIControlStateNormal];
    [_cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
