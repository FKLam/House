//
//  IndexMapFindHouseViewController.swift
//  House
//
//  Created by 吕品 on 2016/7/28.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

import UIKit
class IndexMapFindHouseViewController: BaseViewController, MAMapViewDelegate {

    var mapView = MAMapView.init(frame: CGRect.init(x: 1, y: 1, width: 1, height: 1))
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabbarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabbarHidden = false
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        LocationManager.regAMapKey()
        mapView.frame = view.frame
        view.addSubview(mapView)
        mapView.delegate = self
        mapView.snp_makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        mapView.showsUserLocation = true
        mapView.showsScale = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
