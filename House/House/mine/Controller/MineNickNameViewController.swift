//
//  MineNickNameViewController.swift
//  House
//
//  Created by 吕品 on 16/7/11.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

import UIKit
import MBProgressHUD
class MineNickNameViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    var tableView = UITableView()
    var naviBar = BaseTitleNavigationBar()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabbarHidden = true
        automaticallyAdjustsScrollViewInsets = false
        naviBar = BaseTitleNavigationBar.init(frame: CGRect.init(x: 0, y: 0, width: self.view.bounds.size.width, height: 64), withTitleStr: "昵称")
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(back(tap:)))
        naviBar.leftView.addGestureRecognizer(tap)
        view.addSubview(naviBar)
        
        tableView = UITableView.init(frame: CGRect.init(x: 0, y: 64, width: self.view.bounds.size.width, height: self.view.bounds.size.height - 64), style: .grouped)
        view.addSubview(tableView)
        let rightButton = UIButton.init(type: .system)
        naviBar.addSubview(rightButton)
        rightButton.setTitleColor( .white, for: .normal)
        rightButton.setTitle("保存", for: .normal)
        rightButton.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: 15)
        rightButton.addTarget(self, action: #selector(submit), for: .touchUpInside)
        rightButton.snp.makeConstraints { (make) in
            make.right.equalTo(naviBar.snp.right).offset(-10);
            make.bottom.equalTo(naviBar.snp.bottom).offset(-10);
            make.centerY.equalTo(naviBar.leftView)
            make.height.equalTo(30)
            make.width.equalTo(40)
        }
        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(view).offset(kNaviBarHeight)
            make.left.right.bottom.equalTo(view)
        }
        tableView.isScrollEnabled = false
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(ReuseImageViewAndTextFieldTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(ReuseImageViewAndTextFieldTableViewCell.self))
        // Do any additional setup after loading the view.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(ReuseImageViewAndTextFieldTableViewCell.self)) as! ReuseImageViewAndTextFieldTableViewCell
        cell.mixView.isShowButton = false
        cell.selectionStyle = .none
        cell.mixView.textField.placeholder = "请填写昵称"
        cell.mixView.imageView.image = UIImage.init(named: "MineSection-3-1")
        cell.tag = tagManager + (indexPath as NSIndexPath).row
        return cell
    }
    func submit() -> Swift.Void {
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.label.text = "正在更改"
        let nickName = (view.viewWithTag(1000) as! ReuseImageViewAndTextFieldTableViewCell).mixView.textField.text!

        if nickName == "" {
            self.hide(withTitle: "昵称不能为空")

        } else {
            NetRequest.updateOrGetInformation(fromUrl: PHPHeader.manager().reviseNickNamePHP, withParameters: ["id": UserInfoManager.shared().uid, "name": nickName] , success: { (task, response) in
                if response != nil {
                    print(response)
                    if response is NSDictionary {
                        let res = response as! NSDictionary
                        if (res["flg"] as! NSNumber).description == "1" {
                            UserInfoManager.shared().nickName = nickName
                            self.hide(withTitle: "修改成功")
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {

                               print(self.navigationController?.popToRootViewController(animated: true))
                            })
                        } else {
                            self.hide(withTitle: "修改失败")
                        }
                    }

                } else {
                    self.hide(withTitle: "修改失败")

                }

                }, failure: { (task, error) in
                    print(error)
                    self.hide(withTitle: "修改失败")
            })
        }

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    @objc func back(tap: UITapGestureRecognizer) -> Void {
        self.navigationController!.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
