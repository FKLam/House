//
//  MineTwoMixView.m
//  House
//
//  Created by 吕品 on 16/6/24.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "MineTwoMixView.h"
//上下imageview和label
@implementation MineTwoMixView
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.headImageView = [UIImageView new];
        _headImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_headImageView];
        self.titleLabel = [UILabel new];
        [self addSubview:_titleLabel];
        _titleLabel.font = [UIFont systemFontOfSize:13];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    _headImageView.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.width);
    _titleLabel.frame = CGRectMake(0, self.bounds.size.width + 10, self.bounds.size.width, self.bounds.size.height - self.bounds.size.width - 10);
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
