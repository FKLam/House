//
//  AgentViewController.m
//  House
//
//  Created by 张垚 on 16/6/28.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "AgentViewController.h"
#import "SearchHouseViewController.h"
#import "AgentListTableViewCell.h"
#import "LrdSuperMenu.h"
#import "BaseNavigationBar.h"
#import "MySearchBar.h"
#import "ImageViewAndLabelButton.h"
#import "AgentDetailViewController.h"
#import "AgentListModel.h"
//<<<<<<< .merge_file_acq01N
#import "AgentDetailModel.h"
//=======
#import "AreaAndTypeChooseViewController.h"

#import "AgentListNoSlideTableViewCell.h"

//>>>>>>> .merge_file_Bntp4r
@interface AgentViewController ()< UITableViewDelegate, UITableViewDataSource, AgentListTableViewCellDelegate>
@property (nonatomic, strong) UITableView *tableView;
//侧滑cell
@property (nonatomic, strong) AgentListTableViewCell *mySelectCell;
//顶部 成交量、区域选择
@property (nonatomic, strong) UIButton *chooseButton;
@property (nonatomic, strong) UIButton *areaButton;
@property (nonatomic, strong) UIImageView *selectImageView;
@property (nonatomic, strong) UILabel *selectLabel;
//请求数据页数属性
@property (nonatomic, assign) NSInteger page;
//装数据的数组 里面装的是字典
@property (nonatomic, strong) NSMutableArray *dataDicArr;

@property (nonatomic, assign) NSInteger rankTag;
@property (nonatomic, assign) NSInteger areaTag;

@property (nonatomic, copy) NSString *currenSelectStr;

@property (nonatomic, strong) MBProgressHUD *hud;
@property (nonatomic, strong) MBProgressHUD *selectHud;
@end
static NSString *cellID = @"AgentListTableViewCellID";
static NSString *agentUrlString = @"http://aizufang.hrbzjwl.com/tp/api.php/Jingjiren/index/";



@implementation AgentViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = YES;
}
#pragma mark - 主程序入口
- (void)viewDidLoad {
    [super viewDidLoad];
    //位置自适应
    self.automaticallyAdjustsScrollViewInsets = NO;
    _page = 1;
    _areaTag = 1;
    _rankTag = 1;
    //顶部选择视图 -- 成交量筛选 区域选择
    [self selectView];
    //tableview
    [self initTableView];
    //数据处理
    [self handleData];
    //下拉刷新
    [self setHeaderRefresh];
    //菊花属性设置
    _hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _hud.label.text = @"正在加载数据";
    //    _hud.dimBackground = YES;
    _hud.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
    _hud.backgroundView.backgroundColor = [UIColor whiteColor];
    _hud.bezelView.style = MBProgressHUDBackgroundStyleBlur;
    _hud.animationType = MBProgressHUDAnimationZoomIn;
    // Do any additional setup after loading the view.
}

#pragma mark - 导航栏搜索
/* 重写父类方法 */
- (void)initNavigationBar {
    [super initNavigationBar];
    self.myBar.searchBar.textFiled.placeholder = @"请输入经纪人名或者编号";
    [self.myBar.searchBar.textFiled setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    UITapGestureRecognizer *popTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pop:)];
    [self.myBar.leftView addGestureRecognizer:popTap];
    UITapGestureRecognizer *pushSearchHouseTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(push)];
    [self.myBar.searchBar addGestureRecognizer:pushSearchHouseTap];
}
//返回上一界面
- (void)pop:(UITapGestureRecognizer *)tap {
    [self.navigationController popViewControllerAnimated:YES];
}

/* 点击搜索栏 跳转页面进行搜索 */
- (void)push {
    SearchHouseViewController *searchVC = [[SearchHouseViewController alloc] init];
    searchVC.currentStr = @"经纪人";
    [self.navigationController pushViewController:searchVC animated:YES];
}


#pragma mark - 顶部筛选部分布局以及点击事件
- (void)selectView {
    UIView *superView = self.view;
    _chooseButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:_chooseButton];
    _areaButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:_areaButton];
    UIView *midView = [[UIView alloc] init];
    midView.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:midView];
    [_chooseButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(-1);
        make.top.equalTo(superView.mas_top).offset(63);
        make.height.equalTo(@40);
        make.right.equalTo(midView.mas_left);
    }];
    UIView *segView = [[UIView alloc] initWithFrame:CGRectMake(0, 104, kScreenWidth, 1)];
    [self.view addSubview:segView];
    
    segView.backgroundColor = [UIColor lightGrayColor];
    [_areaButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(64);
        make.height.equalTo(@40);
        make.right.equalTo(superView.mas_right);
        make.width.equalTo(@60);
    }];
    [_areaButton setTitle:@"区域" forState:UIControlStateNormal];
    [_areaButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [midView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@64);
        make.left.equalTo(_chooseButton.mas_right);
        make.height.equalTo(@40);
        make.width.equalTo(@1);
        make.right.equalTo(_areaButton.mas_left);
    }];
    [_chooseButton addTarget:self action:@selector(rank:) forControlEvents:UIControlEventTouchUpInside];
    _chooseButton.selected = NO;
    [_areaButton addTarget:self action:@selector(areaChoose:) forControlEvents:UIControlEventTouchUpInside];
   
    _selectImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 20, 20)];
    _selectImageView.image = [UIImage imageNamed:@"buttonUnselect"];
    [_chooseButton addSubview:_selectImageView];
    _selectLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 10, 150, 20)];
    _selectLabel.text = @"按成交量倒序排列";
    _selectLabel.textColor = [UIColor lightGrayColor];
    [_chooseButton addSubview:_selectLabel];
    
}
//成交量筛选
- (void)rank:(UIButton *)button {
    button.selected = !button.selected;
    if (button.selected) {
        _selectLabel.textColor = KNaviBackColor;
        _selectImageView.image = [UIImage imageNamed:@"buttonSelect"];
        _rankTag = 2;
        [self getRankDataWithRankTag:_rankTag];
    }else {
        _selectLabel.textColor = [UIColor lightGrayColor];
        _selectImageView.image = [UIImage imageNamed:@"buttonUnselect"];
        _rankTag = 1;
        [self getRankDataWithRankTag:_rankTag];
    }
}
//成交量筛选之后请求数据
- (void)getRankDataWithRankTag:(NSInteger)tag {
    _selectHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _selectHud.label.text = @"加载中...";
    _selectHud.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
    _selectHud.animationType = MBProgressHUDAnimationZoomIn;

    if (tag == 1) {
        if (_areaTag == 1) {
            NSDictionary *dic = @{@"page": @"1"};
            [NetRequest getWithURLString:agentUrlString parameters:dic progress:^(NSProgress *progress) {
                
            } success:^(id response) {
                if ([[response[@"flag"] description] isEqualToString:@"0"]) {
                    [self getHudWithAlertMessage:@"暂无信息" WithImageName:@"failIcon"];
                    _dataDicArr = [NSMutableArray array];
                    [_tableView reloadData];
                }else {
                    [self getHudWithAlertMessage:@"筛选成功" WithImageName:@"successIcon"];
                    //将获取的值赋给数据 刷新tableview
                    NSArray *arr = response[@"jingjiren"];
                    _dataDicArr = [NSMutableArray arrayWithArray:arr];
                    [_tableView reloadData];
                }
            } failure:^(NSError *error) {
                [self getHudWithAlertMessage:@"服务器异常，请重试" WithImageName:@"failIcon"];
            }];
        }else {
            if ([_areaButton.titleLabel.text isEqualToString:@"区域"]) {
                NSDictionary *dic = @{@"page": @"1"};
                [NetRequest getWithURLString:agentUrlString parameters:dic progress:^(NSProgress *progress) {
                    
                } success:^(id response) {
                    if ([[response[@"flag"] description] isEqualToString:@"0"]) {
                        [self getHudWithAlertMessage:@"暂无信息" WithImageName:@"failIcon"];
                        _dataDicArr = [NSMutableArray array];
                        [_tableView reloadData];
                    }else {
                        [self getHudWithAlertMessage:@"筛选成功" WithImageName:@"successIcon"];
                        //将获取的值赋给数据 刷新tableview
                        NSArray *arr = response[@"jingjiren"];
                        _dataDicArr = [NSMutableArray arrayWithArray:arr];
                        [_tableView reloadData];
                    }
                } failure:^(NSError *error) {
                    [self getHudWithAlertMessage:@"服务器异常，请重试" WithImageName:@"failIcon"];
                }];
            }else {
                NSString *str = _areaButton.titleLabel.text;
                str = [str stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
                NSString *urlStr = [NSString stringWithFormat:@"%@page/1/qu/%@/", agentUrlString, str];
                [NetRequest getWithURLString:urlStr parameters:nil progress:^(NSProgress *progress) {
                    
                } success:^(id response) {
                    
                    if ([[response[@"flag"] description] isEqualToString:@"0"]) {
                        [self getHudWithAlertMessage:@"暂无信息" WithImageName:@"failIcon"];
                        _dataDicArr = [NSMutableArray array];
                        [_tableView reloadData];
                    }else {
                        [self getHudWithAlertMessage:@"筛选成功" WithImageName:@"successIcon"];
                        //将获取的值赋给数据 刷新tableview
                        NSArray *arr = response[@"jingjiren"];
                        _dataDicArr = [NSMutableArray arrayWithArray:arr];
                        [_tableView reloadData];
                    }
                } failure:^(NSError *error) {
                   [self getHudWithAlertMessage:@"服务器异常，请重试" WithImageName:@"failIcon"];
                }];
            }
        }
    }else {
        if (_areaTag == 1) {
            NSDictionary *dic = @{@"page": @"1", @"shun": @"1"};
            [NetRequest getWithURLString:agentUrlString parameters:dic progress:^(NSProgress *progress) {
                
            } success:^(id response) {
                if ([[response[@"flag"] description] isEqualToString:@"0"]) {
                    [self getHudWithAlertMessage:@"暂无信息" WithImageName:@"failIcon"];
                    _dataDicArr = [NSMutableArray array];
                    [_tableView reloadData];
                }else {
                    [self getHudWithAlertMessage:@"筛选成功" WithImageName:@"successIcon"];
                    //将获取的值赋给数据 刷新tableview
                    NSArray *arr = response[@"jingjiren"];
                    _dataDicArr = [NSMutableArray arrayWithArray:arr];
                    [_tableView reloadData];
                }
            } failure:^(NSError *error) {
                [self getHudWithAlertMessage:@"服务器异常，请重试" WithImageName:@"failIcon"];
            }];
        }else {
            if ([_areaButton.titleLabel.text isEqualToString:@"区域"]) {
                NSDictionary *dic = @{@"page": @"1", @"shun": @"1"};
                [NetRequest getWithURLString:agentUrlString parameters:dic progress:^(NSProgress *progress) {
                    
                } success:^(id response) {
                    if ([[response[@"flag"] description] isEqualToString:@"0"]) {
                        [self getHudWithAlertMessage:@"暂无信息" WithImageName:@"failIcon"];
                        _dataDicArr = [NSMutableArray array];
                        [_tableView reloadData];
                    }else {
                        [self getHudWithAlertMessage:@"筛选成功" WithImageName:@"successIcon"];
                        //将获取的值赋给数据 刷新tableview
                        NSArray *arr = response[@"jingjiren"];
                        _dataDicArr = [NSMutableArray arrayWithArray:arr];
                        [_tableView reloadData];
                    }
                } failure:^(NSError *error) {
                    [self getHudWithAlertMessage:@"服务器异常，请重试" WithImageName:@"failIcon"];
                }];
            }else {
                NSString *str = _areaButton.titleLabel.text;
                str = [str stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
                NSString *urlStr = [NSString stringWithFormat:@"%@page/1/qu/%@/shun/1/", agentUrlString, str];
                [NetRequest getWithURLString:urlStr parameters:nil progress:^(NSProgress *progress) {
                    
                } success:^(id response) {
                    if ([[response[@"flag"] description] isEqualToString:@"0"]) {
                        [self getHudWithAlertMessage:@"暂无信息" WithImageName:@"failIcon"];
                        _dataDicArr = [NSMutableArray array];
                        [_tableView reloadData];
                    }else {
                        [self getHudWithAlertMessage:@"筛选成功" WithImageName:@"successIcon"];
                        //将获取的值赋给数据 刷新tableview
                        NSArray *arr = response[@"jingjiren"];
                        _dataDicArr = [NSMutableArray arrayWithArray:arr];
                        [_tableView reloadData];
                    }
                } failure:^(NSError *error) {
                    [self getHudWithAlertMessage:@"服务器异常，请重试" WithImageName:@"failIcon"];
                }];
            }
        }
    }
    _page = 1;
    [self setFooterRefresh];
}
//区域选择
- (void)areaChoose:(UIButton *)button {
    AreaAndTypeChooseViewController *areaVC = [[AreaAndTypeChooseViewController alloc] init];
    areaVC.tag = 100;
    areaVC.currentStr = button.titleLabel.text;
    [areaVC passInfoToPreviousPage:^(NSString *currentStr, NSInteger currentTag) {
        if ([currentStr isEqualToString:@"不限"]) {
            [button setTitle:@"区域" forState:UIControlStateNormal];
            [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        }else {
            [button setTitle:currentStr forState:UIControlStateNormal];
            [button setTitleColor:KNaviBackColor forState:UIControlStateNormal];
        }
        [self getSelectDataWithCurrentStr:currentStr];
        _currenSelectStr = currentStr;
    }];
    [self.navigationController pushViewController:areaVC animated:YES];
    
}
//区域选择之后的数据请求
- (void)getSelectDataWithCurrentStr:(NSString *)currentStr {
    _selectHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _selectHud.label.text = @"加载中...";
    _selectHud.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
//    _selectHud.backgroundView.backgroundColor = [UIColor whiteColor];
//    _selectHud.bezelView.style = MBProgressHUDBackgroundStyleBlur;
    _selectHud.animationType = MBProgressHUDAnimationZoomIn;
    if ([currentStr isEqualToString:@"不限"]) {
        if (_rankTag == 1) {
            //获取第一页
            NSDictionary *dic = @{@"page": @"1"};
            [NetRequest getWithURLString:agentUrlString parameters:dic progress:^(NSProgress *progress) {
                
            } success:^(id response) {
                if ([[response[@"flag"] description] isEqualToString:@"0"]) {
                    [self getHudWithAlertMessage:@"暂无信息" WithImageName:@"failIcon"];
                    _dataDicArr = [NSMutableArray array];
                    [_tableView reloadData];
                }else {
                    [self getHudWithAlertMessage:@"筛选成功" WithImageName:@"successIcon"];
                    //将获取的值赋给数据 刷新tableview
                    NSArray *arr = response[@"jingjiren"];
                    _dataDicArr = [NSMutableArray arrayWithArray:arr];
                    [_tableView reloadData];
                }
            } failure:^(NSError *error) {
                [self getHudWithAlertMessage:@"服务器异常，请重试" WithImageName:@"failIcon"];
            }];
        }else {
            //获取第一页
            NSDictionary *dic = @{@"page": @"1", @"shun":@"1"};
            [NetRequest getWithURLString:agentUrlString parameters:dic progress:^(NSProgress *progress) {
                
            } success:^(id response) {
                if ([[response[@"flag"] description] isEqualToString:@"0"]) {
                    [self getHudWithAlertMessage:@"暂无信息" WithImageName:@"failIcon"];
                    _dataDicArr = [NSMutableArray array];
                    [_tableView reloadData];
                }else {
                    [self getHudWithAlertMessage:@"筛选成功" WithImageName:@"successIcon"];
                    //将获取的值赋给数据 刷新tableview
                    NSArray *arr = response[@"jingjiren"];
                    _dataDicArr = [NSMutableArray arrayWithArray:arr];
                    [_tableView reloadData];
                }
            } failure:^(NSError *error) {
                [self getHudWithAlertMessage:@"服务器异常，请重试" WithImageName:@"failIcon"];
            }];
        }
    }else {
        currentStr = [currentStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
        if (_rankTag == 1) {
            NSString *urlStr = [NSString stringWithFormat:@"%@page/1/qu/%@/", agentUrlString, currentStr];
            [NetRequest getWithURLString:urlStr parameters:nil progress:^(NSProgress *progress) {
                
            } success:^(id response) {
                if ([[response[@"flag"] description] isEqualToString:@"0"]) {
                    [self getHudWithAlertMessage:@"暂无信息" WithImageName:@"failIcon"];
                    _dataDicArr = [NSMutableArray array];
                    [_tableView reloadData];
                }else {
                    [self getHudWithAlertMessage:@"筛选成功" WithImageName:@"successIcon"];
                    //将获取的值赋给数据 刷新tableview
                    NSArray *arr = response[@"jingjiren"];
                    _dataDicArr = [NSMutableArray arrayWithArray:arr];
                    [_tableView reloadData];
                }
            } failure:^(NSError *error) {
                [self getHudWithAlertMessage:@"服务器异常，请重试" WithImageName:@"failIcon"];
            }];
        }else {
            NSString *urlStr = [NSString stringWithFormat:@"%@shun/1/page/1/qu/%@/", agentUrlString, currentStr];
            [NetRequest getWithURLString:urlStr parameters:nil progress:^(NSProgress *progress) {
                
            } success:^(id response) {
                if ([[response[@"flag"] description] isEqualToString:@"0"]) {
                    [self getHudWithAlertMessage:@"暂无信息" WithImageName:@"failIcon"];
                    _dataDicArr = [NSMutableArray array];
                    [_tableView reloadData];
                }else {
                    [self getHudWithAlertMessage:@"筛选成功" WithImageName:@"successIcon"];
                    //将获取的值赋给数据 刷新tableview
                    NSArray *arr = response[@"jingjiren"];
                    _dataDicArr = [NSMutableArray arrayWithArray:arr];
                    [_tableView reloadData];
                }
            } failure:^(NSError *error) {
                [self getHudWithAlertMessage:@"服务器异常，请重试" WithImageName:@"failIcon"];
            }];
        }
    }
    _areaTag = 2;
    _page = 1;
    [self setFooterRefresh];
}

- (void)getHudWithAlertMessage:(NSString *)alertMessage WithImageName:(NSString *)imageName {
    UIImage *image = [[UIImage imageNamed:imageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    _selectHud.mode = MBProgressHUDModeCustomView;
    _selectHud.customView = imageView;
    _selectHud.label.text = alertMessage;
    [_selectHud hideAnimated:YES afterDelay:1];
}
#pragma mark - 数据请求部分
//请求数据
- (void)handleData {
    //获取第一页
    NSDictionary *dic = @{@"page": [NSString stringWithFormat:@"%ld", (long)_page]};
    [NetRequest getWithURLString:agentUrlString parameters:dic progress:^(NSProgress *progress) {
        
    } success:^(id response) {
        if ([[response[@"flag"] description] isEqualToString:@"1"]) {
            _hud.label.text = @"加载成功";
            [_hud hideAnimated:YES afterDelay:1];
            //将获取的值赋给数据 刷新tableview
            NSArray *arr = response[@"jingjiren"];
            _dataDicArr = [NSMutableArray arrayWithArray:arr];
            [_tableView reloadData];
        }else {
            _hud.label.text = @"加载失败";
            [_hud hideAnimated:YES afterDelay:1];
        }
    } failure:^(NSError *error) {
        _hud.label.text = @"服务器异常，请重试";
        [_hud hideAnimated:YES afterDelay:1];
    }];
    
}

//上拉加载
- (void)setFooterRefresh {
    [self.tableView.mj_footer beginRefreshing];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        //每刷一次 请求的页数叠加
        _page++;
        if (_rankTag == 1 && _areaTag == 1) {
            NSDictionary *dic = @{@"page": [NSString stringWithFormat:@"%ld", (long)_page]};
            //网络请求数据
            [NetRequest getWithURLString:@"http://aizufang.hrbzjwl.com/tp/api.php/Jingjiren/index" parameters:dic progress:^(NSProgress *progress) {
            } success:^(id response) {
                //没有数据 显示已加载全部数据
                if ([[response[@"flag"] description] isEqualToString:@"0"]) {
                    [self.tableView.mj_footer endRefreshingWithNoMoreData];
                }
                //否则数据一直叠加并且刷新tableview显示
                else {
                    for (NSDictionary *dic in response[@"jingjiren"]) {
                        [_dataDicArr addObject:dic];
                    }
                    [_tableView reloadData];
                    [self.tableView.mj_footer endRefreshing];
                }
            } failure:^(NSError *error) {
                NSLog(@"%@", error);
            }];
        }else if (_areaTag == 1 && _rankTag == 2) {
            NSDictionary *dic = @{@"page": [NSString stringWithFormat:@"%ld", (long)_page], @"shun": @"1"};
            [NetRequest getWithURLString:agentUrlString parameters:dic progress:^(NSProgress *progress) {
                
            } success:^(id response) {
                if ([[response[@"flag"] description] isEqualToString:@"0"]) {
                    [self.tableView.mj_footer endRefreshingWithNoMoreData];
                }else {
                    for (NSDictionary *dic in response[@"jingjiren"]) {
                        [_dataDicArr addObject:dic];
                    }
                    [_tableView reloadData];
                    [self.tableView.mj_footer endRefreshing];
                }
            } failure:^(NSError *error) {
                NSLog(@"%@", error);
            }];
        }else {
            if ([_areaButton.titleLabel.text isEqualToString:@"区域"]) {
                if (_rankTag == 1) {
                    //获取第一页
                    NSDictionary *dic = @{@"page": [NSString stringWithFormat:@"%ld", (long)_page]};
                    [NetRequest getWithURLString:agentUrlString parameters:dic progress:^(NSProgress *progress) {
                        
                    } success:^(id response) {
                        if ([[response[@"flag"] description] isEqualToString:@"0"]) {
                            [self.tableView.mj_footer endRefreshingWithNoMoreData];
                        }else {
                            for (NSDictionary *dic in response[@"jingjiren"]) {
                                [_dataDicArr addObject:dic];
                            }
                            [_tableView reloadData];
                            [self.tableView.mj_footer endRefreshing];
                        }
                    } failure:^(NSError *error) {
                        NSLog(@"%@", error);
                    }];
                }else {
                    //获取第一页
                    NSDictionary *dic = @{@"page": [NSString stringWithFormat:@"%ld", (long)_page], @"shun":@"1"};
                    [NetRequest getWithURLString:agentUrlString parameters:dic progress:^(NSProgress *progress) {
                        
                    } success:^(id response) {
                        if ([[response[@"flag"] description] isEqualToString:@"0"]) {
                            [self.tableView.mj_footer endRefreshingWithNoMoreData];
                        }else {
                            for (NSDictionary *dic in response[@"jingjiren"]) {
                                [_dataDicArr addObject:dic];
                            }
                            [_tableView reloadData];
                            [self.tableView.mj_footer endRefreshing];
                        }
                    } failure:^(NSError *error) {
                        NSLog(@"%@", error);
                    }];
                }
            }else {
             NSString *currentStr = [_areaButton.titleLabel.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
                if (_rankTag == 1) {
                    NSString *urlStr = [NSString stringWithFormat:@"%@page/%ld/qu/%@/", agentUrlString, (long)_page, currentStr];
                    [NetRequest getWithURLString:urlStr parameters:nil progress:^(NSProgress *progress) {
                        
                    } success:^(id response) {
                        if ([[response[@"flag"] description] isEqualToString:@"0"]) {
                            [self.tableView.mj_footer endRefreshingWithNoMoreData];
                        }else {
                            for (NSDictionary *dic in response[@"jingjiren"]) {
                                [_dataDicArr addObject:dic];
                            }
                            [_tableView reloadData];
                            [self.tableView.mj_footer endRefreshing];
                        }
                    } failure:^(NSError *error) {
                        NSLog(@"%@", error);
                    }];
                }else {
                    NSString *urlStr = [NSString stringWithFormat:@"%@shun/1/page/%ld/qu/%@/", agentUrlString, (long)_page, currentStr];
                    [NetRequest getWithURLString:urlStr parameters:nil progress:^(NSProgress *progress) {
                        
                    } success:^(id response) {
                        if ([[response[@"flag"] description] isEqualToString:@"0"]) {
                            [self.tableView.mj_footer endRefreshingWithNoMoreData];
                        }else {
                            for (NSDictionary *dic in response[@"jingjiren"]) {
                                [_dataDicArr addObject:dic];
                            }
                            [_tableView reloadData];
                            [self.tableView.mj_footer endRefreshing];
                        }
                    } failure:^(NSError *error) {
                        NSLog(@"%@", error);
                    }];
                }
            }
        }
    }];
}

//下拉刷新
- (void)setHeaderRefresh {
    [self.tableView.mj_header beginRefreshing];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        if (_rankTag == 1 && _areaTag == 1) {
            [self handleData];
        }
        if (_areaTag == 2) {
            [self getSelectDataWithCurrentStr:_currenSelectStr];
        }
        if (_rankTag == 2){
            [self getRankDataWithRankTag:_rankTag];
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.tableView.mj_header endRefreshing];
        });
        
    }];
}

#pragma mark - tableView
//初始化
- (void)initTableView {
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 105, self.view.frame.size.width, kScreenHeight - 105) style:UITableViewStylePlain];
    [self.view addSubview:_tableView];
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    //tableview尾部视图设置为0
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [_tableView registerClass:[AgentListNoSlideTableViewCell class] forCellReuseIdentifier:cellID];
    
}
//行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataDicArr.count;
}
//cell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    AgentListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
//    if (!cell) {
//        cell = [[AgentListTableViewCell alloc] init];
//    }
    AgentListNoSlideTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    NSDictionary *dic = _dataDicArr[indexPath.row];
    AgentListModel *model = [AgentListModel modelWithDic:dic];
    cell.model = model;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //cell.delegate = self;
    return cell;
}
//cell点击事件
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    AgentDetailViewController *agentVC = [[AgentDetailViewController alloc] init];
    NSDictionary *dic = _dataDicArr[indexPath.row];
    AgentListModel *model = [AgentListModel modelWithDic:dic];
    agentVC.model = model;
    [self.navigationController pushViewController:agentVC animated:YES];
    //没有侧滑的cell 点击进入详情
//<<<<<<< .merge_file_acq01N
//    if (self.mySelectCell == nil) {
//        AgentDetailViewController *agentVC = [[AgentDetailViewController alloc] init];
//        NSDictionary *dic = _dataDicArr[indexPath.row];
//        AgentListModel *model = [AgentListModel modelWithDic:dic];
//        agentVC.model = model;
//        agentVC.type = Agent;
//        [self.navigationController pushViewController:agentVC animated:YES];
//    }else {
//        //否则收回cell 点击当前的cell 收回会有一个延时
//        AgentListTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//        if (self.mySelectCell == cell) {
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                [UIView animateWithDuration:0.25 animations:^{
//                    self.mySelectCell.myVIew.frame = CGRectMake(0, 0, cell.frame.size.width + 160, cell.frame.size.height);
//                }];
//                self.mySelectCell = nil;
//            });
//        }else {
//            [UIView animateWithDuration:0.25 animations:^{
//                self.mySelectCell.myVIew.frame = CGRectMake(0, 0, cell.frame.size.width + 160, cell.frame.size.height);
//            }];
//            self.mySelectCell = nil;
//        }
//    }
//=======
//    if (self.mySelectCell == nil) {
//        AgentDetailViewController *agentVC = [[AgentDetailViewController alloc] init];
//        NSDictionary *dic = _dataDicArr[indexPath.row];
//        AgentListModel *model = [AgentListModel modelWithDic:dic];
//        agentVC.model = model;
//        [self.navigationController pushViewController:agentVC animated:YES];
//    }else {
//        //否则收回cell 点击当前的cell 收回会有一个延时
//        AgentListTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//        if (self.mySelectCell == cell) {
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                [UIView animateWithDuration:0.25 animations:^{
//                    self.mySelectCell.myVIew.frame = CGRectMake(0, 0, cell.frame.size.width + 160, cell.frame.size.height);
//                }];
//                self.mySelectCell = nil;
//            });
//        }else {
//            [UIView animateWithDuration:0.25 animations:^{
//                self.mySelectCell.myVIew.frame = CGRectMake(0, 0, cell.frame.size.width + 160, cell.frame.size.height);
//            }];
//            self.mySelectCell = nil;
//        }
//    }
//>>>>>>> .merge_file_Bntp4r
}
//行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}
//滑动tableview cell也收回
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    if (self.mySelectCell == nil) {
        return;
    }else {
        [UIView animateWithDuration:0.25 animations:^{
            self.mySelectCell.myVIew.frame = CGRectMake(0, 0, self.view.frame.size.width + 160, self.mySelectCell.frame.size.height);
        }];
        self.mySelectCell = nil;
    }
}


#pragma mark - cell侧滑事件
//左侧滑
- (void)leftSwipe:(AgentListTableViewCell *)obj {
    //没有cell侧滑 当前cell就侧滑 赋值给属性cell
    if (self.mySelectCell == nil) {
        [UIView animateWithDuration:0.25 animations:^{
            obj.myVIew.frame = CGRectMake(-160, 0, obj.frame.size.width + 160, obj.frame.size.height);
        }];
        self.mySelectCell = obj;
        
    }else {
        //如果左滑当前cell cell不收回
        if (self.mySelectCell == obj) {
            return;
        }
        [UIView animateWithDuration:0.25 animations:^{
            self.mySelectCell.myVIew.frame = CGRectMake(0, 0, obj.frame.size.width + 160, obj.frame.size.height);
        }];
        self.mySelectCell = nil;
    }
}

//右侧滑
- (void)rightSwipe:(AgentListTableViewCell *)obj {
    //没有cell侧滑 直接返回 有侧滑的 收回cell
    if (self.mySelectCell == nil) {
        return;
    }else {
        [UIView animateWithDuration:0.25 animations:^{
            self.mySelectCell.myVIew.frame = CGRectMake(0, 0, obj.frame.size.width + 160, obj.frame.size.height);
        }];
        self.mySelectCell = nil;
    }
}

//侧滑点击事件
-(void)passButton:(ImageViewAndLabelButton *)button {
    if (button.tag == 1000) {
        if (button.selected) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"关注成功" preferredStyle:UIAlertControllerStyleAlert];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self dismissViewControllerAnimated:YES completion:nil];
            });
            [self presentViewController:alertController animated:YES completion:nil];
            
        }else {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"取消关注" preferredStyle:UIAlertControllerStyleAlert];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self dismissViewControllerAnimated:YES completion:nil];
            });
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }else {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:@"%ld", (long)button.tag] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *callAction = [UIAlertAction actionWithTitle:@"拨打" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSLog(@"拨打电话接口");
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        [alertController addAction:callAction];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
