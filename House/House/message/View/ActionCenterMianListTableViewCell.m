//
//  ActionCenterMianListTableViewCell.m
//  House
//
//  Created by 张垚 on 16/9/9.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "ActionCenterMianListTableViewCell.h"
#import "ActionCenterListModel.h"

@interface ActionCenterMianListTableViewCell ()

@property (nonatomic, strong) UIImageView *dotImage;
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UIImageView *mainImageView;
@property (nonatomic, strong) UILabel *titleLabel;

@end




@implementation ActionCenterMianListTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _dotImage = [[UIImageView alloc] init];
        [self.contentView addSubview:_dotImage];
        _lineView = [[UIView alloc] init];
        [self.contentView addSubview:_lineView];
        _timeLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_timeLabel];
        _mainImageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_mainImageView];
        _titleLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_titleLabel];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self.contentView;
    [_dotImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(superView);
        make.left.mas_equalTo(superView.mas_left).offset(15);
        make.width.and.height.mas_equalTo(15);
    }];
    //_dotImage.backgroundColor = [UIColor redColor];
    _dotImage.image = [UIImage imageNamed:@"grayDot"];
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_dotImage.mas_bottom);
        make.centerX.equalTo(_dotImage.mas_centerX);
        make.bottom.mas_equalTo(superView.mas_bottom);
        make.width.mas_equalTo(3);
    }];
    _lineView.backgroundColor = [UIColor colorWithRed:220 / 255.0 green:220 / 255.0 blue:220 / 255.0 alpha:1.0];
    [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_dotImage.mas_right).offset(10);
        make.top.mas_equalTo(_dotImage.mas_bottom).offset(5);
        make.height.mas_equalTo(@25);
        make.right.mas_equalTo(superView.mas_right);
    }];
    _titleLabel.textColor = [UIColor lightGrayColor];
    [_mainImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_timeLabel.mas_bottom).offset(5);
        make.left.mas_equalTo(_dotImage.mas_right).offset(10);
        make.right.mas_equalTo(superView.mas_right).offset(-30);
        make.bottom.mas_equalTo(superView.mas_bottom).offset(-20);
    }];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_mainImageView.mas_left);
        make.bottom.mas_equalTo(_mainImageView.mas_bottom);
        make.width.mas_equalTo(_mainImageView.mas_width);
        make.height.mas_equalTo(50);
    }];
    _titleLabel.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
    _titleLabel.textColor = [UIColor whiteColor];
    _titleLabel.numberOfLines = 0;
}

- (void)setModel:(ActionCenterListModel *)model {
    if (_model != model) {
        _model = model;
    }
    _timeLabel.text = model.addtime;
    [_mainImageView setImageWithURL:[NSURL URLWithString:model.img] placeholderImage:[UIImage imageNamed:@"placeHolderPic"]];
    _titleLabel.text = model.title;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
