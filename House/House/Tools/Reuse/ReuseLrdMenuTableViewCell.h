//
//  ReuseLrdMenuTableViewCell.h
//  House
//
//  Created by 吕品 on 16/6/18.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LrdSuperMenu;
@interface ReuseLrdMenuTableViewCell : UITableViewCell
@property (nonatomic, strong) LrdSuperMenu *menu;
@end
