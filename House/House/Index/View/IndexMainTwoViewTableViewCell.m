//
//  IndexMainTwoViewTableViewCell.m
//  House
//
//  Created by 张垚 on 16/9/27.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "IndexMainTwoViewTableViewCell.h"
#import <AFNetworking/UIButton+AFNetworking.h>


@implementation IndexMainTwoViewTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
   self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.leftButton.tag = 1;
        [self.contentView addSubview:self.leftButton];
        self.midView = [[UIView alloc] init];
        [self.contentView addSubview:self.midView];
        self.rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.rightButton.tag = 2;
        [self.contentView addSubview:self.rightButton];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self.contentView;
    [_leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left);
        make.top.equalTo(superView.mas_top);
        make.bottom.equalTo(superView.mas_bottom);
        make.right.equalTo(superView.mas_centerX).offset(-1);
    }];
    [_midView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(superView.mas_height);
        make.top.equalTo(superView.mas_top);
        make.left.equalTo(_leftButton.mas_right);
        make.width.equalTo(@2);
    }];
    _midView.backgroundColor = [UIColor colorWithRed:0.921 green:0.921 blue:0.921 alpha:1];
    [_rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top);
        make.right.equalTo(superView.mas_right);
        make.left.equalTo(_midView.mas_right);
        make.height.equalTo(superView.mas_height);
    }];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
