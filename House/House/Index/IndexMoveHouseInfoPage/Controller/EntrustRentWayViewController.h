//
//  EntrustRentWayViewController.h
//  House
//
//  Created by 张垚 on 16/7/25.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "BaseViewController.h"

typedef void(^chooseInfoStrBlock)(NSString *wayStr);

@interface EntrustRentWayViewController : BaseViewController
@property (nonatomic, strong) NSString *currentStr;
@property (nonatomic, strong) NSArray *wayArr;
@property (nonatomic, copy) chooseInfoStrBlock chooseInfoBlock;
- (void)passInfoToPreviousPage:(chooseInfoStrBlock)infoBlock;

@end
