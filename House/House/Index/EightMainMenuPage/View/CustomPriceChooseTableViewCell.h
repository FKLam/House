//
//  CustomPriceChooseTableViewCell.h
//  House
//
//  Created by 张垚 on 16/8/12.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomPriceChooseTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *descriptionLabel;
@property (nonatomic, strong) UITextField *minPriceTextfield;
@property (nonatomic, strong) UIView *midView;
@property (nonatomic, strong) UITextField *maxPriceTextfiled;
@property (nonatomic, strong) UILabel *unitLabel;
@property (nonatomic, strong) UIButton *sureButton;
@end
