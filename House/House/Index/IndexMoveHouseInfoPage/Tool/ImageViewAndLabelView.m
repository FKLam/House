//
//  ImageViewAndLabelView.m
//  House
//
//  Created by 张垚 on 16/6/27.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "ImageViewAndLabelView.h"

@implementation ImageViewAndLabelView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _logoImageView = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:_logoImageView];
        _descriptionLabel = [[UILabel alloc] init];
        [self addSubview:_descriptionLabel];
    }
    return self;
}


- (void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self;
    [_logoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top);
        make.left.equalTo(superView.mas_left);
        make.width.equalTo(superView.mas_width);
        make.height.equalTo(_logoImageView.mas_width);
    }];
    _logoImageView.backgroundColor = [UIColor redColor];
    _logoImageView.layer.cornerRadius = _logoImageView.frame.size.width / 2;
    _logoImageView.layer.masksToBounds = YES;
    [_descriptionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left);
        make.width.equalTo(superView.mas_width);
        make.bottom.equalTo(superView.mas_bottom);
        make.top.equalTo(_logoImageView.mas_bottom);
    }];
    _descriptionLabel.textAlignment = NSTextAlignmentCenter;
    _descriptionLabel.textColor = [UIColor colorWithRed:0.4415 green:0.4372 blue:0.4458 alpha:1.0];
    _descriptionLabel.font = [UIFont systemFontOfSize:15];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
