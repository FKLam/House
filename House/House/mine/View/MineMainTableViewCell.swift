//
//  MineMainTableViewCell.swift
//  House
//
//  Created by 吕品 on 16/7/11.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

import UIKit

class MineMainTableViewCell: UITableViewCell {

    let mixView = MineMainCellView.init(frame: CGRect.zero)

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(mixView)
        mixView.snp.makeConstraints { (make) in
            make.edges.equalTo(UIEdgeInsetsMake(5, 5, -5, 5))
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
