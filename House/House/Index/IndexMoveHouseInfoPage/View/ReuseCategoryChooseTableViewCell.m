//
//  ReuseCategoryChooseTableViewCell.m
//  IndexMoveHousePage
//
//  Created by 张垚 on 16/6/12.
//  Copyright © 2016年 众杰网络. All rights reserved.
//

#import "ReuseCategoryChooseTableViewCell.h"
#import "ReuseDescriptionChooseCollectionViewCell.h"

@interface ReuseCategoryChooseTableViewCell ()<UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, strong) NSArray *unSelectImageArr;
@property (nonatomic, strong) NSArray *selectImageArr;
@property (nonatomic, strong) NSArray *matchStrArr;
@property (nonatomic, strong) NSMutableArray *indexArr;
@end


@implementation ReuseCategoryChooseTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _unSelectImageArr = @[@"all-0", @"bed-0", @"refrigerator-0", @"television-0", @"air-condition-0", @"furniture-0", @"wifi-0", @"elevator-0", @"calorifier-0", @"washer-0", @"cook-0"];
        _selectImageArr = @[@"all-1", @"bed-1", @"refrigerator-1", @"television-1", @"air-condition-1", @"furniture-1", @"wifi-1", @"elevator-1", @"calorifier-1", @"washer-1", @"cook-1"];
        _matchStrArr = @[@",全选",@",床", @",冰箱", @",电视", @",空调", @",家具", @",宽带", @",电梯", @",热水器", @",洗衣机", @",可做饭"];
        _indexArr = [NSMutableArray arrayWithCapacity:0];
        _descriptionLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_descriptionLabel];
        UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
        flow.itemSize = CGSizeMake(50, 25);
        flow.minimumInteritemSpacing = 5;
        flow.minimumLineSpacing = 5;
        _categoryChooseCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, 0, 0) collectionViewLayout:flow];
        _categoryChooseCollectionView.backgroundColor = [UIColor whiteColor];
        _categoryChooseCollectionView.scrollEnabled = NO;
        _categoryChooseCollectionView.showsHorizontalScrollIndicator = NO;
        [self.contentView addSubview:_categoryChooseCollectionView];
        _categoryChooseCollectionView.delegate = self;
        _categoryChooseCollectionView.dataSource = self;
        [_categoryChooseCollectionView registerClass:[ReuseDescriptionChooseCollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([ReuseDescriptionChooseCollectionViewCell class])];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self.contentView;
    [_descriptionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(5);
        make.left.equalTo(superView.mas_left).offset(5);
        make.size.mas_equalTo(CGSizeMake(60, 30));
    }];
    _descriptionLabel.adjustsFontSizeToFitWidth = YES;
    _descriptionLabel.font = [UIFont systemFontOfSize:15];
    [_categoryChooseCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(10);
        make.left.equalTo(_descriptionLabel.mas_right).offset(5);
        make.bottom.equalTo(superView.mas_bottom).offset(-10);
        make.right.equalTo(superView.mas_right).offset(-10);
    }];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 11;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ReuseDescriptionChooseCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([ReuseDescriptionChooseCollectionViewCell class]) forIndexPath:indexPath];
    cell.descriptionImageView.image = [UIImage imageNamed:_unSelectImageArr[indexPath.item]];
    cell.isSelect = NO;
    cell.layer.masksToBounds = YES;
    cell.layer.cornerRadius = 4;
    return cell;
    
}
//cell点击事件
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    //获取当前点击的cell
    ReuseDescriptionChooseCollectionViewCell *cell = (ReuseDescriptionChooseCollectionViewCell  *)[collectionView cellForItemAtIndexPath:indexPath];
    //状态取反
    cell.isSelect = !cell.isSelect;
    //如果item选中
        if (cell.isSelect) {
            //切换图片
            cell.descriptionImageView.image = [UIImage imageNamed:_selectImageArr[indexPath.item]];
            //如果点击的为第一个item 所有item的图片都更改
            if (indexPath.item == 0) {
                [self imageForCellWithState:cell.isSelect WithImageArr:_selectImageArr];
            }else {
                //点击添加当前的item
                [_indexArr addObject:[NSString stringWithFormat:@"%ld", (long)indexPath.item]];
                [self submitStrWithIndexArr:_indexArr];
            }
        }
    //取消选中状态的时候
        else {
            //如果是第一个item 图片全部切换
            if (indexPath.item == 0) {
                [self imageForCellWithState:cell.isSelect WithImageArr:_unSelectImageArr];
            }
            //如果是其他的item
            else {
                //第一个item变为未选中状态
                [self stateForAllSelect];
                //移除第一个
                if ([_indexArr containsObject:@"0"]) {
                    [_indexArr removeObject:@"0"];
                }
                //移除当前点击的item
                [_indexArr removeObject:[NSString stringWithFormat:@"%ld", (long)indexPath.item]];
                [self submitStrWithIndexArr:_indexArr];
                //图片切换
                cell.descriptionImageView.image = [UIImage imageNamed:_unSelectImageArr[indexPath.item]];
            }
        }
}
//点击第一个item
- (void)imageForCellWithState:(BOOL)cellState WithImageArr:(NSArray *)imageArr {
    //移除数组中所有的item
    [_indexArr removeAllObjects];
    //遍历切换图片
    for (int i = 0; i < imageArr.count; i++) {
        NSIndexPath *index = [NSIndexPath indexPathForItem:i inSection:0];
ReuseDescriptionChooseCollectionViewCell *cell = (ReuseDescriptionChooseCollectionViewCell  *)[_categoryChooseCollectionView cellForItemAtIndexPath:index];
        cell.descriptionImageView.image = [UIImage imageNamed:imageArr[i]];
        //如果选中的状态 将所有item都加上
        if (cellState) {
            [_indexArr addObject:[NSString stringWithFormat:@"%d", i]];
        }
        //切换所有的item状态
        cell.isSelect = cellState;
    }
    [self submitStrWithIndexArr:_indexArr];
}
//点击其他item 第一个item的变化
- (void)stateForAllSelect {
    NSIndexPath *index = [NSIndexPath indexPathForItem:0 inSection:0];
    ReuseDescriptionChooseCollectionViewCell *cell = (ReuseDescriptionChooseCollectionViewCell  *)[_categoryChooseCollectionView cellForItemAtIndexPath:index];
    cell.descriptionImageView.image = [UIImage imageNamed:_unSelectImageArr[0]];
    cell.isSelect = NO;

}
//将存储item的数组传到这里 找到对应的字符串进行拼接 字符串做修改
- (NSString *)submitStrWithIndexArr:(NSMutableArray *)indexArr {
    //每次进来字符串初始化
    _matchStr = [[NSMutableString alloc] init];
    //遍历数组 拼接字符串
    for (int i = 0; i < indexArr.count; i++) {
        NSInteger m = [indexArr[i] integerValue];
        [_matchStr appendString:_matchStrArr[m]];
    }
    //根据条件 截取字符串 返回最终截取的字符串
    if ([_matchStr hasPrefix:@",全选"]) {
       _matchStr = (NSMutableString *)[_matchStr substringFromIndex:3];
        if ([_matchStr hasPrefix:@","]) {
            _matchStr = (NSMutableString *)[_matchStr substringFromIndex:1];
        }
        return _matchStr;
        
    }else {
        if ([_matchStr hasPrefix:@","]) {
            _matchStr = (NSMutableString *)[_matchStr substringFromIndex:1];
        }
        return _matchStr;
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
