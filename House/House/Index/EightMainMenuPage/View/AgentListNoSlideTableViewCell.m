//
//  AgentListNoSlideTableViewCell.m
//  House
//
//  Created by 张垚 on 16/9/2.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "AgentListNoSlideTableViewCell.h"
#import "AgentListModel.h"



@implementation AgentListNoSlideTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _headImageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_headImageView];
        _nameLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_nameLabel];
        _dividerView = [[UIView alloc] init];
        [self.contentView addSubview:_dividerView];
        _yearLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_yearLabel];
        _storesLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_storesLabel];
        _instructionView = [[UIView alloc] init];
        [self.contentView addSubview:_instructionView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self.contentView;
    [_headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@60);
        make.centerY.equalTo(superView.mas_centerY);
        make.left.equalTo(superView.mas_left).offset(10);
        make.width.equalTo(_headImageView.mas_height);
    }];
    _headImageView.layer.cornerRadius = _headImageView.frame.size.width / 2;
    _headImageView.layer.masksToBounds = YES;
    
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_headImageView.mas_right).offset(10);
        make.centerY.equalTo(superView.mas_centerY);
        make.width.equalTo(@60);
        make.height.equalTo(@20);
    }];
    [_dividerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_nameLabel.mas_right).offset(10);
        make.height.equalTo(@25);
        make.width.equalTo(@1);
        make.centerY.equalTo(superView.mas_centerY);
    }];
    [_yearLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_dividerView.mas_right).offset(10);
        make.bottom.equalTo(superView.mas_centerY);
        make.right.equalTo(_instructionView.mas_left).offset(-2);
        make.height.equalTo(@20);
    }];
    [_storesLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_dividerView.mas_right).offset(10);
        make.top.equalTo(superView.mas_centerY);
        make.right.equalTo(_instructionView.mas_left).offset(-2);
        make.height.equalTo(@20);
    }];
    [_instructionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(superView.mas_right);
        make.centerY.equalTo(superView.mas_centerY);
        make.height.equalTo(@25);
        make.width.equalTo(@3);
    }];
    _dividerView.backgroundColor = [UIColor lightGrayColor];
    _instructionView.backgroundColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0];
}

- (void)setModel:(AgentListModel *)model {
    if (model != _model) {
        _model = model;
    }
    _nameLabel.text = model.truename;
    [_headImageView setImageWithURL:[NSURL URLWithString:model.thumb] placeholderImage:[UIImage imageNamed:@"agentPlaceHolder"]];
    NSMutableAttributedString *yearStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"入职时间 %@", model.career]];
    [yearStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, 4)];
    _yearLabel.attributedText = yearStr;
    NSMutableAttributedString *storeStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"所属门店 %@", model.company]];
    [storeStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, 4)];
    _storesLabel.attributedText = storeStr;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
