//
//  ImageViewAndLabelView.h
//  House
//
//  Created by 张垚 on 16/6/27.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewAndLabelView : UIView

@property (nonatomic, strong) UIButton *logoImageView;
@property (nonatomic, strong) UILabel *descriptionLabel;

@end
