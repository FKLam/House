//
//  AgentDetailMianInfoCollectionViewCell.m
//  House
//
//  Created by 张垚 on 16/7/20.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "AgentDetailMianInfoCollectionViewCell.h"
#import "AgentListModel.h"



@interface AgentDetailMianInfoCollectionViewCell ()
@property (nonatomic, strong) UIImageView *backImageView;
@property (nonatomic, strong) UIImageView *headImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *yearLabel;
@property (nonatomic, strong) UILabel *storeLabel;
@property (nonatomic, strong) UIView *segView;

@end



@implementation AgentDetailMianInfoCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _backImageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_backImageView];
        _headImageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_headImageView];
        _nameLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_nameLabel];
        _segView = [[UIView alloc] init];
        [self.contentView addSubview:_segView];
        _yearLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_yearLabel];
        _storeLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_storeLabel];
    }
    return self;
}

- (void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    UIView *superView = self.contentView;
    _backImageView.frame = self.contentView.frame;
    _backImageView.image = [UIImage imageNamed:@"placeHolderPic"];
    [_headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(30);
        make.size.mas_equalTo(CGSizeMake(70, 70));
        make.centerX.equalTo(superView.mas_centerX);
    }];
    _headImageView.layer.cornerRadius = 35;
    _headImageView.layer.masksToBounds = YES;
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_segView.mas_left).offset(-10);
        make.width.equalTo(@80);
        make.top.equalTo(_headImageView.mas_bottom).offset(30);
        make.height.equalTo(@20);
    }];
    _nameLabel.textColor = [UIColor whiteColor];
    [_segView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(superView.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(1, 30));
        make.centerY.equalTo(_nameLabel.mas_centerY);
    }];
    _segView.backgroundColor = [UIColor lightGrayColor];
    [_yearLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_segView.mas_right).offset(10);
        make.bottom.equalTo(_nameLabel.mas_centerY);
        make.height.equalTo(@20);
        make.right.equalTo(superView.mas_right).offset(-10);
    }];
    _yearLabel.textColor = [UIColor whiteColor];
    [_storeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_segView.mas_right).offset(10);
        make.top.equalTo(_nameLabel.mas_centerY);
        make.height.equalTo(@20);
        make.right.equalTo(superView.mas_right).offset(-10);
    }];
    _storeLabel.textColor = [UIColor whiteColor];
}

- (void)setModel:(AgentListModel *)model {
    if (model != _model) {
        _model = model;
    }
    [_headImageView setImageWithURL:[NSURL URLWithString:model.thumb] placeholderImage:[UIImage imageNamed:@"agentPlaceHolder"]];
    _nameLabel.text = model.truename;
    _yearLabel.text = [NSString stringWithFormat:@"从业年限: %@", model.career];
    _storeLabel.text = [NSString stringWithFormat:@"所属门店: %@", model.company];
}


@end
