//
//  MoveHousePushTableViewCell.h
//  MoveHouse
//
//  Created by 张垚 on 16/6/7.
//  Copyright © 2016年 众杰网络. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LabelAndTextFieldView;
@interface MoveHousePushTableViewCell : UITableViewCell

@property (nonatomic, strong) LabelAndTextFieldView *inputView;
@property (nonatomic, strong) UIButton *pushButton;
@property (nonatomic, assign) BOOL isPush;

@end
