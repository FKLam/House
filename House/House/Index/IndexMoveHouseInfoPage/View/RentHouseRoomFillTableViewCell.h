//
//  RentHouseRoomFillTableViewCell.h
//  IndexMoveHousePage
//
//  Created by 张垚 on 16/6/15.
//  Copyright © 2016年 众杰网络. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MultiLabelAndMultiTextfiledView.h"
@class MultiLabelAndMultiTextfiledView;
@interface RentHouseRoomFillTableViewCell : UITableViewCell

@property (nonatomic, strong) MultiLabelAndMultiTextfiledView *roomView;
@property (nonatomic, strong) UILabel *descriptionLabel;

@end
