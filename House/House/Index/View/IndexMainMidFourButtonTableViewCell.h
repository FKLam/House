//
//  IndexMainMidFourButtonTableViewCell.h
//  House
//
//  Created by 张垚 on 16/10/6.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IndexMainMidFourButtonTableViewCellDelegate <NSObject>

- (void)IndexMainMidFourButtonTableViewCellDidSelectCurrentButtonTag:(NSInteger)buttonTag;

@end

@interface LogoAndTextButton : UIButton
@property (nonatomic, strong) UILabel *logoNameLabel;
@property (nonatomic, strong) UIImageView *logoImage;
@end

@interface IndexMainMidFourButtonTableViewCell : UITableViewCell

@property (nonatomic, assign) id <IndexMainMidFourButtonTableViewCellDelegate> delegate;
@end
