//
//  MultiLabelAndMultiTextfiledView.h
//  IndexMoveHousePage
//
//  Created by 张垚 on 16/6/15.
//  Copyright © 2016年 众杰网络. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MultiLabelAndMultiTextfiledView : UIView

@property (nonatomic, strong) UITextField *buildingNumTextfield;
@property (nonatomic, strong) UILabel *buildingNumLabel;
@property (nonatomic, strong) UITextField *unitNumTextfiled;
@property (nonatomic, strong) UILabel *unitNumLabel;
@property (nonatomic, strong) UITextField *roomNumTextField;
@property (nonatomic, strong) UILabel *roomNumLabel;


@end
