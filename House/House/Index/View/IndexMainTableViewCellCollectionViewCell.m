//
//  IndexMainTableViewCellCollectionViewCell.m
//  House
//
//  Created by 吕品 on 16/6/4.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "IndexMainTableViewCellCollectionViewCell.h"
@interface IndexMainTableViewCellCollectionViewCell ()
@property (nonatomic, strong) UIView *grayView;
@end
@implementation IndexMainTableViewCellCollectionViewCell
#pragma mark init
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.headImageView = [UIImageView new];//显示图片
        [self.contentView addSubview:_headImageView];
        _headImageView.contentMode = UIViewContentModeScaleAspectFit;
        self.titleLabel = [UILabel new];//标题
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_titleLabel];
        _titleLabel.font = [UIFont systemFontOfSize:14.0f];

    }
    return self;
}
#pragma mark layout
- (void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    [super applyLayoutAttributes:layoutAttributes];
    UIView *superView = self.contentView;
    [_headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(superView.mas_top).with.offset(indexMainVCTableViewPadding);
        make.left.mas_equalTo(superView.mas_left).with.offset(indexMainVCTableViewPadding);
        make.bottom.mas_equalTo(_titleLabel.mas_top).with.offset(-indexMainVCTableViewPadding);
        make.right.mas_equalTo(superView.mas_right).with.offset(-indexMainVCTableViewPadding);
        make.width.and.height.equalTo(@(superView.frame.size.width - indexMainVCTableViewPadding * 2));
    }];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.bottom.equalTo(superView);
    }];
}
@end
