//
//  HouseTypeDescriptionTableViewCell.h
//  House
//
//  Created by 张垚 on 16/8/5.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HouseTypeDescriptionTableViewCell : UITableViewCell
@property (nonatomic, strong) UILabel *descriptionLabel;
@property (nonatomic, strong) UITextField *minPriceTextfield;
@property (nonatomic, strong) UILabel *roomLabel;
@property (nonatomic, strong) UITextField *maxPriceTextfiled;
@property (nonatomic, strong) UILabel *hallLabel;
@end
