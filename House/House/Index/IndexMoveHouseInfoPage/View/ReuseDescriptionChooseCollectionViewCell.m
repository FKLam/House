//
//  ReuseDescriptionChooseCollectionViewCell.m
//  IndexMoveHousePage
//
//  Created by 张垚 on 16/6/14.
//  Copyright © 2016年 众杰网络. All rights reserved.
//

#import "ReuseDescriptionChooseCollectionViewCell.h"
#import "Masonry.h"


@implementation ReuseDescriptionChooseCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _descriptionImageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_descriptionImageView];
    }
    UILongPressGestureRecognizer *longpress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longpress:)];
    longpress.minimumPressDuration = 1;
    [self.contentView addGestureRecognizer:longpress];
    return self;
}

- (void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    _descriptionImageView.frame = self.contentView.frame;
}

- (void)longpress:(UILongPressGestureRecognizer *)longpress {
    self.block(longpress);
}
@end
