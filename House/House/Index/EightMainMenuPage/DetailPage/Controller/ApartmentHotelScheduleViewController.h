//
//  ApartmentHotelScheduleViewController.h
//  House
//
//  Created by 张垚 on 16/7/21.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "BaseViewController.h"

@interface ApartmentHotelScheduleViewController : BaseViewController

@property (nonatomic, copy) NSString *itemID;

@end
