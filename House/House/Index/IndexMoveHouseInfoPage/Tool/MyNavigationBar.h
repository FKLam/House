//
//  MyNavigationBar.h
//  House
//
//  Created by 张垚 on 16/6/27.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MySearchBar;


@interface MyNavigationBar : UIView

@property (nonatomic, strong) MySearchBar *mySearchBar;
@property (nonatomic, strong) UIButton *cancelButton;
@end
