//
//  SellHouseEntrustViewController.h
//  House
//
//  Created by 张垚 on 16/6/24.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "BaseViewController.h"
@class MinePublishPicModel;
/**
 @brief 卖房委托
 */
@interface SellHouseEntrustViewController : BaseViewController
@property (nonatomic, strong) MinePublishPicModel *model;
@end
