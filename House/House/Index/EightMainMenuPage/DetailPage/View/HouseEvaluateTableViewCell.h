//
//  HouseEvaluateTableViewCell.h
//  House
//
//  Created by 张垚 on 16/7/14.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>
@class UnfoldFrameModel;


@protocol  HouseEvaluateTableViewCellDelegate <NSObject>
@optional
-(void)UnfoldCellDidClickUnfoldBtn:(UnfoldFrameModel *)model;
- (void)callAgentPhoneNumber;

@end



@interface HouseEvaluateTableViewCell : UITableViewCell
//@property (nonatomic, strong) UIButton *moreButton;

@property (nonatomic, strong) UnfoldFrameModel *frameModel;
@property (nonatomic, assign) id <HouseEvaluateTableViewCellDelegate>delegate;

@property (nonatomic, strong) NSDictionary *remarkDic;

@end
