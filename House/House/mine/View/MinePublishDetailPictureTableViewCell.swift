//
//  MinePublishDetailPictureTableViewCell.swift
//  House
//
//  Created by 吕品 on 2016/9/23.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

import UIKit

class MinePublishDetailPictureTableViewCell: UITableViewCell {

    internal let picImgaeView = UIImageView()
    internal let statusLabel = UILabel()
    internal let areaLabel = UILabel()
    internal let locationLabel = UILabel()
    internal var model: MinePublishPicModel {
        set {

            var status = ""
            switch newValue.status! {
            case "0":
                status = "已删除"
                case "1":
                    status = "未通过"
                case "2":
                status = "待审核"
                case "3":
                status = "通过"
                case "4":
                status = "过期"
            default:
                status = "未通过"
            }

            picImgaeView.setImageWith(URL.init(string: newValue.thumb)!, placeholderImage:UIImage.init(named: "MineBackImage"))
            statusLabel.text = "状态:" + status
            areaLabel.text = newValue.houseearm! + "平方米"
            locationLabel.text = newValue.address
        }
        get {
            return self.model
        }
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(picImgaeView)
        picImgaeView.contentMode = .scaleAspectFit
        contentView.addSubview(statusLabel)
        statusLabel.textAlignment = .left
        contentView.addSubview(areaLabel)
        areaLabel.textAlignment = .left
        contentView.addSubview(locationLabel)
        locationLabel.textAlignment = .left
        locationLabel.textColor = .gray
        //MARK: Test
//        picImgaeView.image = UIImage.init(named: "moveHome-3")
//        statusLabel.text = "111"
//        areaLabel.text = "22"
//        locationLabel.text = "dsadad"

        let topPadding = 20
        let leftPaddig = 10
        let height = 150.0 - 40.0
        picImgaeView.snp.makeConstraints { (make) in
            make.top.equalTo(contentView).offset(topPadding)
            make.left.equalTo(contentView).offset(leftPaddig)
            make.bottom.equalTo(contentView).offset(-topPadding)
            make.width.equalTo(contentView).multipliedBy(0.45)
        }
        statusLabel.snp.makeConstraints { (make) in
            make.top.equalTo(picImgaeView)
            make.left.equalTo(picImgaeView.snp.right)
            make.height.equalTo(height / 3)
            make.right.equalTo(contentView)
        }
        areaLabel.snp.makeConstraints { (make) in
            make.top.equalTo(statusLabel.snp.bottom)
            make.left.equalTo(picImgaeView.snp.right)
            make.height.right.equalTo(statusLabel)
        }
        locationLabel.snp.makeConstraints { (make) in
            make.top.equalTo(areaLabel.snp.top)
            make.left.equalTo(picImgaeView.snp.right)
            make.height.equalTo(height)
        }
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
