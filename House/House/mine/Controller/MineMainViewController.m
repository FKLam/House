//
//  MineMainViewController.m
//  House
//
//  Created by 吕品 on 16/5/31.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "MineMainViewController.h"
#import "IndexMainDoubleADTableViewCell.h"
#import "MineTwoButtonTableViewCell.h"
#import "House-Swift.h"
#import "UIButton+AFNetworking.h"
#import "CategorySeviceViewController.h"
@interface MineMainViewController ()<UITableViewDelegate, UITableViewDataSource, BaseCollectionWithTableViewCellDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, copy) NSArray *dataArray;
@end

@implementation MineMainViewController
#pragma mark cyclelife
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.tabbarHidden = NO;
    [self initTableView];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view.
    self.dataArray = @[@[@"委托服务", @"关注的二手房/租房"], @[@"我发布的房源", @"客户中心", @"推广中心"], @[@"用户反馈"]];
}

#pragma mark initsubviews
//初始化tableview
- (void)initTableView {
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight - kTabBarHeight) style:UITableViewStyleGrouped];
    [self.view addSubview:_tableView];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 160)];
    imageView.userInteractionEnabled = YES;
    //header
    UIButton *imageButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [imageButton setImageForState:UIControlStateNormal withURL:[NSURL URLWithString:[PHPHeader manager].headImageReadPHP] placeholderImage:[UIImage imageNamed:@"MineBackButtonImage"]];
    [NetRequest downloadImageWithUrl:[PHPHeader manager].headImageReadPHP button:imageButton placeHolder:[UIImage imageNamed:@"MineBackButtonImage"]];

    NSLog(@"%@", [PHPHeader manager].headImageReadPHP);
//    [imageButton setImage:[UIImage imageNamed:@"MineBackButtonImage"] forState:UIControlStateNormal];
    [imageButton addTarget:self action:@selector(restoreMineInfo) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:imageButton];

    UIButton *logInfoButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [imageView addSubview:logInfoButton];
    NSString *title;
    UIColor *color;
    UserInfoManager *user = [UserInfoManager sharedManager];
    if (user.isLogin) {
        if (user.nickName) {
            title = user.nickName;
        } else {
            title = @"请填写昵称";
        }
    } else {
        title = @"注册/登录";
    }
    [logInfoButton setTitle:title forState:UIControlStateNormal];
    logInfoButton.titleLabel.adjustsFontSizeToFitWidth = true;
    logInfoButton.layer.masksToBounds = YES;
    logInfoButton.layer.borderWidth = 1.0f;
    logInfoButton.layer.borderColor = [UIColor whiteColor].CGColor;
    logInfoButton.tintColor = [UIColor whiteColor];
    [logInfoButton addTarget:self action:@selector(loginOrReg) forControlEvents:UIControlEventTouchUpInside];
    CGFloat padding = 15;
    CGFloat loginHeight = 30;
    [imageButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(imageView.mas_top).with.offset(padding * 2);
        make.centerX.mas_equalTo(imageView);
        make.width.and.height.mas_equalTo(160 - padding * 4 - loginHeight);
        make.bottom.mas_equalTo(logInfoButton.mas_top).with.offset(-padding);
    }];
    imageButton.layer.masksToBounds = true;
    imageButton.layer.cornerRadius = (160 - padding * 4 - loginHeight) / 2;
    imageButton.layer.borderWidth = 1.0;
    imageButton.layer.borderColor = [UIColor whiteColor].CGColor;
    [logInfoButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(imageView);
        make.height.mas_equalTo(loginHeight);
        make.width.mas_equalTo(80);
        make.bottom.mas_equalTo(imageView.mas_bottom).with.offset(-padding);
    }];
    imageView.image = [UIImage imageNamed:@"MineBackImage"];
    _tableView.tableHeaderView = imageView;
    //footer
    UIView *footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 100)];
    UIButton *logOffButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [footView addSubview:logOffButton];
    _tableView.tableFooterView = footView;

    if (user.Login) {
        title = @"退出登录";
        color = KNaviBackColor;

    } else {
        title = @"登录";
        color = KNaviBackColor;
    }
    logOffButton.backgroundColor = color;
    [logOffButton setTitle:title forState:UIControlStateNormal];
    logOffButton.tintColor = [UIColor whiteColor];
    logOffButton.layer.masksToBounds = YES;
    logOffButton.layer.cornerRadius = 3;
    [logOffButton addTarget:self action:@selector(logOff) forControlEvents:UIControlEventTouchUpInside];
    [logOffButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(_tableView);
        make.height.mas_equalTo(40);
        make.width.mas_equalTo(200);
        make.bottom.mas_equalTo(footView).with.offset(-40);
    }];
    [_tableView registerClass:[MineTwoButtonTableViewCell class] forCellReuseIdentifier:NSStringFromClass([MineTwoButtonTableViewCell class])];
    [_tableView registerClass:[MineMainTableViewCell class] forCellReuseIdentifier:NSStringFromClass([MineMainTableViewCell class])];
    _tableView.delegate = self;
    _tableView.dataSource = self;

}
//获取个人信息
- (void)restoreMineInfo {
    if ([UserInfoManager checkLogin]) {
        MinePersonalInfotmationViewController *personalVC = [[MinePersonalInfotmationViewController alloc] init];
        [self.navigationController pushViewController:personalVC animated:YES];
    } else {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.label.text = @"请先登录";
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hideAnimated:YES afterDelay:1];
        });
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self loginOrReg];
        });
    }
}
//登录注册
- (void)loginOrReg{
    if ([UserInfoManager checkLogin]) {
        MinePersonalInfotmationViewController *personalVC = [[MinePersonalInfotmationViewController alloc] init];
        [self.navigationController pushViewController:personalVC animated:YES];
    } else {
        MineLoginViewController * loginVC = [[MineLoginViewController alloc] init];
        [self.navigationController pushViewController:loginVC animated:YES];
    }
}
//注销
- (void)logOff{
    if (![UserInfoManager checkLogin]) {
        MineLoginViewController * loginVC = [[MineLoginViewController alloc] init];
        [self.navigationController pushViewController:loginVC animated:YES];

    } else {
        [[UserInfoManager sharedManager] logOff];
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.label.text = @"注销成功";
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hideAnimated:YES afterDelay:1];
        });
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self initTableView];
            [_tableView setContentOffset:CGPointMake(0, CGFLOAT_MAX) animated:NO];
        });
    }
}
#pragma mark tableDatasourceAndDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 2 || section == 1) {
        return 2;
    } else if (section == 0) {
        return 1;
    } else {
        return 1;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MineTwoButtonTableViewCell *twoADCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([MineTwoButtonTableViewCell class])];
    MineMainTableViewCell *normalCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([MineMainTableViewCell class])];
    
    if (indexPath.section == 0) {
        twoADCell.array = @[@{@"name" : @"浏览", @"image" : [UIImage imageNamed:@"MineTwoAD-0"]}, @{@"name" : @"订单", @"image" : [UIImage imageNamed:@"MineTwoAD-1"]}];
        twoADCell.delegate = self;
        return twoADCell;
    } else {
        normalCell.mixView.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"MineSection-%ld-%ld",(long)[indexPath section], (long)indexPath.row]];
        //    self.dataArray = @[@[@"委托服务", @"关注的二手房/租房", @"关注的小区"], @[@"我发布的房源", @"客户中心", @"推广中心"], @[@"用户反馈"]];

        normalCell.mixView.label.text = _dataArray[indexPath.section - 1][indexPath.row];
        if (indexPath.section == 1 || indexPath.section == 2) {
            normalCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        return normalCell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 ) {
        return 100;
    }else {
        return 50;
    }
}
#pragma 两个单独的
- (void)tableViewCell:(BaseCollectionWithTableViewCell *)cell didSelectedAtItem:(NSInteger)item {
    if (item == 0) {
        MineFocusViewController *VC = [[MineFocusViewController alloc] init];
        VC.contentType = 1;//浏览
        [self.navigationController pushViewController:VC animated:YES];
    } else {

    }
}
#pragma mark 点击cell
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 1) { //委托服务 关注的二手房
        if (![self isLogin]) {
            [self checkLogin];
            return;
        }
        if (indexPath.row == 0) {
            CategorySeviceViewController *VC = [[CategorySeviceViewController alloc] init];
            VC.navigationController.navigationBar.hidden = YES;
            [self.navigationController pushViewController:VC animated:YES];
        }
        if (indexPath.row == 1) {
            MineFocusViewController *VC = [[MineFocusViewController alloc] init];
            VC.contentType = 0;//关注
            [self.navigationController pushViewController:VC animated:YES];
        }
    }
    if (indexPath.section == 2) { //发布的房源 客户中心
        if (indexPath.row == 0) {
            if (![self isLogin]) {
                [self checkLogin];
            } else {
                MinePublishViewController *publishVC = [[MinePublishViewController alloc] init];
                [self.navigationController pushViewController:publishVC animated:YES];
            }
        }
        if (indexPath.row == 1) {
            NSURL *telUrl = [NSURL URLWithString:[@"telprompt://" stringByAppendingString:unifyServiceNumber]];
            if ([[UIApplication sharedApplication] canOpenURL:telUrl]) {
                [[UIApplication sharedApplication] openURL:telUrl];
            }
        }
    }
    if (indexPath.section == 3) { //用户反馈
        if (indexPath.row == 0) {
            MineFeedbackViewController *vc = [[MineFeedbackViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
}




@end
