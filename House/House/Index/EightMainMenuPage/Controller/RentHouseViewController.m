//
//  RentHouseViewController.m
//  House
//
//  Created by 张垚 on 16/6/28.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "RentHouseViewController.h"
#import "SearchHouseViewController.h"
#import "BaseNavigationBar.h"
#import "MySearchBar.h"
#import "SecondaryDetailViewController.h"
#import "RentHouseListTableViewCell.h"
#import "ImageViewAndLabelButton.h"
#import "RentEntrustViewController.h"
#import "RentHouseListModel.h"
#import "ReuseSelectButtonView.h"
#import "AreaAndTypeChooseViewController.h"
#import "PriceChooseViewController.h"
#import "MoreChooseViewController.h"

#import "RentHouseListNoSlideTableViewCell.h"

@interface RentHouseViewController ()<UITableViewDelegate, UITableViewDataSource, RentHouseListTableViewCellDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) RentHouseListTableViewCell *mySelectCell;


@property (nonatomic, strong) NSMutableArray *titleArray;
//装字典的数组
@property (nonatomic, strong) NSMutableArray *modelArr;
//数据页数
@property (nonatomic, assign) NSInteger page;

//筛选之后 拼接出来的URL
@property (nonatomic, copy) NSString *urlString;

@property (nonatomic, strong) MBProgressHUD *hud;
@property (nonatomic, strong) MBProgressHUD *selectHud;

@property (nonatomic, assign) NSInteger tag;

@property (nonatomic, assign) NSInteger selectPage;

@end

static NSString *cellID = @"RentHouseListTableViewCell";
/* 筛选数据 筛选完之后进行赋值 初始值只是用来判断有没有改动筛选条件 */
static NSString *areaStr = @"不限";
static NSString *priceStr = @"不限";
static NSString *typeStr = @"0";
static NSString *squreString = @"更多";
static NSInteger flag = 0;

@implementation RentHouseViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = YES;
}
#pragma mark - 主程序入口
- (void)viewDidLoad {
    [super viewDidLoad];
    //自适应
    self.automaticallyAdjustsScrollViewInsets = NO;
    //默认是未选择区域状态
    _tag = 1;
    //默认设置页数为1  先加载第一页的数据
    _page = 1;
    //设置区域选择页数为1
    _selectPage = 1;
    //tableview
    [self initTableView];
    //搜索栏
    [self initLrdSuperMenu];
    //获取数据
    [self handleData];
    //下拉刷新
    [self setHeaderRefresh];
    //菊花属性设置
    _hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _hud.label.text = @"正在加载数据";
    _hud.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
    _hud.backgroundView.backgroundColor = [UIColor whiteColor];
    _hud.bezelView.style = MBProgressHUDBackgroundStyleBlur;
    _hud.animationType = MBProgressHUDAnimationZoomIn;
    // Do any additional setup after loading the view.
}

#pragma mark - 导航栏搜索
/* 重写父类方法 */
- (void)initNavigationBar {
    [super initNavigationBar];
    self.myBar.searchBar.textFiled.placeholder = @"请输入小区名";
    [self.myBar.searchBar.textFiled setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    UITapGestureRecognizer *popTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pop:)];
    [self.myBar.leftView addGestureRecognizer:popTap];
    UITapGestureRecognizer *pushSearchHouseTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(push)];
    [self.myBar.searchBar addGestureRecognizer:pushSearchHouseTap];
}

- (void)pop:(UITapGestureRecognizer *)tap {
    [self.navigationController popViewControllerAnimated:YES];
}

/* 点击搜索栏 跳转页面进行搜索 */
- (void)push {
    SearchHouseViewController *searchVC = [[SearchHouseViewController alloc] init];
    searchVC.currentStr = @"租房";
    [self.navigationController pushViewController:searchVC animated:YES];
}

#pragma mark - 数据处理部分
//获取数据 -- 未经过条件筛选
- (void)handleData {
    _modelArr = [NSMutableArray array];
    //获取第一页
    NSDictionary *dic = @{@"page": @"1", @"areaid": [[NSUserDefaults standardUserDefaults] objectForKey:cityID]};
    [NetRequest getWithURLString:@"http://aizufang.hrbzjwl.com/tp/api.php/Rent/index/" parameters:dic progress:^(NSProgress *progress) {
        
    } success:^(id response) {
        if ([response[@"flag"] isEqualToString:@"1"]) {
            _hud.label.text = @"加载成功";
            [_hud hideAnimated:YES afterDelay:1];
            //将获取的值赋给数据 刷新tableview
            _modelArr = [response[@"chuzu"] mutableCopy];
            [_tableView reloadData];
        }else {
            _hud.label.text = @"加载失败";
            [_hud hideAnimated:YES afterDelay:1];
        }
        [self setFooterRefresh];
    } failure:^(NSError *error) {
        _hud.label.text = @"服务器异常";
        [_hud hideAnimated:YES afterDelay:1];
    }];
    [self setFooterRefresh];
}
//条件筛选之后 请求数据
- (void)handleDataWithSelectUrlString:(NSString *)selectUrlString {
    _selectHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _selectHud.label.text = @"加载中...";
    _selectHud.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
    //获取第一页
    NSString *urlStr = [NSString stringWithFormat:@"%@page/1/areaid/%@", selectUrlString, [[NSUserDefaults standardUserDefaults] objectForKey:cityID]];
    [NetRequest getWithURLString:urlStr parameters:nil progress:^(NSProgress *progress) {
        
    } success:^(id response) {
        if ([[response[@"flag"] description] isEqualToString:@"0"]) {
            UIImage *image = [[UIImage imageNamed:@"failIcon"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
            _selectHud.mode = MBProgressHUDModeCustomView;
            _selectHud.customView = imageView;
            _selectHud.label.text = @"暂无此类型房源";
            [_selectHud hideAnimated:YES afterDelay:1];
            _modelArr = [NSMutableArray array];
            [_tableView reloadData];
        }else {
            UIImage *image = [[UIImage imageNamed:@"successIcon"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
            _selectHud.mode = MBProgressHUDModeCustomView;
            _selectHud.customView = imageView;
            _selectHud.label.text = @"筛选成功";
            [_selectHud hideAnimated:YES afterDelay:1];
            //将获取的值赋给数据 刷新tableview
            NSArray *arr = response[@"chuzu"];
            _modelArr = [NSMutableArray arrayWithArray:arr];
            [_tableView reloadData];
        }
    } failure:^(NSError *error) {
        UIImage *image = [[UIImage imageNamed:@"failIcon"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        _selectHud.mode = MBProgressHUDModeCustomView;
        _selectHud.customView = imageView;
        _selectHud.label.text = @"服务器异常，请重试";
        [_selectHud hideAnimated:YES afterDelay:1];
    }];
    [self setFooterRefresh];
}
#pragma mark - 刷新加载
//上拉加载
- (void)setFooterRefresh {
    //判断下拉刷新的是所有数据 还是经过条件筛选之后的数据 tag = 1为所有数据
    if (_tag == 1) {
        [self.tableView.mj_footer beginRefreshing];
        self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            //每刷一次 请求的页数叠加
            _page++;
            NSDictionary *dic = @{@"page": [NSString stringWithFormat:@"%ld", (long)_page], @"areaid": [[NSUserDefaults standardUserDefaults] objectForKey:cityID]};
            //网络请求数据
            [NetRequest getWithURLString:@"http://aizufang.hrbzjwl.com/tp/api.php/Rent/index/" parameters:dic progress:^(NSProgress *progress) {
            } success:^(id response) {
                //没有数据的时候 上拉刷新置为nil
                if ([[response[@"flag"] description] isEqualToString:@"0"]) {
                    self.tableView.mj_footer = nil;
                }else {
                    for (NSDictionary *houseDic in response[@"chuzu"]) {
                        [_modelArr addObject:houseDic];
                    }
                    [_tableView reloadData];
                    [self.tableView.mj_footer endRefreshing];
                }
            } failure:^(NSError *error) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }];
        }];
    }
    //tag = 2 区域选择之后的数据
    else {
        [self.tableView.mj_footer beginRefreshing];
        self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            //每刷一次 请求的页数叠加
            _selectPage++;
            NSString *urlStr = [NSString stringWithFormat:@"%@page/%ld/areaid/%@/", _urlString, (long)_selectPage, [[NSUserDefaults standardUserDefaults] objectForKey:cityID]];
            //网络请求数据
            [NetRequest getWithURLString:urlStr parameters:nil progress:^(NSProgress *progress) {
            } success:^(id response) {
                //没有数据的时候 上拉刷新置为nil
                if ([[response[@"flag"] description] isEqualToString:@"0"]) {
                    self.tableView.mj_footer = nil;
                }else {
                    for (NSDictionary *dic in response[@"chuzu"]) {
                        [_modelArr addObject:dic];
                    }
                    [_tableView reloadData];
                    [self.tableView.mj_footer endRefreshing];
                }
            } failure:^(NSError *error) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }];
        }];
    }
}


//下拉刷新
- (void)setHeaderRefresh {
    [self.tableView.mj_header beginRefreshing];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        if (_tag == 1) {
            [self handleData];
        }else {
            [self handleDataWithSelectUrlString:_urlString];
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.tableView.mj_header endRefreshing];
        });
    }];
}



#pragma mark - 区域搜索
//区域条件筛选视图创建
- (void)initLrdSuperMenu {
    ReuseSelectButtonView *selectView = [[ReuseSelectButtonView alloc] initWithFrame:CGRectMake(0, 64, kScreenWidth, 40) WithButtonTitleArr:@[@"区域", @"价格", @"户型", @"更多"]];
    selectView.backgroundColor = [UIColor whiteColor];
    [selectView.areaButton addTarget:self action:@selector(selectData:) forControlEvents:UIControlEventTouchUpInside];
    [selectView.priceButton addTarget:self action:@selector(selectData:) forControlEvents:UIControlEventTouchUpInside];
    [selectView.typeButton addTarget:self action:@selector(selectData:) forControlEvents:UIControlEventTouchUpInside];
    [selectView.moreButton addTarget:self action:@selector(selectData:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:selectView];
}
//筛选按钮点击事件
- (void)selectData:(UIButton *)button {
    switch (button.tag) {
        case 100:
        {
            AreaAndTypeChooseViewController *areaVC = [[AreaAndTypeChooseViewController alloc] init];
            areaVC.tag = button.tag;
            areaVC.currentStr = button.titleLabel.text;
            [areaVC passInfoToPreviousPage:^(NSString *currentStr, NSInteger currentTag) {
                if ([currentStr isEqualToString:@"不限"]) {
                    [button setTitle:@"区域" forState:UIControlStateNormal];
                    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                }else {
                    [button setTitle:currentStr forState:UIControlStateNormal];
                    [button setTitleColor:KNaviBackColor forState:UIControlStateNormal];
                }
                areaStr = currentStr;
                [self getSelectDataWithAreaString:areaStr withPriceString:priceStr WithTypeString:typeStr WithSqureString:squreString WithDirectionTag:flag];
            }];
            [self.navigationController pushViewController:areaVC animated:YES];
        }
            break;
        case 101:
        {
            PriceChooseViewController *priceVC = [[PriceChooseViewController alloc] init];
            priceVC.tag = 101;
            priceVC.currentStr = button.titleLabel.text;
            [priceVC passInfoToPreviousPage:^(NSString *currentStr, NSString *submitStr) {
                if ([currentStr isEqualToString:@"不限"]) {
                    [button setTitle:@"价格" forState:UIControlStateNormal];
                    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                }else {
                    [button setTitle:currentStr forState:UIControlStateNormal];
                    [button setTitleColor:KNaviBackColor forState:UIControlStateNormal];
                }
                priceStr = submitStr;
                [self getSelectDataWithAreaString:areaStr withPriceString:priceStr WithTypeString:typeStr WithSqureString:squreString WithDirectionTag:flag];
            }];
            [self.navigationController pushViewController:priceVC animated:YES];
        }
            break;
        case 102:
        {
            AreaAndTypeChooseViewController *areaVC = [[AreaAndTypeChooseViewController alloc] init];
            areaVC.tag = button.tag;
            areaVC.currentStr = button.titleLabel.text;
            [areaVC passInfoToPreviousPage:^(NSString *currentStr, NSInteger currentTag) {
                if ([currentStr isEqualToString:@"不限"]) {
                    [button setTitle:@"户型" forState:UIControlStateNormal];
                    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                }else {
                    [button setTitle:currentStr forState:UIControlStateNormal];
                    [button setTitleColor:KNaviBackColor forState:UIControlStateNormal];
                }
                typeStr = [NSString stringWithFormat:@"%ld", (long)currentTag];
                [self getSelectDataWithAreaString:areaStr withPriceString:priceStr WithTypeString:typeStr WithSqureString:squreString WithDirectionTag:flag];
            }];
            [self.navigationController pushViewController:areaVC animated:YES];
        }
            break;
        case 103:
        {
            MoreChooseViewController *moreVC = [[MoreChooseViewController alloc] init];
            moreVC.tag = flag;
            moreVC.currentStr = squreString;
            [moreVC passInfoToPreviousPage:^(NSString *currentStr, NSInteger currentTag) {
                if (currentTag == 0 && [currentStr isEqualToString:@"更多"]) {
                    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                }else {
                    [button setTitleColor:KNaviBackColor forState:UIControlStateNormal];
                }
                squreString = currentStr;
                flag = currentTag;
                [self getSelectDataWithAreaString:areaStr withPriceString:priceStr WithTypeString:typeStr WithSqureString:squreString WithDirectionTag:flag];
            }];
            [self.navigationController pushViewController:moreVC animated:YES];
        }
            break;
        default:
            break;
    }
}
//拼接筛选条件 拼接出新的URL 请求数据进行显示
- (void)getSelectDataWithAreaString:(NSString *)areaString withPriceString:(NSString *)priceString WithTypeString:(NSString *)typeString WithSqureString:(NSString *)squreString WithDirectionTag:(NSInteger)directionTag {
    NSString *directionStr = [NSString stringWithFormat:@"%ld", (long)directionTag];
    NSString *urlStr = @"http://aizufang.hrbzjwl.com/tp/api.php/Rent/index/";
    if (![areaString isEqualToString:@"不限"]) {
        areaString = [areaString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
        urlStr = [NSString stringWithFormat:@"%@area/%@/",urlStr, areaString];
    }
    if (![priceString isEqualToString:@"不限"]) {
        urlStr = [NSString stringWithFormat:@"%@price/%@/",urlStr, priceString];
    }
    if (![typeString isEqualToString:@"0"]) {
        urlStr = [NSString stringWithFormat:@"%@room/%@/",urlStr, typeString];
    }
    if (![squreString isEqualToString:@"更多"]) {
        squreString = [squreString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
        urlStr = [NSString stringWithFormat:@"%@square/%@/",urlStr, squreString];
    }
    if (![directionStr isEqualToString:@"0"]) {
        urlStr = [NSString stringWithFormat:@"%@toward/%@/",urlStr, directionStr];
    }
    //页数置为1 方便刷新
    _selectPage = 1;
    _page = 1;
    
    //下拉刷新 赋值新的url 修改标签值 进行判断
    _urlString = urlStr;
    _tag = 2;

    //拼接之后显示数据
    [self handleDataWithSelectUrlString:urlStr];
    }


#pragma mark - tableview
//初始化
- (void)initTableView {
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 108, self.view.frame.size.width, kScreenHeight - 108) style:UITableViewStylePlain];
    [self.view addSubview:_tableView];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [_tableView registerClass:[RentHouseListNoSlideTableViewCell class] forCellReuseIdentifier:cellID];
}
//行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    [tableView placeholderBaseOnNumber:_modelArr.count iconConfig:^(UIImageView *imageView) {
        // 传入一个想展示的图标
        imageView.image = [UIImage imageNamed:@"emptyTableView"];
    } textConfig:^(UILabel *label) {
        // 设置提示性文字
        label.text      = @"未找到符合条件的房源";
        label.textColor = [UIColor lightGrayColor];
        label.font      = [UIFont systemFontOfSize:15];
    }];
    return _modelArr.count;
}
//cell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    RentHouseListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
//    if (!cell) {
//        cell = [[RentHouseListTableViewCell alloc] init];
//    }
    RentHouseListNoSlideTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    //取model 赋值给cell
    NSDictionary *dic = [NSDictionary dictionaryWithDictionary:_modelArr[indexPath.row]];
    RentHouseListModel *model = [RentHouseListModel modelWithDic:dic];
    cell.model = model;
    cell.tag = [model.itemid integerValue];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
//cell点击事件
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    RentHouseListNoSlideTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    SecondaryDetailViewController *secondaryVC  = [[SecondaryDetailViewController alloc] init];
    secondaryVC.itemID = [NSString stringWithFormat:@"%ld", (long)cell.tag];
    [self.navigationController pushViewController:secondaryVC animated:YES];
    
//    //点击进入详情界面
//    if (self.mySelectCell == nil) {
//        SecondaryDetailViewController *secondaryVC  = [[SecondaryDetailViewController alloc] init];
//        secondaryVC.itemID = [NSString stringWithFormat:@"%ld", (long)cell.tag];
//        [self.navigationController pushViewController:secondaryVC animated:YES];
//    }else {
//        if (self.mySelectCell == cell) {
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                [UIView animateWithDuration:0.25 animations:^{
//                    self.mySelectCell.myVIew.frame = CGRectMake(0, 0, cell.frame.size.width + 300, cell.frame.size.height);
//                }];
//                self.mySelectCell = nil;
//            });
//        }else {
//            [UIView animateWithDuration:0.25 animations:^{
//                self.mySelectCell.myVIew.frame = CGRectMake(0, 0, cell.frame.size.width + 300, cell.frame.size.height);
//            }];
//            self.mySelectCell = nil;
//        }
//    }
}
//行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

#pragma mark - 侧滑功能 本版本弃用

//左侧滑
- (void)leftSwipe:(RentHouseListTableViewCell *)obj {
    if (self.mySelectCell == nil) {
        [UIView animateWithDuration:0.25 animations:^{
            obj.myVIew.frame = CGRectMake(-300, 0, obj.frame.size.width + 300, obj.frame.size.height);
        }];
        self.mySelectCell = obj;
        
    }else {
        [UIView animateWithDuration:0.25 animations:^{
            self.mySelectCell.myVIew.frame = CGRectMake(0, 0, obj.frame.size.width + 300, obj.frame.size.height);
        }];
        self.mySelectCell = nil;
    }
}

//右侧滑
- (void)rightSwipe:(RentHouseListTableViewCell *)obj {
    if (self.mySelectCell == nil) {
        return;
    }else {
        [UIView animateWithDuration:0.25 animations:^{
            self.mySelectCell.myVIew.frame = CGRectMake(0, 0, obj.frame.size.width + 300, obj.frame.size.height);
        }];
        self.mySelectCell = nil;
    }
}

//拖拽tableview
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    if (self.mySelectCell == nil) {
        return;
    }else {
        [UIView animateWithDuration:0.25 animations:^{
            self.mySelectCell.myVIew.frame = CGRectMake(0, 0, self.view.frame.size.width + 300, self.mySelectCell.frame.size.height);
        }];
        self.mySelectCell = nil;
    }
    
}
//侧滑点击事件

-(void)passButton:(ImageViewAndLabelButton *)button {
    if (button.tag == 1000) {
        if (button.selected) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"关注成功" preferredStyle:UIAlertControllerStyleAlert];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self dismissViewControllerAnimated:YES completion:nil];
            });
            [self presentViewController:alertController animated:YES completion:nil];
            
        }else {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"取消关注" preferredStyle:UIAlertControllerStyleAlert];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self dismissViewControllerAnimated:YES completion:nil];
            });
            [self presentViewController:alertController animated:YES completion:nil];
            
        }
    }else if(button.tag == 1001) {
        RentEntrustViewController *sellHouseVC = [[RentEntrustViewController alloc] init];
        [self.navigationController pushViewController:sellHouseVC animated:YES];
    }else {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"相似房型匹配待开发" preferredStyle:UIAlertControllerStyleAlert];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self dismissViewControllerAnimated:YES completion:nil];
        });
        [self presentViewController:alertController animated:YES completion:nil];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
