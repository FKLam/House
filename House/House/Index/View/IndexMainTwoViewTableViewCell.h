//
//  IndexMainTwoViewTableViewCell.h
//  House
//
//  Created by 张垚 on 16/9/27.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IndexMainTwoViewTableViewCell : UITableViewCell

@property (nonatomic, strong) UIButton *leftButton;
@property (nonatomic, strong) UIButton *rightButton;
@property (nonatomic, strong) UIView *midView;


@end
