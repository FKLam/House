//
//  IndexMainCycleScrollerViewTableViewCell.h
//  House
//
//  Created by 张垚 on 16/9/9.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDCycleScrollView.h"

@interface IndexMainCycleScrollerViewTableViewCell : UITableViewCell

@property (nonatomic, strong) SDCycleScrollView *cycleScrollView;
@property (nonatomic, strong) NSArray *imageArr;

@end
