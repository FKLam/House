//
//  ReuseDescriptionChooseCollectionViewCell.h
//  IndexMoveHousePage
//
//  Created by 张垚 on 16/6/14.
//  Copyright © 2016年 众杰网络. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^longpress)(UILongPressGestureRecognizer *);

@interface ReuseDescriptionChooseCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *descriptionImageView;
@property (nonatomic, assign) BOOL isSelect;
@property (nonatomic, copy) longpress block;
@end
