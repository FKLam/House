//
//  AreaSelectedLeftTableViewCell.m
//  House
//
//  Created by 吕品 on 16/6/18.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "AreaSelectedLeftTableViewCell.h"

@implementation AreaSelectedLeftTableViewCell
#pragma mark init

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.nameLabel = [UILabel new];
        [self.contentView addSubview:_nameLabel];
        _nameLabel.textAlignment = NSTextAlignmentCenter;
        _nameLabel.font = [UIFont systemFontOfSize:16];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.nameLabel.frame = self.contentView.frame;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
