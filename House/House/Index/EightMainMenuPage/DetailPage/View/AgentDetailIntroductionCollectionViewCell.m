//
//  AgentDetailIntroductionCollectionViewCell.m
//  House
//
//  Created by 张垚 on 16/7/20.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "AgentDetailIntroductionCollectionViewCell.h"
#import "AgentListModel.h"

@interface AgentDetailIntroductionCollectionViewCell ()
@property (nonatomic, strong) UIView *logoView;
@property (nonatomic, strong) UILabel *personLabel;
@property (nonatomic, strong) UILabel *introdutionLabel;
@end

@implementation AgentDetailIntroductionCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _logoView = [[UIView alloc] init];
        [self.contentView addSubview:_logoView];
        _personLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_personLabel];
        _introdutionLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_introdutionLabel];
    }
    return self;
}

- (void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    UIView *superView = self.contentView;
    [_logoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(2);
        make.top.equalTo(superView.mas_top).offset(10);
        make.size.mas_equalTo(CGSizeMake(5, 20));
    }];
    _logoView.backgroundColor = [UIColor redColor];
    [_personLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_logoView.mas_right).offset(10);
        make.top.equalTo(superView.mas_top).offset(10);
        make.height.equalTo(@20);
        make.right.equalTo(superView.mas_right).offset(-10);
    }];
    _personLabel.text = @"个人介绍";
    [_introdutionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_personLabel.mas_left);
        make.top.equalTo(_personLabel.mas_bottom).offset(10);
        make.right.equalTo(superView.mas_right).offset(-10);
        make.bottom.equalTo(superView.mas_bottom).offset(-10);
    }];
    _introdutionLabel.numberOfLines = 0;
}

- (void)setModel:(AgentListModel *)model {
    if (model != _model) {
        _model = model;
    }
    _introdutionLabel.text = model.personal;
}


@end
