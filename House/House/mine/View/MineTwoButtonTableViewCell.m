//
//  MineTwoButtonTableViewCell.m
//  House
//
//  Created by 吕品 on 16/6/24.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "MineTwoButtonTableViewCell.h"
#import "MineTwoButtonCollectionViewCell.h"
#import "MineTwoMixView.h"
@interface MineTwoButtonTableViewCell ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@property (nonatomic, strong) UIView *middleGrayView;
@property (nonatomic, strong) UICollectionView *collectionView;
@end
@implementation MineTwoButtonTableViewCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UICollectionViewFlowLayout *flowLayout = [UICollectionViewFlowLayout new];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        self.collectionView = [[UICollectionView alloc] initWithFrame:self.contentView.bounds collectionViewLayout:flowLayout];
        [self.contentView addSubview:_collectionView];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.scrollEnabled = NO;
        self.middleGrayView = [UIView new];
        [self.contentView addSubview:_middleGrayView];
        _middleGrayView.backgroundColor = [UIColor lightGrayColor];
        [_collectionView registerClass:[MineTwoButtonCollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([MineTwoButtonCollectionViewCell class])];

     }
    return self;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 2;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MineTwoButtonCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([MineTwoButtonCollectionViewCell class]) forIndexPath:indexPath];
    cell.mixView.headImageView.image = _array[indexPath.item][@"image"];
    cell.mixView.titleLabel.text = _array[indexPath.item][@"name"];
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.contentView.frame.size.width / 2, self.contentView.frame.size.height);
}
- (void)layoutSubviews {
    [super layoutSubviews];
    _collectionView.frame = self.contentView.frame;
    [_middleGrayView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.contentView);
        make.width.mas_equalTo(1);
        make.height.mas_equalTo(self.contentView.frame.size.height - 30);
    }];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self.delegate tableViewCell:self didSelectedAtItem:indexPath.item];
}

@end
