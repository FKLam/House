//
//  ReuseSelectButtonView.m
//  House
//
//  Created by 张垚 on 16/8/11.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "ReuseSelectButtonView.h"

@interface ReuseSelectButtonView ()
@property (nonatomic, strong) UIView *midView1;
@property (nonatomic, strong) UIView *midView2;
@property (nonatomic, strong) UIView *midView3;
@property (nonatomic, strong) UIView *bottomView;
@end



@implementation ReuseSelectButtonView

- (instancetype)initWithFrame:(CGRect)frame WithButtonTitleArr:(NSArray *)buttonTitleArr {
    self = [super initWithFrame:frame];
    if (self) {
        _areaButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:_areaButton];
        _midView1 = [[UIView alloc] init];
        [self addSubview:_midView1];
        _priceButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:_priceButton];
        _midView2 = [[UIView alloc] init];
        [self addSubview:_midView2];
        _typeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:_typeButton];
        _midView3 = [[UIView alloc] init];
        [self addSubview:_midView3];
        _moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:_moreButton];
        _bottomView = [[UIView alloc] init];
        [self addSubview:_bottomView];
        for (int i = 0; i < buttonTitleArr.count; i++) {
            if (i == 0) {
                [_areaButton setTitle:buttonTitleArr[i] forState:UIControlStateNormal];
                _areaButton.tag = i + 100;
            }else if (i == 1) {
                 [_priceButton setTitle:buttonTitleArr[i] forState:UIControlStateNormal];
                _priceButton.tag = i + 100;
            }else if (i == 2) {
                [_typeButton setTitle:buttonTitleArr[i] forState:UIControlStateNormal];
                _typeButton.tag = i + 100;
            }else  {
                [_moreButton setTitle:buttonTitleArr[i] forState:UIControlStateNormal];
                _moreButton.tag = i + 100;
            }
        }
    }
    return self;
}


- (void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self;
    [_areaButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left);
        make.top.equalTo(superView.mas_top);
        make.bottom.equalTo(_bottomView.mas_top);
        make.width.mas_equalTo((kScreenWidth - 3)/ 4);
    }];
    [_areaButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _areaButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [_midView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_areaButton.mas_right);
        make.top.equalTo(superView.mas_top);
        make.height.equalTo(superView.mas_height);
        make.width.equalTo(@1);
    }];
    _midView1.backgroundColor = [UIColor lightGrayColor];
    [_priceButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_midView1.mas_right);
        make.top.equalTo(superView.mas_top);
        make.width.mas_equalTo((kScreenWidth - 3)/ 4);
        make.bottom.equalTo(_bottomView.mas_top);
    }];
     [_priceButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _priceButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [_midView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top);
        make.height.equalTo(superView.mas_height);
        make.left.equalTo(_priceButton.mas_right);
        make.width.equalTo(@1);
    }];
    _midView2.backgroundColor = [UIColor lightGrayColor];
    [_typeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top);
        make.left.equalTo(_midView2.mas_left);
        make.bottom.equalTo(_bottomView.mas_top);
        make.width.mas_equalTo((kScreenWidth - 3)/ 4);
    }];
     [_typeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _typeButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [_midView3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top);
        make.height.equalTo(superView.mas_height);
        make.left.equalTo(_typeButton.mas_right);
        make.width.equalTo(@1);
    }];
    _midView3.backgroundColor = [UIColor lightGrayColor];
    [_moreButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo((kScreenWidth - 3)/ 4);
        make.top.equalTo(superView.mas_top);
        make.right.equalTo(superView.mas_right);
        make.bottom.equalTo(_bottomView.mas_top);
    }];
     [_moreButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _moreButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(superView.mas_bottom);
        make.height.equalTo(@1);
        make.width.equalTo(superView.mas_width);
    }];
    _bottomView.backgroundColor = [UIColor lightGrayColor];
}




/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
