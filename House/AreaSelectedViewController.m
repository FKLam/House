//
//  AreaSelectedViewController.m
//  House
//
//  Created by 吕品 on 16/6/18.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "AreaSelectedViewController.h"
#import "AreaSelectedLeftTableViewCell.h"
#import "AreaSelectedRightTableViewCell.h"

@interface AreaSelectedViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *leftTableView;
@property (nonatomic, strong) UITableView *rightTableView;
@property (nonatomic, copy) NSArray *leftArray;
@end

@implementation AreaSelectedViewController
#pragma mark cyclelife
- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    // Do any additional setup after loading the view.
    [self initTableViews];
    self.leftArray = @[@"道里区", @"道外区", @"南岗区", @"香坊区", @"平房区"];
    [self.leftTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];

}

- (void)initTableViews {
    CGFloat height = kScreenHeight - kNaviBarHeight - kTabBarHeight;
    CGFloat width = kScreenWidth / 3.0f;
    //left
    self.leftTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, kNaviBarHeight, width , height) style:UITableViewStylePlain];
    [self.view addSubview:_leftTableView];
    _leftTableView.delegate = self;
    _leftTableView.dataSource = self;
    _leftTableView.tableFooterView = [UIView new];
    [_leftTableView registerClass:[AreaSelectedLeftTableViewCell class] forCellReuseIdentifier:NSStringFromClass([AreaSelectedLeftTableViewCell class])];
    //right
    self.rightTableView = [[UITableView alloc] initWithFrame:CGRectMake(width, kNaviBarHeight, width * 2, height) style:UITableViewStylePlain];
    [self.view addSubview:_rightTableView];
    _rightTableView.delegate = self;
    _rightTableView.dataSource = self;
    _rightTableView.tableFooterView = [UIView new];
    [_rightTableView registerClass:[AreaSelectedRightTableViewCell class] forCellReuseIdentifier:NSStringFromClass([AreaSelectedRightTableViewCell class])];

}
#pragma mark tableviwe datasouce delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([tableView isEqual:_leftTableView]) {
        return 1;
    } else {
        return _leftArray.count;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([tableView isEqual:_leftTableView]) {
        return _leftArray.count;
    } else {
        return 5;
    }

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AreaSelectedLeftTableViewCell *leftCell = [_leftTableView dequeueReusableCellWithIdentifier:NSStringFromClass([AreaSelectedLeftTableViewCell class])];
    AreaSelectedRightTableViewCell *rightCell = [_rightTableView dequeueReusableCellWithIdentifier:NSStringFromClass([AreaSelectedRightTableViewCell class])];
    if ([tableView isEqual:_leftTableView]) {
        leftCell.nameLabel.text = _leftArray[indexPath.row];
        return leftCell;
    } else {
        rightCell.nameLabel.text = @"申格体育用品";
        rightCell.positionLabel.text = @"南岗区西大直街118号(近骨伤科医院)";
        rightCell.distanceLabel.text = @"250.00km";
        return rightCell;
    }
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if ([tableView isEqual:_leftTableView]) {
        return nil;
    } else {
        return _leftArray[section];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if ([tableView isEqual:_leftTableView]) {
        return 0;
    } else {
        return 25;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEqual:_leftTableView]) {
        [_rightTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:indexPath.row] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    if ([tableView isEqual:_rightTableView]) {

    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEqual:_leftTableView]) {
        return 44;
    } else {
        return 100;
    }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if ([scrollView isEqual:_rightTableView]) {
        AreaSelectedRightTableViewCell *cell = _rightTableView.visibleCells.firstObject;
        NSIndexPath *indexPath = [_rightTableView indexPathForCell:cell];
        [_leftTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.section inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
    }
}










@end