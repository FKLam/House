//
//  SecondaryViewController.m
//  House
//
//  Created by 张垚 on 16/6/27.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "SecondaryViewController.h"
#import "SearchHouseViewController.h"
#import "SecondaryListTableViewCell.h"
#import "ImageViewAndLabelButton.h"
#import "BaseNavigationBar.h"
#import "MySearchBar.h"
#import "SellHouseEntrustViewController.h"
#import "RentHouseDetailViewController.h"
#import "SecondaryListModel.h"
#import "ReuseSelectButtonView.h"
#import "AreaAndTypeChooseViewController.h"
#import "PriceChooseViewController.h"
#import "MoreChooseViewController.h"

#import "SecondaryHouseListNoSlideTableViewCell.h"

//代理协议
@interface SecondaryViewController ()<UITableViewDelegate, UITableViewDataSource, SecondaryListTableViewCellDelegate>
//tableview属性
@property (nonatomic, strong) UITableView *tableView;
//判断是否侧滑的cell属性
@property (nonatomic, strong) SecondaryListTableViewCell *mySelectCell;
//四个选择区域标题数组
@property (nonatomic, strong) NSMutableArray *titleArray;
//装数据的数组
@property (nonatomic, strong) NSMutableArray *dataDicArr;
//请求数据页数
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) NSInteger selectPage;

//筛选之后 拼接出来的URL
@property (nonatomic, copy) NSString *urlString;

@property (nonatomic, strong) MBProgressHUD *hud;
@property (nonatomic, strong) MBProgressHUD *selectHud;

@end

static NSString *urlStr = @"http://aizufang.hrbzjwl.com/tp/api.php/iosapp/asecondary/";
static NSString *cellID = @"SecondaryViewControllerCellID";
/* 筛选数据 筛选完之后进行赋值 初始值只是用来判断有没有改动筛选条件 */
static NSString *areaStr = @"不限";
static NSString *priceStr = @"不限";
static NSString *typeStr = @"0";
static NSString *squreString = @"更多";
//朝向需要判断的flag 为0的时候就表示为选择朝向
static NSInteger flag = 0;
//用来判断是否进行过区域筛选
static NSInteger tag = 1;

@implementation SecondaryViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = YES;
}
#pragma mark - 主程序入口
- (void)viewDidLoad {
    [super viewDidLoad];
    
    //默认页数为1 即从第一页开始请求数据
    _page = 1;
    _selectPage = 1;
    //我们自己设置布局 不由VC自动布局
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    //区域选择
    [self initLrdSuperMenu];
    //tableview
    [self initTableView];
    [self handleData];
    [self setHeaderRefresh];
    _hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _hud.label.text = @"正在加载数据";
    _hud.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
    _hud.backgroundView.backgroundColor = [UIColor whiteColor];
    _hud.bezelView.style = MBProgressHUDBackgroundStyleBlur;
    _hud.animationType = MBProgressHUDAnimationZoomIn;
    // Do any additional setup after loading the view.
}

#pragma mark - 顶部视图创建 返回根视图
- (void)initNavigationBar {
    //父类
    [super initNavigationBar];
    self.myBar.searchBar.textFiled.placeholder = @"请输入小区名";
    //占位符颜色
    [self.myBar.searchBar.textFiled setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    //添加手势给返回视图、搜索栏
    UITapGestureRecognizer *popTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pop:)];
    [self.myBar.leftView addGestureRecognizer:popTap];
    UITapGestureRecognizer *pushSearchHouseTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(push)];
    [self.myBar.searchBar addGestureRecognizer:pushSearchHouseTap];
    
}
//返回根视图
- (void)pop:(UITapGestureRecognizer *)tap {
    [self.navigationController popViewControllerAnimated:YES];
}

/* 点击搜索栏 跳转页面进行搜索 */
- (void)push {
    SearchHouseViewController *searchVC = [[SearchHouseViewController alloc] init];
    searchVC.currentStr = @"二手房";
    [self.navigationController pushViewController:searchVC animated:YES];
}
#pragma mark - 数据请求部分
//请求数据
- (void)handleData {
    _dataDicArr = [NSMutableArray array];
    //获取第一页
    NSDictionary *dic = @{@"page": @"1", @"areaid": [[NSUserDefaults standardUserDefaults] objectForKey:cityID]};
    [NetRequest getWithURLString:urlStr parameters:dic progress:^(NSProgress *progress) {
    } success:^(id response) {
        if ([response[@"flag"] isEqualToString:@"1"]) {
            _hud.label.text = @"加载成功";
            [_hud hideAnimated:YES afterDelay:1];
            //将获取的值赋给数据 刷新tableview
            _dataDicArr = [response[@"chuzu"] mutableCopy];
            [_tableView reloadData];
        }else {
            _hud.label.text = @"加载失败";
            [_hud hideAnimated:YES afterDelay:1];
        }
        
    } failure:^(NSError *error) {
        _hud.label.text = @"服务器异常";
        [_hud hideAnimated:YES afterDelay:1];
    }];
    [self setFooterRefresh];
}
//条件筛选之后 请求数据
- (void)handleDataWithSelectUrlString:(NSString *)selectUrlString {
    _selectHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _selectHud.label.text = @"加载中...";
    _selectHud.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
    _selectHud.animationType = MBProgressHUDAnimationZoomIn;
    //获取第一页
    NSString *urlStr = [NSString stringWithFormat:@"%@page/1/areaid/%@", selectUrlString, [[NSUserDefaults standardUserDefaults] objectForKey:cityID]];
    [NetRequest getWithURLString:urlStr parameters:nil progress:^(NSProgress *progress) {
        
    } success:^(id response) {
        if ([[response[@"flag"] description] isEqualToString:@"0"]) {
            UIImage *image = [[UIImage imageNamed:@"failIcon"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
            _selectHud.mode = MBProgressHUDModeCustomView;
            _selectHud.customView = imageView;
            _selectHud.label.text = @"暂无此类型房源";
            [_selectHud hideAnimated:YES afterDelay:1];
            _dataDicArr = [NSMutableArray array];
            [_tableView reloadData];
        }else {
            UIImage *image = [[UIImage imageNamed:@"successIcon"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
            _selectHud.mode = MBProgressHUDModeCustomView;
            _selectHud.customView = imageView;
            _selectHud.label.text = @"筛选成功";
            [_selectHud hideAnimated:YES afterDelay:1];
            //将获取的值赋给数据 刷新tableview
            NSArray *arr = response[@"chuzu"];
            _dataDicArr = [NSMutableArray arrayWithArray:arr];
            [_tableView reloadData];
        }
    } failure:^(NSError *error) {
        UIImage *image = [[UIImage imageNamed:@"failIcon"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        _selectHud.mode = MBProgressHUDModeCustomView;
        _selectHud.customView = imageView;
        _selectHud.label.text = @"服务器异常，请重试";
        [_selectHud hideAnimated:YES afterDelay:1];
    }];
    [self setFooterRefresh];
}
#pragma mark - 刷新加载
//上拉加载
- (void)setFooterRefresh {
    //判断下拉刷新的是所有数据 还是经过条件筛选之后的数据
    if (tag == 1) {
        [self.tableView.mj_footer beginRefreshing];
        self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            //每刷一次 请求的页数叠加
            _page++;
            NSDictionary *dic = @{@"page": [NSString stringWithFormat:@"%ld", (long)_page], @"areaid": [[NSUserDefaults standardUserDefaults] objectForKey:cityID]};
            //网络请求数据
            [NetRequest getWithURLString:@"http://aizufang.hrbzjwl.com/tp/api.php/iosapp/asecondary/" parameters:dic progress:^(NSProgress *progress) {
            } success:^(id response) {
                //没有数据的时候 上拉刷新置为nil
                if ([[response[@"flag"] description] isEqualToString:@"0"]) {
                    self.tableView.mj_footer = nil;
                }else {
                    for (NSDictionary *houseDic in response[@"chuzu"]) {
                        [_dataDicArr addObject:houseDic];
                    }
                    [_tableView reloadData];
                    [self.tableView.mj_footer endRefreshing];
                }
            } failure:^(NSError *error) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }];
        }];
    }else {
        [self.tableView.mj_footer beginRefreshing];
        self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            //每刷一次 请求的页数叠加
            _selectPage++;
            NSString *urlStr = [NSString stringWithFormat:@"%@page/%ld/areaid/%@", _urlString, (long)_selectPage, [[NSUserDefaults standardUserDefaults] objectForKey:cityID]];
            //网络请求数据
            [NetRequest getWithURLString:urlStr parameters:nil progress:^(NSProgress *progress) {
            } success:^(id response) {
                if ([[response[@"flag"] description] isEqualToString:@"0"]) {
                    self.tableView.mj_footer = nil;
                }else {
                    for (NSDictionary *dic in response[@"chuzu"]) {
                        [_dataDicArr addObject:dic];
                    }
                    [_tableView reloadData];
                    [self.tableView.mj_footer endRefreshing];
                }
            } failure:^(NSError *error) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }];
        }];
    }
}

//下拉刷新
- (void)setHeaderRefresh {
    [self.tableView.mj_header beginRefreshing];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        if (tag == 1) {
            [self handleData];
        }else {
            [self handleDataWithSelectUrlString:_urlString];
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.tableView.mj_header endRefreshing];
        });
    }];
}

#pragma mark - 区域选择创建、代理协议方法
//区域条件筛选视图创建
- (void)initLrdSuperMenu {
    ReuseSelectButtonView *selectView = [[ReuseSelectButtonView alloc] initWithFrame:CGRectMake(0, 64, kScreenWidth, 40) WithButtonTitleArr:@[@"区域", @"价格", @"户型", @"更多"]];
    selectView.backgroundColor = [UIColor whiteColor];
    [selectView.areaButton addTarget:self action:@selector(selectData:) forControlEvents:UIControlEventTouchUpInside];
    [selectView.priceButton addTarget:self action:@selector(selectData:) forControlEvents:UIControlEventTouchUpInside];
    [selectView.typeButton addTarget:self action:@selector(selectData:) forControlEvents:UIControlEventTouchUpInside];
    [selectView.moreButton addTarget:self action:@selector(selectData:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:selectView];
}
//筛选按钮点击事件
- (void)selectData:(UIButton *)button {
        switch (button.tag) {
            case 100:
            {
                AreaAndTypeChooseViewController *areaVC = [[AreaAndTypeChooseViewController alloc] init];
                areaVC.tag = button.tag;
                areaVC.currentStr = button.titleLabel.text;
                [areaVC passInfoToPreviousPage:^(NSString *currentStr, NSInteger currentTag) {
                    if ([currentStr isEqualToString:@"不限"]) {
                        [button setTitle:@"区域" forState:UIControlStateNormal];
                        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    }else {
                        [button setTitle:currentStr forState:UIControlStateNormal];
                        [button setTitleColor:KNaviBackColor forState:UIControlStateNormal];
                    }
                    areaStr = currentStr;
                    [self getSelectDataWithAreaString:areaStr withPriceString:priceStr WithTypeString:typeStr WithSqureString:squreString WithDirectionTag:flag];
                }];
                [self.navigationController pushViewController:areaVC animated:YES];
            }
                break;
            case 101:
            {
                PriceChooseViewController *priceVC = [[PriceChooseViewController alloc] init];
                priceVC.tag = 100;
                priceVC.currentStr = button.titleLabel.text;
                [priceVC passInfoToPreviousPage:^(NSString *currentStr, NSString *submitStr) {
                    if ([currentStr isEqualToString:@"不限"]) {
                        [button setTitle:@"价格" forState:UIControlStateNormal];
                        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    }else {
                        [button setTitle:currentStr forState:UIControlStateNormal];
                        [button setTitleColor:KNaviBackColor forState:UIControlStateNormal];
                    }
                    priceStr = submitStr;
                     [self getSelectDataWithAreaString:areaStr withPriceString:priceStr WithTypeString:typeStr WithSqureString:squreString WithDirectionTag:flag];
                }];
                [self.navigationController pushViewController:priceVC animated:YES];
            }
                break;
            case 102:
            {
                AreaAndTypeChooseViewController *areaVC = [[AreaAndTypeChooseViewController alloc] init];
                areaVC.tag = button.tag;
                areaVC.currentStr = button.titleLabel.text;
                [areaVC passInfoToPreviousPage:^(NSString *currentStr, NSInteger currentTag) {
                    if ([currentStr isEqualToString:@"不限"]) {
                        [button setTitle:@"户型" forState:UIControlStateNormal];
                        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    }else {
                        [button setTitle:currentStr forState:UIControlStateNormal];
                        [button setTitleColor:KNaviBackColor forState:UIControlStateNormal];
                    }
                    typeStr = [NSString stringWithFormat:@"%ld", (long)currentTag];
                     [self getSelectDataWithAreaString:areaStr withPriceString:priceStr WithTypeString:typeStr WithSqureString:squreString WithDirectionTag:flag];
                }];
                [self.navigationController pushViewController:areaVC animated:YES];
            }
                break;
            case 103:
            {
                MoreChooseViewController *moreVC = [[MoreChooseViewController alloc] init];
                moreVC.tag = flag;
                moreVC.currentStr = squreString;
                [moreVC passInfoToPreviousPage:^(NSString *currentStr, NSInteger currentTag) {
                    if (currentTag == 0 && [currentStr isEqualToString:@"更多"]) {
                        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    }else {
                        [button setTitleColor:KNaviBackColor forState:UIControlStateNormal];
                    }
                    squreString = currentStr;
                    flag = currentTag;
                     [self getSelectDataWithAreaString:areaStr withPriceString:priceStr WithTypeString:typeStr WithSqureString:squreString WithDirectionTag:flag];
                }];
                [self.navigationController pushViewController:moreVC animated:YES];
            }
                break;
            default:
                break;
        }
}
//拼接筛选条件 拼接出新的URL 请求数据进行显示
- (void)getSelectDataWithAreaString:(NSString *)areaString withPriceString:(NSString *)priceString WithTypeString:(NSString *)typeString WithSqureString:(NSString *)squreString WithDirectionTag:(NSInteger)directionTag {
    NSString *directionStr = [NSString stringWithFormat:@"%ld", (long)directionTag];
    NSString *urlStr = @"http://aizufang.hrbzjwl.com/tp/api.php/iosapp/asecondary/";
    if (![areaString isEqualToString:@"不限"]) {
        areaString = [areaString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
        urlStr = [NSString stringWithFormat:@"%@area/%@/",urlStr, areaString];
    }
    if (![priceString isEqualToString:@"不限"]) {
        urlStr = [NSString stringWithFormat:@"%@price/%@/",urlStr, priceString];
    }
    if (![typeString isEqualToString:@"0"]) {
        urlStr = [NSString stringWithFormat:@"%@room/%@/",urlStr, typeString];
    }
    if (![squreString isEqualToString:@"更多"]) {
        squreString = [squreString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
        urlStr = [NSString stringWithFormat:@"%@square/%@/",urlStr, squreString];
    }
    if (![directionStr isEqualToString:@"0"]) {
        urlStr = [NSString stringWithFormat:@"%@toward/%@/",urlStr, directionStr];
    }
    //筛选之后 将页数置为1 刷新得重新开始
    _selectPage = 1;
    _page = 1;
    //拼接之后显示数据
    [self handleDataWithSelectUrlString:urlStr];
    //下拉刷新 赋值新的url 修改标签值 进行判断
    _urlString = [NSString stringWithFormat:@"%@", urlStr];
    tag = 2;
}


#pragma mark - tableView
//创建tableview
- (void)initTableView {
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 104, self.view.frame.size.width, self.view.bounds.size.height - 104) style:UITableViewStylePlain];
    [self.view addSubview:_tableView];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [_tableView registerClass:[SecondaryHouseListNoSlideTableViewCell class] forCellReuseIdentifier:cellID];
}
//行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    [tableView placeholderBaseOnNumber:_dataDicArr.count iconConfig:^(UIImageView *imageView) {
                    // 传入一个想展示的图标
                    imageView.image = [UIImage imageNamed:@"emptyTableView"];
                } textConfig:^(UILabel *label) {
                    // 设置提示性文字
                    label.text      = @"未找到符合条件的房源";
                    label.textColor = [UIColor lightGrayColor];
                    label.font      = [UIFont systemFontOfSize:15];
                }];

    return _dataDicArr.count;
}
//cell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    SecondaryListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
//    if (!cell) {
//        cell = [[SecondaryListTableViewCell alloc] init];
//    }
    SecondaryHouseListNoSlideTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    NSDictionary *dic = _dataDicArr[indexPath.row];
    SecondaryListModel *model = [SecondaryListModel modelWithDic:dic];
    cell.model = model;
    cell.tag = [model.itemid integerValue];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //cell.delegate = self;
    return cell;
}
//cell点击事件

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SecondaryHouseListNoSlideTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    RentHouseDetailViewController *detailVC = [[RentHouseDetailViewController alloc] init];
    detailVC.itemid = [NSString stringWithFormat:@"%ld", (long)cell.tag];
    [self.navigationController pushViewController:detailVC animated:YES];
    
    
//    SecondaryListTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//    //cell没有侧滑现象 就进入详情
//    if (self.mySelectCell == nil) {
//        RentHouseDetailViewController *detailVC = [[RentHouseDetailViewController alloc] init];
//        detailVC.itemid = [NSString stringWithFormat:@"%ld", (long)cell.tag];
//        [self.navigationController pushViewController:detailVC animated:YES];
//    }
//    //cell有侧滑 判断是否是当前点击的cell 点击收回侧滑的cell
//    else {
//        if (self.mySelectCell == cell) {
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                [UIView animateWithDuration:0.25 animations:^{
//                    self.mySelectCell.myVIew.frame = CGRectMake(0, 0, cell.frame.size.width + 300, cell.frame.size.height);
//                }];
//                self.mySelectCell = nil;
//            });
//        }else {
//            [UIView animateWithDuration:0.25 animations:^{
//                self.mySelectCell.myVIew.frame = CGRectMake(0, 0, cell.frame.size.width + 300, cell.frame.size.height);
//            }];
//            self.mySelectCell = nil;
//        }
//    }
}
//行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}


#pragma mark - 侧滑功能 按钮点击事件（代理从cell中传出）-- 本版本暂时不用
//左侧滑

- (void)leftSwipe:(SecondaryListTableViewCell *)obj {
    //如果没有cell侧滑的话 直接侧滑目前的cell 否则就收回已经侧滑的cell
    if (self.mySelectCell == nil) {
        [UIView animateWithDuration:0.25 animations:^{
            obj.myVIew.frame = CGRectMake(-300, 0, obj.frame.size.width + 300, obj.frame.size.height);
        }];
        //赋值表示已经有侧滑的cell 做一个记录 再次侧滑的时候先取 看看是否有侧滑的 再做判断
        self.mySelectCell = obj;

    }else {
        if (obj == self.mySelectCell) {
            return;
        }
        [UIView animateWithDuration:0.25 animations:^{
             self.mySelectCell.myVIew.frame = CGRectMake(0, 0, obj.frame.size.width + 300, obj.frame.size.height);
        }];
        //赋值为空 下次判断
        self.mySelectCell = nil;
    }
}
//右滑手势
- (void)rightSwipe:(SecondaryListTableViewCell *)obj {
    //cell有侧滑的 就收回 没有的话 直接返回
    if (self.mySelectCell == nil) {
        return;
    }else {
        [UIView animateWithDuration:0.25 animations:^{
            self.mySelectCell.myVIew.frame = CGRectMake(0, 0, obj.frame.size.width + 300, obj.frame.size.height);
        }];
        //赋值为空 下次做判断
        self.mySelectCell = nil;
    }
}

//开始拖拽tableview 侧滑收回
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    if (self.mySelectCell == nil) {
        return;
    }else {
        [UIView animateWithDuration:0.25 animations:^{
            self.mySelectCell.myVIew.frame = CGRectMake(0, 0, self.view.frame.size.width + 300, self.mySelectCell.frame.size.height);
        }];
        self.mySelectCell = nil;
    }
    
}
//侧滑点击事件
-(void)passButton:(ImageViewAndLabelButton *)button {
    if (button.tag == 1000) {
        if (button.selected) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"关注成功" preferredStyle:UIAlertControllerStyleAlert];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self dismissViewControllerAnimated:YES completion:nil];
            });
            [self presentViewController:alertController animated:YES completion:nil];
 
        }else {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"取消关注" preferredStyle:UIAlertControllerStyleAlert];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self dismissViewControllerAnimated:YES completion:nil];
            });
            [self presentViewController:alertController animated:YES completion:nil];

        }
           }else if(button.tag == 1001) {
               SellHouseEntrustViewController *sellHouseVC = [[SellHouseEntrustViewController alloc] init];
               [self.navigationController pushViewController:sellHouseVC animated:YES];
    }else {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"相似房型匹配待开发" preferredStyle:UIAlertControllerStyleAlert];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self dismissViewControllerAnimated:YES completion:nil];
        });
        [self presentViewController:alertController animated:YES completion:nil];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
