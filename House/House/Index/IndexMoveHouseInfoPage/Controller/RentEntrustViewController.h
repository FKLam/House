//
//  RentEntrustViewController.h
//  IndexMoveHousePage
//
//  Created by 张垚 on 16/6/15.
//  Copyright © 2016年 众杰网络. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@class MinePublishPicModel;
@interface RentEntrustViewController : BaseViewController
@property (nonatomic, strong) MinePublishPicModel *model;
@end
