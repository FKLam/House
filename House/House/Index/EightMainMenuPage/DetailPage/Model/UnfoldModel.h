//
//  UnfoldModel.h
//  House
//
//  Created by 张垚 on 16/7/14.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UnfoldModel : NSObject

/* 显示内容 */
@property (nonatomic, copy) NSString *contextStr;
/* cell状态 YES表示折叠起来 NO表未折叠 */
@property (nonatomic, assign) BOOL isFold;

@end
