//
//  AgentListTableViewCell.m
//  House
//
//  Created by 张垚 on 16/7/5.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "AgentListTableViewCell.h"
#import "AgentListDetaiVIew.h"
#import "ImageViewAndLabelButton.h"
#import "AgentListModel.h"

@interface AgentListTableViewCell ()<AgentDetailViewDelegate>
@property (nonatomic, strong) UISwipeGestureRecognizer *leftSwipe;

@property (nonatomic, strong) UISwipeGestureRecognizer *rightSwipe;
@property (nonatomic, strong) NSArray *titleArr;
@property (nonatomic, strong) NSArray *imageArr;
@property (nonatomic, strong) NSArray *colorArr;

@end


@implementation AgentListTableViewCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        /* cell 添加手势 */
        _leftSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftOffset:)];
        
        _leftSwipe.direction = UISwipeGestureRecognizerDirectionLeft;
        [self addGestureRecognizer:_leftSwipe];
        
        _rightSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightOffset:)];
        _rightSwipe.direction = UISwipeGestureRecognizerDirectionRight;
        [self addGestureRecognizer:_rightSwipe];
        
        /* 选择框背景颜色 */
        _colorArr = @[[UIColor colorWithRed:0.6667 green:0.6667 blue:0.6667 alpha:1.0], [UIColor colorWithRed:0.3347 green:0.8369 blue:1.0 alpha:1.0]];
        _titleArr = @[@"关注", @"电话"];
        _imageArr = @[[UIImage imageNamed:@"attentionSuccessWhite"], [UIImage imageNamed:@"callPhone"]];
        _myVIew = [[AgentListDetaiVIew alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth + 160, 80) WithTitleArr:_titleArr WithImageArr:_imageArr WithColorArr:_colorArr WithButtonNum:2];
        _myVIew.delegate = self;
        [self.contentView addSubview:_myVIew];
        _myVIew.backgroundColor = [UIColor whiteColor];
        
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    _myVIew.nameLabel.font = [UIFont systemFontOfSize:15];
    
}

//侧滑点击事件
- (void)passButton:(ImageViewAndLabelButton *)button {
    if (button.tag == 1000) {
        button.selected = !button.selected;
        if (button.selected) {
            button.logoImage.image = [UIImage imageNamed:@"attentionSuccess"];
        }else {
            button.logoImage.image = [UIImage imageNamed:@"attentionSuccessWhite"];
        }
    }else {
        button.tag = [_model.mobile integerValue];
    }
    [self.delegate passButton:button];
}


- (void)leftOffset:(UISwipeGestureRecognizer *)swipe {
    [self.delegate leftSwipe:self];
}

- (void)rightOffset:(UISwipeGestureRecognizer *)swipe {
    [self.delegate rightSwipe:self];
}

- (void)setModel:(AgentListModel *)model {
    if (model != _model) {
        _model = model;
    }
    _myVIew.nameLabel.text = model.truename;
    [_myVIew.headImageView setImageWithURL:[NSURL URLWithString:model.thumb] placeholderImage:[UIImage imageNamed:@"placeHolderImage"]];
    NSMutableAttributedString *yearStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"入职时间 %@", model.career]];
    [yearStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, 4)];
    _myVIew.yearLabel.attributedText = yearStr;
    NSMutableAttributedString *storeStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"所属门店 %@", model.company]];
    [storeStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, 4)];
    _myVIew.storesLabel.attributedText = storeStr;
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
