//
//  ReuseRecyclePicTableViewCell.m
//  House
//
//  Created by 吕品 on 16/6/7.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "ReuseRecyclePicTableViewCell.h"
#import "ZJRecyclePicView.h"

@implementation ReuseRecyclePicTableViewCell
#pragma mark init
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.picView = [[ZJRecyclePicView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, indexMainVCTableViewTopADHeight)];
        [self.contentView addSubview:_picView];
    }
    return self;
}
#pragma mark layout
- (void)layoutSubviews {
    [super layoutSubviews];
    _picView.frame = self.contentView.frame;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
