//
//  PriceChooseViewController.h
//  House
//
//  Created by 张垚 on 16/8/12.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "BaseViewController.h"

typedef void(^chooseInfosBlock)(NSString *currentStr, NSString *submitStr);
@interface PriceChooseViewController : BaseViewController

@property (nonatomic, copy) chooseInfosBlock chooseInfoBlock;
- (void)passInfoToPreviousPage:(chooseInfosBlock)infoBlock;
@property (nonatomic, assign) NSInteger tag;
@property (nonatomic, copy) NSString *currentStr;

@end
