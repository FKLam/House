//
//  UserInfoHistoryManager.swift
//  House
//
//  Created by 吕品 on 2016/9/9.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

import UIKit
public enum UserInfoStoreType: Int{
    case rent
    case secondary
}

class UserInfoHistoryManager: NSObject {
    static let sharedManager = UserInfoHistoryManager.init()
    private let rent = "rent"
    private let secondary = "secondary"
    var count = 20//规定好显示的个数每页
    private func createDBWithName(_ name: String = "userhistory") -> YTKKeyValueStore {
        let YTKDB = YTKKeyValueStore.init(dbWithName: name)
        return YTKDB!
    }

    func storeInfor(_ itemId: String, with type: UserInfoStoreType) -> Swift.Void {
        switch type {
        case .rent:
            createDBWithName().createTable(withName: rent)
            createDBWithName().put(itemId, withId: itemId, intoTable: rent)
            break
        case .secondary:
            createDBWithName().createTable(withName: secondary)
            createDBWithName().put(itemId, withId: itemId, intoTable: secondary)
            break
        }
    }
    func readInfoWithType(_ type: UserInfoStoreType) -> NSArray! {
        var arr: NSArray!

        switch type {
        case .rent:
            arr = createDBWithName().getAllItems(fromTable: rent)
            break
        case .secondary:
            arr = createDBWithName().getAllItems(fromTable: secondary)
            break
        }
        guard count > arr.count else {
            return arr
        }
        let tempArr = NSMutableArray.init()
        for i in arr.count - count ... arr.count {
            tempArr.add(arr[i])
        }
        return tempArr.copy() as! NSArray
    }
}
