//
//  MineTwoMixView.h
//  House
//
//  Created by 吕品 on 16/6/24.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MineTwoMixView : UIView
@property (nonatomic, strong) UIImageView *headImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@end
