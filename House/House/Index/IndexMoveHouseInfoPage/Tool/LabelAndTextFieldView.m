//
//  LabelAndTextFieldView.m
//  baoxian
//
//  Created by 吕品 on 16/6/2.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "LabelAndTextFieldView.h"


@implementation LabelAndTextFieldView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)init {
    self = [super init];
    if (self) {
        self.titleLabel = [[UILabel alloc] init];
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        [self addSubview:_titleLabel];
        self.contextTextField = [[UITextField alloc] init];
        [self addSubview:_contextTextField];
    }
    return self;
}
- (void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self;
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top);
        make.left.equalTo(superView.mas_left);
        make.width.equalTo(@60);
        make.bottom.equalTo(superView.mas_bottom);
    }];
    _titleLabel.font = [UIFont systemFontOfSize:15];
    _titleLabel.adjustsFontSizeToFitWidth = YES;
    [_contextTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top);
        make.left.equalTo(_titleLabel.mas_right).offset(5);
        make.right.equalTo(superView.mas_right);
        make.bottom.equalTo(superView.mas_bottom);
    }];
    _contextTextField.font = [UIFont systemFontOfSize:15];

}

@end
