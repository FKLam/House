//
//  IndoorMatchTableViewCell.m
//  House
//
//  Created by 张垚 on 16/7/19.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "IndoorMatchTableViewCell.h"
#import "ReuseDescriptionChooseCollectionViewCell.h"
#import "RentHouseDetailModel.h"


@interface IndoorMatchTableViewCell ()<UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, strong) UICollectionView *mainCollectionView;
@property (nonatomic, strong) UILabel *indoorLabel;
@property (nonatomic, copy) NSArray *unSelectImageArr;
@property (nonatomic, copy) NSArray *selectImageArr;
@property (nonatomic, copy) NSArray *matchStrArr;

@end

@implementation IndoorMatchTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _unSelectImageArr = @[@"bed-0", @"refrigerator-0", @"television-0", @"air-condition-0", @"furniture-0", @"wifi-0", @"elevator-0", @"calorifier-0", @"washer-0", @"cook-0"];
        _selectImageArr = @[@"bed-1", @"refrigerator-1", @"television-1", @"air-condition-1", @"furniture-1", @"wifi-1", @"elevator-1", @"calorifier-1", @"washer-1", @"cook-1"];
        _matchStrArr = @[@"床", @"冰箱", @"电视", @"空调", @"家具", @"宽带", @"电梯", @"热水器", @"洗衣机", @"可做饭"];
        _indoorLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_indoorLabel];
        UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
        flow.scrollDirection = UICollectionViewScrollDirectionVertical;
        flow.itemSize = CGSizeMake(60, 25);
        flow.minimumInteritemSpacing = 5;
        flow.minimumLineSpacing = 5;
        _mainCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, 0, 0) collectionViewLayout:flow];
        _mainCollectionView.backgroundColor = [UIColor whiteColor];
        _mainCollectionView.scrollEnabled = NO;
        _mainCollectionView.showsHorizontalScrollIndicator = NO;
        [self.contentView addSubview:_mainCollectionView];
        _mainCollectionView.delegate = self;
        _mainCollectionView.dataSource = self;
        [_mainCollectionView registerClass:[ReuseDescriptionChooseCollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([ReuseDescriptionChooseCollectionViewCell class])];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self.contentView;
    [_indoorLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(10);
        make.top.equalTo(superView.mas_top).offset(10);
        make.height.equalTo(@30);
        make.right.equalTo(superView.mas_right);
    }];
    _indoorLabel.text = @"室内配套";
    _indoorLabel.font = [UIFont systemFontOfSize:19];
    [_mainCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(30);
        make.top.equalTo(_indoorLabel.mas_bottom).offset(20);
        make.bottom.equalTo(superView.mas_bottom).offset(-10);
        make.right.equalTo(superView.mas_right).offset(-30);
    }];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _unSelectImageArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ReuseDescriptionChooseCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([ReuseDescriptionChooseCollectionViewCell class]) forIndexPath:indexPath];
    if (_model.peitaor == NULL) {
        NSString *imageStr = _unSelectImageArr[indexPath.item];
        cell.descriptionImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@", imageStr]];
    }else {
        NSString *imageStr = _unSelectImageArr[indexPath.item];
        cell.descriptionImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@", imageStr]];
        cell.descriptionImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@", imageStr]];
        NSArray *matchArr = [_model.peitaor componentsSeparatedByString:@","];
        for (int j = 0; j < matchArr.count; j++) {
            if ([matchArr[j] isEqualToString:_matchStrArr[indexPath.item]]) {
                NSString *imageStr = _selectImageArr[indexPath.item];
                cell.descriptionImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@", imageStr]];
            }
        }
    }
    cell.layer.masksToBounds = YES;
    cell.layer.cornerRadius = 4;
    return cell;
    
}


@end
