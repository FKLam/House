//
//  IndexMainTableViewCell.h
//  House
//
//  Created by 吕品 on 16/6/3.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseCollectionWithTableViewCell.h"

/*!
 *  @author 众杰网络, 16-06-07 10:06:29
 *
 *  @brief 主页tableview带有collectionview的cell
 *
 *  @since 1.0
 */

@interface IndexMainTableViewCell : BaseCollectionWithTableViewCell
//代理方法

@property (nonatomic, copy) NSArray<NSString *>* titlesArray;//标题数组
@property (nonatomic, copy) NSArray<UIImage *>* headImageArray;//图片数组
@property (nonatomic, assign) BOOL showGrayView;//是否显示灰色分割
@end
