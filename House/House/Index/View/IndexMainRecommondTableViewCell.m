//
//  IndexMainRecommondTableViewCell.m
//  House
//
//  Created by 吕品 on 16/6/12.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "IndexMainRecommondTableViewCell.h"
@interface IndexMainRecommondTableViewCell ()
//控件s
@property (nonatomic, strong) UIImageView *headImageView;//图片

@property (nonatomic, strong) UILabel *nameLabel;//名称
@property (nonatomic, strong) UILabel *areaLabel;//面积
@property (nonatomic, strong) UILabel *positionLabel;//位置

@property (nonatomic, strong) UILabel *priceLabel;//价格
@property (nonatomic, strong) UILabel *pricePerMeterLabel;//价格每平米

//stackviews
@property (nonatomic, strong) UIStackView *tagStackView;//中间标签
@property (nonatomic, strong) UIStackView *priceStackView;//右侧价格
@property (nonatomic, strong) UIStackView *detailStackView;//中间详情
@property (nonatomic, strong) UIStackView *mainStackView;//大的
@end
@implementation IndexMainRecommondTableViewCell
#pragma mark init
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.headImageView = [UIImageView new];
        [self.contentView addSubview:_headImageView];

        self.nameLabel = [UILabel new];
        [self.contentView addSubview:_nameLabel];

        self.areaLabel = [UILabel new];
        [self.contentView addSubview:_areaLabel];

        self.positionLabel = [UILabel new];
        [self.contentView addSubview:_positionLabel];

        self.priceLabel = [UILabel new];
        [self.contentView addSubview:_priceLabel];

        self.pricePerMeterLabel = [UILabel new];
        [self.contentView addSubview:_pricePerMeterLabel];

        self.tagStackView = [[UIStackView alloc] init];
        [self.contentView addSubview:_tagStackView];
        for (NSInteger i = 0; i < 4; i ++) {
            UILabel *tagsLabel = [UILabel new];
            tagsLabel.textAlignment = NSTextAlignmentCenter;
            tagsLabel.layer.masksToBounds = YES;
            tagsLabel.layer.borderWidth = 1.5f;
            tagsLabel.layer.borderColor = [KMainTableViewTagsColorArray[i] CGColor];
            tagsLabel.textColor = KMainTableViewTagsColorArray[i];
            [_tagStackView addArrangedSubview:tagsLabel];
        }
        _tagStackView.axis = UILayoutConstraintAxisHorizontal;
        _tagStackView.alignment = UIStackViewAlignmentFill;
        _tagStackView.distribution = UIStackViewDistributionFillEqually;
        _tagStackView.spacing = 10.0f;

        self.detailStackView = [[UIStackView alloc] initWithArrangedSubviews:@[_nameLabel, _areaLabel, _positionLabel, _tagStackView]];
        [self.contentView addSubview:_detailStackView];
        _detailStackView.axis = UILayoutConstraintAxisVertical;
        _detailStackView.alignment = UIStackViewAlignmentFill;
        _detailStackView.distribution = UIStackViewDistributionFillEqually;
        _detailStackView.spacing = 5.0f;

        self.priceStackView = [[UIStackView alloc] initWithArrangedSubviews:@[_priceLabel, _pricePerMeterLabel]];
        [self.contentView addSubview:_priceStackView];
        _priceStackView.axis = UILayoutConstraintAxisVertical;
        _priceStackView.alignment = UIStackViewAlignmentFill;
        _priceStackView.distribution = UIStackViewDistributionFillEqually;
        _priceStackView.spacing = 30;
        self.mainStackView = [[UIStackView alloc] initWithArrangedSubviews:@[_headImageView, _detailStackView, _priceStackView]];
        [self.contentView addSubview:_mainStackView];
        _mainStackView.axis = UILayoutConstraintAxisHorizontal;
        _mainStackView.alignment = UIStackViewAlignmentFill;
        _mainStackView.distribution = UIStackViewDistributionFillEqually;
        _mainStackView.spacing = 10.0f;

        _headImageView.backgroundColor = [UIColor redColor];
        _nameLabel.backgroundColor = [UIColor greenColor];
        _areaLabel.backgroundColor = [UIColor yellowColor];
        _priceLabel.backgroundColor = [UIColor purpleColor];
        _pricePerMeterLabel.backgroundColor = [UIColor grayColor];
    }
    return self;
}

- (void)setTagsArray:(NSArray<NSString *> *)tagsArray {
    self.tagsArray = tagsArray;
    for (NSInteger i = 0; i < tagsArray.count; i ++) {
        UILabel *label = _tagStackView.arrangedSubviews[i];
        label.text = tagsArray[i];
    }
}

#pragma mark layout
- (void)layoutSubviews {
    [super layoutSubviews];
    [_mainStackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(10, 10, 10, 10));
    }];


}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
