//
//  AreaInforManager.swift
//  House
//
//  Created by 吕品 on 2016/8/29.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

import UIKit
import YTKKeyValueStore
class AreaInforManager: NSObject {

    internal class func getArrayWith(_ dbName: String = dataBaseName, tableName: String = tableName, cityID: String = UserInfoManager.shared().userCityID) -> [AnyObject]? {
        return YTKKeyValueStore.init(dbWithName: dbName).getObjectById(cityID, fromTable: tableName) as! [NSDictionary]
    }
    internal class func getNameWithID(_ id: String) -> String {
        let arr = getArrayWith()
        for dic in arr! {
            let dictionay = dic as! NSDictionary
            if dictionay["id"] as! String == id {
                return dictionay["areaname"] as! String
            }
        }
        return ""
    }

}
