//
//  MoreChooseViewController.m
//  House
//
//  Created by 张垚 on 16/8/12.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "MoreChooseViewController.h"
#import "BaseTitleNavigationBar.h"


@interface MoreChooseViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UIButton *resetButton;
@property (nonatomic, strong) UIButton *sureButton;
@property (nonatomic, strong) BaseTitleNavigationBar *myBar;
@property (nonatomic, strong) UITableView *leftTableView;
@property (nonatomic, strong) UITableView *rightTableView;
@property (nonatomic, strong) NSArray *areaDataArr;
@property (nonatomic, strong) NSArray *dirctionDataArr;
@property (nonatomic, strong) NSArray *leftTableViewDataArr;

@end

static NSString *cellID = @"MoreChooseViewControllerID";

@implementation MoreChooseViewController

-(void)viewWillAppear:(BOOL)animated {
    self.tabBarController.tabBar.hidden = YES;
}

#pragma mark - 主程序入口
- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self initTableView];
    [self initBottomView];
    //顶部视图
    _myBar = [[BaseTitleNavigationBar alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 64) WithTitleStr:@"更多筛选"];
    [self.view addSubview:_myBar];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(back)];
    [_myBar.leftView addGestureRecognizer:tap];
    //本地数据
    _areaDataArr = @[@"30㎡以下", @"30-50㎡", @"50-70㎡", @"70-90㎡", @"90-120㎡", @"120-140㎡", @"140-160㎡", @"160-200㎡", @"200㎡以上"];
    _leftTableViewDataArr = @[@"建筑面积", @"朝向"];
    // Do any additional setup after loading the view.
}
#pragma mark - 底部视图

//返回上一层级
- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
}

//底部视图
- (void)initBottomView {
    UIView *bottonView = [[UIView alloc] init];
    [self.view addSubview:bottonView];
    UIView *superView = self.view;
    [bottonView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(superView.mas_bottom);
        make.left.equalTo(superView.mas_left);
        make.width.equalTo(superView.mas_width);
        make.height.equalTo(@60);
    }];
    bottonView.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0];
    
    _resetButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _resetButton.layer.cornerRadius = 4;
    _resetButton.layer.masksToBounds = YES;
    _resetButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _resetButton.layer.borderWidth = 1.0f;
    [_resetButton setTitle:@"重置" forState:UIControlStateNormal];
    [_resetButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_resetButton addTarget:self action:@selector(resetSubmitInfo:) forControlEvents:UIControlEventTouchUpInside];
    [bottonView addSubview:_resetButton];
    _sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [bottonView addSubview:_sureButton];
    _sureButton.layer.cornerRadius = 4;
    _sureButton.layer.masksToBounds = YES;
    [_sureButton setTitle:@"确定" forState:UIControlStateNormal];
    [_sureButton addTarget:self action:@selector(submitInfo:) forControlEvents:UIControlEventTouchUpInside];
    [_resetButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bottonView.mas_left).offset(20);
        make.top.equalTo(bottonView.mas_top).offset(10);
        make.bottom.equalTo(bottonView.mas_bottom).offset(-10);
        make.right.equalTo(bottonView.mas_centerX).offset(-20);
    }];
    [_sureButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bottonView.mas_centerX).offset(20);
        make.top.equalTo(bottonView.mas_top).offset(10);
        make.bottom.equalTo(bottonView.mas_bottom).offset(-10);
        make.right.equalTo(bottonView.mas_right).offset(-20);
    }];
    _sureButton.backgroundColor = [UIColor redColor];
}

-(void)passInfoToPreviousPage:(chooseInfoBlock)infoBlock {
    _chooseInfoBlock = infoBlock;
}
- (void)resetSubmitInfo:(UIButton *)button {
    _tag = 0;
    _currentStr = @"更多";
    [_leftTableView reloadData];
    [_rightTableView reloadData];
}

- (void)submitInfo:(UIButton *)button {
    _chooseInfoBlock(_currentStr, _tag);
    [self back];
}

#pragma mark - tableview
//初始化
- (void)initTableView {
    _leftTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64,kScreenWidth / 3, kScreenHeight - 64) style:UITableViewStylePlain];
    _leftTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _leftTableView.scrollEnabled = NO;
    [self.view addSubview:_leftTableView];
    _leftTableView.delegate = self;
    _leftTableView.dataSource = self;
    
    _rightTableView = [[UITableView alloc] initWithFrame:CGRectMake(kScreenWidth / 3, 64,2 * kScreenWidth / 3, kScreenHeight - 64) style:UITableViewStylePlain];
    _rightTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.view addSubview:_rightTableView];
    _rightTableView.delegate = self;
    _rightTableView.dataSource = self;
}
//行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == _leftTableView) {
        return _leftTableViewDataArr.count;
    }else {
        return _areaDataArr.count;
    }
    
}
//cell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == _leftTableView) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellID];
            
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.text = _leftTableViewDataArr[indexPath.row];
        if (indexPath.row == 1) {
            if (_tag >= 1) {
                cell.textLabel.textColor = KNaviBackColor;
            }else {
                cell.textLabel.textColor = [UIColor blackColor];
            }
        }else {
            if ([_currentStr isEqualToString:@"更多"]) {
                cell.textLabel.textColor = [UIColor blackColor];
            }else {
                cell.textLabel.textColor = KNaviBackColor;
            }
        }
        return cell;
    }else {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellID];
        }
        cell.textLabel.text = _areaDataArr[indexPath.row];
        if ([_areaDataArr containsObject:@"30㎡以下"]) {
            if (indexPath.row == [_areaDataArr indexOfObject:_currentStr]) {
                cell.textLabel.textColor = KNaviBackColor;
            }else {
                cell.textLabel.textColor = [UIColor blackColor];
            }
        }else {
            
            if (_tag >= 1) {
                if (indexPath.row == _tag - 1) {
                    cell.textLabel.textColor = KNaviBackColor;
                }else {
                     cell.textLabel.textColor = [UIColor blackColor];
                }
            }else {
                cell.textLabel.textColor = [UIColor blackColor];
            }
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

//点击事件
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (tableView == _leftTableView) {
        if (indexPath.row == 0) {
            _areaDataArr = @[@"30㎡以下", @"30-50㎡", @"50-70㎡", @"70-90㎡", @"90-120㎡", @"120-140㎡", @"140-160㎡", @"160-200㎡", @"200㎡以上"];
            [_rightTableView reloadData];
        }else {
            _areaDataArr = @[@"东", @"南", @"西", @"北", @"东南", @"西南", @"东北", @"西北", @"南北", @"东西"];
            [_rightTableView reloadData];
        }
    }else {
        cell.textLabel.textColor = [UIColor redColor];
        if ([_areaDataArr containsObject:@"30㎡以下"]) {
            _currentStr = _areaDataArr[indexPath.row];
            [_leftTableView reloadData];
            [_rightTableView reloadData];
        }else {
            _tag = indexPath.row + 1;
            [_leftTableView reloadData];
            [_rightTableView reloadData];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
