//
//  BaseViewController.h
//  House
//
//  Created by 吕品 on 16/5/31.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>

/*!
 *  @author 众杰网络, 16-06-07 09:06:53
 *
 *  @brief VC基类
 *
 *  @since 1.0
 */
@interface BaseViewController : UIViewController
@property (nonatomic, assign) BOOL tabbarHidden;
/*!
 *  @author 众杰网络, 16-07-14 13:07:21
 *
 *  @brief public method hide HUD with title(duration 1.0)
 *
 *  @param title hide hud
 *
 *  @since 1.0
 */
- (void)hideWithTitle:(NSString *)title;

/**
 check is login or not

 @return BOOL
 */
- (BOOL)isLogin;

- (void)checkLogin;

@end
