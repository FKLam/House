//
//  SecondaryHouseDetailTableViewCell.m
//  House
//
//  Created by 张垚 on 16/7/19.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "SecondaryHouseDetailTableViewCell.h"
#import "RentHouseDetailModel.h"


@interface SecondaryHouseDetailTableViewCell ()
@property (nonatomic, strong) UILabel *characteristicLabel;
@property (nonatomic, strong) UILabel *perPriceLabel;
@property (nonatomic, strong) UILabel *downPaymentLabel;
@property (nonatomic, strong) UILabel *monthPaymentLabel;
@property (nonatomic, strong) UILabel *directionLabel;
@property (nonatomic, strong) UILabel *floorLabel;
@property (nonatomic, strong) UILabel *roomModelLabel;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UILabel *decorateLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *numberLabel;
@property (nonatomic, strong) UILabel *vilageLabel;
@property (nonatomic, strong) UIButton *vilageButton;
@property (nonatomic, strong) UILabel *apartmentLabel;
@end



@implementation SecondaryHouseDetailTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
//        _characteristicLabel = [[UILabel alloc] init];
//        [self.contentView addSubview:_characteristicLabel];
        _perPriceLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_perPriceLabel];
        _downPaymentLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_downPaymentLabel];
        _monthPaymentLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_monthPaymentLabel];
        _directionLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_directionLabel];
        _floorLabel = [[UILabel alloc] init];
        //_floorLabel.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:_floorLabel];
        _roomModelLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_roomModelLabel];
        _priceLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_priceLabel];
        _decorateLabel = [[UILabel alloc] init];
        _decorateLabel.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:_decorateLabel];
        _timeLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_timeLabel];
        _numberLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_numberLabel];
        _vilageLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_vilageLabel];
//        _vilageButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        [self.contentView addSubview:_vilageButton];
        _apartmentLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_apartmentLabel];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self.contentView;

    [_downPaymentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(15);
        make.top.equalTo(superView.mas_top).offset(5);
        make.height.equalTo(@20);
        make.right.equalTo(superView.mas_right);
    }];
    
    [_monthPaymentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(kScreenWidth / 2);
        make.top.equalTo(superView.mas_top).offset(5);
        make.height.equalTo(@20);
        make.right.equalTo(superView.mas_right);
    }];
    
    [_floorLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(15);
        make.top.equalTo(_downPaymentLabel.mas_bottom).offset(10);
        make.height.equalTo(@20);
        make.right.equalTo(_directionLabel.mas_left).offset(-15);
    }];
    
    [_directionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
    make.left.equalTo(superView.mas_left).offset(kScreenWidth / 2);
    make.top.equalTo(_downPaymentLabel.mas_bottom).offset(10);
    make.height.equalTo(@20);
    make.right.equalTo(superView.mas_right);
    }];
    
    [_decorateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(15);
        make.top.equalTo(_floorLabel.mas_bottom).offset(10);
        make.height.equalTo(@20);
        make.right.equalTo(_apartmentLabel.mas_left).offset(-15);
    }];

    [_apartmentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(kScreenWidth / 2);
        make.top.equalTo(_floorLabel.mas_bottom).offset(10);
        make.height.equalTo(@20);
        make.right.equalTo(superView.mas_right);
    }];
    
    [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(15);
        make.top.equalTo(_decorateLabel.mas_bottom).offset(10);
        make.height.equalTo(@20);
        make.right.equalTo(_numberLabel.mas_left).offset(-15);
    }];

    [_numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(kScreenWidth / 2);
        make.top.equalTo(_decorateLabel.mas_bottom).offset(10);
        make.height.equalTo(@20);
        make.right.equalTo(superView.mas_right);
    }];
    
    [_vilageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(15);
        make.top.equalTo(_timeLabel.mas_bottom).offset(10);
        make.height.equalTo(@20);
        make.right.equalTo(superView.mas_right).offset(-15);
    }];
    
}

- (void)setModel:(RentHouseDetailModel *)model {
    if (model != _model) {
        _model = model;
    }
    if ([[model.shoufu description] isEqualToString:@"0"]) {
        NSMutableAttributedString *downPaymentStr = [[NSMutableAttributedString alloc] initWithString:@"首付:无"];
        [downPaymentStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, 3)];
        _downPaymentLabel.attributedText = downPaymentStr;
    }else {
        NSMutableAttributedString *downPaymentStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"首付:%@万", [model.shoufu description]]];
        [downPaymentStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, 3)];
        _downPaymentLabel.attributedText = downPaymentStr;
    }
    if ([[model.yuegong description] isEqualToString:@"0"]) {
        NSMutableAttributedString *monthPaymentStr = [[NSMutableAttributedString alloc] initWithString:@"月供:无"];
        [monthPaymentStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, 3)];
        _monthPaymentLabel.attributedText = monthPaymentStr;
    }else {
        NSMutableAttributedString *monthPaymentStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"月供:%@元/月", [model.yuegong description]]];
        [monthPaymentStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, 3)];
        _monthPaymentLabel.attributedText = monthPaymentStr;
    }
    if ([self checkNum:[model.houseyear description]]) {
        NSMutableAttributedString *timeStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"房龄:%@年", [model.houseyear description]]];
        [timeStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, 3)];
        _floorLabel.attributedText = timeStr;
    }else {
        NSMutableAttributedString *timeStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"房龄:%@", [model.houseyear description]]];
        [timeStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, 3)];
        _floorLabel.attributedText = timeStr;
    }
    NSMutableAttributedString *directionStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"朝向:%@", [model.toward description]]];
    [directionStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, 3)];
    _directionLabel.attributedText = directionStr;
    
    NSMutableAttributedString *floorStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"楼层:第%@层/共%@层", [model.floor1 description], [model.floor2 description]]];
    [floorStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, 3)];
    _decorateLabel.attributedText = floorStr;
    
    NSMutableAttributedString *apartmentStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"编号:%@", [model.code description]]];
    [apartmentStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, 3)];
    _apartmentLabel.attributedText = apartmentStr;
    
    
    NSMutableAttributedString *decorateStr = [[NSMutableAttributedString alloc] initWithString:@"装修:"];
    [decorateStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, 3)];
    if ([model.zhuanxiu isEqualToString:@"1"]) {
        [decorateStr appendAttributedString:[[NSAttributedString alloc] initWithString:@"毛坯"] ];
    }else if ([model.zhuanxiu isEqualToString:@"2"]) {
        [decorateStr appendAttributedString:[[NSAttributedString alloc] initWithString:@"精装修"] ];
    }else if ([model.zhuanxiu isEqualToString:@"3"]) {
        [decorateStr appendAttributedString:[[NSAttributedString alloc] initWithString:@"简装修"] ];
    }else {
        [decorateStr appendAttributedString:[[NSAttributedString alloc] initWithString:@"豪华装修"] ];
    }
    _timeLabel.attributedText = decorateStr;
    
    
    NSMutableAttributedString *areaStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"使用面积:%@㎡", [model.shiyongmj description]]];
    [areaStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, 4)];
    _numberLabel.attributedText = areaStr;
    
    NSMutableAttributedString *vilageStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"上架时间:%@", model.adddate]];
    [vilageStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, 4)];
    _vilageLabel.attributedText = vilageStr;
    
}

/* 检查输入的是否全为数字 返回BOOL值 */
-(BOOL)checkNum:(NSString *)inputStr {
    /* 创建扫描类 */
    NSScanner *scanner = [NSScanner scannerWithString:inputStr];
    int value;
    /* [scanner scanInt:&value]该方法将扫描的int放入value 返回布尔值 */
    /* [scanner isAtEnd] 该方法判断是否走到了字符串末端 返回的也是布尔值 */
    return[scanner scanInt:&value] && [scanner isAtEnd];
}


@end
