//
//  ColorManager.h
//  House
//
//  Created by 吕品 on 16/6/3.
//  Copyright © 2016年 ZJWL. All rights reserved.
//
/*!
 *  @author 众杰网络, 16-06-03 18:06:09
 *
 *  @brief 各种颜色
 *
 *  @since 1
 */
#ifndef ColorManager_h
#define ColorManager_h
#pragma mark naviBarBackColor
#define KNaviBackColor [UIColor colorWithRed:0.831 green:0.149 blue:0.102 alpha:1.0]
#define KMainTableViewTagsColorArray @[[UIColor orangeColor], [UIColor cyanColor], [UIColor yellowColor], [UIColor purpleColor]]
#endif /* ColorManager_h */
