//
//  IndexMainTableViewCell.m
//  House
//
//  Created by 吕品 on 16/6/3.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "IndexMainTableViewCell.h"
#import "IndexMainTableViewCellCollectionViewCell.h"

@interface IndexMainTableViewCell ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@property (nonatomic, strong) UICollectionView *collectionView;

@end
@implementation IndexMainTableViewCell
#pragma  mark init
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        self.collectionView = [[UICollectionView alloc] initWithFrame:self.contentView.frame collectionViewLayout:flowLayout];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView.scrollEnabled = NO;//禁止滑动
        [self.contentView addSubview:_collectionView];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.contentInset = UIEdgeInsetsMake(indexMainVCTableViewPadding, indexMainVCTableViewPadding, indexMainVCTableViewPadding, indexMainVCTableViewPadding);
        [_collectionView registerClass:[IndexMainTableViewCellCollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([IndexMainTableViewCellCollectionViewCell class])];
    }
    return self;
}
- (void)setShowGrayView:(BOOL)showGrayView {
    _showGrayView = showGrayView;
    [_collectionView reloadData];
}

#pragma mark datasource and delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return  _titlesArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    IndexMainTableViewCellCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([IndexMainTableViewCellCollectionViewCell class]) forIndexPath:indexPath];
    cell.titleLabel.text = _titlesArray[indexPath.item];
    cell.headImageView.image = _headImageArray[indexPath.item];
    cell.isWithGrayView = _showGrayView;
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat itemWidth = (self.contentView.frame.size.width - indexMainVCTableViewPadding * (4 + 1)) / (4);
    return CGSizeMake(itemWidth, itemWidth + indexMainVCTableViewCollectionViewCellLabelHeight);
}
#pragma mark delegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self.delegate tableViewCell:self didSelectedAtItem:indexPath.item];
}
#pragma mark layout
- (void)layoutSubviews {
    [super layoutSubviews];
    self.collectionView.frame = self.contentView.frame;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
