//
//  StoresInfomationManager.m
//  House
//
//  Created by 吕品 on 2016/9/27.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "StoresInfomationManager.h"
#import <YTKKeyValueStore/YTKKeyValueStore.h>
#import <FMDB/FMDB.h>
NSString *const dbName = @"mendian.sqlite";
NSString *const haerbin = @"haerbin";
@implementation StoresInfomationManager

+ (YTKKeyValueStore *)YTKStore {
    YTKKeyValueStore *store = [[YTKKeyValueStore alloc] initDBWithName:dbName];
    [store createTableWithName:haerbin];
    return store;
}

+ (NSString *)getNameFromID:(NSString *)uid {
    NSArray *arr = [self getAllObject];
    for (StoresInfomationModel *model in arr) {
        NSLog(@"%@", model.userid);
        if ([model.userid isEqualToString:uid]) {
            NSMutableString *str = [NSMutableString stringWithString:model.company];
            [str deleteCharactersInRange:NSMakeRange(0, 2)];
            [str deleteCharactersInRange:NSMakeRange(str.length - 2, 2)];
            return str;
        }
    }
    return @"";
}

+ (void)createTable {
    [[self YTKStore] createTableWithName:haerbin];
}

+ (NSArray *)getAllObject {
    [self YTKStore];
    NSString * sql = [NSString stringWithFormat:@"SELECT * from %@", haerbin];
    __block NSMutableArray * result = [NSMutableArray array];
    [[FMDatabaseQueue databaseQueueWithPath:[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:dbName]] inDatabase:^(FMDatabase *db) {
        FMResultSet * rs = [db executeQuery:sql];
        while ([rs next]) {
            StoresInfomationModel *model = [[StoresInfomationModel alloc] init];
            model.userid = [rs stringForColumn:@"id"];
            model.company = [rs stringForColumn:@"json"];
            [result addObject:model];
        }
        [rs close];
    }];
    [self getData];
    return result;
}

+ (void)getData {

   [NetRequest getInformationWithGetMethodWithUrl:[NSString stringWithFormat:@"http://aizufang.hrbzjwl.com/tp/api.php/Mendian/index/areaid/%@", [UserInfoManager sharedManager].userCityID] success:^(NSURLSessionDataTask *task, id responseObject) {
       NSLog(@"%@", responseObject);
       if ([[responseObject[@"flag"] description] isEqualToString:@"1"]) {
           NSArray *arr = responseObject[@"mendian"];
           NSMutableArray *array = [NSMutableArray array];
           for (NSDictionary *dic in arr) {
               StoresInfomationModel *model = [[StoresInfomationModel alloc] init];
               model.userid = dic[@"userid"];
               model.company = dic[@"company"];
               [array addObject:model];
           }
           [self putObjectsWithArray:array];
       }
   } failure:^(NSURLSessionDataTask *task, NSError *error) {

   }];
}

+ (void)putObjectsWithArray:(NSArray<StoresInfomationModel *>*)arr {
    for (StoresInfomationModel *model in arr) {
        [[self YTKStore] putString:model.company withId:model.userid intoTable:haerbin];
    }
}


@end

@interface StoresInfomationModel()

@end

@implementation StoresInfomationModel


@end


