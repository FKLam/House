//
//  PublishMainRightCellModel.swift
//  House
//
//  Created by 吕品 on 2016/8/30.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

import UIKit

class PublishMainRightCellModel: BaseModel {
    var addr: String?
    var areaid: NSNumber?
    var company: String?
    var email: String?
    var groupid: NSNumber?
    var mobile: NSNumber?
    var truename: String?
    var userid: NSNumber?
}
