//
//  AgentListDetaiVIew.h
//  House
//
//  Created by 张垚 on 16/7/5.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ImageViewAndLabelButton;

/* 代理协议 将三个按钮点击方法传出去 */
@protocol AgentDetailViewDelegate <NSObject>

- (void)passButton:(ImageViewAndLabelButton *)button;

@end



@interface AgentListDetaiVIew : UIView

/* 协议属性 */
@property (nonatomic, assign) id<AgentDetailViewDelegate>delegate;

@property (nonatomic, strong) UIStackView *buttonStackView;
@property (nonatomic, strong) UIImageView *headImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UIView *dividerView;
@property (nonatomic, strong) UILabel *yearLabel;
@property (nonatomic, strong) UILabel *storesLabel;
@property (nonatomic, strong) UIView *instructionView;

- (instancetype)initWithFrame:(CGRect)frame WithTitleArr:(NSArray *)titleArr WithImageArr:(NSArray *)imageArr WithColorArr:(NSArray *)colorArr WithButtonNum:(NSUInteger)buttonNum;

@end
