//
//  MinePublishPicModel.swift
//  House
//
//  Created by 吕品 on 2016/9/27.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

import UIKit

class MinePublishPicModel: BaseModel {
    var itemid: String?
    var areaid: String?
    var typeid: String?
    var price: String?
    var thumb: String!
    var thumb1: String?
    var thumb2: String?
    var thumb3: String?
    var thumb4: String?
    var truename : String?
    var telephone: String?
    var address: String?
    var editor: String?
    var status: String?
    var housename: String?
    var houseearm: String?
    var room: String?
    var bus: String?
    var peitaor: String?
    var sex: String?
    var lou: String?
    var danyuan: String?
    var fangjian: String?
    var userid: String?
    var street: String?
    var rentmethod: String?
    var mendian: String?
    var number: String?
    var shiyongmj: String?
    var company: String?
    var areaname: String?
}


/*
 itemid: "3139",
 typeid: "0",
 areaid: "32",
 price: "55585",
 thumb: "http://aizufang.hrbzjwl.com/tp/Public/Uploads/file86433_jpg",
 thumb1: "",
 thumb2: "",
 thumb3: "",
 thumb4: "",
 truename: "兔兔",
 telephone: "13154521007",
 mobile: "13154521007",
 address: "来来来，4号楼2单元6室",
 editor: "小龙",
 adddate: "2016-09-27",
 status: "2",
 housename: "吐口",
 room: "4",
 houseearm: "555",
 bus: "一年",
 peitaor: "床,冰箱,电视,空调,家具,宽带,电梯,热水器,洗衣机,可做饭",
 sex: "1",
 lou: "4",
 danyuan: "2",
 fangjian: "6",
 userid: "69",
 street: "来来来，",
 rentmethod: "2",
 mendian: "50",
 
 itemid: "68",
 catid: "0",
 mycatid: "0",
 typeid: null,
 areaid: "32",
 level: "0",
 elite: "0",
 title: "",
 style: "",
 fee: "0",
 introduce: "",
 unit: "",
 price: "55",
 days: "0",
 tag: "",
 keyword: "",
 pptword: "",
 hits: "0",
 thumb: "http://aizufang.hrbzjwl.com/tp/Public/Uploads/file3900_jpg",
 thumb1: "",
 thumb2: "",
 thumb3: "",
 thumb4: "",
 username: "",
 groupid: "0",
 company: "",
 vip: "0",
 validated: "0",
 truename: "看看咯",
 telephone: "13154521007",
 mobile: "13154521007",
 address: "阿里郎33号楼55单元25室",
 email: "",
 msn: "",
 qq: "",
 ali: "",
 skype: "",
 totime: "0",
 editor: "小龙",
 edittime: "0",
 editdate: "0000-00-00",
 addtime: "1474853221",
 adddate: "2016-09-26",
 ip: "",
 template: "0",
 status: "2",
 linkurl: "",
 filepath: "",
 note: "",
 housename: "旮旯",
 houseid: null,
 room: "5",
 hall: null,
 toilet: "",
 balcony: "",
 houseearm: "55",
 floor1: null,
 floor2: null,
 cqxz: null,
 houseyear: null,
 map: null,
 peitao: null,
 fyts: null,
 bus: null,
 zhuanxiu: null,
 toward: null,
 fix: null,
 range: null,
 istop: "0",
 ishot: "0",
 to_time: "0",
 hot_time: "0",
 sex: "2",
 lou: "33",
 danyuan: "55",
 fangjian: "25",
 userid: "69",
 street: "阿里郎",
 public: "0",
 genjin: "0",
 shtj: "0",
 shtg_time: null,
 companyid: null,
 mendian: "53",
 fabumethod: null,
 shiyongmj: "25",
 actions: "App",
 shoufu: null,
 yuegong: null

 */
