//
//  AgentDetailMianInfoCollectionViewCell.h
//  House
//
//  Created by 张垚 on 16/7/20.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AgentListModel;

@interface AgentDetailMianInfoCollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) AgentListModel *model;
@end
