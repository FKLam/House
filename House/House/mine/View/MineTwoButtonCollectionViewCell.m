//
//  MineTwoButtonCollectionViewCell.m
//  House
//
//  Created by 吕品 on 16/6/24.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "MineTwoButtonCollectionViewCell.h"
#import "MineTwoMixView.h"

@implementation MineTwoButtonCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.mixView = [[MineTwoMixView alloc] init];
        [self.contentView addSubview:_mixView];
    }
    return self;
}

- (void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    [super applyLayoutAttributes:layoutAttributes];
    _mixView.frame = CGRectMake(0, 0, 40, 60);
    _mixView.center = self.contentView.center;
}
@end
