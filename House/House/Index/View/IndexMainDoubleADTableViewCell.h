//
//  IndexMainDoubleADTableViewCell.h
//  House
//
//  Created by 吕品 on 16/6/7.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseCollectionWithTableViewCell.h"

/*!
 *  @author 众杰网络, 16-06-07 13:06:13
 *
 *  @brief 带有两个广告的tableviewcell
 *
 *  @since 1.0
 */
@interface IndexMainDoubleADTableViewCell : BaseCollectionWithTableViewCell
@property (nonatomic, copy) NSArray *imageArray;
@end
