//
//  RecycleViewCollectionViewCell.m
//  House
//
//  Created by 张垚 on 16/7/12.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "RecycleViewCollectionViewCell.h"

@implementation RecycleViewCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
   self = [super initWithFrame:frame];
    if (self) {
        _mainImageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_mainImageView];
        
        _pageNumLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_pageNumLabel];
    }
    return self;
}

- (void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    UIView *superView = self.contentView;
    _mainImageView.frame = self.contentView.frame;
    _mainImageView.backgroundColor = [UIColor greenColor];
    [_pageNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(superView.mas_bottom).offset(-10);
        make.right.equalTo(superView.mas_right).offset(-15);
        make.height.equalTo(@30);
        make.width.equalTo(@80);
    }];
    _pageNumLabel.backgroundColor = [UIColor colorWithRed:57 / 255 green:62 / 255 blue:65 / 255 alpha:0.7];
    _pageNumLabel.textColor = [UIColor whiteColor];
    _pageNumLabel.textAlignment = NSTextAlignmentCenter;
    _pageNumLabel.layer.cornerRadius = 4;
    _pageNumLabel.layer.masksToBounds = YES;
}

@end
