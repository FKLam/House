//
//  AgentListModel.h
//  House
//
//  Created by 张垚 on 16/8/4.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "BaseModel.h"

@interface AgentListModel : BaseModel
//经纪人id
@property (nonatomic, strong) NSNumber *userid;
//真实姓名
@property (nonatomic, copy) NSString *truename;
//电话
@property (nonatomic, copy) NSString *mobile;
//分组id
@property (nonatomic, copy) NSString *groupid;
//图片
@property (nonatomic, copy) NSString *thumb;
//个人介绍
@property (nonatomic, copy) NSString *personal;
//入职年份
@property (nonatomic, copy) NSString *career;
//所属门店id
@property (nonatomic, copy) NSString *companyid;
//门店
@property (nonatomic, copy) NSString *company;
//地区id
@property (nonatomic, strong) NSNumber *areaid;
//email
@property (nonatomic, copy) NSString *email;
//社区门店地址
@property (nonatomic, copy) NSString *addr;


@end
