//
//  ReuseRandomCodeTableViewCell.m
//  IndexMoveHousePage
//
//  Created by 张垚 on 16/6/8.
//  Copyright © 2016年 众杰网络. All rights reserved.
//

#import "ReuseRandomCodeTableViewCell.h"
#import "LabelAndTextFieldView.h"


static CGFloat topPadding = 15.0f;
static CGFloat leftPadding = 5.0f;
static CGFloat rightPadding = 10.0f;
static CGFloat bottomPadding = 10.0f;

@implementation ReuseRandomCodeTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _inputView = [[LabelAndTextFieldView alloc] init];
        [self.contentView addSubview:_inputView];
        _getRandomCodeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _getRandomCodeButton.backgroundColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0];
        [self.contentView addSubview:_getRandomCodeButton];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self.contentView;
    [_inputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(topPadding);
        make.left.equalTo(superView.mas_left).offset(leftPadding);
        make.right.equalTo(_getRandomCodeButton.mas_left).offset(-rightPadding);
        make.bottom.equalTo(superView.mas_bottom).offset(-bottomPadding);
    }];
    [_getRandomCodeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(superView.mas_right).offset(-rightPadding);
        make.centerY.equalTo(superView.mas_centerY);
        make.height.equalTo(superView.mas_height);
        make.width.equalTo(@120);
    }];
    [_getRandomCodeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_getRandomCodeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
    _getRandomCodeButton.titleLabel.font = [UIFont systemFontOfSize:15];
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
