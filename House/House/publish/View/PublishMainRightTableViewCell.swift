//
//  PublishMainRightTableViewCell.swift
//  House
//
//  Created by 吕品 on 2016/8/30.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

import UIKit

class PublishMainRightTableViewCell: UITableViewCell {

    internal var nameLabel = UILabel()
    internal var addressLabel = UILabel()
    internal var telLabel = UILabel()
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(nameLabel)
        nameLabel.font = UIFont.systemFont(ofSize: 15)
        nameLabel.textAlignment = .left
        contentView.addSubview(addressLabel)
        addressLabel.font = UIFont.systemFont(ofSize: 12)
        addressLabel.textAlignment = .left
        contentView.addSubview(telLabel)
        telLabel.textAlignment = .left
        telLabel.font = UIFont.systemFont(ofSize: 12)
        
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        let padding: CGFloat = 10.0
        let height = (100 - padding * 3) / 3

        nameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(contentView.snp.top).offset(padding)
            make.left.equalTo(contentView.snp.left).offset(padding)
            make.right.equalTo(contentView.snp.right).offset(-padding)
            make.height.equalTo(height)
        }
        addressLabel.snp.makeConstraints { (make) in
            make.top.equalTo(nameLabel.snp.bottom).offset(padding)
            make.left.right.height.equalTo(nameLabel)
        }
        telLabel.snp.makeConstraints { (make) in
            make.top.equalTo(addressLabel.snp.bottom).offset(padding)
            make.left.right.height.equalTo(addressLabel)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
