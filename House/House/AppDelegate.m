//
//  AppDelegate.m
//  House
//
//  Created by 吕品 on 16/5/31.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "AppDelegate.h"
#import "PulishMainViewController.h"
#import "IndexMainViewController.h"
#import "MessageMainViewController.h"
#import "MineMainViewController.h"
#import <UMSocial.h>
#import <UMSocialQQHandler.h>
#import <UMSocialSinaSSOHandler.h>
#import <UMSocialWechatHandler.h>
#import "GuideViewController.h"


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"first"]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"first"];
        [self CreatGuidePage];
    }else {
        [self creatMainView];
    }
    //默认城市
    [[NSUserDefaults standardUserDefaults] setObject:@"23" forKey:cityID];
    [[NSUserDefaults standardUserDefaults] synchronize];
   //友盟分享
    [self threeSideData];
    [self.window makeKeyAndVisible];
    return YES;
}

//友盟分享
- (void)threeSideData {
    [UMSocialData setAppKey:@"57cfcf67e0f55aaf9e002512"];
    //    //设置微信AppId、appSecret，分享url
    [UMSocialWechatHandler setWXAppId:@"wxd96c3b015b52a040" appSecret:@"1d434b5ac04fded9dab4605a7175506f" url:@"http://www.umeng.com/social"];
    //    //设置手机QQ 的AppId，Appkey，和分享URL，需要#import "UMSocialQQHandler.h"
    [UMSocialQQHandler setQQWithAppId:@"1105608283" appKey:@"tGLLsfNIlM7RfXRT" url:@"http://www.umeng.com/social"];
    //    //打开新浪微博的SSO开关，设置新浪微博回调地址，这里必须要和你在新浪微博后台设置的回调地址一致。需要 #import "UMSocialSinaSSOHandler.h"
    [UMSocialSinaSSOHandler openNewSinaSSOWithAppKey:@"3353368965"
                                              secret:@"d4abcd7253c6bc675add1874bc1e42a2"
                                         RedirectURL:@"http://sns.whalecloud.com/sina2/callback"];
}

//引导页创建
- (void)CreatGuidePage {
    GuideViewController *guideVC = [[GuideViewController alloc] init];
    self.window.rootViewController = guideVC;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeVC) name:@"changeVC" object:nil];
}

//引导页通知
- (void)changeVC {
    [self creatMainView];
}
//四个tabbar视图创建
- (void)creatMainView {
    //主页
    IndexMainViewController *indexMainVC = [[IndexMainViewController alloc] init];
    UINavigationController *indexMainNC = [[UINavigationController alloc] initWithRootViewController:indexMainVC];
    
    //活动
    MessageMainViewController *messageMainVC = [[MessageMainViewController alloc] init];
    UINavigationController *messageMainNC = [[UINavigationController alloc] initWithRootViewController:messageMainVC];
    
    //社区门店
    PulishMainViewController *publishMainVC = [[PulishMainViewController alloc] init];
    UINavigationController *publishMianNC = [[UINavigationController alloc] initWithRootViewController:publishMainVC];
    
    //我的
    MineMainViewController *mineMainVC = [[MineMainViewController alloc] init];
    UINavigationController *mineMainNC = [[UINavigationController alloc] initWithRootViewController:mineMainVC];
    //tab
    UITabBarController *tab = [[UITabBarController alloc] init];
    tab.viewControllers = @[indexMainNC, messageMainNC, publishMianNC, mineMainNC];
    for (NSInteger i = 0; i < KTABTitleArray.count; i ++) {
        UITabBarItem *item = [[UITabBarItem alloc] initWithTitle:KTABTitleArray[i] image:[[UIImage imageNamed:[NSString stringWithFormat:@"unSelectedTab%ld", (long)i]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:[NSString stringWithFormat:@"selectedTab%ld", (long)i]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        [item setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor redColor]} forState:UIControlStateSelected];
        tab.viewControllers[i].tabBarItem = item;
    }
    self.window.rootViewController = tab;
}


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    BOOL result = [UMSocialSnsService handleOpenURL:url];
    if (result == FALSE) {
        //调用其他SDK，例如支付宝SDK等
    }
    return result;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
