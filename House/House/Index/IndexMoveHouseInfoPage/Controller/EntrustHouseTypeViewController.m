//
//  EntrustHouseTypeViewController.m
//  House
//
//  Created by 张垚 on 16/7/25.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "EntrustHouseTypeViewController.h"
#import "BaseTitleNavigationBar.h"

@interface EntrustHouseTypeViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) BaseTitleNavigationBar *myBar;
@property (nonatomic, strong) NSArray *dataArr;
@end

@implementation EntrustHouseTypeViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = YES;
}


#pragma mark - 主程序入口
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    if (_tag == 1) {
        _dataArr = @[@"一居室", @"两居室", @"三居室", @"四居室", @"五居室"];
        //自定义导航栏
        _myBar = [[BaseTitleNavigationBar alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 64) WithTitleStr:@"户型筛选"];
        [self.view addSubview:_myBar];
    }else if (_tag == 2) {
        _dataArr = @[@"整租", @"合厨"];
        //自定义导航栏
        _myBar = [[BaseTitleNavigationBar alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 64) WithTitleStr:@"租赁方式"];
        [self.view addSubview:_myBar];
    }else if (_tag == 4) {
        _dataArr = @[@"毛坯", @"精装", @"简装", @"豪华装修"];
        //自定义导航栏
        _myBar = [[BaseTitleNavigationBar alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 64) WithTitleStr:@"装修方式"];
        [self.view addSubview:_myBar];
    }else if (_tag == 5) {
        _dataArr = @[@"现金", @"公积金贷款", @"商贷"];
        //自定义导航栏
        _myBar = [[BaseTitleNavigationBar alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 64) WithTitleStr:@"付款方式方式"];
        [self.view addSubview:_myBar];
    }else {
        [self handleData];
        //自定义导航栏
        _myBar = [[BaseTitleNavigationBar alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 64) WithTitleStr:@"门店选择"];
        [self.view addSubview:_myBar];
    }
    [self initTableView];
    //添加手势
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(back)];
    [_myBar.leftView addGestureRecognizer:tap];
    // Do any additional setup after loading the view.
}

- (void)handleData {
    _dataArr = [NSArray array];
    [NetRequest getWithURLString:@"http://aizufang.hrbzjwl.com/tp/api.php/Index/mendiancx" parameters:@{@"areaid": _areaID} progress:^(NSProgress *progress) {
        
    } success:^(id response) {
        _dataArr = response;
        NSMutableArray *arr = [NSMutableArray array];
        for (NSDictionary *dic in _dataArr) {
            StoresInfomationModel *model = [[StoresInfomationModel alloc] init];
            model.userid = dic[@"userid"];
            model.company = dic[@"company"];
            [arr addObject:model];
        }
        [StoresInfomationManager putObjectsWithArray:arr];
        [_tableView reloadData];
    } failure:^(NSError *error) {
        
    }];
}

//返回上一层级
- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
}

/* block传值 */
- (void)passInfoToPreviousPage:(chooseInfoBlock)infoBlock {
    self.chooseInfoBlock = infoBlock;
}


#pragma mark - tableview
/* 创建 */
- (void)initTableView {
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, kScreenWidth, kScreenHeight - 64) style:UITableViewStylePlain];
    [self.view addSubview:_tableView];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:NSStringFromClass([UITableViewCell class])];
    _tableView.tableFooterView = [UIView new];
}
//行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataArr.count;
}

/* cell */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:NSStringFromClass([UITableViewCell class])];
    if (_tag == 3) {
        NSDictionary *dic = _dataArr[indexPath.row];
        cell.textLabel.text = dic[@"company"];
        cell.tag = [dic[@"userid"] integerValue];
    }else {
       cell.textLabel.text = _dataArr[indexPath.row];
        cell.tag = indexPath.row + 1;
    }
    //判断之前选择的方式 标注为红色
    if ([cell.textLabel.text isEqualToString:_currentStr]) {
        cell.textLabel.textColor = KNaviBackColor;
    }
    return cell;
}
//cell点击事件
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    _chooseInfoBlock(cell.textLabel.text, cell.tag);
    [self back];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
