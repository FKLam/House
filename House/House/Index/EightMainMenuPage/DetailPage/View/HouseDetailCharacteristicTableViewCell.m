//
//  HouseDetailCharacteristicTableViewCell.m
//  House
//
//  Created by 张垚 on 16/10/7.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "HouseDetailCharacteristicTableViewCell.h"
#import "RentHouseDetailModel.h"


@implementation HouseDetailCharacteristicTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _tagLabel1 = [[UILabel alloc] init];
        [self.contentView addSubview:_tagLabel1];
        
        _tagLabel2 = [[UILabel alloc] init];
        [self.contentView addSubview:_tagLabel2];
        
        _tagLabel3 = [[UILabel alloc] init];
        [self.contentView addSubview:_tagLabel3];
        
        _tagLabel4 = [[UILabel alloc] init];
        [self.contentView addSubview:_tagLabel4];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self.contentView;
    
    [_tagLabel1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(20);
        make.top.equalTo(superView.mas_top).offset(5);
        make.height.equalTo(@20);
        make.width.equalTo(@70);
    }];
    
    [_tagLabel2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_tagLabel1.mas_right).offset(5);
        make.top.equalTo(superView.mas_top).offset(5);
        make.height.equalTo(@20);
        make.width.equalTo(@70);
    }];
    
    [_tagLabel3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_tagLabel2.mas_right).offset(5);
        make.top.equalTo(superView.mas_top).offset(5);
        make.height.equalTo(@20);
        make.width.equalTo(@70);
    }];
    
    [_tagLabel4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_tagLabel3.mas_right).offset(5);
        make.top.equalTo(superView.mas_top).offset(5);
        make.height.equalTo(@20);
        make.width.equalTo(@70);
    }];
}

- (void)setModel:(RentHouseDetailModel *)model {
    if (model != _model) {
        _model = model;
    }
    NSArray *colorArr = @[[UIColor redColor], [UIColor purpleColor], [UIColor orangeColor], [UIColor cyanColor]];
    if ([model.fyts isEqualToString:@""]) {
        return;
    }else {
        for (int i = 0; i < [model.fyts componentsSeparatedByString:@","].count; i++) {
            if (i == 0) {
                _tagLabel1.layer.borderColor = [UIColor redColor].CGColor;
                _tagLabel1.text = [model.fyts componentsSeparatedByString:@","][i];
                _tagLabel1.textColor = colorArr[i];
                
                _tagLabel1.layer.borderWidth = 1.0;
                _tagLabel1.font = [UIFont systemFontOfSize:13];
                _tagLabel1.text = [model.fyts componentsSeparatedByString:@","][i];
                _tagLabel1.textAlignment = NSTextAlignmentCenter;
            }else if (i == 1) {
                _tagLabel2.text = [model.fyts componentsSeparatedByString:@","][i];
                _tagLabel2.textColor = colorArr[i];
                _tagLabel2.layer.borderColor = [UIColor purpleColor].CGColor;
                _tagLabel2.layer.borderWidth = 1.0;
                _tagLabel2.font = [UIFont systemFontOfSize:13];
                _tagLabel2.text = [model.fyts componentsSeparatedByString:@","][i];
                _tagLabel2.textAlignment = NSTextAlignmentCenter;
            }else if (i == 2) {
                _tagLabel3.text = [model.fyts componentsSeparatedByString:@","][i];
                _tagLabel3.textColor = colorArr[i];
                _tagLabel3.layer.borderColor = [UIColor orangeColor].CGColor;
                _tagLabel3.layer.borderWidth = 1.0;
                _tagLabel3.font = [UIFont systemFontOfSize:13];
                _tagLabel3.text = [model.fyts componentsSeparatedByString:@","][i];
                _tagLabel3.textAlignment = NSTextAlignmentCenter;
            }else {
                _tagLabel4.text = [model.fyts componentsSeparatedByString:@","][i];
                _tagLabel4.textColor = colorArr[i];
                _tagLabel4.layer.borderColor = [UIColor cyanColor].CGColor;
                _tagLabel4.layer.borderWidth = 1.0;
                _tagLabel4.font = [UIFont systemFontOfSize:13];
                _tagLabel4.text = [model.fyts componentsSeparatedByString:@","][i];
                _tagLabel4.textAlignment = NSTextAlignmentCenter;
            }
        }
        
    }
}

- (void)setCurrentModel:(RentHouseDetailModel *)currentModel {
    if (currentModel != _currentModel) {
        _currentModel = currentModel;
    }
    NSArray *colorArr = @[[UIColor redColor], [UIColor purpleColor], [UIColor orangeColor], [UIColor cyanColor]];
    if ([currentModel.fytsr isEqualToString:@""]) {
        return;
    }else {
        for (int i = 0; i < [currentModel.fytsr componentsSeparatedByString:@","].count; i++) {
            if (i == 0) {
                _tagLabel1.layer.borderColor = [UIColor redColor].CGColor;
                _tagLabel1.text = [currentModel.fytsr componentsSeparatedByString:@","][i];
                _tagLabel1.textColor = colorArr[i];
                
                _tagLabel1.layer.borderWidth = 1.0;
                _tagLabel1.font = [UIFont systemFontOfSize:13];
                _tagLabel1.text = [currentModel.fytsr componentsSeparatedByString:@","][i];
                _tagLabel1.textAlignment = NSTextAlignmentCenter;
            }else if (i == 1) {
                _tagLabel2.text = [currentModel.fytsr componentsSeparatedByString:@","][i];
                _tagLabel2.textColor = colorArr[i];
                _tagLabel2.layer.borderColor = [UIColor purpleColor].CGColor;
                _tagLabel2.layer.borderWidth = 1.0;
                _tagLabel2.font = [UIFont systemFontOfSize:13];
                _tagLabel2.text = [currentModel.fytsr componentsSeparatedByString:@","][i];
                _tagLabel2.textAlignment = NSTextAlignmentCenter;
            }else if (i == 2) {
                _tagLabel3.text = [currentModel.fytsr componentsSeparatedByString:@","][i];
                _tagLabel3.textColor = colorArr[i];
                _tagLabel3.layer.borderColor = [UIColor orangeColor].CGColor;
                _tagLabel3.layer.borderWidth = 1.0;
                _tagLabel3.font = [UIFont systemFontOfSize:13];
                _tagLabel3.text = [currentModel.fytsr componentsSeparatedByString:@","][i];
                _tagLabel3.textAlignment = NSTextAlignmentCenter;
            }else {
                _tagLabel4.text = [currentModel.fytsr componentsSeparatedByString:@","][i];
                _tagLabel4.textColor = colorArr[i];
                _tagLabel4.layer.borderColor = [UIColor cyanColor].CGColor;
                _tagLabel4.layer.borderWidth = 1.0;
                _tagLabel4.font = [UIFont systemFontOfSize:13];
                _tagLabel4.text = [currentModel.fytsr componentsSeparatedByString:@","][i];
                _tagLabel4.textAlignment = NSTextAlignmentCenter;
            }
        }
        
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
