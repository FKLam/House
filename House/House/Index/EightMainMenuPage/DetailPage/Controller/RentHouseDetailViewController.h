//
//  RentHouseDetailViewController.h
//  House
//
//  Created by 张垚 on 16/7/19.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "BaseViewController.h"

@interface RentHouseDetailViewController : BaseViewController

@property (nonatomic, copy) NSString *itemid;

@end
