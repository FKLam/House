//
//  MineMainCellView.swift
//  House
//
//  Created by 吕品 on 16/7/11.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

import UIKit

class MineMainCellView: UIView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    let label = UILabel()
    let imageView = UIImageView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(label)
        self.addSubview(imageView)
        label.textAlignment = .left
        imageView.contentMode = .scaleAspectFit
        imageView.snp.makeConstraints { (make) in
            make.width.equalTo(30)
            make.left.top.equalTo(self).offset(7)
            make.bottom.equalTo(self.snp.bottom).offset(-7)
        }
        label.snp.makeConstraints { (make) in
            make.left.equalTo(imageView.snp.right).offset(10)
            make.right.top.bottom.equalTo(self)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
