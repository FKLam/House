//
//  PublishMainLeftTableViewCell.swift
//  House
//
//  Created by 吕品 on 2016/8/29.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

import UIKit

class PublishMainLeftTableViewCell: UITableViewCell {

    var label: UILabel!

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        label = UILabel()
        contentView.addSubview(label)
        label?.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 17)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        label.snp.makeConstraints { (make) in
            make.top.left.bottom.right.equalTo(contentView)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
