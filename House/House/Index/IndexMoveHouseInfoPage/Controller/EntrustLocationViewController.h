//
//  EntrustLocationViewController.h
//  House
//
//  Created by 张垚 on 16/7/25.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "BaseViewController.h"

typedef void(^chooseInfoBlock)(NSString *currentStr, NSInteger currentTag);
@interface EntrustLocationViewController : BaseViewController
@property (nonatomic, strong) NSString *currentStr;
@property (nonatomic, copy) chooseInfoBlock chooseInfoBlock;
- (void)passInfoToPreviousPage:(chooseInfoBlock)infoBlock;
@end
