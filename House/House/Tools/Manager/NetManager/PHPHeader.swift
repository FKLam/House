//
//  PHPHeader.swift
//  House
//
//  Created by 吕品 on 16/7/11.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

import UIKit
private let PHPHeadera : String = "http://aizufang.hrbzjwl.com"

class PHPHeader: NSObject {



    /*
     —^ _ ^—注册接口=================================================================
     http://192.168.88.189/junrongdichan/tp/api.php/Index/app_login

     act:act 验证码成功失败
     tel:手机号码
     pwd:密码
     repwd:重复验证密码

     flg=1 注册成功
     flg=0 注册失败
     flg=2 两次密码不一致
     flg=3 手机号已被注册
     */
    let regPHP : String = PHPHeadera + "/tp/api.php/Index/app_login"
    /*
     验证码
     http://192.168.88.189/junrongdichan/tp/api.php/Index/app_yzm
     mobile:手机号码

     yzm:验证码
     mobile:手机号
     act:1
     */
    let yzmRogerPHP = PHPHeadera + "/tp/api.php/Index/app_yzm"
    /*
     登录

     flg=1 用户不存在
     flg=2 登录成功
     flg=3 密码错误
     
     tel手机号
     pwd密码
     */
    let loginPHP = PHPHeadera + "/tp/api.php/Index/app_index"
    /*
        忘记密码
        http://192.168.88.189/junrongdichan/tp/api.php/Index/app_password
     和注册一样
     */

    let forgetPasswordPHP = PHPHeadera + "/tp/api.php/Index/app_password"
    /*
     http://192.168.88.189/junrongdichan/tp/api.php/Index/app_editmobile
     修改手机
     id
     phone
     */
    let revisePhonePHP = PHPHeadera + "/tp/api.php/Index/app_editmobile"

    /*
     http://192.168.88.189/junrongdichan/tp/api.php/Index/App_name_edit/id/54/name/非常玩家
     修改昵称
     
     id
     name
     */
    let reviseNickNamePHP = PHPHeadera + "/tp/api.php/Index/App_name_edit"
    /*
     —^ _ ^—密码修改==================================================================
     http://192.168.88.189/junrongdichan/tp/api.php/Index/App_editpassword/id/55/old/222/new/7777
     
     id
     old
     new

     返回值 1成功 2原密码输入错误 0修改失败
     */
    let revisePassword = PHPHeadera + "/tp/api.php/Index/App_editpassword"

    /*
     http://192.168.88.189/junrongdichan/tp/api.php/Index/App_upload
     target_file:""
     dir: ""
     id: userid
     */
    let headImageUploadPHP = PHPHeadera + "/tp/api.php/Index/App_upload"

    //http://192.168.88.107/junrongdichan/tp/Public/Uploads/headImage8.jpg
    let headImageReadPHP = PHPHeadera + "/tp/Public/Uploads/headImage" + UserInfoManager.shared().uid + ".png"

    class func manager() -> PHPHeader {
        let manager = PHPHeader()
        return manager
    }


}
