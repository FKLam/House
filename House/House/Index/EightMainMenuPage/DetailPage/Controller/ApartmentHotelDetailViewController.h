//
//  ApartmentHotelDetailViewController.h
//  House
//
//  Created by 张垚 on 16/7/21.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "BaseViewController.h"

@interface ApartmentHotelDetailViewController : BaseViewController
@property (nonatomic, copy) NSString *hotelID;
@property (nonatomic, copy) NSString *titleStr;

@end
