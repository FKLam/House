//
//  RentHouseListNoSlideTableViewCell.m
//  House
//
//  Created by 张垚 on 16/9/2.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "RentHouseListNoSlideTableViewCell.h"
#import "RentHouseListModel.h"



@implementation RentHouseListNoSlideTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        /* 详情界面控件 */
        _roomImageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_roomImageView];
        
        _tilteLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_tilteLabel];
        
        _doorModelLabels = [[UILabel alloc] init];
        [self.contentView addSubview:_doorModelLabels];
        
        _communityLabels = [[UILabel alloc] init];
        [self.contentView addSubview:_communityLabels];
        
        _priceLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_priceLabel];
        
        _areaSizeLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_areaSizeLabel];
        
        _tagLabel1 = [[UILabel alloc] init];
        [self.contentView addSubview:_tagLabel1];
        
        _tagLabel2 = [[UILabel alloc] init];
        [self.contentView addSubview:_tagLabel2];
        
        _tagLabel3 = [[UILabel alloc] init];
        [self.contentView addSubview:_tagLabel3];
        
        _tagLabel4 = [[UILabel alloc] init];
        [self.contentView addSubview:_tagLabel4];
    }
    return self;
}



- (void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self.contentView;
    [_roomImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(10);
        make.centerY.equalTo(superView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(110, 85));
    }];
    
    _roomImageView.layer.cornerRadius = 6;
    _roomImageView.layer.masksToBounds = YES;
    [_tilteLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_roomImageView.mas_right).offset(10);
        make.top.equalTo(_roomImageView.mas_top).offset(5);
        make.height.equalTo(@15);
        make.right.equalTo(superView.mas_right).offset(-10);
    }];
    
    _tilteLabel.font = [UIFont systemFontOfSize:15 weight:17];
    [_doorModelLabels mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(superView.mas_centerY);
        make.left.equalTo(_roomImageView.mas_right).offset(10);
        make.height.equalTo(@15);
        make.right.equalTo(_priceLabel.mas_left).offset(-10);
    }];
    
    _doorModelLabels.font = [UIFont systemFontOfSize:10];
    _doorModelLabels.textAlignment = NSTextAlignmentLeft;
    _doorModelLabels.textColor = [UIColor colorWithRed:0.3912 green:0.3874 blue:0.395 alpha:1.0];
    
    [_communityLabels mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_centerY);
        make.left.equalTo(_roomImageView.mas_right).offset(10);
        make.height.equalTo(@15);
        make.right.equalTo(_priceLabel.mas_left).offset(-10);
    }];
    
    _communityLabels.textAlignment = NSTextAlignmentLeft;
    _communityLabels.font = [UIFont systemFontOfSize:10];
    _communityLabels.textColor = [UIColor colorWithRed:0.3912 green:0.3874 blue:0.395 alpha:1.0];
    [_tagLabel1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_roomImageView.mas_right).offset(10);
        make.top.equalTo(_communityLabels.mas_bottom).offset(5);
        make.height.equalTo(@15);
        make.width.equalTo(@40);
    }];
    
    [_tagLabel2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_tagLabel1.mas_right).offset(5);
        make.top.equalTo(_communityLabels.mas_bottom).offset(5);
        make.height.equalTo(@15);
        make.width.equalTo(@40);
    }];
    
    [_tagLabel3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_tagLabel2.mas_right).offset(5);
        make.top.equalTo(_communityLabels.mas_bottom).offset(5);
        make.height.equalTo(@15);
        make.width.equalTo(@40);
    }];
    
    [_tagLabel4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_tagLabel3.mas_right).offset(5);
        make.top.equalTo(_communityLabels.mas_bottom).offset(5);
        make.height.equalTo(@15);
        make.width.equalTo(@40);
    }];
    [_priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(superView.mas_centerY);
        make.right.equalTo(superView.mas_right).offset(-10);
        make.width.equalTo(@60);
        make.height.equalTo(@15);
    }];
    [_areaSizeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(superView.mas_right).offset(-10);
        make.top.equalTo(superView.mas_centerY);
        make.width.equalTo(@60);
        make.height.equalTo(@15);
    }];
    
    _priceLabel.textAlignment = NSTextAlignmentRight;
    _priceLabel.textColor = KNaviBackColor;
    _priceLabel.font = [UIFont systemFontOfSize:15];
    _areaSizeLabel.font = [UIFont systemFontOfSize:10];
    _areaSizeLabel.textColor = [UIColor colorWithRed:0.3912 green:0.3874 blue:0.395 alpha:1.0];
    _areaSizeLabel.textAlignment = NSTextAlignmentRight;
}


- (void)setModel:(RentHouseListModel *)model {
    if (model != _model) {
        _model = model;
    }
    _tilteLabel.text = [model.title description];
    [_roomImageView setImageWithURL:[NSURL URLWithString:[model.thumb description]] placeholderImage:[UIImage imageNamed:@"placeHolderImage"]];
    _doorModelLabels.text = [NSString stringWithFormat:@"%@室%@厅·%@㎡·%@", [model.room description], [model.hall description], [model.houseearm description], [model.toward description]];
    _communityLabels.text = [NSString stringWithFormat:@"%@", [model.housename description]];
    _priceLabel.text = [NSString stringWithFormat:@"%@/月", [model.price description]];
    if ([[model.rentmethod description] isEqualToString:@"1"]) {
        _areaSizeLabel.text = @"整租";
    }else {
        _areaSizeLabel.text = @"合厨";
    }
    
    NSArray *colorArr = @[[UIColor redColor], [UIColor purpleColor], [UIColor orangeColor], [UIColor cyanColor]];
    if ([[model.fytsr description] isEqualToString:@""]) {
        return;
    }else {
        if ([model.fytsr componentsSeparatedByString:@","].count == 1) {
            _tagLabel1.layer.borderColor = [UIColor redColor].CGColor;
            _tagLabel1.text = [model.fytsr componentsSeparatedByString:@","][0];
            _tagLabel1.textColor = colorArr[0];
            
            _tagLabel1.layer.borderWidth = 1.0;
            _tagLabel1.font = [UIFont systemFontOfSize:8];
            _tagLabel1.text = [model.fytsr componentsSeparatedByString:@","][0];
            _tagLabel1.textAlignment = NSTextAlignmentCenter;
            
            _tagLabel2.textColor = [UIColor clearColor];
            _tagLabel2.layer.borderColor = [UIColor clearColor].CGColor;
            _tagLabel3.textColor = [UIColor clearColor];
            _tagLabel3.layer.borderColor = [UIColor clearColor].CGColor;
            _tagLabel4.textColor = [UIColor clearColor];
            _tagLabel4.layer.borderColor = [UIColor clearColor].CGColor;
            
        }else if ([model.fytsr componentsSeparatedByString:@","].count == 2) {
            _tagLabel1.layer.borderColor = [UIColor redColor].CGColor;
            _tagLabel1.text = [model.fytsr componentsSeparatedByString:@","][0];
            _tagLabel1.textColor = colorArr[0];
            
            _tagLabel1.layer.borderWidth = 1.0;
            _tagLabel1.font = [UIFont systemFontOfSize:8];
            _tagLabel1.text = [model.fytsr componentsSeparatedByString:@","][0];
            _tagLabel1.textAlignment = NSTextAlignmentCenter;
            
            _tagLabel2.text = [model.fytsr componentsSeparatedByString:@","][1];
            _tagLabel2.textColor = colorArr[1];
            _tagLabel2.layer.borderColor = [UIColor purpleColor].CGColor;
            _tagLabel2.layer.borderWidth = 1.0;
            _tagLabel2.font = [UIFont systemFontOfSize:8];
            _tagLabel2.text = [model.fytsr componentsSeparatedByString:@","][1];
            _tagLabel2.textAlignment = NSTextAlignmentCenter;
            
            _tagLabel3.textColor = [UIColor clearColor];
            _tagLabel3.layer.borderColor = [UIColor clearColor].CGColor;
            _tagLabel4.textColor = [UIColor clearColor];
            _tagLabel4.layer.borderColor = [UIColor clearColor].CGColor;
            
        }else if ([model.fytsr componentsSeparatedByString:@","].count == 3) {
            _tagLabel1.layer.borderColor = [UIColor redColor].CGColor;
            _tagLabel1.text = [model.fytsr componentsSeparatedByString:@","][0];
            _tagLabel1.textColor = colorArr[0];
            
            _tagLabel1.layer.borderWidth = 1.0;
            _tagLabel1.font = [UIFont systemFontOfSize:8];
            _tagLabel1.text = [model.fytsr componentsSeparatedByString:@","][0];
            _tagLabel1.textAlignment = NSTextAlignmentCenter;
            
            _tagLabel2.text = [model.fytsr componentsSeparatedByString:@","][1];
            _tagLabel2.textColor = colorArr[1];
            _tagLabel2.layer.borderColor = [UIColor purpleColor].CGColor;
            _tagLabel2.layer.borderWidth = 1.0;
            _tagLabel2.font = [UIFont systemFontOfSize:8];
            _tagLabel2.text = [model.fytsr componentsSeparatedByString:@","][1];
            _tagLabel2.textAlignment = NSTextAlignmentCenter;
            
            _tagLabel3.text = [model.fytsr componentsSeparatedByString:@","][2];
            _tagLabel3.textColor = colorArr[2];
            _tagLabel3.layer.borderColor = [UIColor orangeColor].CGColor;
            _tagLabel3.layer.borderWidth = 1.0;
            _tagLabel3.font = [UIFont systemFontOfSize:8];
            _tagLabel3.text = [model.fytsr componentsSeparatedByString:@","][2];
            _tagLabel3.textAlignment = NSTextAlignmentCenter;
            
            _tagLabel4.textColor = [UIColor clearColor];
            _tagLabel4.layer.borderColor = [UIColor clearColor].CGColor;
        }else {
            for (int i = 0; i < [model.fytsr componentsSeparatedByString:@","].count; i++) {
                if (i == 0) {
                    _tagLabel1.layer.borderColor = [UIColor redColor].CGColor;
                    _tagLabel1.text = [model.fytsr componentsSeparatedByString:@","][i];
                    _tagLabel1.textColor = colorArr[i];
                    
                    _tagLabel1.layer.borderWidth = 1.0;
                    _tagLabel1.font = [UIFont systemFontOfSize:8];
                    _tagLabel1.text = [model.fytsr componentsSeparatedByString:@","][i];
                    _tagLabel1.textAlignment = NSTextAlignmentCenter;
                }else if (i == 1) {
                    _tagLabel2.text = [model.fytsr componentsSeparatedByString:@","][i];
                    _tagLabel2.textColor = colorArr[i];
                    _tagLabel2.layer.borderColor = [UIColor purpleColor].CGColor;
                    _tagLabel2.layer.borderWidth = 1.0;
                    _tagLabel2.font = [UIFont systemFontOfSize:8];
                    _tagLabel2.text = [model.fytsr componentsSeparatedByString:@","][i];
                    _tagLabel2.textAlignment = NSTextAlignmentCenter;
                }else if (i == 2) {
                    _tagLabel3.text = [model.fytsr componentsSeparatedByString:@","][i];
                    _tagLabel3.textColor = colorArr[i];
                    _tagLabel3.layer.borderColor = [UIColor orangeColor].CGColor;
                    _tagLabel3.layer.borderWidth = 1.0;
                    _tagLabel3.font = [UIFont systemFontOfSize:8];
                    _tagLabel3.text = [model.fytsr componentsSeparatedByString:@","][i];
                    _tagLabel3.textAlignment = NSTextAlignmentCenter;
                }else {
                    _tagLabel4.text = [model.fytsr componentsSeparatedByString:@","][i];
                    _tagLabel4.textColor = colorArr[i];
                    _tagLabel4.layer.borderColor = [UIColor cyanColor].CGColor;
                    _tagLabel4.layer.borderWidth = 1.0;
                    _tagLabel4.font = [UIFont systemFontOfSize:8];
                    _tagLabel4.text = [model.fytsr componentsSeparatedByString:@","][i];
                    _tagLabel4.textAlignment = NSTextAlignmentCenter;
                }
            }
        }
    }
}




- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
