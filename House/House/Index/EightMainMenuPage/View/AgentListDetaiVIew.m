//
//  AgentListDetaiVIew.m
//  House
//
//  Created by 张垚 on 16/7/5.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "AgentListDetaiVIew.h"
#import "ImageViewAndLabelButton.h"


@implementation AgentListDetaiVIew


- (instancetype)initWithFrame:(CGRect)frame WithTitleArr:(NSArray *)titleArr WithImageArr:(NSArray *)imageArr WithColorArr:(NSArray *)colorArr WithButtonNum:(NSUInteger)buttonNum {
    self = [super initWithFrame:frame];
    if (self) {
#pragma mark -  按钮视图
        /* uistackview初始化 属性设置 */
        _buttonStackView = [[UIStackView alloc] init];
        [self addSubview:_buttonStackView];
        /* 布局方向 */
        _buttonStackView.axis = UILayoutConstraintAxisHorizontal;
        /* 间距 */
        _buttonStackView.spacing = 0.0f;
        /* 每个视图之间的关系 */
        _buttonStackView.distribution = UIStackViewDistributionFillEqually;
        /* 对齐方式 */
        _buttonStackView.alignment = UIStackViewAlignmentFill;
        UIView *superView = self;
        NSInteger width = 80 * buttonNum;
        [_buttonStackView mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(superView.mas_top);
            make.right.equalTo(superView.mas_right);
            make.height.equalTo(superView.mas_height);
            make.width.mas_equalTo(width);
        }];
        /* 循环铺button */
        for (int i = 0; i < buttonNum; i++) {
            ImageViewAndLabelButton *button = [ImageViewAndLabelButton buttonWithType:UIButtonTypeCustom];
            button.backgroundColor = colorArr[i];
            button.logoNameLabel.text = titleArr[i];
            button.logoNameLabel.textColor = [UIColor whiteColor];
            button.logoImage.image = imageArr[i];
            button.tag = i + 1000;
            [button addTarget:self action:@selector(pushDetail:) forControlEvents:UIControlEventTouchUpInside];
            [_buttonStackView addArrangedSubview:button];
        }
        
        _headImageView = [[UIImageView alloc] init];
        [self addSubview:_headImageView];
        _nameLabel = [[UILabel alloc] init];
        [self addSubview:_nameLabel];
        _dividerView = [[UIView alloc] init];
        [self addSubview:_dividerView];
        _yearLabel = [[UILabel alloc] init];
        [self addSubview:_yearLabel];
        _storesLabel = [[UILabel alloc] init];
        [self addSubview:_storesLabel];
        _instructionView = [[UIView alloc] init];
        [self addSubview:_instructionView];
        
        
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self;
    [_headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@60);
        make.centerY.equalTo(superView.mas_centerY);
        make.left.equalTo(superView.mas_left).offset(10);
        make.width.equalTo(_headImageView.mas_height);
    }];
    _headImageView.layer.cornerRadius = _headImageView.frame.size.width / 2;
    _headImageView.layer.masksToBounds = YES;
    
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_headImageView.mas_right).offset(10);
        make.centerY.equalTo(superView.mas_centerY);
        make.width.equalTo(@60);
        make.height.equalTo(@20);
    }];
    [_dividerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_nameLabel.mas_right).offset(10);
        make.height.equalTo(@25);
        make.width.equalTo(@1);
        make.centerY.equalTo(superView.mas_centerY);
    }];
    [_yearLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_dividerView.mas_right).offset(10);
        make.bottom.equalTo(superView.mas_centerY);
        make.right.equalTo(_instructionView.mas_left).offset(-2);
        make.height.equalTo(@20);
    }];
    [_storesLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_dividerView.mas_right).offset(10);
        make.top.equalTo(superView.mas_centerY);
        make.right.equalTo(_instructionView.mas_left).offset(-2);
        make.height.equalTo(@20);
    }];
    [_instructionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_buttonStackView.mas_left);
        make.centerY.equalTo(superView.mas_centerY);
        make.height.equalTo(@25);
        make.width.equalTo(@3);
    }];
    _headImageView.backgroundColor = [UIColor redColor];
    _dividerView.backgroundColor = [UIColor lightGrayColor];
    _instructionView.backgroundColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0];
}

- (void)pushDetail:(ImageViewAndLabelButton *)button {
    [self.delegate passButton:button];
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
