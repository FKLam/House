//
//  BaseTitleNavigationBar.h
//  House
//
//  Created by 张垚 on 16/7/8.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTitleNavigationBar : UIView

- (instancetype)initWithFrame:(CGRect)frame WithTitleStr:(NSString *)title;

@property (nonatomic, strong) UIView *leftView;
@property (nonatomic, strong) UILabel *titleLabel;

@end
