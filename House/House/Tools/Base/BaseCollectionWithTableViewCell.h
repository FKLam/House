//
//  BaseCollectionWithTableViewCell.h
//  House
//
//  Created by 吕品 on 16/6/8.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BaseCollectionWithTableViewCell;
@protocol BaseCollectionWithTableViewCellDelegate <NSObject>
@optional
- (void)tableViewCell:(BaseCollectionWithTableViewCell *)cell didSelectedAtItem:(NSInteger)item;

@end
@interface BaseCollectionWithTableViewCell : UITableViewCell
@property (nonatomic, assign) id<BaseCollectionWithTableViewCellDelegate>delegate;
@end
