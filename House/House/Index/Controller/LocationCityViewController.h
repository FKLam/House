//
//  LocationCityViewController.h
//  House
//
//  Created by 张垚 on 16/6/24.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "BaseViewController.h"

typedef void(^cityNameBlock)(NSString *cityName);

@interface LocationCityViewController : BaseViewController

@property (nonatomic, copy) cityNameBlock cityBlock;
- (void)passCityBlock:(cityNameBlock )cityNameBlock;

@end
