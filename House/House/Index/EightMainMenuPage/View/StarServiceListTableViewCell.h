//
//  StarServiceListTableViewCell.h
//  House
//
//  Created by 张垚 on 16/8/22.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>
@class StarServiceListModel;



@interface StarServiceListButton : UIButton

@end


@interface StarServiceListTableViewCell : UITableViewCell
@property (nonatomic, strong) StarServiceListModel *model;
@property (nonatomic, strong) UILabel *tilteLabel;
@property (nonatomic, strong) UIButton *callButton;
@property (nonatomic, strong) StarServiceListButton *serviceButton1;
@property (nonatomic, strong) StarServiceListButton *serviceButton2;
@property (nonatomic, strong) StarServiceListButton *serviceButton3;
@property (nonatomic, strong) UILabel *contentLabel;

+ (CGFloat)heightForModel:(StarServiceListModel *)model;

@end
