//
//  NetRequest.h
//  House
//
//  Created by 吕品 on 16/5/31.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface NetRequest : NSObject
/*!
 *  @author 众杰网络, 16-05-31 15:05:54
 *
 *  @brief 图片上传(POST)
 *
 *  @param image      要上传的图片
 *  @param url        接口
 *  @param parameters 参数(字典)
 *  @param progress   上传进度
 *  @param success    成功回调
 *  @param failure    失败回调
 *
 *  @since 1.0
 */
+ (void)upLoadImage:(UIImage *)image
            withUrl:(NSString *)url
     withParameters:(NSDictionary *)parameters
           progress:(void (^)(NSProgress *progress))progress
            success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
            failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;
/*!
 *  @author 众杰网络, 16-05-31 15:05:56
 *
 *  @brief 资料更新或读取资料(POST)
 *
 *  @param url        接口
 *  @param parameters 参数（字典）
 *  @param success    成功回调
 *  @param failure    失败回调
 *
 *  @since 1.0
 */
+ (void)updateOrGetInformationFromUrl:(NSString *)url
                        withParameters:(NSDictionary *)parameters
                               success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                               failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;
/*!
 *  @author 众杰网络, 16-05-31 15:05:44
 *
 *  @brief 读取信息(GET)
 *
 *  @param url     接口
 *  @param success 成功回调
 *  @param failure 失败回调
 *
 *  @since 1.0
 */
+ (void)getInformationWithGetMethodWithUrl:(NSString *)url
                                   success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                   failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+ (void)getCodeNumberWithPhoneNumber:(NSString *)phonenumber
                             success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                             failure:(void (^)(NSURLSessionDataTask *task, NSError *))failure;

+(void)getWithURLString:(NSString *)URLString
             parameters:(id)parameters
               progress:(void(^)(NSProgress *progress))progress
                success:(void (^)(id response))success
                failure:(void (^)(NSError *error))failure;

+ (void)downloadImageWithUrl:(NSString *)url imageView:(UIImageView *)imageView placeHolder:(UIImage *)placeHolder;
+ (void)downloadImageWithUrl:(NSString *)url button:(UIButton *)button placeHolder:(UIImage *)placeHolder;
+ (void)downLoadImageWithUrl:(NSString *)url withBlock:(void (^)(UIImage *image))block;
+ (void)upLoadImageArr:(NSArray *)imageArr
               withUrl:(NSString *)url
        withParameters:(NSDictionary *)parameters
              progress:(void (^)(NSProgress *))progress
               success:(void (^)(NSURLSessionDataTask *, id))success
               failure:(void (^)(NSURLSessionDataTask *, NSError *))failure;
//委托服务上传图片
+ (void)upLoadEntrustImageArr:(NSArray *)imageArr
                      withUrl:(NSString *)url
               withParameters:(NSDictionary *)parameters
                     progress:(void (^)(NSProgress *))progress
                      success:(void (^)(NSURLSessionDataTask *, id))success
                      failure:(void (^)(NSURLSessionDataTask *, NSError *))failure;
@end
