//
//  UIView+Property.m
//  LPRecyclePictureScrollView
//
//  Created by 吕品 on 16/5/12.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "UIView+Property.h"

@implementation UIView (Property)
- (CGFloat)height {
    return self.bounds.size.height;
}
- (void)setHeight:(CGFloat)height {
    self.frame = CGRectMake(self.x, self.y, self.width, height);
}

- (CGFloat)width {
    return self.bounds.size.width;
}
- (void)setWidth:(CGFloat)width {
    self.frame = CGRectMake(self.x, self.y, width, self.height);
}
- (CGSize)size {
    return self.bounds.size;
}
- (void)setSize:(CGSize)size {
    self.size = size;
}
- (CGFloat)x {
    return self.bounds.origin.x;
}
- (void)setX:(CGFloat)x {
    self.frame = CGRectMake(x, self.y, self.width, self.height);
}
- (CGFloat)y {
    return self.bounds.origin.y;
}
- (void)setY:(CGFloat)y {
    self.frame = CGRectMake(self.x, y, self.width, self.height);
}
- (CGPoint)origin {
    return self.bounds.origin;
}
- (void)setOrigin:(CGPoint)origin {
    self.frame = CGRectMake(origin.x, origin.y, self.width, self.height);
}
- (CGFloat)center_x {
    return self.center.x;
}
- (void)setCenter_x:(CGFloat)center_x {
    self.center = CGPointMake(center_x, self.center_y);
}
- (CGFloat)center_y {
    return self.center.y;
}
- (void)setCenter_y:(CGFloat)center_y {
    self.center = CGPointMake(self.center_x, center_y);
}



@end
