//
//  ReuseCategoryChooseTableViewCell.h
//  IndexMoveHousePage
//
//  Created by 张垚 on 16/6/12.
//  Copyright © 2016年 众杰网络. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReuseCategoryChooseTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *descriptionLabel;
@property (nonatomic, strong) UICollectionView *categoryChooseCollectionView;
@property (nonatomic, strong) NSMutableString *matchStr;

- (NSString *)submitStrWithIndexArr:(NSMutableArray *)indexArr;
@end
    
