//
//  IndexMainViewController.m
//  House
//
//  Created by 吕品 on 16/5/31.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "IndexMainViewController.h"
#import "ZJRecyclePicView.h"
#import "LocationManager.h"
#import "IndexMainTableViewCell.h"
#import "ReuseRecyclePicTableViewCell.h"
#import "IndexMainDoubleADTableViewCell.h"
#import "IndexMainRecommondTableViewCell.h"
#import "LrdSuperMenu.h"
#import "ReuseLrdMenuTableViewCell.h"
#import "AreaSelectedViewController.h"
#import "CleanHouseViewController.h"
#import "MoveHouseViewController.h"
#import "DecorateHouseViewController.h"
#import "CategorySeviceViewController.h"
#import "LocationCityViewController.h"
#import "SearchHouseViewController.h"
#import "SecondaryViewController.h"
#import "ApartmentHotelViewController.h"
#import "RentHouseViewController.h"
#import "AgentViewController.h"
#import "StoresViewController.h"
#import "RentEntrustViewController.h"
#import "StarServiceViewController.h"
#import "BaseNavigationBar.h"
#import "MySearchBar.h"
#import "House-swift.h"
#import "RentHouseListNoSlideTableViewCell.h"
#import "RentHouseListModel.h"
#import "SecondaryDetailViewController.h"
#import "RentHouseViewController.h"
#import "IndexMainGoodHouseTableViewCell.h"
#import "IndexMainCycleScrollerViewTableViewCell.h"
#import "SDCycleScrollView.h"
#import "ActionCenterDetailViewController.h"
#import "IndexMainTwoViewTableViewCell.h"
#import <AFNetworking/UIButton+AFNetworking.h>
#import "IndexMainMidFourButtonTableViewCell.h"


@interface IndexMainViewController ()<UITableViewDelegate, UITableViewDataSource, ZJRecyclePicDelegate, BaseCollectionWithTableViewCellDelegate, UISearchBarDelegate, SDCycleScrollViewDelegate, IndexMainMidFourButtonTableViewCellDelegate>
@property (nonatomic, strong) UITableView *tableView;//主tableview

@property (nonatomic, copy) NSArray *menuTitleArray;
@property (nonatomic, copy) NSArray *areaArray;
@property (nonatomic, copy) NSArray *priceArray;
@property (nonatomic, copy) NSArray *modeArray;
@property (nonatomic, copy) NSArray *moreArray;
@property (nonatomic, copy) NSArray *totalArray;
@property (nonatomic, copy) NSArray *dataArr;
@property (nonatomic, strong) BaseNavigationBar *myBar;

@property (nonatomic, strong) NSMutableArray *headCircleImageArr;
@property (nonatomic, strong) NSMutableArray *midCircleImageArr;
@property (nonatomic, strong) NSMutableArray *footCircleImageArr;


@property (nonatomic, strong) NSArray *headCircleArr;
@property (nonatomic, strong) NSArray *midCircleArr;
@property (nonatomic, strong) NSArray *footCircleArr;

@property (nonatomic, strong) NSDictionary *rentHouseLoanDic;
@end

@implementation IndexMainViewController
#pragma mark cycle life
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = NO;
    //[self locationRequest];//定位请求
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self initNavigationBar];
    [self initTableView];
    //数据请求
    [self handleData];
    [self getHeadCircleData];
    [self getMidCircleData];
    [self getFootCircleData];
    [self getRenyHouseLoanData];
    //下拉刷新
    [self setHeaderRefresh];
}



- (void)initNavigationBar {
    _myBar = [[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 64)];
    [self.view addSubview:_myBar];
    _myBar.searchBar.textFiled.placeholder = @"请输入要查找的房源";
    /* 修改输入文字的颜色 */
    _myBar.searchBar.textFiled.textColor = [UIColor whiteColor];
    /* 设置占位符的颜色 */
    [_myBar.searchBar.textFiled setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    _myBar.searchBar.imageView.image = [UIImage imageNamed:@"search"];
    UITapGestureRecognizer *pushSearchHouseTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pushSearchHouse:)];
    [_myBar.searchBar addGestureRecognizer:pushSearchHouseTap];
    UITapGestureRecognizer *cityListTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cityListTap:)];
    [_myBar.leftView addGestureRecognizer:cityListTap];
    _myBar.leftImageView.frame = CGRectMake(0, 2.5, 20, 25);
    _myBar.leftImageView.image = [UIImage imageNamed:@"location"];
    _myBar.leftLabel.text = @"哈尔滨";
}
#pragma mark - 所有的跳转界面
//跳转搜索页
- (void)pushSearchHouse:(UITapGestureRecognizer *)tap {
    SearchHouseViewController *searchVC = [[SearchHouseViewController alloc] init];
    searchVC.currentStr = @"租房";
    [self.navigationController pushViewController:searchVC animated:YES];
}
//跳转到城市列表 选择好城市之后返回进行显示
- (void)cityListTap:(UITapGestureRecognizer *)tap {
//    LocationCityViewController *locationVC = [[LocationCityViewController alloc] init];
//    [locationVC passCityBlock:^(NSString *cityName) {
//        _myBar.leftLabel.text = cityName;
//    }];
//    [self.navigationController pushViewController:locationVC animated:YES];
}
//点击精品房源更多 跳转到租房界面
- (void)getMoreInfo {
    RentHouseViewController *rentVC = [[RentHouseViewController alloc] init];
    [self.navigationController pushViewController:rentVC animated:YES];
}

#pragma mark - 数据处理

//下拉刷新
- (void)setHeaderRefresh {
    [self.tableView.mj_header beginRefreshing];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        //获取本界面所有的网络请求数据
        [self handleData];
        [self getHeadCircleData];
        [self getMidCircleData];
        [self getFootCircleData];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.tableView.mj_header endRefreshing];
        });
    }];
}

//精品房源数据
- (void)handleData {
    _dataArr = [NSArray array];
    [NetRequest getWithURLString:@"http://aizufang.hrbzjwl.com/tp/api.php/Rent/index2" parameters:@{@"areaid": [[NSUserDefaults standardUserDefaults] objectForKey:cityID]} progress:^(NSProgress *progress) {
        
    } success:^(id response) {
        if ([[response[@"flag"] description] isEqualToString:@"1"]) {
            _dataArr = response[@"chuzu"];
            [_tableView reloadData];
        }else {
            
        }
    } failure:^(NSError *error) {
        NSLog(@"%@", error);
    }];
}
- (void)getRenyHouseLoanData {
    _rentHouseLoanDic = [NSDictionary dictionary];
    [NetRequest getWithURLString:@"http://aizufang.hrbzjwl.com/tp/index.php/Api/Guanggao/zfd" parameters:nil progress:^(NSProgress *progress) {
        
    } success:^(id response) {
        if ([[response[@"flag"] description] isEqualToString:@"1"]) {
            _rentHouseLoanDic = [response[@"zfd"] firstObject];
        }else {
            
        }
    } failure:^(NSError *error) {
        NSLog(@"%@", error);
    }];
}



//顶部轮播图数据
- (void)getHeadCircleData {
    _headCircleImageArr = [NSMutableArray array];
    _headCircleArr = [NSArray array];
    [NetRequest getInformationWithGetMethodWithUrl:@"http://aizufang.hrbzjwl.com/tp/index.php/Api/Guanggao/head" success:^(NSURLSessionDataTask *task, id responseObject) {
        //后台没给返回flag参数
        _headCircleArr = responseObject;
        //遍历数据 将轮播图url存在一起
        for (NSDictionary *dic in responseObject) {
            [_headCircleImageArr addObject:dic[@"img"]];
        }
        [_tableView reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
    }];
}
//中间轮播图
- (void)getMidCircleData {
    _midCircleImageArr = [NSMutableArray array];
    _midCircleArr = [NSArray array];
    [NetRequest getInformationWithGetMethodWithUrl:@"http://aizufang.hrbzjwl.com/tp/index.php/Api/Guanggao/middle" success:^(NSURLSessionDataTask *task, id responseObject) {
        _midCircleArr = responseObject;
        //获取所有的轮播图
        for (NSDictionary *dic in responseObject) {
            [_midCircleImageArr addObject:dic[@"img"]];
        }
        [_tableView reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
    }];
}
//获取底部的轮播图
- (void)getFootCircleData {
    _footCircleImageArr = [NSMutableArray array];
    _footCircleArr = [NSArray array];
    [NetRequest getInformationWithGetMethodWithUrl:@"http://aizufang.hrbzjwl.com/tp/index.php/Api/Guanggao/foot" success:^(NSURLSessionDataTask *task, id responseObject) {
        _footCircleArr = responseObject;
        //获取所有的轮播图
        for (NSDictionary *dic in responseObject) {
            [_footCircleImageArr addObject:dic[@"img"]];
        }
        [_tableView reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
    }];
}
//轮播图代理协议
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    //首先获取当前点击的cell 然后获取点击的轮播图位置 将网址传到广告详情页进行显示
    IndexMainCycleScrollerViewTableViewCell *cell = (IndexMainCycleScrollerViewTableViewCell *)[[cycleScrollView superview] superview];
    NSIndexPath *path = [_tableView indexPathForCell:cell];
    ActionCenterDetailViewController *actionDetailVC = [[ActionCenterDetailViewController alloc] init];
    if (path.section == 0) {
        NSDictionary *dic = _headCircleArr[index];
        actionDetailVC.titleStr = dic[@"title"];
        actionDetailVC.urlStr = [NSString stringWithFormat:@"%@/id/%@", dic[@"content"], dic[@"id"]];
    }else if (path.section == 2) {
        NSDictionary *dic = _midCircleArr[index];
        actionDetailVC.titleStr = dic[@"title"];
        actionDetailVC.urlStr = [NSString stringWithFormat:@"%@/id/%@", dic[@"content"], dic[@"id"]];
    }else if (path.section == 4) {
        NSDictionary *dic = _footCircleArr[index];
        actionDetailVC.titleStr = dic[@"title"];
        actionDetailVC.urlStr = [NSString stringWithFormat:@"%@/id/%@", dic[@"content"], dic[@"id"]];
    }
    [self.navigationController pushViewController:actionDetailVC animated:YES];
}

#pragma mark initViews
- (void)initTableView {
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, kScreenWidth, kScreenHeight - 112) style:UITableViewStylePlain];
    [self.view addSubview:_tableView];
    _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView registerClass:[ReuseRecyclePicTableViewCell class] forCellReuseIdentifier:NSStringFromClass([ReuseRecyclePicTableViewCell class])];
    [_tableView registerClass:[IndexMainTableViewCell class] forCellReuseIdentifier:NSStringFromClass([IndexMainTableViewCell class])];
    [_tableView registerClass:[IndexMainDoubleADTableViewCell class] forCellReuseIdentifier:NSStringFromClass([IndexMainDoubleADTableViewCell class])];
    [_tableView registerClass:[IndexMainRecommondTableViewCell class] forCellReuseIdentifier:NSStringFromClass([IndexMainRecommondTableViewCell class])];
    
    [_tableView registerClass:[RentHouseListNoSlideTableViewCell class] forCellReuseIdentifier:NSStringFromClass([RentHouseListNoSlideTableViewCell class])];
    [_tableView registerClass:[IndexMainGoodHouseTableViewCell class] forCellReuseIdentifier:NSStringFromClass([IndexMainGoodHouseTableViewCell class])];
    [_tableView registerClass:[IndexMainCycleScrollerViewTableViewCell class] forCellReuseIdentifier:NSStringFromClass([IndexMainCycleScrollerViewTableViewCell class])];
    [_tableView registerClass:[IndexMainTwoViewTableViewCell class] forCellReuseIdentifier:NSStringFromClass([IndexMainTwoViewTableViewCell class])];
    
    [_tableView registerClass:[IndexMainMidFourButtonTableViewCell class] forCellReuseIdentifier:NSStringFromClass([IndexMainMidFourButtonTableViewCell class])];
}
#pragma mark tableViewDelegateAndDatesource
//分区数
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 7;
}
//行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 6) {
        return _dataArr.count;
    } else {
        return 1;
    }
}
//cell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    IndexMainTableViewCell *indexMainTableViewCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IndexMainTableViewCell class])];
    indexMainTableViewCell.delegate = self;
    
    IndexMainMidFourButtonTableViewCell *buttonCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IndexMainMidFourButtonTableViewCell class])];
    buttonCell.delegate = self;
    
    IndexMainCycleScrollerViewTableViewCell *cycleCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IndexMainCycleScrollerViewTableViewCell class])];
    cycleCell.cycleScrollView.delegate = self;
    
    IndexMainTwoViewTableViewCell *viewCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IndexMainTwoViewTableViewCell class])];
    
    switch (indexPath.section) {//顶部
        case 0: {
            cycleCell.cycleScrollView.imageURLStringsGroup = _headCircleImageArr;
            return cycleCell;
        }
            break;
        case 1: {//8个图标
            NSMutableArray *headImageArray = [NSMutableArray array];
            for (NSInteger i = 0 ; i < KindexMainTableViewCollectionViewHeadArray.count; i ++) {
                UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"IndexMainCollectionIcon-%ld", (long)i]];
                [headImageArray addObject:image];
            }
            indexMainTableViewCell.headImageArray = headImageArray.copy;
            indexMainTableViewCell.titlesArray = KindexMainTableViewCollectionViewHeadArray;
            indexMainTableViewCell.showGrayView = NO;
            indexMainTableViewCell.tag = tagManager;
            return indexMainTableViewCell;
        }
            break;
        case 2: {//两个广告
            [viewCell.leftButton setImageForState:UIControlStateNormal withURL:[NSURL URLWithString:[_midCircleImageArr firstObject]] placeholderImage:[UIImage imageNamed:@"placeHolderPic"]];
            [viewCell.rightButton setImageForState:UIControlStateNormal withURL:[NSURL URLWithString:[_midCircleImageArr lastObject]] placeholderImage:[UIImage imageNamed:@"placeHolderPic"]];
            [viewCell.leftButton addTarget:self action:@selector(getWebInfo:) forControlEvents:UIControlEventTouchUpInside];
            [viewCell.rightButton addTarget:self action:@selector(getWebInfo:) forControlEvents:UIControlEventTouchUpInside];
            viewCell.selectionStyle = UITableViewCellSelectionStyleNone;
            return viewCell;
        }
            break;
        case 3: {//搬家、保洁、装修、租房贷模块
            return buttonCell;
        }
            break;
        case 4: {//轮播
            cycleCell.cycleScrollView.imageURLStringsGroup = _footCircleImageArr;
            return cycleCell;
        }
            break;
        case 5: {//推荐
            IndexMainGoodHouseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IndexMainGoodHouseTableViewCell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell.moreButton addTarget:self action:@selector(getMoreInfo) forControlEvents:UIControlEventTouchUpInside];
            return cell;
        }
            break;
        case 6: {
            RentHouseListNoSlideTableViewCell *rentCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RentHouseListNoSlideTableViewCell class])];
            rentCell.selectionStyle = UITableViewCellSelectionStyleNone;
            NSDictionary *dic = [NSDictionary dictionaryWithDictionary:_dataArr[indexPath.row]];
            RentHouseListModel *model = [RentHouseListModel modelWithDic:dic];
            rentCell.model = model;
            rentCell.tag = [model.itemid integerValue];
            return rentCell;
        }
            break;
            default:
            return nil;
            break;
    }
}

- (void)getWebInfo:(UIButton *)button {
    ActionCenterDetailViewController *actionDetailVC = [[ActionCenterDetailViewController alloc] init];
    if (button.tag == 1) {
        NSDictionary *dic = _midCircleArr[button.tag - 1];
        actionDetailVC.titleStr = dic[@"title"];
        actionDetailVC.urlStr = [NSString stringWithFormat:@"%@/id/%@", dic[@"content"], dic[@"id"]];
    }else if (button.tag == 2) {
        NSDictionary *dic = _midCircleArr[button.tag - 2];
        actionDetailVC.titleStr = dic[@"title"];
        actionDetailVC.urlStr = [NSString stringWithFormat:@"%@/id/%@", dic[@"content"], dic[@"id"]];
    }
    [self.navigationController pushViewController:actionDetailVC animated:YES];
}


//行高

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat itemWidth = (self.view.frame.size.width - indexMainVCTableViewPadding * (4 + 1)) / 4;
    CGFloat itemHeight = (itemWidth + indexMainVCTableViewCollectionViewCellLabelHeight) * 2 + indexMainVCTableViewPadding * 3;
    if (indexPath.section == 0) {
        return 5 * kScreenWidth / 12;
    } else if (indexPath.section == 1) {
        return itemHeight;
    } else if (indexPath.section == 2) {
        return 2 * kScreenWidth / 9;
    } else if (indexPath.section == 3) {
        return itemHeight / 3 ;
    } else if (indexPath.section == 4) {
        return 11 * kScreenWidth / 36;
    }else if (indexPath.section == 6) {
        return 100;
    }else if (indexPath.section == 5) {
        return 40;
    }else {
        return 100;
    }
}
#pragma mark IndexMainTableViewCellDelegate
- (void)tableViewCell:(BaseCollectionWithTableViewCell *)cell didSelectedAtItem:(NSInteger)item {
    if (cell.tag == tagManager) { //上面八个
        switch (item) {
            case 0: {
                SecondaryViewController *secondayVC = [[SecondaryViewController alloc] init];
                [self.navigationController pushViewController:secondayVC animated:YES];
            }
                break;
            case 1: {
                ApartmentHotelViewController *apartmentVC = [[ApartmentHotelViewController alloc] init];
                [self.navigationController pushViewController:apartmentVC animated:YES];
            }
                break;
            case 2: {
                RentHouseViewController *rentHouseVC = [[RentHouseViewController alloc] init];
                [self.navigationController pushViewController:rentHouseVC animated:YES];
            }
                break;
            case 3: {
                StoresViewController *storesVC = [[StoresViewController alloc] init];
                [self.navigationController pushViewController:storesVC animated:YES];
            }
                break;
            case 4: {
//                IndexMapFindHouseViewController *findVC = [[IndexMapFindHouseViewController alloc] init];
//                [self.navigationController pushViewController:findVC animated:YES];
                [self alertMessage:@"暂未开通,敬请期待"];
            }
                break;
            case 5: {
                if ([self isLogin]) {
                    CategorySeviceViewController *categoryServiceVC = [[CategorySeviceViewController alloc] init];
                    [self.navigationController pushViewController:categoryServiceVC animated:YES];
                }else {
                    [self alertMessage:@"请先登录账号"];
                }
            }
                break;
            case 6: {
                StarServiceViewController *starVC = [[StarServiceViewController alloc] init];
                [self.navigationController pushViewController:starVC animated:YES];
            }
                break;
            case 7: {
                AgentViewController *agentVC = [[AgentViewController alloc] init];
                [self.navigationController pushViewController:agentVC animated:YES];
            }
                break;
            default:
                break;
        }
    }
}

- (void)IndexMainMidFourButtonTableViewCellDidSelectCurrentButtonTag:(NSInteger)buttonTag {
    switch (buttonTag) {
        case 100: {//搬家
            CleanHouseViewController *cleanVC = [CleanHouseViewController new];
            [self.navigationController pushViewController:cleanVC animated:YES];
        }
            break;
        case 101: {//保洁
            MoveHouseViewController *moveHouseVC = [MoveHouseViewController new];
            [self.navigationController pushViewController:moveHouseVC animated:YES];
        }
            break;
        case 102: {//装修
            DecorateHouseViewController *decorateVC = [DecorateHouseViewController new];
            [self.navigationController pushViewController:decorateVC animated:YES];
        }
            break;
        case 103: {
            ActionCenterDetailViewController *actionDetailVC = [[ActionCenterDetailViewController alloc] init];
            actionDetailVC.titleStr = _rentHouseLoanDic[@"title"];
            actionDetailVC.urlStr = [NSString stringWithFormat:@"%@/id/%@", _rentHouseLoanDic[@"content"], _rentHouseLoanDic[@"id"]];
            [self.navigationController pushViewController:actionDetailVC animated:YES];
        }
            break;
        default:
            break;
    }
}

//提交表单不完整的提示信息
- (void)alertMessage:(NSString *)message {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:YES completion:nil];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            MineLoginViewController *mineVC = [[MineLoginViewController alloc] init];
            [self.navigationController pushViewController:mineVC animated:YES];
        });
    });
    [self presentViewController:alertController animated:YES completion:nil];
}



- (void)didSelectedPicAtIndex:(NSInteger)index {
    //点击轮播
    AreaSelectedViewController *areaVC = [AreaSelectedViewController new];
    [self.navigationController pushViewController:areaVC animated:YES];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 6) {
        RentHouseListNoSlideTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        SecondaryDetailViewController *secondaryVC  = [[SecondaryDetailViewController alloc] init];
        secondaryVC.itemID = [NSString stringWithFormat:@"%ld", (long)cell.tag];
        [self.navigationController pushViewController:secondaryVC animated:YES];
    }
}

#pragma mark location request
- (void)locationRequest {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.label.text = locationProgressing;
    LocationManager *manager = [LocationManager manager];
    [manager requestLocationWithBlock:^(CLLocation *location, AMapLocationReGeocode *regeocode, NSError *error) {
        if (error) {
            hud.label.text = locationFaild;
            self.title = pleaseTryAgain;
        } else {
            hud.label.text = locationSuccess;
            self.myBar.leftLabel.text = regeocode.city;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hideAnimated:YES afterDelay:1];
        });
    }];
}


@end
