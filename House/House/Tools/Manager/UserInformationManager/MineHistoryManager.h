//
//  MineHistoryManager.h
//  House
//
//  Created by 吕品 on 2016/9/13.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <Foundation/Foundation.h>
@class YTKKeyValueStore;
@interface MineHistoryManager : NSObject
@property (nonatomic, assign) NSInteger page;
//取
+ (NSArray *)getInfoWithType:(NSInteger)type andNumber:(NSInteger)number;
//存(5二手, 7租)
+ (void)storeItemID:(NSString* )itemid withType:(NSInteger)type;

@end
