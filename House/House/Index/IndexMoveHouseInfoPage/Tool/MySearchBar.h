//
//  MySearchBar.h
//  House
//
//  Created by 张垚 on 16/6/27.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MySearchBar : UIView

@property (nonatomic, strong) UITextField *textFiled;
@property (nonatomic, strong) UIImageView *imageView;


@end
