//
//  MinePulishHeaderFooterView.swift
//  House
//
//  Created by 吕品 on 2016/9/23.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

import UIKit

class MinePulishHeaderFooterView: UITableViewHeaderFooterView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    internal let deleteButton = UIButton.init(type: .system)
    internal let editButton = UIButton.init(type: .system)
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        deleteButton.backgroundColor = .gray
        deleteButton.tintColor = .white
        deleteButton.layer.masksToBounds = true
        deleteButton.layer.cornerRadius = 2
        editButton.backgroundColor = .orange
        editButton.tintColor = .white
        editButton.layer.masksToBounds = true
        editButton.layer.cornerRadius = 2
        deleteButton.setTitle("删除", for: .normal)
        editButton.setTitle("修改", for: .normal)
        contentView.addSubview(deleteButton)
        contentView.addSubview(editButton)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        let padding = 5.0
        let width = 80
        deleteButton.snp.makeConstraints { (make) in
            make.top.equalTo(contentView.snp.top).offset(padding)
            make.left.equalTo(contentView.snp.left).offset(padding)
            make.bottom.equalTo(contentView.snp.bottom).offset(-padding)
            make.width.equalTo(width)
        }
        editButton.snp.makeConstraints { (make) in
            make.top.bottom.width.equalTo(deleteButton)
            make.right.equalTo(contentView.snp.right).offset(-padding)
            make.width.equalTo(width)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
