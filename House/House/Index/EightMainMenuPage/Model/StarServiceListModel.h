//
//  StarServiceListModel.h
//  House
//
//  Created by 张垚 on 16/8/23.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "BaseModel.h"

@interface StarServiceListModel : BaseModel

@property (nonatomic, copy) NSString *itemid;
@property (nonatomic, copy) NSString *sever;
@property (nonatomic, copy) NSString *xingxing;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *name1;
@property (nonatomic, copy) NSString *name2;
@property (nonatomic, copy) NSString *name3;
@property (nonatomic, copy) NSString *introduce1;
@property (nonatomic, copy) NSString *introduce2;
@property (nonatomic, copy) NSString *introduce3;
@property (nonatomic, copy) NSString *currentIntroduction;

@end
