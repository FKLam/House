//
//  IndexMainDoubleADTableViewCell.m
//  House
//
//  Created by 吕品 on 16/6/7.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "IndexMainDoubleADTableViewCell.h"
#import "IndexMainTableViewCellDoubleADCollectionViewCell.h"
@interface IndexMainDoubleADTableViewCell ()<UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate>
@property (nonatomic, strong) UICollectionView *collectionView;
@end
@implementation IndexMainDoubleADTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        self.collectionView = [[UICollectionView alloc] initWithFrame:self.contentView.frame collectionViewLayout:flowLayout];
        [self.contentView addSubview:_collectionView];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.scrollEnabled = NO;
        _collectionView.backgroundColor = [UIColor whiteColor];
        [_collectionView registerClass:[IndexMainTableViewCellDoubleADCollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([IndexMainTableViewCellDoubleADCollectionViewCell class])];
    }
    return self;
}
#pragma mark collectionViewDelegateAndDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 2;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    IndexMainTableViewCellDoubleADCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([IndexMainTableViewCellDoubleADCollectionViewCell class]) forIndexPath:indexPath];
    cell.headImageView.image = _imageArray[indexPath.item];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self.delegate tableViewCell:self didSelectedAtItem:indexPath.item];
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath{
    return CGSizeMake(self.contentView.frame.size.width / 2, self.contentView.frame.size.height);
}
#pragma mark layout
- (void)layoutSubviews {
    [super layoutSubviews];
    self.collectionView.frame = self.contentView.frame;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
