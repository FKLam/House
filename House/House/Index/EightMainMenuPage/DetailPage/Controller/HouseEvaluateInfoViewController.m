//
//  HouseEvaluateInfoViewController.m
//  House
//
//  Created by 张垚 on 16/7/15.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "HouseEvaluateInfoViewController.h"
#import "BaseTitleNavigationBar.h"
#import "UnfoldModel.h"
#import "UnfoldFrameModel.h"
#import "HouseEvaluateTableViewCell.h"

@interface HouseEvaluateInfoViewController ()<UITableViewDelegate, UITableViewDataSource, HouseEvaluateTableViewCellDelegate>
@property (nonatomic, strong) BaseTitleNavigationBar *myBar;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@end

@implementation HouseEvaluateInfoViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    //顶部视图
    _myBar = [[BaseTitleNavigationBar alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 64) WithTitleStr:@"经纪人房评"];
    [self.view addSubview:_myBar];
    //返回视图添加手势 用于返回
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(back:)];
    [_myBar.leftView addGestureRecognizer:tap];
    [self initLoadData];
    [self initTableView];
    // Do any additional setup after loading the view.
}

#pragma mark - 数据处理
-(void)initLoadData
{
    self.dataArray = [NSMutableArray array];
    NSArray *array  = @[@"维基百科（Wikipedia），是一个基于维基技术的多语言百科全书协作计划，这是一部用多种语言编写的网络百科全书。  维基百科一词取自于该网站核心技术“Wiki”以及具有百科全书之意的共同创造出来的新混成词“Wikipedia”，维基百科是由非营利组织维基媒体基金会负责营运，并接受捐赠。",@"2002年2月，由Edgar Enyedy领导，非常活跃的西班牙语维基百科突然退出维基百科并建立了他们自己的自由百科（Enciclopedia Libre）；理由是未来可能会有商业广告及失去主要的控制权。",@"来看看劳动法克林顿刷卡思考对方卡拉卡斯的楼房卡拉卡斯的疯狂拉萨的罚款 ",@"Apple is supplying this information to help you plan for the adoption of the technologies and programming interfaces described herein for use on Apple-branded products. This information is subject to change, and software implemented according to this document should be tested with final operating system software and final documentation. Newer versions of this document may be provided with future betas of the API or technology.",@"2015年11月1日，英文维基百科条目数突破500万",@"2015年6月15日，维基百科全面采用HTTPS：保护用户敏感信息。"];
    
    for (NSString *str in array) {
        UnfoldModel *model = [[UnfoldModel alloc]init];
        model.contextStr = str;
        model.isFold = NO;//给出初始值
        
        UnfoldFrameModel *frameModel = [[UnfoldFrameModel alloc]init];
        frameModel.model = model;
        [self.dataArray addObject:frameModel];
    }
    
}

#pragma mark - tableView
- (void)initTableView {
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, kScreenWidth, kScreenHeight - 64) style:UITableViewStylePlain];
    [self.view addSubview:_tableView];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView registerClass:[HouseEvaluateTableViewCell class] forCellReuseIdentifier:NSStringFromClass([HouseEvaluateTableViewCell class])];
}
//行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
//行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
        UnfoldFrameModel *frameModel = self.dataArray[indexPath.row];
        return frameModel.cellHeight;
   
}
//cell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        HouseEvaluateTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([HouseEvaluateTableViewCell class])];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        //2,设置cell的数据
        UnfoldFrameModel *frameModel = self.dataArray[indexPath.row];
        cell.frameModel = frameModel;
        cell.delegate = self;
        return cell;
}

#pragma mark - VC delegate
//隐藏 展开文本
-(void)UnfoldCellDidClickUnfoldBtn:(UnfoldFrameModel *)frameModel
{
    UnfoldModel *model = frameModel.model;
    model.isFold = !model.isFold;
    frameModel.model = model;//这句话很关键，要把值设置回来，因为其setModel方法中会重新计算frame
    [_tableView reloadData];
}

- (void)callAgentPhoneNumber:(NSInteger)phoneNumber {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:@"%ld", (long)phoneNumber] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *callAction = [UIAlertAction actionWithTitle:@"拨打" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"拨打电话接口");
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:callAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}



- (void)back:(UITapGestureRecognizer *)tap {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
