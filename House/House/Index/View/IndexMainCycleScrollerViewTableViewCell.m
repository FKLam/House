//
//  IndexMainCycleScrollerViewTableViewCell.m
//  House
//
//  Created by 张垚 on 16/9/9.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "IndexMainCycleScrollerViewTableViewCell.h"

@implementation IndexMainCycleScrollerViewTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier  {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:self.contentView.frame imageURLStringsGroup:nil];
        //占位图片
        _cycleScrollView.placeholderImage = [UIImage imageNamed:@"placeHolderImage"];
        //单张图隐藏pagecontrol
        _cycleScrollView.hidesForSinglePage = YES;
        [self.contentView addSubview:_cycleScrollView];
    }
    return self;
}



- (void)layoutSubviews {
    [super layoutSubviews];
    _cycleScrollView.frame = self.contentView.frame;
}

- (void)setImageArr:(NSArray *)imageArr {
    if (_imageArr != imageArr) {
        _imageArr = imageArr;
    }
    _cycleScrollView.imageURLStringsGroup = imageArr;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
