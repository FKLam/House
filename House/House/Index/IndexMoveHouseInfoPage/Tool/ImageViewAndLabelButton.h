//
//  ImageViewAndLabelButton.h
//  House
//
//  Created by 张垚 on 16/6/30.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewAndLabelButton : UIButton

@property (nonatomic, strong) UIImageView *logoImage;
@property (nonatomic, strong) UILabel *logoNameLabel;
@end
