//
//  StoreDetailView.m
//  House
//
//  Created by 张垚 on 16/7/21.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "StoreDetailView.h"


@interface StoreDetailView ()

@end

@implementation storeButton

- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    return CGRectMake(0, 0, contentRect.size.height, contentRect.size.height);
    
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    
    return CGRectMake(contentRect.size.height, 0, contentRect.size.width - contentRect.size.height , contentRect.size.height);
}


@end


@implementation StoreDetailView


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _backgroundImageView = [[UIImageView alloc] init];
        [self addSubview:_backgroundImageView];
        

        _mapButton = [storeButton buttonWithType:UIButtonTypeCustom];
        [_mapButton setImage:[UIImage imageNamed:@"location"] forState:UIControlStateNormal];
        [self addSubview:_mapButton];
        
        _addressLabel = [[UILabel alloc] init];
        [self addSubview:_addressLabel];
        
        _numberButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:_numberButton];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self;
    [_backgroundImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left);
        make.top.equalTo(superView.mas_top);
        make.size.mas_equalTo(superView.frame.size);
    }];

    [_mapButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_centerY).offset(-10);
        make.centerX.equalTo(superView.mas_centerX);
        make.width.equalTo(@100);
        make.height.equalTo(@25);
    }];
    [_mapButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_mapButton.mas_bottom).offset(5);
        make.left.equalTo(superView.mas_left);
        make.right.equalTo(superView.mas_right);
        make.height.equalTo(@20);
    }];
    _addressLabel.textAlignment = NSTextAlignmentCenter;
    _addressLabel.font = [UIFont systemFontOfSize:15];
    _addressLabel.textColor = [UIColor whiteColor];
    [_numberButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_addressLabel.mas_bottom).offset(5);
        make.left.equalTo(superView.mas_left).offset(20);
        make.right.equalTo(superView.mas_right).offset(-20);
        make.height.equalTo(@35);
    }];
    _numberButton.backgroundColor = [UIColor colorWithRed:0.942 green:0.4738 blue:0.0027 alpha:0.7];
    _numberButton.layer.cornerRadius = 6;
    _numberButton.layer.masksToBounds = YES;
    [_numberButton setImage:[UIImage imageNamed:@"callAgentPhone"] forState:UIControlStateNormal];
    [_numberButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
