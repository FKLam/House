//
//  ReuseSelectButtonView.h
//  House
//
//  Created by 张垚 on 16/8/11.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ReuseSelectButtonView : UIView

@property (nonatomic, strong) UIButton *areaButton;
@property (nonatomic, strong) UIButton *priceButton;
@property (nonatomic, strong) UIButton *typeButton;
@property (nonatomic, strong) UIButton *moreButton;

- (instancetype)initWithFrame:(CGRect)frame WithButtonTitleArr:(NSArray *)buttonTitleArr;


@end
