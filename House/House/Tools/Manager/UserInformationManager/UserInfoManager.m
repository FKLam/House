//
//  UserInfoManager.m
//  House
//
//  Created by 吕品 on 2016/7/26.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "UserInfoManager.h"
@implementation UserInfoManager

+ (UserInfoManager *)sharedManager {
    static UserInfoManager *manager = nil;
    if (!manager) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            manager = [[UserInfoManager alloc] init];
        });
    }
    return manager;
}
+ (BOOL)checkLogin {
    return [self sharedManager].isLogin;
}
- (void)initWithUserID:(NSString *)userId phoneNumber:(NSString *)phoneNumber userName:(NSString *)name{
    [[self userDefault] setObject:userId forKey:userID];
    [[self userDefault] setObject:phoneNumber forKey:userPhoneNumber];
    [[self userDefault] setBool:YES forKey:userLoginStatus];
    [[self userDefault] setObject:name forKey:userNickNmae];
    [self syncUserDefault];
}


- (BOOL)isLogin {
    return [[self userDefault] boolForKey:userLoginStatus];
}

- (NSString *)uid {
    return [[self userDefault] stringForKey:userID] != nil ? [[self userDefault] stringForKey:userID] : @"";
}

- (NSString *)phoneNumber {
    return [[self userDefault] stringForKey:userPhoneNumber];
}

- (NSString *)nickName {
    return [[self userDefault] stringForKey:userNickNmae];
}
- (NSString *)userCityID {
    return [[self userDefault] stringForKey:cityID];
}

- (void)setNickName:(NSString *)nickName {
    [[self userDefault] setObject:nickName forKey:userNickNmae];
    [self syncUserDefault];
}

- (void)setPhoneNumber:(NSString *)phoneNumber {
    [[self userDefault] setObject:phoneNumber forKey:userPhoneNumber];
    [self syncUserDefault];
}

- (void)setUserCityID:(NSString *)userCityID {
    [[self userDefault] setObject:userCityID forKey:cityID];
    [self syncUserDefault];
}

- (void)logOff {
    [[self userDefault] removeObjectForKey:userPhoneNumber];
    [[self userDefault] removeObjectForKey:userID];
    [[self userDefault] removeObjectForKey:userNickNmae];
    [[self userDefault] removeObjectForKey:userLoginStatus];
    [self syncUserDefault];
}

- (void)syncUserDefault {
    [[self userDefault] synchronize];
}

- (NSUserDefaults *)userDefault {
    return [NSUserDefaults standardUserDefaults];
}
@end
