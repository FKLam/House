//
//  MinePasswordChangeViewController.swift
//  House
//
//  Created by 吕品 on 16/7/11.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

import UIKit
import MBProgressHUD
class MinePasswordChangeViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    var naviBar = BaseTitleNavigationBar()
    
    fileprivate var tableView = UITableView()
    fileprivate var imageArray = [UIImage]()
    fileprivate var titleArray = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        tabbarHidden = true
        automaticallyAdjustsScrollViewInsets = false
        
        naviBar = BaseTitleNavigationBar.init(frame: CGRect.init(x: 0, y: 0, width: self.view.bounds.size.width, height: 64), withTitleStr: "密码修改")
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(back(tap:)))
        naviBar.leftView.addGestureRecognizer(tap)
        view.addSubview(naviBar)
        
        tableView = UITableView.init(frame: CGRect.init(x: 0, y: 64, width: self.view.bounds.size.width, height: self.view.bounds.size.height - 64), style: .plain)
        view.addSubview(tableView)
//        let rightButton = UIButton.init(type: .system)
//        naviBar.addSubview(rightButton)
//        rightButton.setTitleColor( .white, for: .normal)
//        rightButton.setTitle("保存", for: .normal)
//        rightButton.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: 15)
//        rightButton.addTarget(self, action: #selector(submit), for: .touchUpInside)
//        rightButton.snp.makeConstraints { (make) in
//            make.right.equalTo(naviBar.snp.right).offset(-10);
//            make.bottom.equalTo(naviBar.snp.bottom).offset(-10);
//            make.centerY.equalTo(naviBar.leftView)
//            make.height.equalTo(30)
//            make.width.equalTo(40)
//        }

        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(view).offset(kNaviBarHeight)
            make.left.bottom.right.equalTo(view)
        }
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(ReuseImageViewAndTextFieldTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(ReuseImageViewAndTextFieldTableViewCell.self))
        let image = UIImage.init(named: "MinePassword")!
        imageArray = [image, image, image]
        titleArray = ["请输入原密码", "请输入新密码", "请再次输入新密码"]
        tableView.isScrollEnabled = false
        let footView = UIView.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 150))
        tableView.tableFooterView = footView
        let button = UIButton.init(type: .system)
        footView.addSubview(button)
        button.setTitle("提交", for: UIControlState())
        button.addTarget(self, action: #selector(submit), for: .touchUpInside)
        button.snp.makeConstraints { (make) in
            make.centerX.equalTo(view)
            make.height.equalTo(50)
            make.top.equalTo(footView.snp.top).offset(40)
            make.width.equalTo(view.frame.width - 40)
        }
        button.backgroundColor = naviColor
        button.layer.masksToBounds = true
        button.layer.cornerRadius = 2
        button.tintColor = UIColor.white
        button.titleLabel?.font = UIFont.systemFont(ofSize: 20)
//        footView.backgroundColor = UIColor.redColor()

        // Do any additional setup after loading the view.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return imageArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(ReuseImageViewAndTextFieldTableViewCell.self)) as! ReuseImageViewAndTextFieldTableViewCell
        cell.mixView.isShowButton = false
        cell.mixView.imageView.image = imageArray[(indexPath as NSIndexPath).row]
        cell.mixView.textField.placeholder = titleArray[(indexPath as NSIndexPath).row]
        cell.mixView.textField.isSecureTextEntry = true
        cell.tag = tagManager + (indexPath as NSIndexPath).row
        cell.selectionStyle = .none
        return cell
    }
    func submit() -> Void {
        let oldPassword = (view.viewWithTag(1000) as! ReuseImageViewAndTextFieldTableViewCell).mixView.textField.text!//old password
        let newPassword = (view.viewWithTag(1001) as! ReuseImageViewAndTextFieldTableViewCell).mixView.textField.text!//new password
        let newRePassword = (view.viewWithTag(1002) as! ReuseImageViewAndTextFieldTableViewCell).mixView.textField.text!//make sure new passored
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.label.text = "正在提交"
        if newPassword != newRePassword {
            self.hide(withTitle: "两次输入的密码不一致")
        } else if newRePassword == "" {
            self.hide(withTitle: "密码不能为空")
        } else {
            NetRequest.updateOrGetInformation(fromUrl: PHPHeader.manager().revisePassword, withParameters: ["id": UserInfoManager.shared().uid, "old": oldPassword, "new": newPassword], success: { (task, responseObj) in
                print(responseObj)
                if responseObj is NSDictionary {
                    let reponse = responseObj as! NSDictionary
                    if (reponse["flg"] as! NSNumber).description == "1" {
                        self.hide(withTitle: "更改成功")
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                            print(self.navigationController?.popToRootViewController(animated: true))
                        })
                    } else {
                        self.hide(withTitle: "更改失败")
                    }
                }

            }) { (task, error) in
                print(error)
                self.hide(withTitle: "更改失败")
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    @objc func back(tap: UITapGestureRecognizer) -> Void {
        self.navigationController!.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
