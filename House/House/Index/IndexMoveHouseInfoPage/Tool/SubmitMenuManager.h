//
//  SubmitMenuManager.h
//  House
//
//  Created by 张垚 on 16/7/25.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SubmitMenuManager : NSObject

//买房租房
+(NSDictionary *)submitTableView:(UITableView *)tableView WithKeyArr:(NSArray *)KeyArr;
//装修
+(NSDictionary *)submitDecorateTableView:(UITableView *)tableView WithKeyArr:(NSArray *)KeyArr;
//保洁
+(NSDictionary *)submitCleanHouseTableView:(UITableView *)tableView WithKeyArr:(NSArray *)KeyArr;

//搬家
+(NSDictionary *)submitMoveHouseTableView:(UITableView *)tableView WithKeyArr:(NSArray *)KeyArr WithCellNumber:(NSInteger)cellNumber;
//酒店预订
+(NSDictionary *)submitHotelTableView:(UITableView *)tableView WithKeyArr:(NSArray *)KeyArr WithHotelId:(NSString *)HotelID;

@end
