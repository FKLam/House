//
//  SecondaryListDetailView.m
//  House
//
//  Created by 张垚 on 16/6/30.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "SecondaryListDetailView.h"
#import "ImageViewAndLabelButton.h"

@interface SecondaryListDetailView ()

@end


@implementation SecondaryListDetailView

//重写初始化方法
- (instancetype)initWithFrame:(CGRect)frame WithTitleArr:(NSArray *)titleArr WithImageArr:(NSArray *)imageArr WithColorArr:(NSArray *)colorArr WithButtonNum:(NSUInteger)buttonNum {
    self = [super initWithFrame:frame];
    if (self) {
#pragma mark - 标签视图 按钮视图
        /* 详情界面控件 */
        _roomImageView = [[UIImageView alloc] init];
        [self addSubview:_roomImageView];
        
        _tilteLabel = [[UILabel alloc] init];
        [self addSubview:_tilteLabel];
        
        _doorModelLabels = [[UILabel alloc] init];
        [self addSubview:_doorModelLabels];
        
        _communityLabels = [[UILabel alloc] init];
        [self addSubview:_communityLabels];
        
        _priceLabel = [[UILabel alloc] init];
        [self addSubview:_priceLabel];
        
        _areaSizeLabel = [[UILabel alloc] init];
        [self addSubview:_areaSizeLabel];

        /* uistackview初始化 属性设置 */
        _buttonStackView = [[UIStackView alloc] init];
        [self addSubview:_buttonStackView];
        /* 布局方向 */
        _buttonStackView.axis = UILayoutConstraintAxisHorizontal;
        /* 间距 */
        _buttonStackView.spacing = 0.0f;
        /* 每个视图之间的关系 */
        _buttonStackView.distribution = UIStackViewDistributionFillEqually;
        /* 对齐方式 */
        _buttonStackView.alignment = UIStackViewAlignmentFill;
        UIView *superView = self;
        NSInteger width = 100 * buttonNum;
        [_buttonStackView mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(superView.mas_top);
            make.right.equalTo(superView.mas_right);
            make.height.equalTo(superView.mas_height);
            make.width.mas_equalTo(width);
        }];
        /* 循环铺button 侧滑的三个按钮*/
        for (int i = 0; i < buttonNum; i++) {
            ImageViewAndLabelButton *button = [ImageViewAndLabelButton buttonWithType:UIButtonTypeCustom];
            button.selected = NO;
            //颜色赋值
            button.backgroundColor = colorArr[i];
            //标题
            button.logoNameLabel.text = titleArr[i];
            button.logoNameLabel.textColor = [UIColor whiteColor];
            button.logoNameLabel.font = [UIFont systemFontOfSize:15];
            //button图片
            button.logoImage.image = [UIImage imageNamed:imageArr[i]];
            button.tag = i + 1000;
            [button addTarget:self action:@selector(pushDetail:) forControlEvents:UIControlEventTouchUpInside];
            [_buttonStackView addArrangedSubview:button];
        }
        
        _tagLabel1 = [[UILabel alloc] init];
        [self addSubview:_tagLabel1];
        
        _tagLabel2 = [[UILabel alloc] init];
        [self addSubview:_tagLabel2];
        
        _tagLabel3 = [[UILabel alloc] init];
        [self addSubview:_tagLabel3];
        
        _tagLabel4 = [[UILabel alloc] init];
        [self addSubview:_tagLabel4];
    }
    return self;
}
//代理协议方法
- (void)pushDetail:(ImageViewAndLabelButton *)button {
    [self.delegate passButton:button];
}

- (void)layoutSubviews {
        [super layoutSubviews];
        UIView *superView = self;
        [_roomImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(superView.mas_left).offset(10);
            make.centerY.equalTo(superView.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(110, 85));
        }];
        _roomImageView.layer.cornerRadius = 6;
        _roomImageView.layer.masksToBounds = YES;
        [_tilteLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_roomImageView.mas_right).offset(10);
            make.top.equalTo(_roomImageView.mas_top).offset(5);
            make.height.equalTo(@15);
            make.right.equalTo(_buttonStackView.mas_left).offset(-10);
        }];
        _tilteLabel.font = [UIFont systemFontOfSize:15 weight:17];
        [_doorModelLabels mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(superView.mas_centerY);
            make.left.equalTo(_roomImageView.mas_right).offset(10);
            make.height.equalTo(@15);
            make.right.equalTo(_priceLabel.mas_left).offset(-10);
        }];
        _doorModelLabels.font = [UIFont systemFontOfSize:10];
        _doorModelLabels.textAlignment = NSTextAlignmentLeft;
        _doorModelLabels.textColor = [UIColor colorWithRed:0.3912 green:0.3874 blue:0.395 alpha:1.0];
    
        [_communityLabels mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(superView.mas_centerY);
            make.left.equalTo(_roomImageView.mas_right).offset(10);
            make.height.equalTo(@15);
            make.right.equalTo(_priceLabel.mas_left).offset(-10);
        }];
    
        _communityLabels.textAlignment = NSTextAlignmentLeft;
        _communityLabels.font = [UIFont systemFontOfSize:10];
        _communityLabels.textColor = [UIColor colorWithRed:0.3912 green:0.3874 blue:0.395 alpha:1.0];
    [_tagLabel1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_roomImageView.mas_right).offset(10);
        make.top.equalTo(_communityLabels.mas_bottom).offset(5);
        make.height.equalTo(@15);
        make.width.equalTo(@40);
    }];
    [_tagLabel2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_tagLabel1.mas_right).offset(5);
        make.top.equalTo(_communityLabels.mas_bottom).offset(5);
        make.height.equalTo(@15);
        make.width.equalTo(@40);
    }];
    
    [_tagLabel3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_tagLabel2.mas_right).offset(5);
        make.top.equalTo(_communityLabels.mas_bottom).offset(5);
        make.height.equalTo(@15);
        make.width.equalTo(@40);
    }];
    
    [_tagLabel4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_tagLabel3.mas_right).offset(5);
        make.top.equalTo(_communityLabels.mas_bottom).offset(5);
        make.height.equalTo(@15);
        make.width.equalTo(@40);
    }];
       [_priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
           make.bottom.equalTo(superView.mas_centerY);
           make.right.equalTo(_buttonStackView.mas_left).offset(-10);
           make.width.equalTo(@60);
           make.height.equalTo(@15);
       }];
        [_areaSizeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_buttonStackView.mas_left).offset(-10);
            make.top.equalTo(superView.mas_centerY);
            make.width.equalTo(@60);
            make.height.equalTo(@15);
        }];
        _priceLabel.textAlignment = NSTextAlignmentRight;
        _priceLabel.textColor = [UIColor redColor];
        _priceLabel.font = [UIFont systemFontOfSize:15];
        _areaSizeLabel.font = [UIFont systemFontOfSize:10];
        _areaSizeLabel.textColor = [UIColor colorWithRed:0.3912 green:0.3874 blue:0.395 alpha:1.0];
        _areaSizeLabel.textAlignment = NSTextAlignmentRight;
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
