//
//  AgentHouseChooseCollectionViewCell.m
//  House
//
//  Created by 张垚 on 16/7/21.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "AgentHouseChooseCollectionViewCell.h"



@implementation AgentHouseChooseCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _segView = [[UIView alloc] init];
        [self.contentView addSubview:_segView];
        _secondaryButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.contentView addSubview:_secondaryButton];
        _rentHouseButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.contentView addSubview:_rentHouseButton];
        _halfView1 = [[UIView alloc] init];
        [self.contentView addSubview:_halfView1];
        _halfView2 = [[UIView alloc] init];
        [self.contentView addSubview:_halfView2];
    }
    return self;
}
- (void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    UIView *superView = self.contentView;
    [_secondaryButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left);
        make.width.mas_equalTo(kScreenWidth / 2);
        make.top.equalTo(superView.mas_top);
        make.bottom.equalTo(superView.mas_bottom).offset(-3);
    }];
    _secondaryButton.tag = 1;
    [_secondaryButton addTarget:self action:@selector(choose:) forControlEvents:UIControlEventTouchUpInside];
    [_secondaryButton setTitle:@"二手房" forState:UIControlStateNormal];
    [_secondaryButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_rentHouseButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(superView.mas_right
                           );
        make.width.mas_equalTo(kScreenWidth / 2);
        make.top.equalTo(superView.mas_top);
        make.bottom.equalTo(superView.mas_bottom).offset(-3);
    }];
    _rentHouseButton.tag = 2;
    [_rentHouseButton addTarget:self action:@selector(choose:) forControlEvents:UIControlEventTouchUpInside];
    [_rentHouseButton setTitle:@"租房" forState:UIControlStateNormal];
    [_rentHouseButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_segView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kScreenWidth);
        make.height.equalTo(@1);
        make.bottom.equalTo(superView.mas_bottom).offset(-1);
    }];
    _segView.backgroundColor = [UIColor lightGrayColor];
    [_halfView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left);
        make.width.mas_equalTo(kScreenWidth / 2);
        make.height.equalTo(@3);
        make.bottom.equalTo(superView.mas_bottom);
    }];
    _halfView1.backgroundColor = [UIColor colorWithRed:0.4 green:0.8 blue:1.0 alpha:1.0];
    [_halfView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(superView.mas_right);
        make.width.mas_equalTo(kScreenWidth / 2);
        make.height.equalTo(@3);
        make.bottom.equalTo(superView.mas_bottom);
    }];
    _halfView2.backgroundColor = [UIColor clearColor];
}

- (void)choose:(UIButton *)button {
    if (button.tag == 2) {
        _halfView1.backgroundColor = [UIColor clearColor];
        _halfView2.backgroundColor = [UIColor colorWithRed:0.4 green:0.8 blue:1.0 alpha:1.0];
    }else {
        _halfView2.backgroundColor = [UIColor clearColor];
        _halfView1.backgroundColor = [UIColor colorWithRed:0.4 green:0.8 blue:1.0 alpha:1.0];
    }
}

@end
