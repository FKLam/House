//
//  ActionCenterListModel.h
//  House
//
//  Created by 张垚 on 16/9/9.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "BaseModel.h"

@interface ActionCenterListModel : BaseModel

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *img;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *addtime;


@end
