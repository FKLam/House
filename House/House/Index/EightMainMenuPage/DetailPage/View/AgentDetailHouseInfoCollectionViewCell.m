//
//  AgentDetailHouseInfoCollectionViewCell.m
//  House
//
//  Created by 张垚 on 16/7/20.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "AgentDetailHouseInfoCollectionViewCell.h"
#import "AgentDetailModel.h"


@interface AgentDetailHouseInfoCollectionViewCell ()
@property (nonatomic, strong) UIImageView *mainImageView;
@property (nonatomic, strong) UILabel *addressLabel;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UILabel *infoLabel;
@end

@implementation AgentDetailHouseInfoCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _mainImageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_mainImageView];
        _addressLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_addressLabel];
        _priceLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_priceLabel];
        _infoLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_infoLabel];
    }
    return self;
}
-(void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    UIView *superView = self.contentView;
    [_mainImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(10);
        make.top.equalTo(superView.mas_top).offset(15);
        make.bottom.equalTo(superView.mas_bottom).offset(-50);
        make.right.equalTo(superView.mas_right).offset(-10);
    }];
    [_addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_mainImageView.mas_left);
        make.top.equalTo(_mainImageView.mas_bottom).offset(5);
        make.width.mas_equalTo(@60);
        make.height.equalTo(@20);
    }];
    _addressLabel.font = [UIFont systemFontOfSize:15];
    [_priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_addressLabel.mas_right);
        make.top.equalTo(_mainImageView.mas_bottom).offset(5);
        make.right.equalTo(superView.mas_right);
        make.height.equalTo(@20);
    }];
    _priceLabel.font = [UIFont systemFontOfSize:15];
    _priceLabel.textAlignment = NSTextAlignmentRight;
    _priceLabel.textColor = [UIColor redColor];
    [_infoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_mainImageView.mas_left);
        make.top.equalTo(_priceLabel.mas_bottom).offset(5);
        make.right.equalTo(superView.mas_right);
        make.height.equalTo(@20);
    }];
    _infoLabel.font = [UIFont systemFontOfSize:15];
    _infoLabel.textColor = [UIColor lightGrayColor];
}

- (void)setModel:(AgentDetailModel *)model {
    if (model != _model) {
        _model = model;
    }
    [_mainImageView setImageWithURL:[NSURL URLWithString:model.thumb] placeholderImage:[UIImage imageNamed:@"placeHolderImage"]];
    _addressLabel.text = model.title;
    if (_flag == 1) {
        _priceLabel.text = [NSString stringWithFormat:@"%@万", model.price];
    }else {
        _priceLabel.text = [NSString stringWithFormat:@"%@元/月", model.price];
    }
    
    _infoLabel.text = [NSString stringWithFormat:@"%@室%@厅 %@㎡", model.room, model.hall, model.houseearm];
}



@end
