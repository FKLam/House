//
//  RentHouseDetailModel.h
//  House
//
//  Created by 张垚 on 16/8/2.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "BaseModel.h"

@interface RentHouseDetailModel : BaseModel
//房源id
@property (nonatomic, copy) NSString *itemid;
//区域id
@property (nonatomic, copy) NSString *areaid;
//标题
@property (nonatomic, copy) NSString *title;
//价格
@property (nonatomic, copy) NSString *price;
//室
@property (nonatomic, copy) NSString *room;
//厅
@property (nonatomic, copy) NSString *hall;
//面积
@property (nonatomic, copy) NSString *houseearm;
//地址
@property (nonatomic, copy) NSString *address;
//年份
@property (nonatomic, copy) NSString *houseyear;
//上架时间
@property (nonatomic, copy) NSString *adddate;
//配套设施
@property (nonatomic, copy) NSString *peitaor;
//第几层
@property (nonatomic, copy) NSString *floor1;
//总共多少层
@property (nonatomic, copy) NSString *floor2;
//谁发布的
@property (nonatomic, copy) NSString *userid;
//经纪人房评
@property (nonatomic, copy) NSDictionary *remark;
//经纬度
@property (nonatomic, copy) NSString *map;
//编号
@property (nonatomic, copy) NSString *code;
//配套设施
@property (nonatomic, copy) NSString *peitao;
//轮播图
@property (nonatomic, strong) NSArray *thumb;
//付款方式
@property (nonatomic, copy) NSString *bus;
//租赁方式
@property (nonatomic, copy) NSString *rentmethod;

@property (nonatomic, copy) NSString *zhuanxiu;

@property (nonatomic, copy) NSString *toward;

@property (nonatomic, copy) NSString *shiyongmj;

@property (nonatomic, copy) NSString *shoufu;

@property (nonatomic, copy) NSString *yuegong;

@property (nonatomic, copy) NSString *housename;

@property (nonatomic, copy) NSString *fyts;

@property (nonatomic, copy) NSString *fytsr;
@end
