//
//  ActionCenterMianListTableViewCell.h
//  House
//
//  Created by 张垚 on 16/9/9.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ActionCenterListModel;

@interface ActionCenterMianListTableViewCell : UITableViewCell
@property (nonatomic, strong) ActionCenterListModel *model;
@end
