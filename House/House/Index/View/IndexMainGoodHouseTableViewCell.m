//
//  IndexMainGoodHouseTableViewCell.m
//  House
//
//  Created by 张垚 on 16/9/6.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "IndexMainGoodHouseTableViewCell.h"

@implementation moreButton

- (CGRect)imageRectForContentRect:(CGRect)contentRect {
   return CGRectMake(contentRect.size.width - 25, contentRect.size.height / 4, 25, contentRect.size.height / 2);
}
- (CGRect)titleRectForContentRect:(CGRect)contentRect {
    return CGRectMake(0, 0, contentRect.size.width - 25 , contentRect.size.height);
}

@end


@implementation IndexMainGoodHouseTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _titleLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_titleLabel];
        _moreButton = [[moreButton alloc] init];
        [self.contentView addSubview:_moreButton];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self.contentView;
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(10);
        make.top.equalTo(superView.mas_top);
        make.bottom.equalTo(superView.mas_bottom);
        make.right.equalTo(_moreButton.mas_left).offset(-20);
    }];
    _titleLabel.text = @"精品房源推荐";
    _titleLabel.textColor = [UIColor orangeColor];
    [_moreButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(superView.mas_right);
        make.top.equalTo(superView.mas_top);
        make.bottom.equalTo(superView.mas_bottom);
        make.width.equalTo(@100);
    }];
    _moreButton.titleLabel.textAlignment = NSTextAlignmentRight;
    [_moreButton setTitle:@"更多" forState:UIControlStateNormal];
    _moreButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [_moreButton setImage:[UIImage imageNamed:@"moreHouseInfo"] forState:UIControlStateNormal];
    [_moreButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
