//
//  UnfoldFrameModel.h
//  House
//
//  Created by 张垚 on 16/7/14.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <Foundation/Foundation.h>
@class UnfoldModel;



@interface UnfoldFrameModel : NSObject
/* model属性 */
@property (nonatomic, strong) UnfoldModel *model;
/* 文字内容尺寸 */
@property (nonatomic, assign) CGRect contextFrame;
/* 展开收缩按钮大小位置 */
@property (nonatomic, assign) CGRect btnFrame;
/* Cell高度 */
@property (nonatomic, assign) CGFloat cellHeight;

@end
