//
//  MoveHousePushTableViewCell.m
//  MoveHouse
//
//  Created by 张垚 on 16/6/7.
//  Copyright © 2016年 众杰网络. All rights reserved.
//

#import "MoveHousePushTableViewCell.h"
#import "LabelAndTextFieldView.h"


static CGFloat topPadding = 15.0f;
static CGFloat leftPadding = 5.0f;
static CGFloat rightPadding = 10.0f;
static CGFloat bottomPadding = 10.0f;


@implementation MoveHousePushTableViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _inputView = [[LabelAndTextFieldView alloc] init];
        [self.contentView addSubview:_inputView];
        _pushButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_pushButton setImage:[UIImage imageNamed:@"push"] forState:UIControlStateNormal];
        [self.contentView addSubview:_pushButton];
    }
    return self;
}
- (void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self.contentView;
    [_inputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(topPadding);
        make.left.equalTo(superView.mas_left).offset(leftPadding);
        make.right.equalTo(_pushButton.mas_left).offset(-rightPadding);
        make.bottom.equalTo(superView.mas_bottom).offset(-bottomPadding);
    }];
    [_pushButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(superView.mas_right).offset(-rightPadding);
        make.centerY.equalTo(superView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(20, 18));
    }];
}
- (void)setIsPush:(BOOL)isPush {
    _isPush = isPush;
    _pushButton.hidden = !isPush;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
