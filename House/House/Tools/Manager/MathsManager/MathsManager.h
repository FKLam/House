//
//  MathsManager.h
//  House
//
//  Created by 吕品 on 16/6/3.
//  Copyright © 2016年 ZJWL. All rights reserved.
//
/*!
 *  @author 众杰网络, 16-06-03 18:06:58
 *
 *  @brief 处理各种数字
 *
 *  @since 1.0
 */
#ifndef MathsManager_h
#define MathsManager_h


#pragma mark HUD
static NSTimeInterval HUDShowDuration = 1.0f;
#pragma mark pulic
static const NSInteger tagManager = 1000;
#define kScreenWidth [UIScreen mainScreen].bounds.size.width
#define kScreenHeight [UIScreen mainScreen].bounds.size.height
#define kNaviBarHeight 64.0f
#define kTabBarHeight 44.0f
#pragma mark indexMainVCTableView 
static CGFloat indexMainVCTableViewPadding = 10.0f;
static CGFloat indexMainVCTableViewCollectionViewCellLabelHeight = 25.0f;
static NSInteger indexMainVCTableViewCellCollectionViewCellCount = 8;
static CGFloat indexMainVCTableViewTopADHeight = 150.0f;
#endif /* MathsManager_h */
