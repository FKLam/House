//
//  HouseDetailCharacteristicTableViewCell.h
//  House
//
//  Created by 张垚 on 16/10/7.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RentHouseDetailModel;

@interface HouseDetailCharacteristicTableViewCell : UITableViewCell

//四个标签
@property (nonatomic, strong) UILabel *tagLabel1;
@property (nonatomic, strong) UILabel *tagLabel2;
@property (nonatomic, strong) UILabel *tagLabel3;
@property (nonatomic, strong) UILabel *tagLabel4;


@property (nonatomic, strong) RentHouseDetailModel *model;
@property (nonatomic, strong) RentHouseDetailModel *currentModel;

@end
