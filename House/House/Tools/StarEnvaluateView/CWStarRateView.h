//
//  CWStarRateView.h
//  StarRateDemo
//
//  Created by WANGCHAO on 14/11/4.
//  Copyright (c) 2014年 wangchao. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CWStarRateView;
/* 协议 */
@protocol CWStarRateViewDelegate <NSObject>
/* 选择实现的方法 */
@optional
/* 开始的视图到变化之后的新视图 用于显示评价星级服务*/
- (void)starRateView:(CWStarRateView *)starRateView scroePercentDidChange:(CGFloat)newScorePercent;
@end

@interface CWStarRateView : UIView

@property (nonatomic, assign) CGFloat scorePercent;//得分值，范围为0--1，默认为1
@property (nonatomic, assign) BOOL hasAnimation;//是否允许动画，默认为NO
@property (nonatomic, assign) BOOL allowIncompleteStar;//评分时是否允许不是整星，默认为NO

/* 星星数 */
@property (nonatomic, assign) NSInteger numberOfStars;

@property (nonatomic, weak) id<CWStarRateViewDelegate>delegate;
/* 重写初始化方法 */
- (instancetype)initWithFrame:(CGRect)frame numberOfStars:(NSInteger)numberOfStars;

@end