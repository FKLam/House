//
//  BudgetRageChooseTableViewCell.h
//  IndexMoveHousePage
//
//  Created by 张垚 on 16/6/17.
//  Copyright © 2016年 众杰网络. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BudgetRageChooseTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *descriptionLabel;
@property (nonatomic, strong) UITextField *minPriceTextfield;
@property (nonatomic, strong) UIView *midView;
@property (nonatomic, strong) UITextField *maxPriceTextfiled;
@property (nonatomic, strong) UILabel *unitLabel;
@property (nonatomic, assign) NSInteger typeTag;
@end
