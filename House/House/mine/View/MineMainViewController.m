//
//  MineMainViewController.m
//  House
//
//  Created by 吕品 on 16/5/31.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "MineMainViewController.h"
#import "IndexMainDoubleADTableViewCell.h"
#import "MineTwoButtonTableViewCell.h"
#import "House-Swift.h"
@interface MineMainViewController ()<UITableViewDelegate, UITableViewDataSource, BaseCollectionWithTableViewCellDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, copy) NSArray *dataArray;
@end

@implementation MineMainViewController
#pragma mark cyclelife
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initTableView];
    self.dataArray = @[@[@"委托服务", @"关注的二手房/租房", @"关注的小区"], @[@"我发布的房源", @"客户中心", @"推广中心"], @[@"购房计算器", @"用户反馈"]];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}
#pragma mark initsubviews
//初始化tableview
- (void)initTableView {
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight - kTabBarHeight) style:UITableViewStyleGrouped];
    [self.view addSubview:_tableView];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 160)];
    imageView.userInteractionEnabled = YES;
    //header
    UIButton *imageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [imageButton setImage:[UIImage imageNamed:@"MineBackButtonImage"] forState:UIControlStateNormal];
    [imageButton addTarget:self action:@selector(restoreMineInfo) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:imageButton];

    UIButton *logInfoButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [imageView addSubview:logInfoButton];
    [logInfoButton setTitle:@"登录/注册" forState:UIControlStateNormal];
    logInfoButton.layer.masksToBounds = YES;
    logInfoButton.layer.borderWidth = 1.0f;
    logInfoButton.layer.borderColor = [UIColor whiteColor].CGColor;
    logInfoButton.tintColor = [UIColor whiteColor];
    [logInfoButton addTarget:self action:@selector(loginOrReg:) forControlEvents:UIControlEventTouchUpInside];
    CGFloat padding = 15;
    CGFloat loginHeight = 30;
    [imageButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(imageView.mas_top).with.offset(padding * 2);
        make.centerX.mas_equalTo(imageView);
        make.width.and.height.mas_equalTo(160 - padding * 4 - loginHeight);
        make.bottom.mas_equalTo(logInfoButton.mas_top).with.offset(-padding);
    }];

    [logInfoButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(imageView);
        make.height.mas_equalTo(loginHeight);
        make.width.mas_equalTo(80);
        make.bottom.mas_equalTo(imageView.mas_bottom).with.offset(-padding);
    }];
    imageView.image = [UIImage imageNamed:@"MineBackImage"];
    _tableView.tableHeaderView = imageView;
    //footer
    UIView *footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 100)];
    UIButton *logOffButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [footView addSubview:logOffButton];
    _tableView.tableFooterView = footView;
    NSString *title;
    UIColor *color;
    if ([UserInfoManager checkStatus]) {
        title = @"退出登录";
        color = [UIColor redColor];
    }
    logOffButton.backgroundColor = color;
    [logOffButton setTitle:title forState:UIControlStateNormal];
    logOffButton.tintColor = [UIColor whiteColor];
    logOffButton.layer.masksToBounds = YES;
    logOffButton.layer.cornerRadius = 3;
    [logOffButton addTarget:self action:@selector(logOff) forControlEvents:UIControlEventTouchUpInside];
    [logOffButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(_tableView);
        make.height.mas_equalTo(40);
        make.width.mas_equalTo(200);
        make.bottom.mas_equalTo(footView).with.offset(-40);
    }];
    [_tableView registerClass:[MineTwoButtonTableViewCell class] forCellReuseIdentifier:NSStringFromClass([MineTwoButtonTableViewCell class])];
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:NSStringFromClass([UITableViewCell class])];
    _tableView.delegate = self;
    _tableView.dataSource = self;

}
//获取个人信息
- (void)restoreMineInfo {
    if ([UserInfoManager checkStatus]) {
        
    }
}
//登录注册
- (void)loginOrReg:(UIButton *)button {

}
//注销
- (void)logOff{

}
#pragma mark tableDatasourceAndDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 2 || section == 1) {
        return 3;
    } else if (section == 0) {
        return 1;
    } else {
        return 2;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MineTwoButtonTableViewCell *twoADCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([MineTwoButtonTableViewCell class])];
    UITableViewCell *normalCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([UITableViewCell class])];

    if (indexPath.section == 0) {
        twoADCell.array = @[@{@"name" : @"浏览", @"image" : [UIImage imageNamed:@"MineTwoAD-0"]}, @{@"name" : @"订单", @"image" : [UIImage imageNamed:@"MineTwoAD-1"]}];
        twoADCell.delegate = self;
        return twoADCell;
    } else {
        normalCell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"MineSection-%ld-%ld",[indexPath section], indexPath.row]];
        normalCell.textLabel.text = _dataArray[indexPath.section - 1][indexPath.row];
        if (indexPath.section == 1 || indexPath.section == 2) {
            normalCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        return normalCell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 ) {
        return 100;
    }else {
        return 50;
    }
}
#pragma 两个单独的
- (void)tableViewCell:(BaseCollectionWithTableViewCell *)cell didSelectedAtItem:(NSInteger)item {

}
#pragma mark 点击cell
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}




@end
