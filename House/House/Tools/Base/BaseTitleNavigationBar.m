//
//  BaseTitleNavigationBar.m
//  House
//
//  Created by 张垚 on 16/7/8.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "BaseTitleNavigationBar.h"

@implementation BaseTitleNavigationBar


- (instancetype)initWithFrame:(CGRect)frame WithTitleStr:(NSString *)title {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:212 / 255.0f green:38 / 255.0f blue:26 / 255.0f alpha:1.0];
        _leftView = [[UIView alloc] init];
        [self addSubview:_leftView];
       UIImageView *leftImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
        leftImageView.image = [UIImage imageNamed:@"newBack"];
        [_leftView addSubview:leftImageView];
       UILabel *leftLabel = [[UILabel alloc] initWithFrame:CGRectMake(25, 0, 55, 25)];
        leftLabel.text = @"返回";
        leftLabel.textColor = [UIColor whiteColor];
        [_leftView addSubview:leftLabel];
        
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.font = [UIFont systemFontOfSize:18 weight:15];
        _titleLabel.text = title;
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_titleLabel];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self;
    [_leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(5);
        make.bottom.equalTo(superView.mas_bottom).offset(-3);
        make.height.equalTo(@30);
        make.width.equalTo(@80);
    }];
    
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_leftView.mas_right);
        make.bottom.equalTo(superView.mas_bottom).offset(-5);
        make.height.equalTo(@30);
        make.right.equalTo(superView.mas_right).offset(-85);
    }];
 
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
