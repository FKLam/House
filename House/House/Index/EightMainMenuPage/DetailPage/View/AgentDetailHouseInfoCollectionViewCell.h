//
//  AgentDetailHouseInfoCollectionViewCell.h
//  House
//
//  Created by 张垚 on 16/7/20.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AgentDetailModel;

@interface AgentDetailHouseInfoCollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) AgentDetailModel *model;
@property (nonatomic, assign) NSInteger flag;

@end
