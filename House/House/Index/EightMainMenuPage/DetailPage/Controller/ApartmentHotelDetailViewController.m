//
//  ApartmentHotelDetailViewController.m
//  House
//
//  Created by 张垚 on 16/7/21.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "ApartmentHotelDetailViewController.h"
#import "BaseTitleNavigationBar.h"
#import "ApartmentHotelScheduleViewController.h"

@interface ApartmentHotelDetailViewController ()

@property (nonatomic, strong) BaseTitleNavigationBar *myBar;
//底部视图属性
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UIButton *scheduleButton;
//webview属性
@property (nonatomic, strong) UIWebView *webView;
//存放网络图片的数组
@property (nonatomic, strong) NSArray *imageArr;
//轮播图
@property (nonatomic, strong) SDCycleScrollView *cycleScrollView;

@property (nonatomic, strong) MBProgressHUD *hud;

@end

@implementation ApartmentHotelDetailViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = YES;
}

#pragma mark - 主程序入口
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    _hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _hud.label.text = @"正在加载数据";
    //    _hud.dimBackground = YES;
    _hud.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
    _hud.backgroundView.backgroundColor = [UIColor whiteColor];
    _hud.bezelView.style = MBProgressHUDBackgroundStyleBlur;
    _hud.animationType = MBProgressHUDAnimationZoomIn;
    //详情数据
    [self getHotelDetailData];
    //webview创建 属性设置
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 64, kScreenWidth, kScreenHeight - 114)];
    //这两个属性为了防止出现黑色视图
    _webView.backgroundColor = [UIColor clearColor];
    _webView.opaque = NO;
     [self.view addSubview:_webView];
    //可以滑动 但是没有边缘反弹效果
    _webView.scrollView.bounces = NO;
    _webView.scrollView.showsHorizontalScrollIndicator = NO;
    //增加一个200展示内容 用于展示轮播图
    _webView.scrollView.contentInset = UIEdgeInsetsMake(200, 0, 0, 0);
    _cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, -200, kScreenWidth, 200) imageURLStringsGroup:_imageArr];
    //占位图片
    _cycleScrollView.placeholderImage = [UIImage imageNamed:@"placeHolderImage"];
    [_webView.scrollView addSubview:_cycleScrollView];
   //底部视图
    [self initBottomView];
    
    //顶部视图
    _myBar = [[BaseTitleNavigationBar alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 64) WithTitleStr:_titleStr];
    [self.view addSubview:_myBar];
    //返回视图添加手势 用于返回
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(back:)];
    [_myBar.leftView addGestureRecognizer:tap];
    // Do any additional setup after loading the view.
}

#pragma mark - 数据请求部分
- (void)getHotelDetailData {
    [NetRequest getWithURLString:@"http://aizufang.hrbzjwl.com/tp/api.php/Gongyujd/xiangqing" parameters:@{@"id": _hotelID} progress:^(NSProgress *progress) {
        
    } success:^(id response) {
        if ([response[@"flag"] isEqualToString:@"1"]) {
            _hud.label.text = @"加载成功";
            [_hud hideAnimated:YES afterDelay:1];
            //网页展示
            [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:response[@"url"]]]];
            //获取轮播图片 并赋值
            _imageArr = response[@"lunbo"];
            _cycleScrollView.imageURLStringsGroup = _imageArr;
            
        }else {
            _hud.label.text = @"加载失败";
            [_hud hideAnimated:YES afterDelay:1];
        }
        
    } failure:^(NSError *error) {
        _hud.label.text = @"服务器异常";
        [_hud hideAnimated:YES afterDelay:1];
    }];
    
}
#pragma mark - 底部视图
- (void)initBottomView {
    UIView *superView = self.view;
//    _priceLabel = [[UILabel alloc] init];
//    _priceLabel.font = [UIFont systemFontOfSize:20];
//    _priceLabel.backgroundColor = [UIColor colorWithRed:0.902 green:0.902 blue:0.902 alpha:1.0];
//    _priceLabel.textAlignment = NSTextAlignmentCenter;
//    NSMutableAttributedString *priceLabel = [[NSMutableAttributedString alloc] initWithString:@"价格: ¥230.00"];
//    [priceLabel addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, 3)];
//    _priceLabel.textColor = [UIColor redColor];
//    _priceLabel.attributedText = priceLabel;
//    [self.view addSubview:_priceLabel];
    _scheduleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:_scheduleButton];
    _scheduleButton.backgroundColor = [UIColor orangeColor];
    [_scheduleButton setTitle:@"酒店预约" forState:UIControlStateNormal];
    _scheduleButton.titleLabel.font = [UIFont systemFontOfSize:20 weight:20];
//    [_priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(kScreenWidth / 2);
//        make.height.equalTo(@60);
//        make.bottom.equalTo(superView.mas_bottom);
//        make.left.equalTo(superView.mas_left);
//    }];
    [_scheduleButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kScreenWidth);
        make.height.equalTo(@50);
        make.bottom.equalTo(superView.mas_bottom);
        make.right.equalTo(superView.mas_right);
    }];
    [_scheduleButton addTarget:self action:@selector(schedule:) forControlEvents:UIControlEventTouchUpInside];
    
}
//预定房间点击事件
- (void)schedule:(UIButton *)button {
//    ApartmentHotelScheduleViewController *apartVC = [[ApartmentHotelScheduleViewController alloc] init];
//    apartVC.itemID = _hotelID;
//    [self.navigationController pushViewController:apartVC animated:YES];
    NSMutableString *str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",unifyServiceNumber];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}

//返回手势
- (void)back:(UITapGestureRecognizer *)tap {
    [self.navigationController popViewControllerAnimated:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
