//
//  ReuseHouseBaseInfoTableViewCell.m
//  House
//
//  Created by 张垚 on 16/7/12.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "ReuseHouseBaseInfoTableViewCell.h"
#import "RentHouseDetailModel.h"

@interface ReuseHouseBaseInfoTableViewCell ()

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIView *segmentView;

@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UIView *segView1;
@property (nonatomic, strong) UIView *segView2;
@property (nonatomic, strong) UILabel *roomNameModelLabel;
@property (nonatomic, strong) UILabel *roomModelLabel;
@property (nonatomic, strong) UILabel *areaNameLabel;
@property (nonatomic, strong) UILabel *areaLabel;

@end

@implementation ReuseHouseBaseInfoTableViewCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _titleLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_titleLabel];
        _segmentView = [[UIView alloc] init];
        [self.contentView addSubview:_segmentView];
        _priceNameLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_priceNameLabel];
        _priceLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_priceLabel];
        _segView1 = [[UIView alloc] init];
        [self.contentView addSubview:_segView1];
        _roomNameModelLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_roomNameModelLabel];
        _roomModelLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_roomModelLabel];
        _segView2 = [[UIView alloc] init];
        [self.contentView addSubview:_segView2];
        _areaNameLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_areaNameLabel];
        _areaLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_areaLabel];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self.contentView;
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(10);
        make.top.equalTo(superView.mas_top).offset(10);
        make.right.equalTo(superView.mas_right).offset(-10);
        make.height.equalTo(@20);
    }];
    _titleLabel.textColor = [UIColor colorWithRed:0.0926 green:0.0922 blue:0.093 alpha:1.0];
    [_segmentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(5);
        make.top.equalTo(_titleLabel.mas_bottom).offset(5);
        make.right.equalTo(superView.mas_right).offset(-5);
        make.height.equalTo(@1);
    }];
    _segmentView.backgroundColor = [UIColor lightGrayColor];
    [_priceNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_segmentView.mas_bottom).offset(20);
        make.left.equalTo(superView.mas_left);
        make.width.mas_equalTo(kScreenWidth / 3 - 1);
        make.height.equalTo(@20);
    }];
    _priceNameLabel.textAlignment = NSTextAlignmentCenter;
    [_priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_priceNameLabel.mas_bottom).offset(5);
        make.left.equalTo(superView.mas_left);
        make.width.mas_equalTo(kScreenWidth / 3 - 1);
        make.height.equalTo(@20);
    }];
    _priceLabel.textColor = [UIColor orangeColor];
    _priceLabel.textAlignment = NSTextAlignmentCenter;
    [_segView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_priceNameLabel.mas_top);
        make.left.equalTo(_priceNameLabel.mas_right);
        make.width.mas_equalTo(@1);
        make.height.equalTo(@45);
    }];
    _segView1.backgroundColor = [UIColor lightGrayColor];
    [_roomNameModelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_segmentView.mas_bottom).offset(20);
        make.left.equalTo(_segView1.mas_right);
        make.width.mas_equalTo(kScreenWidth / 3 - 1);
        make.height.equalTo(@20);
    }];
    _roomNameModelLabel.textAlignment = NSTextAlignmentCenter;
    _roomNameModelLabel.text = @"房型";
    [_roomModelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_roomNameModelLabel.mas_bottom).offset(5);
        make.left.equalTo(_segView1.mas_right);
        make.width.mas_equalTo(kScreenWidth / 3 - 1);
        make.height.equalTo(@20);
    }];
    _roomModelLabel.textColor = [UIColor orangeColor];
    _roomModelLabel.textAlignment = NSTextAlignmentCenter;
    [_segView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_priceNameLabel.mas_top);
        make.left.equalTo(_roomNameModelLabel.mas_right);
        make.width.mas_equalTo(@1);
        make.height.equalTo(@45);
    }];
    _segView2.backgroundColor = [UIColor lightGrayColor];
    [_areaNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(superView.mas_right);
        make.left.equalTo(_segView2.mas_right);
        make.top.equalTo(_segmentView.mas_bottom).offset(20);
        make.height.equalTo(@20);
    }];
    _areaNameLabel.text = @"建筑面积";
    _areaNameLabel.textAlignment = NSTextAlignmentCenter;
    [_areaLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(superView.mas_right);
        make.left.equalTo(_segView2.mas_right);
        make.top.equalTo(_areaNameLabel.mas_bottom).offset(5);
        make.height.equalTo(@20);
    }];
    _areaLabel.textAlignment = NSTextAlignmentCenter;
    _areaLabel.textColor = [UIColor orangeColor];
    
}

- (void)setModel:(RentHouseDetailModel *)model {
    if (model != _model) {
        _model = model;
    }
    if ([_priceNameLabel.text isEqualToString:@"租金"]) {
        _priceLabel.text = [NSString stringWithFormat:@"%@元/月",[model.price description]] ;
        _titleLabel.text = [model.housename description];
    }else {
        _priceLabel.text = [NSString stringWithFormat:@"%@万", [model.price description]];
        _titleLabel.text = [model.housename description];
    }
    _roomModelLabel.text = [NSString stringWithFormat:@"%@室%@厅", [model.room description], [model.hall description]];
    _areaLabel.text = [NSString stringWithFormat:@"%@㎡", [model.houseearm description]];
    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
