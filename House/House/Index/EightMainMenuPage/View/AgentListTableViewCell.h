//
//  AgentListTableViewCell.h
//  House
//
//  Created by 张垚 on 16/7/5.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AgentListDetaiVIew.h"
@class AgentListTableViewCell;
@class ImageViewAndLabelButton;
@class AgentListModel;

@protocol AgentListTableViewCellDelegate <NSObject>


- (void)leftSwipe:(AgentListTableViewCell *)obj;
- (void)rightSwipe:(AgentListTableViewCell *)obj;

- (void)passButton:(ImageViewAndLabelButton *)button;

@end



@interface AgentListTableViewCell : UITableViewCell

@property (nonatomic, assign) id <AgentListTableViewCellDelegate> delegate;
@property (nonatomic, strong) AgentListDetaiVIew *myVIew;
@property (nonatomic, strong) AgentListModel *model;

@end
