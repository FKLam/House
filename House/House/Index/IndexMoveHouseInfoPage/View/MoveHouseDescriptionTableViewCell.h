//
//  MoveHouseDescriptionTableViewCell.h
//  House
//
//  Created by 张垚 on 16/8/8.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoveHouseDescriptionTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UITextView *mainTextView;

@end
