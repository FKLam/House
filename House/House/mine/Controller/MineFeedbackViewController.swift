 //
//  MineFeedbackViewController.swift
//  House
//
//  Created by 吕品 on 2016/9/12.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

import UIKit
import MBProgressHUD
class MineFeedbackViewController: BaseViewController, UITextFieldDelegate {

    var naviBar = BaseTitleNavigationBar()
    
    let textView = UITextView.init()
    let textField = UITextField.init()
    override func viewDidLoad() {
        super.viewDidLoad()
        naviBar = BaseTitleNavigationBar.init(frame: CGRect.init(x: 0, y: 0, width: self.view.bounds.size.width, height: 64), withTitleStr: "用户反馈")
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(back(tap:)))
        
        naviBar.leftView.addGestureRecognizer(tap)
        view.addSubview(naviBar)
        tabbarHidden = true
        automaticallyAdjustsScrollViewInsets = false
        // Do any additional setup after loading the view.
        view.addSubview(textView)
        view.backgroundColor = UIColor.white
        textView.layer.masksToBounds = true
        textView.layer.borderColor = UIColor.black.cgColor
        textView.layer.borderWidth = 1.5
        textView.snp.makeConstraints({ (make) in
            make.left.right.equalTo(view)
            make.top.equalTo(view.snp.top).offset(64)
            make.height.equalTo(200)
        })
        textView.font = UIFont.systemFont(ofSize: 18)
        view.addSubview(textField)
        textField.snp.makeConstraints { (make) in
            make.top.equalTo(textView.snp.bottom).offset(10)
            make.left.right.equalTo(view)
            make.height.equalTo(30)
        }
        textField.delegate = self
        textField.borderStyle = .roundedRect
        textField.placeholder = "请输入您的手机号"
        navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "提交", style: .plain, target: self, action: #selector(submitData))
    }
    //MARK: -提交信息
    @objc fileprivate func submitData() -> Swift.Void {
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.label.text = "提交中"

        NetRequest.updateOrGetInformation(fromUrl: "http://aizufang.hrbzjwl.com/tp/api.php/Index/userfankui", withParameters: ["yijian": textView.text, "tel": textField.text!, "uid": UserInfoManager.shared().uid], success: { (task, responseObject) in
            if responseObject is NSDictionary {
                let res = responseObject as! NSDictionary
                if (res["flg"] as! NSNumber == NSNumber.init(value: 1)) {
                    hud.mode = .text
                    hud.label.text = "提交成功"
                    DispatchQueue.main.async {
                        hud.hide(animated: true, afterDelay: 1.0)
                    }
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1, execute: { 
                        self.navigationController!.popToRootViewController(animated: true)
                    })

                } else {
                    hud.mode = .text
                    hud.label.text = "服务器错误,请稍后再试"
                    DispatchQueue.main.async {
                        hud.hide(animated: true, afterDelay: 1)
                    }
                }
            }

           
            }) { (task, error) in
                print(error)
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        view.endEditing(true)
        return true
    }
    @objc func back(tap: UITapGestureRecognizer) -> Void {
        self.navigationController!.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}




