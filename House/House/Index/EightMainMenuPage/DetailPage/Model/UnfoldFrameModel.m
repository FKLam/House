//
//  UnfoldFrameModel.m
//  House
//
//  Created by 张垚 on 16/7/14.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "UnfoldFrameModel.h"
#import "UnfoldModel.h"



@implementation UnfoldFrameModel

- (void)setModel:(UnfoldModel *)model {
    _model = model;
    CGFloat margin = 8;
    /* 显示内容label的宽度 */
    CGFloat contentLabelW =  kScreenWidth - 2 * margin;
    /* 内容高度 一行和多行 */
    CGFloat contentH = [model.contextStr boundingRectWithSize:CGSizeMake(contentLabelW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName :[UIFont systemFontOfSize:17]} context:nil].size.height;
    CGFloat oneLineH = [@"这是一行" boundingRectWithSize:CGSizeMake(contentLabelW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName :[UIFont systemFontOfSize:17]} context:nil].size.height;
    /* 展开按钮高度 */
    CGFloat btnH = 0;
    /* 如果未折叠 */
    if (!model.isFold) {
        /* 大于三行 显示三行 小于三行 按钮高度为0 */
        if (contentH >= 3 * oneLineH) {
            contentH = 3 * oneLineH;
            btnH = 30;
        }else{
            btnH = 0;
        }
    }
    /* 折叠的情况下显示按钮 */
    else{
        btnH = 30;
    }
    /* 内容尺寸 */
    self.contextFrame = CGRectMake(margin, margin + 130,contentLabelW,contentH);
    /* 按钮宽度 */
    CGFloat btnW = 80;
    /* cell高度 */
    CGFloat cellMaxH = 0;
    
    if (btnH == 0) {//只有一行的时候，就隐藏UbflodBtn
        self.btnFrame = CGRectZero;
        cellMaxH = CGRectGetMaxY(self.contextFrame);
    }else{
        self.btnFrame = CGRectMake((kScreenWidth - btnW) / 2, CGRectGetMaxY(self.contextFrame) + 10, btnW, 30);
        cellMaxH = CGRectGetMaxY(self.btnFrame);
    }
    
    self.cellHeight = cellMaxH + margin;
}



@end
