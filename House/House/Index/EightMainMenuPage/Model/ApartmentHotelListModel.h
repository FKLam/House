//
//  ApartmentHotelListModel.h
//  House
//
//  Created by 张垚 on 16/8/18.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "BaseModel.h"

@interface ApartmentHotelListModel : BaseModel
//酒店id
@property (nonatomic, copy) NSString *itemid;
//区id
@property (nonatomic, copy) NSString *areaid;
//酒店名称
@property (nonatomic, copy) NSString *housename;
//价格（元/天)
@property (nonatomic, copy) NSString *price;
//房间类型
@property (nonatomic, copy) NSString *type;
//图
@property (nonatomic, copy) NSString *thumb;
//街道
@property (nonatomic, copy) NSString *streets;


@end
