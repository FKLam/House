//
//  ImageViewAndLabelButton.m
//  House
//
//  Created by 张垚 on 16/6/30.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "ImageViewAndLabelButton.h"

@implementation ImageViewAndLabelButton


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _logoImage = [[UIImageView alloc] init];
        [self addSubview:_logoImage];
        _logoNameLabel = [[UILabel alloc] init];
        [self addSubview:_logoNameLabel];
    }
    return self;
}


-(void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self;
    [_logoImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.centerX.equalTo(superView.mas_centerX);
        make.bottom.equalTo(superView.mas_centerY);
    }];
   // _logoImage.backgroundColor = [UIColor purpleColor];
    [_logoNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(40, 20));
        make.centerX.equalTo(superView.mas_centerX);
        make.top.equalTo(superView.mas_centerY);
    }];
    //_logoNameLabel.backgroundColor = [UIColor yellowColor];
    _logoNameLabel.textAlignment = NSTextAlignmentCenter;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
