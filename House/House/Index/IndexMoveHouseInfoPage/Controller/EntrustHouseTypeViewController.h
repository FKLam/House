//
//  EntrustHouseTypeViewController.h
//  House
//
//  Created by 张垚 on 16/7/25.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "BaseViewController.h"

typedef void(^chooseInfoBlock)(NSString *currentStr, NSInteger currentTag);
@interface EntrustHouseTypeViewController : BaseViewController

@property (nonatomic, strong) NSString *currentStr;
@property (nonatomic, copy) chooseInfoBlock chooseInfoBlock;
@property (nonatomic, assign) NSInteger tag;
@property (nonatomic, copy) NSString *areaID;

- (void)passInfoToPreviousPage:(chooseInfoBlock)infoBlock;
@end
