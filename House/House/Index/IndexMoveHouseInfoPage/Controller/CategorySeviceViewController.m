//
//  CategorySeviceViewController.m
//  House
//
//  Created by 张垚 on 16/6/23.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "CategorySeviceViewController.h"
#import "RentEntrustViewController.h"
#import "RentHouseEntrustViewController.h"
#import "BuyHouseEntrustViewController.h"
#import "SellHouseEntrustViewController.h"

@interface CategorySeviceViewController ()

@property (nonatomic, strong) UIImageView *mainImageView;
@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UIStackView *mainStackView;
@property (nonatomic, copy) NSMutableArray *titleArr;
@property (nonatomic, copy) NSMutableArray *colorArr;

@end

@implementation CategorySeviceViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = YES;
    self.navigationController.navigationBarHidden = YES;
}
#pragma mark - 主程序入口
- (void)viewDidLoad {
    [super viewDidLoad];
    _mainImageView = [[UIImageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.view addSubview:_mainImageView];
    _mainImageView.image = [UIImage imageNamed:@"categoryService"];
    _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:_backButton];
    [_backButton setFrame:CGRectMake(0, 30, 40, 30)];
    [_backButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [_backButton setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    _titleArr = @[@"租房委托", @"出租委托", @"买房委托", @"卖房委托"].mutableCopy;
    /* uistackview初始化 属性设置 */
    _mainStackView = [[UIStackView alloc] init];
    [self.view addSubview:_mainStackView];
    /* 布局方向 */
    _mainStackView.axis = UILayoutConstraintAxisVertical;
    /* 间距 */
    _mainStackView.spacing = 30.0f;
    /* 每个视图之间的关系 */
    _mainStackView.distribution = UIStackViewDistributionFillEqually;
    /* 对齐方式 */
    _mainStackView.alignment = UIStackViewAlignmentFill;
    UIView *superView = self.view;
    [_mainStackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(30);
        make.top.equalTo(superView.mas_top).offset(160);
        make.size.mas_equalTo(CGSizeMake(100, 250));
    }];
    
   /* 循环铺button */
    for (int i = 0; i < 4; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.tag = i;
        [button setTitle:_titleArr[i] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(pushDetail:) forControlEvents:UIControlEventTouchUpInside];
        /* 设置背景颜色 */
        switch (i) {
            case 0:
                button.backgroundColor = [UIColor orangeColor];
                break;
            case 1:
                button.backgroundColor = [UIColor redColor];
                break;
            case 2:
                button.backgroundColor = [UIColor purpleColor];
                break;
            case 3:
                button.backgroundColor = [UIColor orangeColor];
                break;
            default:
                break;
        }
      [_mainStackView addArrangedSubview:button];
    }
    [self bottomNumberView];
    // Do any additional setup after loading the view.
}
#pragma mark - 跳转详情
- (void)pushDetail:(UIButton *)button {
    switch (button.tag) {
        case 0://租房委托
        {
            RentHouseEntrustViewController *rentHouseVC = [[RentHouseEntrustViewController alloc] init];
            [self.navigationController pushViewController:rentHouseVC animated:YES];
        }
            break;
        case 1://出租委托
        {
            RentEntrustViewController *rentVC = [[RentEntrustViewController alloc] init];
            [self.navigationController pushViewController:rentVC animated:YES];
        }
            break;
        case 2://买房委托
        {
            BuyHouseEntrustViewController *buyHouseVC = [[BuyHouseEntrustViewController alloc] init];
            [self.navigationController pushViewController:buyHouseVC animated:YES];
        }
            break;
        case 3://卖房委托
        {
            SellHouseEntrustViewController *sellHouseVC = [[SellHouseEntrustViewController alloc] init];
            [self.navigationController pushViewController:sellHouseVC animated:YES];
        }
            break;
        default:
            break;
    }
}


#pragma mark - 返回根视图
- (void)back:(UIButton *)button {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)bottomNumberView {
    UIView *numberView = [[UIView alloc] init];
    numberView.userInteractionEnabled = YES;
    _mainImageView.userInteractionEnabled = YES;
    [_mainImageView addSubview:numberView];
    [numberView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_mainImageView.mas_left).offset(40);
        make.bottom.equalTo(_mainImageView.mas_bottom).offset(-30);
        make.right.equalTo(_mainImageView.mas_right).offset(-20);
        make.height.equalTo(@40);
    }];
    UIImageView *numberImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 40)];
    [numberView addSubview:numberImageView];
    numberImageView.image = [UIImage imageNamed:@"servicephone"];
    UILabel *label = [[UILabel alloc] init];
    [numberView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(numberView.mas_left).offset(30);
        make.top.equalTo(numberView.mas_top);
        make.height.equalTo(@40);
        make.right.equalTo(numberView.mas_right);
    }];
    label.text = @"客服热线:4000404717";
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont systemFontOfSize:20 weight:17];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callNumber)];
    [numberView addGestureRecognizer:tap];
}

- (void)callNumber {
    NSMutableString *str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",unifyServiceNumber];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
