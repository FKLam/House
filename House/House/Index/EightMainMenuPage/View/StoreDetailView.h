//
//  StoreDetailView.h
//  House
//
//  Created by 张垚 on 16/7/21.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface storeButton : UIButton

@end

@interface StoreDetailView : UIView

@property (nonatomic, strong) UIImageView *backgroundImageView;
@property (nonatomic, strong) UILabel *addressLabel;
@property (nonatomic, strong) storeButton *mapButton;
@property (nonatomic, strong) UIButton *numberButton;


@end
