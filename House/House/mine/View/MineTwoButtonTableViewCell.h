//
//  MineTwoButtonTableViewCell.h
//  House
//
//  Created by 吕品 on 16/6/24.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "BaseCollectionWithTableViewCell.h"

@interface MineTwoButtonTableViewCell : BaseCollectionWithTableViewCell
@property (nonatomic, copy) NSArray<NSDictionary *> *array;
@end
