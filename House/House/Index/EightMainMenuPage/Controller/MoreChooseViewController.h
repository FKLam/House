//
//  MoreChooseViewController.h
//  House
//
//  Created by 张垚 on 16/8/12.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "BaseViewController.h"

typedef void(^chooseInfoBlock)(NSString *currentStr, NSInteger currentTag);

@interface MoreChooseViewController : BaseViewController

@property (nonatomic, copy) chooseInfoBlock chooseInfoBlock;
- (void)passInfoToPreviousPage:(chooseInfoBlock)infoBlock;
@property (nonatomic, assign) NSInteger tag;
@property (nonatomic, copy) NSString *currentStr;

@end
