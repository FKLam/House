//
//  HouseTypeDescriptionTableViewCell.m
//  House
//
//  Created by 张垚 on 16/8/5.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import "HouseTypeDescriptionTableViewCell.h"

@implementation HouseTypeDescriptionTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _descriptionLabel = [[UILabel alloc] init];
        _descriptionLabel.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:_descriptionLabel];
        _minPriceTextfield = [[UITextField alloc] init];
        [self.contentView addSubview:_minPriceTextfield];
        _roomLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_roomLabel];
        _maxPriceTextfiled = [[UITextField alloc] init];
        [self.contentView addSubview:_maxPriceTextfiled];
        _hallLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_hallLabel];
        
    }
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    UIView *superView = self.contentView;
    [_descriptionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(10);
        make.left.equalTo(superView.mas_left).offset(5);
        make.centerY.equalTo(superView.mas_centerY);
        make.width.equalTo(@60);
    }];
    _descriptionLabel.font = [UIFont systemFontOfSize:15];
    [_minPriceTextfield mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(10);
        make.left.equalTo(_descriptionLabel.mas_right).offset(5);
        make.centerY.equalTo(superView.mas_centerY);
        make.width.equalTo(@60);
    }];
    _minPriceTextfield.tag = 100;
    _minPriceTextfield.layer.cornerRadius = 4;
    _minPriceTextfield.layer.masksToBounds = YES;
    _minPriceTextfield.layer.borderColor = [UIColor colorWithRed:0.902 green:0.902 blue:0.902 alpha:1.0].CGColor;
    _minPriceTextfield.layer.borderWidth = 1.0;
    _minPriceTextfield.textAlignment = NSTextAlignmentCenter;
    [_roomLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(superView.mas_centerY);
        make.left.equalTo(_minPriceTextfield.mas_right).offset(5);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    _roomLabel.textAlignment = NSTextAlignmentLeft;
    _roomLabel.textColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0];
    _roomLabel.text = @"室";
    
    [_maxPriceTextfiled mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(10);
        make.left.equalTo(_roomLabel.mas_right).offset(5);
        make.centerY.equalTo(superView.mas_centerY);
        make.width.equalTo(_minPriceTextfield.mas_width);
    }];
    _maxPriceTextfiled.tag = 101;
    _maxPriceTextfiled.layer.cornerRadius = 4;
    _maxPriceTextfiled.layer.masksToBounds = YES;
    _maxPriceTextfiled.layer.borderColor = [UIColor colorWithRed:0.902 green:0.902 blue:0.902 alpha:1.0].CGColor;
    _maxPriceTextfiled.layer.borderWidth = 1.0;
    _maxPriceTextfiled.textAlignment = NSTextAlignmentCenter;
    [_hallLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(10);
        make.left.equalTo(_maxPriceTextfiled.mas_right).offset(5);
        make.centerY.equalTo(superView.mas_centerY);
        make.width.equalTo(@60);
    }];
    _hallLabel.textAlignment = NSTextAlignmentLeft;
    _hallLabel.textColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0];
    _hallLabel.text = @"厅";
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
