//
//  SecondaryHouseListNoSlideTableViewCell.h
//  House
//
//  Created by 张垚 on 16/9/2.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SecondaryListModel;

@interface SecondaryHouseListNoSlideTableViewCell : UITableViewCell

@property (nonatomic, strong) SecondaryListModel *model;
/* 详情属性 */
@property (nonatomic, strong) UIImageView *roomImageView;
@property (nonatomic, strong) UILabel *tilteLabel;
@property (nonatomic, strong) UILabel *doorModelLabels;
@property (nonatomic, strong) UILabel *communityLabels;

//四个标签
@property (nonatomic, strong) UILabel *tagLabel1;
@property (nonatomic, strong) UILabel *tagLabel2;
@property (nonatomic, strong) UILabel *tagLabel3;
@property (nonatomic, strong) UILabel *tagLabel4;
//右侧视图
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UILabel *areaSizeLabel;
@property (nonatomic, strong) NSArray *tagTitleArr;

@end
