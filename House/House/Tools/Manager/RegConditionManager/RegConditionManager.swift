//
//  RegConditionManager.swift
//  House
//
//  Created by 吕品 on 16/7/4.
//  Copyright © 2016年 ZJWL. All rights reserved.
//

import UIKit

class RegConditionManager: NSObject {

    fileprivate class func check(_ regular: String, text: String) -> Bool {
        let predicate = NSPredicate.init(format: "SELF MATCHES %@", regular)

        return predicate.evaluate(with: text as AnyObject)
    }

    class func checkPhoneNumber(_ number: String) -> Bool {
        /*
         中国移动号段：134、135、136、137、138、139、150、151、152、157、158、159、147、182、183、184、187、188、1705、178
         中国联通号段：130、131、132、145（145属于联通无线上网卡号段）、155、156、185、186 、176、1709、
         中国电信号段：133 、153 、180 、181 、189、1700、177
         */
        let YDRegular = "^1((3[4-9]|5[0-27-9]|47|8[23478]|78)\\d{8}|705\\d{7})$";
        let LTRegular = "^1((3[0-2]|45|5[56]|8[56]|76)[0-9]|709)\\d{7}$";
        let DXRegular = "^1((33|53|8[019]|77)[0-9]|700)\\d{7}$";
        return (check(YDRegular, text: number) || check(LTRegular, text: number) || check(DXRegular, text: number))
    }
    /*
     + (BOOL)check:(NSString *)regular text:(NSString *)text
     {
     NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regular];
     return [predicate evaluateWithObject:text];
     }
     */
}
